<?php
//phpinfo();
/*
 //이 부분은 libscpdb_agent_php.ini 파일을 샐성하면 없어도 된다 
if(!extension_loaded('libscpdb_agent_php')) {
        dl('libscpdb_agent_php.'. PHP_SHLIB_SUFFIX);
}
*/

//함수목록 보는부분

$functions = get_extension_funcs('libscpdb_agent_php');
echo "Functions available in the test extension:<br>";
foreach($functions as $func) {
    echo $func."<br>";
}

/* DAMO SCP API ret : 0,118(성공) 나머지(실패)
   API 를 초기화 한다
*/
$ret = agentinit('/usr/lib/damo_scp/scpdb_agent.ini');
echo "\n scp_init RET : ";
echo $ret;
echo "<br>\n";

/* DAMO SCP API
   키 파일에서 컨택스트를 가져온다
*/
//$ctx = AgentCipherCreateContextImportFile('/home/jykim/key.scp');
$ctx = AgentCipherCreateContext('SCP_PRD_ANYHELP','LGU_DB','SME','AES256','CBC');
/* DAMO SCP API    
   encrypt function 
*/
$enc = AgentCipherEncrypt($ctx,'123456-1234567');
echo "ENC : ";
echo $enc;
echo "<br>\n";

/* DAMO SCP API
   decrypt function
*/ 
$dec = AgentCipherDecrypt($ctx,$enc);  
echo "DEC : ";
echo $dec;
echo "<br>\n";

/* DAMO SCP API     
   encrypt function 
*/
$enc = AgentCipherEncryptB64($ctx,'123456-1234567');
echo "ENC : ";
echo $enc;
echo "<br>\n";

/* DAMO SCP API 
   decrypt function
*/ 
$dec = AgentCipherDecryptB64($ctx,$enc);  
echo "DEC : ";
echo $dec;
echo "<br>\n";


$hashplain = '222222';
$hash1 = AgentCipherHashB64('SHA512', $hashplain);
echo "HASH BASE64 : ";
echo $hash1;
echo "<br>\n";
echo "END\n";

$hash2 = AgentCipherHash('SHA512', $hashplain);
echo "HASH HEX : ";
echo $hash2;
echo "<br>\n";
echo "END\n";

$hash3 = AgentHexToB64($hash2);
echo "HASH BASE64 : ";
echo $hash3;
echo "<br>\n";
echo "END\n";


$hash4 = AgentB64ToHex($hash3);
echo "HASH HEX : ";
echo $hash4;
echo "<br>\n";
echo "END\n";

$ret = AgentCipherEncryptFilePath($ctx,'/home/jykim/php-5.1.6/ext/libscpdb_agent_php/plain.txt','/home/jykim/php-5.1.6/ext/libscpdb_agent_php/enc.txt');
echo "ENC FILE LEN : ";
echo $ret;
echo "<br>\n";

$ret = AgentCipherDecryptFilePath($ctx,'/home/jykim/php-5.1.6/ext/libscpdb_agent_php/enc.txt','/home/jykim/php-5.1.6/ext/libscpdb_agent_php/dec.txt');
echo "DEC FILE LEN : ";
echo $ret;
echo "<br>\n";

?> 

