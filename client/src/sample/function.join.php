<?php
header('Content-Type: text/html; charset=utf-8');
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.constants.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.LGU.php");

$lgu = new LGU();

$admin_id = $_POST['admin_id'];
$admin_enable = $_POST['admin_enable'];
$admin_pw = $_POST['admin_pw'];
$admin_company = $_POST['admin_company'];
$admin_name = $_POST['admin_name'];
$admin_volume = $_POST['admin_all_cnt'];
$admin_business_sector = $_POST['admin_business_sector'];
$admin_phone = $_POST['admin_phone'];
$admin_tel = $_POST['admin_tel'];
$admin_memo = $_POST['admin_memo'];
$admin_mail = $_POST['admin_mail'];
$admin_business_num = $_POST['admin_business_num'];
$admin_manage_num = $_POST['admin_manage_num'];
$admin_produc_type = $_POST['admin_product'];
$admin_volume_pc = $_POST['admin_pc_cnt'];
$admin_volume_mobile = $_POST['admin_pc_m_n_v_cnt'];
$admin_url = $_POST['admin_url'];
$admin_service_start_date = $_POST['admin_service_start_date'];
$admin_bill_account = $_POST['admin_bill_account'];
$admin_sign_num = $_POST['admin_sign_num'];
$admin_service_join_date = $_POST['admin_service_join_date'];
//required
$admin_service_expired_date = DEFAULT_MOBILE_EXPIRED_DATE;
$mobile_expired_date = DEFAULT_MOBILE_EXPIRED_DATE;


$admindata = array(
	"id"=>$admin_id, 
	"password"=>$admin_pw, 
	"company"=>$admin_company, 
	"name"=>$admin_name, 
	"email"=>$admin_mail, 
	"volume"=>$admin_volume, 
	"business_sector"=>$admin_business_sector,
	"phone"=>$admin_phone,
	"tel"=>$admin_tel,
	"url"=>$admin_url, 
	"others"=>$admin_memo, 
	"business_num"=>$admin_business_num, 
	"enable"=>$admin_enable, 
	"managenum"=>$admin_manage_num, 
	"function"=>$admin_produc_type, 
	"mobile_volume"=>$admin_volume_mobile,
	"service_start_date"=>$admin_service_start_date,
	"service_join_date"=>$admin_service_join_date,
	"service_expired_date"=>$admin_service_expired_date,
	"bill_account"=>$admin_bill_account,
	"sign_num"=>$admin_sign_num,
	"mobile_expired_date"=>$mobile_expired_date
);

//for dev
echo "----------- DATA -------------"."<br/>";
echo print_r($admindata);
echo "<br/>"."----------- //DATA -----------"."<br/>";

//create admin
$res = $lgu->CreateAdmin($admindata);

//for dev
echo "<br/>"."----------- RESULT -------------"."<br/>";
echo "LOG : " . $res;
echo "<br/>"."----------- //RESULT -----------"."<br/>";

if($res == "SUPPORTERALREADYEXISTS" || $res == "ADMINALREADYEXISTS") {// 이미 존재하는 ID인 경우

	//empty

}
else if($res == "OK"){//성공

	$created = $lgu->GetAdminByAdid($admin_id);
	$lgu->CreateTroubleTypeDefault($created->adnum);	//default touble type 생성

}