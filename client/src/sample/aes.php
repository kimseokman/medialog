<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.constants.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.AES256.php");

	$ase256 = new AES256();

	echo "String Key : " . AES_KEY . "<br />";
	echo "String IV " . AES_IV . "<br />";

	echo "origin key : " . $ase256->key . "<br />";
	echo "origin iv : " . $ase256->iv . "<br />";
	
	$key = $ase256->key;
	$iv = $ase256->iv;

	$data = "오주병1";
?>
<html>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />
</head>
<body>
<?
	echo "data : " . $data . "<br />";
	$encrypt_data = $ase256->encrypt($key, $iv, $data);
    echo "encrypt_data : " . $encrypt_data . "<br />";
    $decrypt_data = $ase256->decrypt($key, $iv, $encrypt_data);
    echo "decrypt_data : " . $decrypt_data . "<br />";
?>
</body>
</html>
