<?php
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.constants.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGUtils.php");

$utils_obj = new ASGUtils();

$id_length_max = ID_MAX_LENGTH;
$pw_length = PASSWORD_LENGTH;
$pw_length_min = PASSWORD_LENGTH_MIN;
$pw_length_max = PASSWORD_LENGTH_MAX;
$pw_safe_cnt = PASSWORD_SAFE_CNT;

$p_pc = PRODUCT_CODE_PC;
$p_mobile_paid = PRODUCT_CODE_MOBILE_PAID;
$p_mobile_free = PRODUCT_CODE_MOBILE_FREE;
$p_video_paid = PRODUCT_CODE_VIDEO_PAID;
$p_video_free = PRODUCT_CODE_VIDEO_FREE;
$p_mobile_video_paid = PRODUCT_CODE_MOBILE_VIDEO_PAID;
$p_mobile_video_free = PRODUCT_CODE_MOBILE_VIDEO_FREE;

//default
$default_pc_volume = PC_VOLUME_DEFAULT;
$default_pc_m_n_v_volume = M_N_V_VOLUME_DEFAULT;
$default_all_volume = ALL_VOLUME_DEFAULT;
$defaul_access_url = CUSTOMER_ACCESS_URL_DEFAULT;
$defaul_current_date = CURRENT_DATE_DEFAULT;

$business_list = $utils_obj->GetBusinessList();//업종 리스트

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=960" />
	<meta name="apple-mobile-web-app-title" content="UPLUS" />
	<title>Admin Create Sample</title>
	<style>
	.create_title{margin:0 auto;width:800px;text-align:center}
	.create_section{margin:0 auto;width:800px}
	table{border-collapse: collapse;border:1px solid #eee}
	table thead{background-color:#eee}
	table tbody tr{border-bottom:1px solid #eee}
	table tbody tr th, table tbody tr td{border-right:1px solid #eee}
	table tbody tr td.ref{font-size:12px}
	.btns{margin:0 auto;width:800px}
	.btns button{float:right;width:150px;}
	</style>
	
</head>
<body>
	<h3 class="create_title">Sample : Admin Create</h3>
	<div class="create_section">
		<form name="createAdmin" method="post" action="./function.join.php">
			<table>
				<colgroup>
					<col style="width:150px;" />
					<col style="width:400px;" />
					<col style="width:*;" />
				</colgroup>
				<thead>
					<tr>
						<th>구분</th>
						<th>입력</th>
						<th>참고</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th><label for="admin_id">ID</label></th>
						<td><input type="text" id="admin_id" name="admin_id" /></td>
						<td class="ref">
							<p>상담원 ID 중복시 return "SUPPORTERALREADYEXISTS"</p>
							<p>관리자 ID 중복시 return "ADMINALREADYEXISTS"</p>
							<p>DB colume : 영문, 숫자 구성으로 <? echo $id_length_max; ?>자 이내</p>
						</td>
					</tr>
					<tr>
						<th><label for="admin_enable">활성화</label></th>
						<td>
							<select name="admin_enable" id="admin_enable">
								<option value="Y">Y</option>
								<option value="N">N</option>
							</select>
						</td>
						<td class="ref">&nbsp;</td>
					</tr>
					<tr>
						<th><label for="admin_pw">Password</label></th>
						<td><input type="password" id="admin_pw" name="admin_pw" /></td>
						<td class="ref">U+ 정책 : <? echo $pw_length_min; ?>~<? echo $pw_length_max; ?>자 내의 영문 대/소문자, 숫자, 특수문자 조합</td>
					</tr>
					<tr>
						<th><label for="admin_company">회사 이름</label></th>
						<td><input type="text" id="admin_company" name="admin_company" /></td>
						<td class="ref">&nbsp;</td>
					</tr>
					<tr>
						<th><label for="admin_business_num">사업자번호</label></th>
						<td><input type="text" id="admin_business_num" name="admin_business_num" /></td>
						<td class="ref">&nbsp;</td>
					</tr>
					<tr>
						<th><label for="admin_business_sector">업종</label></th>
						<td>
							<select name="admin_business_sector">
							<?
							$business_list_cnt = count($business_list);
							
							for($i = 0; $i < $business_list_cnt; $i++){
							?>
								<option value="<? echo $business_list[$i]["name"]; ?>" ><? echo $business_list[$i]["name"]; ?></option>
							<?
							}//end of : for($i = 0; $i < $pc_func_cnt; $i++)
							?>
							</select>
						</td>
						<td class="ref">&nbsp;</td>
					</tr>
					<tr>
						<th><label for="admin_name">담당자 이름</label></th>
						<td><input type="text" id="admin_name" name="admin_name" /></td>
						<td class="ref">&nbsp;</td>
					</tr>
					<tr>
						<th><label for="admin_mail">이메일</label></th>
						<td><input type="text" id="admin_mail" name="admin_mail" /></td>
						<td class="ref">&nbsp;</td>
					</tr>
					<tr>
						<th><label for="admin_phone">연락처1</label></th>
						<td><input type="text" id="admin_phone" name="admin_phone" /></td>
						<td class="ref">&nbsp;</td>
					</tr>
					<tr>
						<th><label for="admin_tel">연락처2</label></th>
						<td><input type="text" id="admin_tel" name="admin_tel" /></td>
						<td class="ref">SMS 발송시 사용되는 번호</td>
					</tr>
					<tr>
						<th><label for="admin_service_join_date">가입일자</label></th>
						<td><input type="text" id="admin_service_join_date" name="admin_service_join_date" value="<? echo $defaul_current_date; ?>" /></td>
						<td class="ref">&nbsp;</td>
					</tr>
					<tr>
						<th><label for="admin_service_start_date">개통일자</label></th>
						<td><input type="text" id="admin_service_start_date" name="admin_service_start_date" value="<? echo $defaul_current_date; ?>" /></td>
						<td class="ref">&nbsp;</td>
					</tr>
					<tr>
						<th><label>상품 선택</label></th>
						<td>
							<p>
								<input type="radio" id="admin_product_pc" name="admin_product" value="<? echo $p_pc; ?>" checked /> <label for="admin_product_pc">애니서포트 (PC)</label>
							</p>
							<p>
								<input type="radio" id="admin_product_pc_m" name="admin_product" value="<? echo $p_mobile_paid; ?>" /> <label for="admin_product_pc_m">애니서포트 + Mobile</label> 
							</p>
							<p>	
								<input type="radio" id="admin_product_pc_v" name="admin_product" value="<? echo $p_video_paid; ?>" /> <label for="admin_product_pc_v">애니서포트 + Video</label>
							</p>	
							<p>	
								<input type="radio" id="admin_product_pc_m_v" name="admin_product" value="<? echo $p_mobile_video_paid; ?>" /> <label for="admin_product_pc_m_v">애니서포트 + Mobile + Video</label>
							</p>
						</td>
						<td class="ref">전체 상담원 수 : volume  <br> 모바일 상담원 : mobile_volume  <br> 예)PC 2, 모바일/비디오 1 인 경우 , volume : 3 , mobile_volume : 1</td>
					</tr>
					<tr>
						<th><label>상담원 수</label></th>
						<td>
							<p>
								<label for="admin_pc_cnt">애니서포트 PC</label> : <input type="number" id="admin_pc_cnt" name="admin_pc_cnt" value="<? echo $default_pc_volume; ?>" min="0" max="500" />
							</p>
							<p>
								<label for="admin_pc_m_n_v_cnt">애니서포트 PC + Mobile / Video</label> : <input type="number" id="admin_pc_m_n_v_cnt" name="admin_pc_m_n_v_cnt" value="<? echo $default_pc_m_n_v_volume; ?>" min="0" max="500" />
							</p>
							<p>
								<label for="admin_all_cnt">전체 : </label> <input type="number" id="admin_all_cnt" name="admin_all_cnt" value="<? echo $default_all_volume; ?>" min="0" max="500" />
							</p>
						</td>
						<td class="ref">&nbsp;</td>
					</tr>
					<tr>
						<th><label for="admin_bill_account">청구계정번호</label></th>
						<td><input type="text" id="admin_bill_account" name="admin_bill_account" /></td>
						<td class="ref">&nbsp;</td>
					</tr>
					<tr>
						<th><label for="admin_sign_num">가입번호</label></th>
						<td><input type="text" id="admin_sign_num" name="admin_sign_num" /></td>
						<td class="ref">&nbsp;</td>
					</tr>
					<tr>
						<th><label for="admin_url">고객 접속 URL</label></th>
						<td><input type="text" id="admin_url" name="admin_url" value="<? echo $defaul_access_url; ?>" /></td>
						<td class="ref">&nbsp;</td>
					</tr>
					<tr>
						<th><label for="admin_manage_num">관리 번호</label></th>
						<td><input type="text" id="admin_manage_num" name="admin_manage_num" /></td>
						<td class="ref">&nbsp;</td>
					</tr>
					<tr>
						<th><label for="admin_memo">비고(메모)</label></th>
						<td>
							<textarea type="text" id="admin_memo" name="admin_memo" maxlength="100" /></textarea>
						</td>
						<td class="ref">&nbsp;</td>
					</tr>
				</tbody>
			</table>
			<div class="btns">
				<button>생성</button>
			</div>
		</form>
	</div>
</body>
</html>