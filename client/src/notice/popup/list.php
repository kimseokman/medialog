<?php
$notice_pop_act = $_POST['notice_popup_action'];//notice flag ('open', 'close')
$direct_detail_action = $_POST['direct_detail_action'];
$notice_detail_seq = $_POST['seq'];

$notice_page = 1;//page : 0 -> total
$notice_search_type = "";
$notice_search_val = "";

$notice_arr = $main_obj->GetNoticeList($notice_page, $notice_search_type, $notice_search_val);

$notice_pagination = $notice_arr['pagination'];
$notice_content = $notice_arr['content'];
$notice_page_num = $notice_arr['page_num'];

function print_last($row_index, $last_index){
	if($row_index == $last_index)
		return "last";
}

function print_imp($print_flag){
	if($print_flag)
		return "imp";
}

if($notice_pop_act == 'open'){
?>
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<input type="hidden" id="direct_detail_action" value="<? echo $direct_detail_action; ?>" />
	<input type="hidden" id="direct_detail_seq" value="<? echo $notice_detail_seq; ?>" />
	<!-- 공지사항 전체리스트 팝업 -->
	<div id="popup" class="popup_notice">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<a href="#" class="close close_pop_notice" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<div class="popup_notice_tit">
					<strong>공지사항</strong>
					<p>U+ Biz 원격지원의 새로운 서비스 정보와 소식을 전해드립니다.</p>
				</div>

				<!-- 공지사항 테이블 -->
				<!-- [D] 제목을 클릭하면 해당공지사항의 상세화면(공지사항 뷰페이지)이 보여짐. -->
				
				<div class="ajax_pop_notice_content">
					<table class="style_board_default" summary="글번호, 분류, 제목, 작성자, 날짜가 있는 게시판" width="100%">
						<caption><span>글번호, 분류, 제목, 작성자, 날짜가 있는 게시판</span></caption>
						<colgroup>
							<col style="width:37px;" />
							<col style="width:*;" />
							<col style="width:56px;" />
							<col style="width:75px;" />
						</colgroup>
						<thead>
							<tr>
								<th>NO</th>
								<th>제목</th>
								<th>작성자</th>
								<th class="last">날짜</th>
							</tr>
						</thead>
						<tbody>
						<?
						$notice_content_size = sizeof($notice_content);
						if($notice_content_size == 0){
						?>
							<tr class="last">
								<td colspan="4">검색 결과 없음</td>
							</tr>
						<?
						}//end of : if($notice_content_size == 0)
						else{
							$notice_last_node_num = $notice_content_size - 1;
							for ($i = 0; $i < $notice_content_size; $i++){
						?>
							<tr class="<? echo print_last($i, $notice_last_node_num); ?>">
								<td><? echo $notice_content[$i]['num']; ?></td>
								<td class="title">
									<a href="#" class="pop_notice_title" onClick="ajax_pop_notice_detail(event, <? echo $notice_content[$i]['seq']; ?>)"><? echo $notice_content[$i]['title']; ?></a>
								</td>
								<td><? echo $notice_content[$i]['name']; ?></td>
								<td><? echo $notice_content[$i]['regdate']; ?></td>
							</tr>
						<?
							}//end of : for ($i = 0; $i < sizeof($notice_content); $i++)
						}//end of : if($notice_content_size != 0)
						?>
						</tbody>
					</table>

					<!-- [D] 공지 항목이 10개가 초과되면 페이징 처리 -->
					<?
					$page_prev = $notice_pagination->GetIsPrev();
					$page_list = $notice_pagination->GetPageList();
					$page_next = $notice_pagination->GetIsNext();
					?>
					<div class="paging">
					<?
					if($page_prev['is_older_page']){
					?>
						<a href="#" class="btn_arrow ar_fst" onClick="notice_pagination(event, <? echo $page_prev['older_page']; ?>)">첫페이지</a>
					<?
					}
					else{
					?>
						<a href="#" class="btn_arrow ar_fst">첫페이지</a>
					<?
					}

					if($page_prev['is_prev_page']){
					?>
						<a href="#" class="btn_arrow ar_prev" onClick="notice_pagination(event, <? echo $page_prev['prev_page']; ?>)">이전페이지</a>
					<?
					}
					else{
					?>
						<a href="#" class="btn_arrow ar_prev">이전페이지</a>
					<?
					}

					while(list($k, $v) = each($page_list)){
					?>
						<a href="#" onClick="notice_pagination(event, <? echo $k; ?>)"><span class="<? echo print_imp($v['is_current']);?>"><? echo $k; ?></span></a>
					<?
					}//end of : while(list($k, $v) = each($page_list))
					
					if($page_next['is_next_page']){
					?>
						<a href="#" class="btn_arrow ar_next" onClick="notice_pagination(event, <? echo $page_next['next_page']; ?>)"><span class="hidden">다음페이지</span></a>
					<?
					}
					else{
					?>
						<a href="#" class="btn_arrow ar_next"><span class="hidden">다음페이지</span></a>
					<?
					}

					if($page_next['is_newer_page']){
					?>
						<a href="#" class="btn_arrow ar_last" onClick="notice_pagination(event, <? echo $page_next['newer_page']; ?>)"><span class="hidden">마지막페이지</span></a>
					<?
					}
					else{
					?>
						<a href="#" class="btn_arrow ar_last"><span class="hidden">마지막페이지</span></a>
					<?
					}
					?>
					</div>
				</div>
				<div class="pop_notice_footer">
					<!-- [D] 검색 내용이 없을 경우, 화면이동 없음. -->
					<div class="search_box">
						<div class="u_biz_noti_pop">
							<div class="choice_box u_biz_noti_input_pop">
								<span class="comm_sel w_110 selUp"><!-- [150811] -->
									<select>
										<option value="title">제목</option>
										<option value="name">작성자</option>
									</select>
								</span>
								<div>
									<input class="notice_search_val" type="text" style="width: 180px;" /><a class="search_notice_btn" href="#"><img src="<? echo IMG_DIR; ?>/board/btn_search.gif" alt="검색" /></a>
								</div>
							</div>
						</div>
					</div>
					<!-- [D] 공지사항 전체리스트 화면으로 이동 -->
					<div class="btn_list">
						<a href="#" class="go_to_notice_list_btn">
							<img src="<? echo IMG_DIR; ?>/board/btn_list.gif" alt="목록" />
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //공지사항 전체리스트 팝업 -->
</div>
<?	
}//end of : if($notice_pop_act == 'open')
?>