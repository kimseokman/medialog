<?php
$notice_page = 1;//page : 0 -> total
$notice_search_type = "";
$notice_search_val = "";

$notice_arr = $main_obj->GetNoticeList($notice_page, $notice_search_type, $notice_search_val);

$notice_pagination = $notice_arr['pagination'];
$notice_content = $notice_arr['content'];
$notice_page_num = $notice_arr['page_num'];
?>
<div class="rolling_notice">
	<div class="slide_notice_tit">
		<strong>공지사항</strong>
		<a class="open_pop_notice" href="#">더보기</a>
	</div>
	<ul class="bxslider">
	<?
	for ($i = 0; $i < sizeof($notice_content); $i++){
	?>
		<li>
			<div class="slide_notice">
				<dl class="first">
					<dt><? echo $notice_content[$i]['num']; ?></dt>
					<dd><a href="#" onClick="notice_detail_direct(event, <? echo $notice_content[$i]['seq']; ?>)"><? echo $notice_content[$i]['title']; ?></a></dd>
				</dl>
				<dl class="second">
					<dt><? echo $content[$i]['name']; ?></dt>
					<dd><? echo $content[$i]['regdate']; ?></dd>
				</dl>
			</div>
		</li>
	<?
	}//end of : for ($i = 0; $i < sizeof($notice_content); $i++)
	?>
	</ul>
</div>