<?php
include_once("../../_func/function.common.php");
include_once("../common/config.php");

include_once("../common/header.php");

$sptid = $_POST["userid"];
if($_POST["userid"] == NULL){
	$sptid = $_GET["userid"];
}

$pw = APP_COWORK_LOGIN_PW;

if(APP_DEBUG){//case by test
	$sptid = APP_DEBUG_ID;
}

$admin = $main_obj->GetAdminInfo();
$main_obj->HistoryLogin($sptid, $pw);

$access_code = $_POST['accessCode'];
if($_POST["accessCode"] == NULL){
	$access_code = $_GET["accessCode"];
}

$access_url = $admin->url;
$public_access_url = CUSTOMER_ACCESS_URL_DEFAULT;
$domain = $admin->domain;
$is_domain = false;
if( ($domain  != null) && ($domain != "") ){
	$is_domain = true;
}
?>
<div class="sms_send">
	<div class="check_area">
	<?
	if($is_domain){
	?>
		<div class="url_box">
			<input type="radio" class="" id="url_chek" name="url_text" value="<? echo $access_url; ?>" checked />
			<label class="url_chek" for="url_chek"><? echo $access_url; ?></label>
		</div>
	<?
	}
	?>
		<div class="url_box01">
			<input type="radio" class="" id="url_chek01" name="url_text" value="<? echo $public_access_url; ?>" <? if(!$is_domain) echo "checked"; ?> />
			<label class="url_chek01" for="url_chek01"><? echo $public_access_url; ?></label>
		</div>
	</div>
	<input type="hidden" class="access_code" value="<? echo $access_code; ?>" />
	<input type="hidden" class="userid" value="<? echo $sptid; ?>" />
	<input type="hidden" class="usernum" value="<? echo $_SESSION["sptnum"]; ?>" />
	<div class="input_area">
		<div class="input_list">
			<div class="text_box">
				<div class="send_view">
					원격지원 접속을 위한 안내입니다.<br/>
					URL [<span class="apply_url"><? echo $admin->url; ?></span>]<br />
					접속인증번호 <span>[<? echo $access_code; ?>]</span>
				</div>
			</div>
			<div class="receive_num">
				<div class="num_enter">
					<input type="text" class="phone_num1" name="phone_num1" title="수신번호 입력" />
				</div>
				<p class="err_num">수신자번호를 입력해주세요</p>
			</div>
			<div class="post_num">
				<div class="num_enter">
					<input type="text" class="phone_num2" name="phone_num2" value="<? echo $admin->tel;?>" title="발신번호" readonly />
				</div>
			</div>
			<p class="btn_sms"><a href="#"><img src="<? echo APP_DIR . IMG_DIR; ?>/sub/btn_smsSend.gif" alt="sms보내기" /></a></p>
		</div>
	</div>
</div>
<!-- popup : send_mail_res -->
<?
include_once("./popup/send_sms_res.php");
?>
<!-- popup : send_mail_res -->
<?
include_once("../common/footer.php");