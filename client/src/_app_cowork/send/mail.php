<?php
include_once("../../_func/function.common.php");
include_once("../common/config.php");

include_once("../common/header.php");

$sptid = $_POST["userid"];
if($_POST["userid"] == NULL){
	$sptid = $_GET["userid"];
}

$pw = APP_COWORK_LOGIN_PW;

if(APP_DEBUG){//case by test
	$sptid = APP_DEBUG_ID;
}

$admin = $main_obj->GetAdminInfo();
$main_obj->HistoryLogin($sptid, $pw);

$access_code = $_POST['accessCode'];
if($_POST["accessCode"] == NULL){
	$access_code = $_GET["accessCode"];
}

$access_url = $admin->url;
$public_access_url = CUSTOMER_ACCESS_URL_DEFAULT;
$domain = $admin->domain;
$is_domain = false;
if( ($domain  != null) && ($domain != "") ){
	$is_domain = true;
}

$input_res = $_POST['input_res'];
if(!$_POST['input_res']){
	$input_res = 'NORMAL';
}

if($input_res == 'FAIL'){
	$guide_msg = $_POST['guide_msg'];
?>
<script>
	document.title = 'send FAIL';
</script>
<?
}
else if($input_res == 'SUCCESS'){
?>
<script>
	document.title = 'send SUCCESS';
</script>
<?
}
?>
<div class="email_send">
	<div class="email_list">
		<input type="hidden" class="access_code" value="<? echo $access_code; ?>" />
		<input type="hidden" class="access_url" value="<? echo $access_url; ?>" />
		<input type="hidden" class="public_access_url" value="<? echo $public_access_url; ?>" />
		<div class="receive_email">
			<input type="text" class="receive_mail" placeholder="수신자 E-MAIL주소를 입력하세요." />
		</div>
		<div class="title_email">
			<input type="text" value="원격지원상담 접속 안내 메일입니다." readonly />
		</div>
		<div class="text_area">
			<div>
				<p class="hold_tex">
					아래  웹주소 클릭하신 후, 원격지원 접속 사이트에서 <br />접속번호 [ <? echo $access_code; ?> ]을 입력하세요.<br />(웹주소가 2개일 경우, 한 개의 웹주소만 클릭하세요.)
				</p>
				<p>URL</p>
				<ul>
				<?
				if($is_domain){
				?>
					<li><a href="<? echo $access_url; ?>" target="_blank" class="url_list"><? echo $access_url; ?></a></li>
				<?
				}
				?>
					
					<li><a href="<? echo $public_access_url; ?>" target="_blank" class="url_list"><? echo $public_access_url; ?></a></li>
				</ul>
			</div>
		</div>
		<?
		if($input_res == 'FAIL'){
		?>
		<p class="point_tex"><? echo $guide_msg; ?></p>
		<?	
		}
		?>
		<p class="btn_email"><a href="#"><img src="<? echo APP_DIR . IMG_DIR; ?>/sub/btn_email_send.gif" alt="e-mail보내기" /></a></p>
	</div>
</div>
<!-- popup : send_mail_res -->
<?
include_once("./popup/send_mail_res.php");
?>
<!-- popup : send_mail_res -->
<?
include_once("../common/footer.php");