<?php
include_once("../../_func/function.common.php");
include_once("../common/config.php");

include_once("../common/header.php");

$sptid = $_POST["userid"];
if($_POST["userid"] == NULL){
	$sptid = $_GET["userid"];
}
$pw = APP_COWORK_LOGIN_PW;

if(APP_DEBUG){//case by test
	$sptid = APP_DEBUG_ID;
}

$main_obj->HistoryLogin($sptid, $pw);
$app_login_res = $main_obj->HistoryConfirmLogin();

if($app_login_res != "OK"){
?>
<form name="redirectBanForm" method="post" action="<? echo $config_path["NO_AUTH"]; ?>">
</form>
<script>
	document.redirectBanForm.submit();
</script>
<?	
}//end of : if($app_login_res == "")

$page = $_POST['page'];
if(!$_POST['page']){
	$page = 0;
}

$search_type = 'sptid';
$search_val = $sptid;

$start_time = $_POST['start_time'];
if(!$_POST['start_time']){
	$start_time = date("Y-m-d");
	$start_YYYY = date("Y");
	$start_MM = date("m");
	$start_DD = date("d");
}
else{
	$start_time_arr = explode("-", $start_time);
	$start_YYYY = $start_time_arr[0];
	$start_MM = $start_time_arr[1];
	$start_DD = $start_time_arr[2];
}

$end_time = $_POST['end_time'];
if(!$_POST['end_time']){
	$end_time = date("Y-m-d");
	$end_YYYY = date("Y");
	$end_MM = date("m");
	$end_DD = date("d");
}
else{
	$end_time_arr = explode("-", $end_time);
	$end_YYYY = $end_time_arr[0];
	$end_MM = $end_time_arr[1];
	$end_DD = $end_time_arr[2];
}


$exist_report = $main_obj->CheckReportExsit($search_type, $search_val, $start_time, $end_time);
$report_arr = $main_obj->GetReportList($page, $search_type, $search_val, $start_time, $end_time);

$content = $report_arr["datainfo"];
$pagination = $report_arr["pageInfo"];
$content_size = count($content);
$last_node = $content_size - 1;

$year_arr = $utils_obj->GetYears();
$YYYY_cnt = count($year_arr);
$month_arr = $utils_obj->GetMonths();
$MM_cnt = count($month_arr);
$this_last_day = $utils_obj->GetLastDay(date('m'), date('Y'));
$day_arr = $utils_obj->GetDays($this_last_day);
$DD_cnt = count($day_arr);

function print_last_bot($base, $current){
	$class_name = "";

	if($base == $current){
		$class_name = "last_bot";
	}

	return $class_name;
}

function print_point($base, $current){
	$class_name = "";

	if($base == $current){
		$class_name = "point";
	}

	return $class_name;
}

function print_selected($base, $current){
	$str_value = "";

	if($base == $current){
		$str_value = "selected";
	}

	return $str_value;
}
?>
<input type="hidden" class="sptid" value="<? echo $sptid; ?>" />
<div class="record_manager">
	<div class="choice_box search_record_box">
		<!-- [150824] -->
		<span class="comm_sel w_65 start_YYYY">
			<select>
			<?
			for($i=0; $i < $YYYY_cnt; $i++){
			?>
				<option value="<? echo $year_arr[$i]; ?>" <? echo print_selected($start_YYYY, $year_arr[$i]);?> ><? echo $year_arr[$i]; ?></option>
			<?
			}//for($i=0; $i < $YYYY_cnt; $i++)
			?>
			</select>
		</span>
		<span class="comm_sel w_49 start_MM">
			<select>
			<?
			for($i=0; $i < $MM_cnt; $i++){
			?>
				<option value="<? echo $month_arr[$i]; ?>" <? echo print_selected($start_MM, $month_arr[$i]); ?> ><? echo str_pad($month_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
			<?
			}//for($i=0; $i < $MM_cnt; $i++)
			?>
			</select>
		</span>
		<span class="comm_sel w_49 start_DD">
			<select>
			<?
			for($i=0; $i < $DD_cnt; $i++){
			?>
				<option value="<? echo $day_arr[$i]; ?>" <? echo print_selected($start_DD, $day_arr[$i]); ?> ><? echo str_pad($day_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
			<?
			}//for($i=0; $i < $DD_cnt; $i++)pad
			?>
			</select>
		</span>
		<div class="cen_line">~ </div>
		<span class="comm_sel w_65 end_YYYY">
			<select>
			<?
			for($i=0; $i < $YYYY_cnt; $i++){
			?>
				<option value="<? echo $year_arr[$i]; ?>" <? echo print_selected($end_YYYY, $year_arr[$i]);?> ><? echo $year_arr[$i]; ?></option>
			<?
			}//for($i=0; $i < $YYYY_cnt; $i++)
			?>
			</select>
		</span>
		<span class="comm_sel w_49 end_MM">
			<select>
			<?
			for($i=0; $i < $MM_cnt; $i++){
			?>
				<option value="<? echo $month_arr[$i]; ?>" <? echo print_selected($end_MM, $month_arr[$i]); ?> ><? echo str_pad($month_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
			<?
			}//for($i=0; $i < $MM_cnt; $i++)
			?>
			</select>
		</span>
		<span class="comm_sel w_49 end_DD">
			<select>
			<?
			for($i=0; $i < $DD_cnt; $i++){
			?>
				<option value="<? echo $day_arr[$i]; ?>" <? echo print_selected($end_DD, $day_arr[$i]); ?> ><? echo str_pad($day_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
			<?
			}//for($i=0; $i < $DD_cnt; $i++)pad
			?>
			</select>
		</span>
		<!-- //[150824] -->
		<div class="search"><a href="#" class="search_record"><img src="<? echo APP_DIR . IMG_DIR; ?>/sub/img_btn_search.gif" alt="검색" /></a></div>
		<div class="excel"><a href="#" class="export_record"><img src="<? echo APP_DIR . IMG_DIR; ?>/sub/img_btn_excel.gif" alt="엑셀다운로드" /></a></div>
	</div>
	<div>
		<!-- 지원기록 관리 상단 고정 th -->
		<table class="style_board_default manager_tbl"style="width:100%;" summary="날짜, 고객명,시작시간,종료시간,고객os,해결여부표">
			<caption><span>지원기록 관리</span></caption>
			<colgroup>
				<col style="width:80px;" />
				<col style="width:60px;" />
				<col style="width:70px;" />
				<col style="width:70px;" />
				<col style="width:70px;" />
				<col style="width:*;" />
			</colgroup>
			<thead>
				<tr>
					<th class="no_line">날짜</th>
					<th class="no_line">고객명</th>
					<th class="no_line">시작시간</th>
					<th class="no_line">종료시간</th>
					<th class="no_line">고개OS</th>
					<th class="no_line last" style="padding-right:10px;">해결여부</th>
				</tr>
			</thead>
		</table>
		<!-- 테이블 하단 td 내용 -->
		<div class="manager_tbl_sub ajax_record">
			<table class="style_board_default first_tbl manager_tbl"style="width:100%;" summary="날짜, 고객명,시작시간,종료시간,고객os,해결여부표">
				<caption><span>지원기록 관리</span></caption>
				<colgroup>
					<col style="width:80px;" />
					<col style="width:60px;" />
					<col style="width:70px;" />
					<col style="width:70px;" />
					<col style="width:70px;" />
					<col style="width:*;" />
				</colgroup>
				<tbody>
				<?
				if($exist_report == FALSE){// not exist report
				?>
					<tr class="last_bot">
						<td colspan="6">원격 지원 기록이 없습니다.</td>
					</tr>
				<?
				}
				else{//exist report
					for($i=0; $i < $content_size; $i++){
						$user_os = $utils_obj->GetOS($content[$i]["user_os"]);
						$user_oper = $utils_obj->GetOperationLog($content[$i]["operationlog"]);

						$start_time_arr = explode(' ', $content[$i]['starttime']);
						$end_time_arr = explode(' ', $content[$i]['endtime']);

						$date = $start_time_arr[0];
						$support_start_time = $start_time_arr[1];
						$support_end_time = $end_time_arr[1];
				?>
					<tr class="is_data <? echo print_last_bot($last_node, $i); ?>" onClick="pageTitleSet(event, '<? echo APP_RECORD_TITLE; ?>', <? echo $content[$i]['reportnum']; ?>)">
						<td><? echo $date; ?></td>
						<td><? echo $content[$i]['name']; ?></td>
						<td><? echo $support_start_time; ?></td>
						<td><? echo $support_end_time; ?></td>
						<td>
							<div class="overflow_wrap_70">
								<span class="overflow_dot">
									<? echo $user_os; ?>
								</span>
							</div>
						</td>
						<td class="<? echo print_point(RESOLVE, $user_oper); ?>"><? echo $user_oper; ?></td>
					</tr>
				<?						
					}
				}//end of : for($i=0; $i < $content_size; $i++)
				?>	
				</tbody>
			</table>
		</div>
	</div>
</div>
<?
include_once("../common/footer.php");