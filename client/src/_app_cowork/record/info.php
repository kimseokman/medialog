<?php
include_once("../../_func/function.common.php");
include_once("../common/config.php");

include_once("../common/header.php");

$sptid = $_POST["userid"];
if($_POST["userid"] == NULL){
	$sptid = $_GET["userid"];
}
$pw = APP_COWORK_LOGIN_PW;

$report_num = $_POST["report_num"];
if($_POST["report_num"] == NULL){
	$report_num = $_GET["report_num"];
}

if(APP_DEBUG){//case by test
	$sptid = APP_DEBUG_ID;
	$report_num = APP_DEBUG_REPORT_NUM;
}

$main_obj->HistoryLogin($sptid, $pw);
$app_login_res = $main_obj->HistoryConfirmLogin();

if($app_login_res != "OK"){
?>
<form name="redirectBanForm" method="post" action="<? echo $config_path["NO_AUTH"]; ?>">
</form>
<script>
	document.redirectBanForm.submit();
</script>
<?	
}//end of : if($app_login_res == "")


$detail_report = $main_obj->GetReportByHistory($report_num);

if($detail_report == "NO_AUTH"){
?>
<form name="redirectBanForm" method="post" action="<? echo $config_path["NO_AUTH"]; ?>">
</form>
<script>
	document.redirectBanForm.submit();
</script>
<?	
}

if($detail_report->deleted == ""){
	$agent_id = $detail_report->sptid;
}
else{
	$agent_id = $detail_report->deleted;
}

$user_oper = $utils_obj->GetOperationLog($detail_report->operationlog);
$trouble_arr = $main_obj->GetTroubleTypeList();

$trouble_size = count($trouble_arr);

function print_selected($base, $current){
	$str_value = "";

	if($base == $current){
		$str_value = "selected";
	}

	return $str_value;
}

function print_checked($base, $current){
	$attr_val =  "";

	if($base == $current){
		$attr_val =  "checked";
	}

	return $attr_val;
}
?>
<form name="editRecordForm" method="post" action="/_func/function.edit_record.php">
	<input type="hidden" class="sptid" name="sptid" value="<? echo $sptid; ?>" />
	<input type="hidden" class="report_num" name="report_num" value="<? echo $report_num; ?>" />
	
	<div class="control_support">
		<div class="ip_dater">
			<ul>
				<li>IP : <? echo $detail_report->user_pub_ip." / ".$detail_report->user_pri_ip; ?></li>
			</ul>
			<ul>
				<li>원격 지원 시작 : <? echo $detail_report->starttime; ?></li>
				<li>원격 지원 종료 : <? echo $detail_report->endtime; ?></li>
			</ul>
		</div>
		<table class="style_board_userinfo" summary="고객명,연락처,이메일,유형,내용,해결여부 표" style="width:99%;">
			<caption>지원기록확인</caption>
			<colgroup>
				<col style="width:80px;" />
				<col style="width:*;" />
			</colgroup>
			<tbody>
				<tr>
					<th><label for="user_name">고객명</label></th>
					<td><input type="text" id="user_name" name="cus_name" value="<? echo $detail_report->name; ?>" /></td>
				</tr>
				<tr>
					<th><label for="user_tell">연락처</label></th>
					<td><input type="text" id="user_tell" name="cus_tel" value="<? echo $detail_report->tel; ?>" /></td>
				</tr>
				<tr>
					<th><label for="user_email">이메일</label></th>
					<td><input type="text" id="user_email" name="cus_mail" value="<? echo $detail_report->email; ?>" /></td>
				</tr>
				<tr>
					<th><label for="user_dep">유형</label></th>
					<td>
						<span class="comm_sel w_318"><!-- [150824] -->
							<select name="cus_support_type">
							<?
							for($i=0; $i < $trouble_size; $i++){
							?>
								<option value="<? echo $trouble_arr[$i]['trbnum']; ?>" <? echo print_selected($trouble_arr[$i]['trbnum'], $detail_report->trbnum)?>><? echo $trouble_arr[$i]['trbname']; ?></option>
							<?
							}//end of : for($i=0; $i < $trouble_size; $i++)
							?>
							</select>
						</span>
					</td>
				</tr>
				<tr>
					<th><label for="user_conts">내용</label></th>
					<td>
						<textarea class="tex_area" cols="30" rows="8" name="cus_conts" id="user_conts" ><? echo htmlspecialchars($detail_report->report); ?></textarea>
					</td>
				</tr>
				<tr class="last">
					<th>해결여부</th>
					<td>
						<div class="radio_sty">
							<p>
								<input type="radio" name="choice_list" id="resolve" value="<? echo RESOLVE; ?>" <? echo print_checked(RESOLVE, $user_oper); ?> />
								<label for="resolve">해결</label>
							</p>
							<p>
								<input type="radio" name="choice_list" id="defer" value="<? echo HOLD; ?>" <? echo print_checked(HOLD, $user_oper); ?> />
								<label for="defer">보류</label>
							</p>
							<p>
								<input type="radio" name="choice_list" id="need" value="<? echo NEED_DIRECT_SUPPORT; ?>" <? echo print_checked(NEED_DIRECT_SUPPORT, $user_oper); ?> />
								<label for="need">지원필요</label>
							</p>
							<p>
								<input type="radio" name="choice_list" id="unchecked" value="<? echo UNCHECKED; ?>" <? echo print_checked(UNCHECKED, $user_oper); ?> />
								<label for="unchecked">미체크</label>
							</p>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<p class="ip_btn">
			<a href="#" class="edit_record_btn">
				<img src="<? echo APP_DIR . IMG_DIR; ?>/sub/btn_ip_apply.gif" alt="확인버튼" />
			</a>
		</p>
	</div>
</form>
<!-- popup : edit_record_res -->
<?
include_once("./popup/edit_record_res.php");
?>
<!-- popup : edit_record_res -->
<?
include_once("../common/footer.php");