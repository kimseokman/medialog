<?php
$record_edit_res_popup_action = $_POST['record_edit_res_popup_action'];
$record_edit_res_popup_msg = $_POST['record_edit_res_popup_msg'];
$sptid = $_POST['sptid'];
$report_num = $_POST['report_num'];

if($record_edit_res_popup_action == 'open'){
?>
<!-- 지원 기록 수정 결과 창 -->
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<span class="popup_title_txt"></span>
			<a href="#" class="close close_pop_record_edit_res" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p class="h_num70">
					<? echo $record_edit_res_popup_msg; ?>
				</p>
				<input type="hidden" class="sptid" value="<? echo $sptid; ?>" />
				<input type="hidden" class="report_num" value="<? echo $report_num; ?>" />
				<div class="btn">
					<a href="#" class="confirm_pop_record_edit_res"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //지원 기록 수정 결과 창 -->
<?
}// end of : if($record_edit_res_popup_action == 'open')
?>