<?php

include_once "../_lib/_class/class.ASGConfig.php";
include_once "../_lib/_class/class.ASGMain.php";

$sptid = $_GET["sptid"];
$weblogin = $_GET["weblogin"];

if($sptid == "")
{
	echo "0.0.0.0";
	return;
}

$Config = new ASGConfig();
$livesupport = new ASGMain();

if($weblogin == 1)
{
	$sptid = $livesupport->GetSptIdByTitlekey($sptid); // webmode일경우 sptid에는 titlekey값이 들어감

	if($sptid == "NOID")
	{
		$fp = fopen(dirname(dirname( __FILE__ ))."\_lib\_class\kms_addr.txt" , "r");
	
		if(!$fp)
		{
			echo("0.0.0.0");
		}
		else
		{
			$addr = fread($fp , 16);
			fclose($fp);
			echo($addr);
			return;			
		}
	}
}

$kmsip = $livesupport->RouteKms($sptid);

if($kmsip == "NOIP")
{
	$fp = fopen(dirname(dirname( __FILE__ ))."\_lib\_class\kms_addr.txt" , "r");
	
	if(!$fp)
	{
		$kmsip = "0.0.0.0";
	}
	else
	{
		$addr = fread($fp , 16);
		fclose($fp);
		$kmsip = $addr;
	}
}
echo $kmsip;
?>