<?php
	
//include_once "../_lib/_class/class.ASGConfig.php";
include_once "../_lib/_class/class.ASGMain.php";

$accesscode = $_GET["accesscode"];	//normal host
$sptid = $_GET["sptid"];			// asserver

if($accesscode == "" && $sptid == "")
{
	echo "0.0.0.0";
	return;
}

//$Config = new ASGConfig();
$livesupport = new ASGMain();

if($accesscode != "")
{
	
	if(!$livesupport->IsAccesscodeValid($accesscode)) 
	{
		echo "0.0.0.0";
		return;
	}

	$kmsip = $livesupport->CheckKms($accesscode);

	if($kmsip == "NOIP")
	{
		$fp = fopen(dirname(dirname( __FILE__ ))."\_lib\_class\kms_addr.txt" , "r");
	
		if(!$fp)
		{
			echo("0.0.0.0");
		}
		else
		{
			$addr = fread($fp , 16);
			$kmsip = $addr;
			fclose($fp);
		}
	}
}
else if($sptid != "")
{
	
	$kmsip = $livesupport->CheckKmsBySptid($sptid);

	if($kmsip == "NOIP")
	{
		$fp = fopen(dirname(dirname( __FILE__ ))."\_lib\_class\kms_addr.txt" , "r");
	
		if(!$fp)
		{
			echo("0.0.0.0");
			return;
		}
		else
		{
			$addr = fread($fp , 16);
			$kmsip = $addr;
			fclose($fp);
		}
	}

}
echo $kmsip;
?>