<html>
<head><meta content="text/html;charset=utf-8" http-equiv="Content-Type" /></head>
<body style="margin:0; padding:0; font-family:Gulim, 굴림; color:#333; font-size:11px;">
<div style="text-align:center;">
	<table style="width:610px;text-align:left;" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td  style="height:100px; background:#fff;">
					<img src="<? echo $img_url; ?>/mailing/logo_ubizsupport.gif" style="border:none; vertical-align:top;" alt="LG U+ 원격 지원 서비스" />
				</td>
			</tr>

			<tr>
				<td style="height:120px; border-left:1px solid #d6d6d6; border-right:1px solid #d6d6d6; background:#fff;">
					<!-- [D] U+Biz 원격지원 홈페이지 링크 -->
					<a href="<? echo $home_url; ?>" target="_blank"><img src="<? echo $img_url; ?>/mailing/title_request.gif" style="border:none; vertical-align:top;" alt="원격지원 상담 접속을 위한 안내 메일입니다." /></a>
				</td>
			</tr>

			<tr>
				<td style="width: 548px; padding:0 30px; border-left:1px solid #d7d7d7; border-right:1px solid #d7d7d7;">
					<table style="line-height:0;padding:0 40px;border-top:1px solid #eaeaea;" cellspacing="0" cellpadding="0" width="468px">
						<tbody>
							<tr>
								<td>
									<div style="margin:120px 0 0; line-height:1; font-size:16px; color:#333333;font-weight:bold;">원격지원 상담 접속을 위한 안내 메일입니다.</div>
								</td>
							</tr>

							<tr>
								<td>
									<!-- [D] 원격지원 상담요청 안내문구 -->
									<div style="margin:15px 0 40px; color:#888888; line-height:18px; font-size:12px;">
										아래  웹주소 클릭하신 후, 원격지원 접속 사이트에서 접속번호[<span style="font-weight:bold;color:#ec098d;"><? echo $show_acc_code; ?></span>]을 입력하세요.<br />(웹주소가 2개일 경우, 한 개의 웹주소만 클릭하세요.)
									</div>
								</td>
							</tr>

							<tr>
								<td style="padding-bottom:10px;color:#333333; font-size:12px; line-height:1; font-weight:bold;">URL</td>
							</tr>

							<tr>
								<td>
									<!-- [D] 원격 지원 접속URL / 클릭하면 E-Mail 경로 원격 지원 접속 사이트 링크 / 사용하는 도메인 모두 기재됨 -->
									<div style="width:410px; padding:12px 25px; font-size:12px; line-height:18px; color:#ec098d; border:4px solid #eaeaea; word-break:break-all;<? echo $remote_url_css; ?>"><a href="<? echo $remote_url; ?>" target="_blank" style="color:#ec098d;"><? echo $remote_url; ?></a></div>
									<div style="width:410px; margin:15px 0 120px;padding:12px 25px; font-size:12px; line-height:18px; color:#ec098d; border:4px solid #eaeaea; word-break:break-all;"><a href="<? echo $remote_url2; ?>" target="_blank" style="color:#ec098d;"><? echo $remote_url2; ?></a></div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>

			<tr>
				<td style="margin:0; padding:0;">
					<img src="<? echo $img_url; ?>/mailing/footer_ubizsupport.gif"  style="border:none; vertical-align:top;" alt="고객센터: <? echo $contact_us; ?>. 본 메일은 발신전용 메일입니다. 서울특별시 용산구 한강대로 32 LG유플러스 빌딩. Copyright 2015 LG Uplus Corp. All Rights Reserved." />
				</td>
			</tr>
		</tbody>
	</table>
</div>
</body>
</html>