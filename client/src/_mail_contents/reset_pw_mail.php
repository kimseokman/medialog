<html>
<head></head>
<body style="margin:0; padding:0; font-family:Gulim, 굴림; color:#333; font-size:11px;">
<div style="text-align:center;">
	<table style="width:610px;text-align:left;" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td  style="height:100px; background:#fff;">
					<img src="<? echo $img_url; ?>/mailing/logo_ubizsupport.gif" style="border:none; vertical-align:top;" alt="LG U+ 원격 지원 서비스" />
				</td>
			</tr>

			<tr>
				<td style="height:120px; border-left:1px solid #d6d6d6; border-right:1px solid #d6d6d6; background:#fff;">
					<!-- [D] U+Biz 원격지원 홈페이지 링크 -->
					<a href="<? echo $home_url; ?>" target="_blank"><img src="<? echo $img_url; ?>/mailing/title_tempw.gif" style="border:none; vertical-align:top;" alt="임시 비밀번호 안내 메일입니다." /></a>
				</td>
			</tr>

			<tr>
				<td style="width: 548px; padding:0 30px; border-left:1px solid #d7d7d7; border-right:1px solid #d7d7d7;">
					<table style="line-height:0;padding:0 40px;border-top:1px solid #eaeaea;" cellspacing="0" cellpadding="0" width="468px">
						<tbody>
							<tr>
								<td style="padding:120px 0 40px;margin:0; line-height:1; font-size:16px; text-align:center;color:#333333;font-weight:bold;">임시 비밀번호를 안내드립니다.</td>
							</tr>

							<tr>
								<td>
									<!-- [D] 임시비밀번호는 영문,숫자,특수문자 조합 10자리수가 임의로 발송됨.  -->
									<table style="width:468px;text-align:center; border-collapse:collapse; table-layout: fixed;">
										<tr>
											<th style="height: 43px; font-size:12px; line-height:18px; color:#555; vertical-align: middle; background:#f9f9f9; border-top:1px solid #bfbfbf; border-bottom:1px solid #bfbfbf; letter-spacing: -1px;">임시 비밀번호</th>
											<td style="height: 43px; font-size:14px; line-height:18px; color:#ec098d; font-weight:bold; border-top:1px solid #e2e2e2; border-bottom:1px solid #e2e2e2;"><? echo $new_password; ?></td>
										</tr>
									</table>
								</td>
							</tr>

							<tr>
								<td>
									<div style="margin:15px 0 50px 0; color:#999999; line-height:1;  font-size:12px;">※ 임시비밀번호 로그인 후, <span style="color:#ec098d;">반드시 비밀번호를 변경</span>해주세요.</div>
								</td>
							</tr>

							<tr>
								<td style="padding:0 0 120px 0; text-align:center;">
									<!-- [D] 관리자사이트 로그인화면 링크 -->
									<a href="<? echo $login_url; ?>" target="_blank"><img src="<? echo $img_url; ?>/mailing/btn_mailinglogin.gif" style="cursor:pointer; border:none; vertical-align:top;" alt="로그인하기" /></a>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>

			<tr>
				<td style="margin:0; padding:0;">
					<img src="<? echo $img_url; ?>/mailing/footer_ubizsupport.gif" style="border:none; vertical-align:top;" alt="고객센터: <? echo $contact_us; ?>.  본 메일은 발신전용 메일입니다. 서울특별시 용산구 한강대로 32 LG유플러스 빌딩. Copyright 2015 LG Uplus Corp. All Rights Reserved." />
				</td>
			</tr>
		</tbody>
	</table>
</div>
</body>
</html>