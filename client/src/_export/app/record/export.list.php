<?php
/**
 * export_app_record_list.php
 * 2015.08.25 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
include_once($_SERVER['DOCUMENT_ROOT'] . APP_DIR . "/common/config.php");

$export_name = EXPORT_APP_RECORD;
if( $utils_obj->is_ie() ) $export_name = $utils_obj->utf2euc($export_name);
$export_name = $utils_obj->GetFilenamePostFix($export_name);
header("Content-Type: application/vnd.ms-excel; charset=utf-8");   
header("Content-Disposition: attachment; filename=\"".$export_name.".xls\""); 
header("Content-Description: PHP4 Generated Data");

$sptid = $_POST["sptid"];
$pw = APP_COWORK_LOGIN_PW;

$main_obj->HistoryLogin($sptid, $pw);
$app_login_res = $main_obj->HistoryConfirmLogin();

if($app_login_res != "OK"){
?>
<form name="redirectBanForm" method="post" action="<? echo $config_path["NO_AUTH"]; ?>">
</form>
<script>
	document.redirectBanForm.submit();
</script>
<?	
}//end of : if($app_login_res == "")

$page = $_POST['page'];
if(!$_POST['page']){
	$page = 0;
}

$search_type = 'sptid';
$search_val = $sptid;

$start_time = $_POST['start_time'];
if(!$_POST['start_time']){
	$start_time = date("Y-m-d");
	$start_YYYY = date("Y");
	$start_MM = date("m");
	$start_DD = date("d");
}
else{
	$start_time_arr = explode("-", $start_time);
	$start_YYYY = $start_time_arr[0];
	$start_MM = $start_time_arr[1];
	$start_DD = $start_time_arr[2];
}

$end_time = $_POST['end_time'];
if(!$_POST['end_time']){
	$end_time = date("Y-m-d");
	$end_YYYY = date("Y");
	$end_MM = date("m");
	$end_DD = date("d");
}
else{
	$end_time_arr = explode("-", $end_time);
	$end_YYYY = $end_time_arr[0];
	$end_MM = $end_time_arr[1];
	$end_DD = $end_time_arr[2];
}


$exist_report = $main_obj->CheckReportExsit($search_type, $search_val, $start_time, $end_time);
$report_arr = $main_obj->GetReportList($page, $search_type, $search_val, $start_time, $end_time);

$content = $report_arr["datainfo"];
$pagination = $report_arr["pageInfo"];
$content_size = count($content);
$last_node = $content_size - 1;

?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style> 
br{mso-data-placement:same-cell;}
.bg_color_white{background-color:#FFF;}
.bg_color_gray{background-color:#F3F3F3;}
.text_mark{font-weight:bold;}
table td{text-align:center;}
</style>
</head>
<body class="bg_color_white"> 
	<table width="600px" border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col style="width:80px;" />
			<col style="width:60px;" />
			<col style="width:70px;" />
			<col style="width:70px;" />
			<col style="width:*;" />
			<col style="width:70px;" />
		</colgroup>
		<thead>
			<tr class="bg_color_gray">
				<th>날짜</th>
				<th>고객명</th>
				<th>시작시간</th>
				<th>종료시간</th>
				<th>고개OS</th>
				<th>해결여부</th>
			</tr>
		</thead>
		<tbody>
		<?
		if($exist_report == FALSE){// not exist report
		?>
			<tr>
				<td colspan="6">원격 지원 기록이 없습니다.</td>
			</tr>
		<?
		}
		else{//exist report
			for($i=0; $i < $content_size; $i++){
				$user_os = $utils_obj->GetOS($content[$i]["user_os"]);
				$user_oper = $utils_obj->GetOperationLog($content[$i]["operationlog"]);

				$start_time_arr = explode(' ', $content[$i]['starttime']);
				$end_time_arr = explode(' ', $content[$i]['endtime']);

				$date = $start_time_arr[0];
				$support_start_time = $start_time_arr[1];
				$support_end_time = $end_time_arr[1];
		?>
			<tr>
				<td><? echo $date; ?></td>
				<td><? echo $content[$i]['name']; ?></td>
				<td><? echo $support_start_time; ?></td>
				<td><? echo $support_end_time; ?></td>
				<td><? echo $user_os; ?></td>
				<td><? echo $user_oper; ?></td>
			</tr>
		<?						
			}
		}//end of : for($i=0; $i < $content_size; $i++)
		?>
		</tbody>
	</table>
</body> 
</html>