<?php
/**
 * export_admin_statistics_day_list.php
 * 2015.08.18 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
$export_name = EXPORT_STAT_DAY;
if( $utils_obj->is_ie() ) $export_name = $utils_obj->utf2euc($export_name);
$export_name = $utils_obj->GetFilenamePostFix($export_name);
header("Content-Type: application/vnd.ms-excel; charset=utf-8");   
header("Content-Disposition: attachment; filename=\"".$export_name.".xls\""); 
header("Content-Description: PHP4 Generated Data");

$search_year = $_POST['search_year'];
if(!$_POST['search_year']){
	$search_year = date("Y");
	$start_YYYY = date("Y");
}

$search_month = $_POST['search_month'];
if(!$_POST['search_month']){
	$search_month = date("m");
	$start_MM = date("m");
}

$result = $main_obj->GetReportByDay($search_year, $search_month);

$total = 0;
if($search_year == ""){
	$tmp = mktime();
}
else{
	$tmp = mktime(0,0,1,$search_month,1,$search_year);
}

$days = date("t",$tmp);

for($i=0;$i<$days;$i++){
	$data[$i] = 0;
}

while($row = mysql_fetch_array($result)){
	$t = intVal(substr($row['starttime'],8,2));
	$z = --$t;
	$data[$z]++;
	$total++;
}

$totalPer = 0;
$data_size = count($data);
$last_node = $data_size - 1;
for($i=0; $i < $data_size; $i++){
	if($total != 0){
		$per[$i] = $rate = $data[$i]/$total*100;
	}else{
		$per[$i] = 0;
	}
	$totalPer += $per[$i];
}
?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style> 
br{mso-data-placement:same-cell;}
.bg_color_white{background-color:#FFF;}
.bg_color_gray{background-color:#F3F3F3;}
.text_mark{font-weight:bold;}
table td{text-align:center;}
</style>
</head>
<body class="bg_color_white"> 
	<table border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col style="width:15%;" />
			<col style="width:*;" />
			<col style="width:20%;" />
		</colgroup>
		<thead>
			<tr class="bg_color_gray">
				<th>날짜(일)</th>
				<th>접속횟수(회)</th>
				<th>백분율</th>
			</tr>
		</thead>
		<tbody>
		<?
		for($i=0; $i < $data_size; $i++){
		?>	
			<tr>
				<td><? echo $i+1; ?></td>
				<td><? echo $data[$i]; ?></td>
				<td><? echo sprintf("%.2f",$per[$i]); ?>%</td>
			</tr>
		<?
		}
		?>
		</tbody>
	</table>
</body>
</html>	