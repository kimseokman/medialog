<?php
/**
 * export_admin_statistics_support_time_list.php
 * 2015.08.18 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
$export_name = EXPORT_STAT_SUPPORT_TIME;
if( $utils_obj->is_ie() ) $export_name = $utils_obj->utf2euc($export_name);
$export_name = $utils_obj->GetFilenamePostFix($export_name);
header("Content-Type: application/vnd.ms-excel; charset=utf-8");   
header("Content-Disposition: attachment; filename=\"".$export_name.".xls\""); 
header("Content-Description: PHP4 Generated Data");

$start_time = $_POST['start_time'];
if(!$_POST['start_time']){
	$start_time = date("Y-m-d");
	$start_YYYY = date("Y");
	$start_MM = date("m");
	$start_DD = date("d");
}
else{
	$start_time_arr = explode("-", $start_time);
	$start_YYYY = $start_time_arr[0];
	$start_MM = $start_time_arr[1];
	$start_DD = $start_time_arr[2];
}

$end_time = $_POST['end_time'];
if(!$_POST['end_time']){
	$end_time = date("Y-m-d");
	$end_YYYY = date("Y");
	$end_MM = date("m");
	$end_DD = date("d");
}
else{
	$end_time_arr = explode("-", $end_time);
	$end_YYYY = $end_time_arr[0];
	$end_MM = $end_time_arr[1];
	$end_DD = $end_time_arr[2];
}

$result = $main_obj->GetReportByUseTime($start_time,$end_time);

$time[0] = "0 ~ 10";
$time[1] = "10 ~ 20";
$time[2] = "20 ~ 30";
$time[3] = "30 ~ 40";
$time[4] = "40 ~ 50";
$time[5] = "50 ~ 60";
$time[6] = "60 ~ ";
$time[7] = "etc";
$data_size = count($time);
$last_node = $data_size - 1;

$total = 0;

for($i=0; $i < $data_size; $i++){
	$data[$i] = 0;
}

while($row = mysql_fetch_array($result)){
	$sTime = mktime(intVal(substr($row["starttime"],11,2)),intVal(substr($row["starttime"],14,2)),intVal(substr($row["starttime"],17,2)),intVal(substr($row["starttime"],5,2)),intVal(substr($row["starttime"],8,2)),intVal(substr($row["starttime"],0,4)));
	$eTime = mktime(intVal(substr($row["endtime"],11,2)),intVal(substr($row["endtime"],14,2)),intVal(substr($row["endtime"],17,2)),intVal(substr($row["endtime"],5,2)),intVal(substr($row["endtime"],8,2)),intVal(substr($row["endtime"],0,4)));

	$useTime = intVal($eTime) - intVal($sTime);

	if(intVal($useTime) < 0){
		$data[7]++;
	}else if($useTime == 0){
		$data[0]++;
	}else{
		$t = $useTime / 600;
		if($t > 5){
			$data[6]++;
		}else{
			$data[$t]++;
		}
	}

	$total++;
}

$totalPer = 0;
for($i=0; $i < $data_size; $i++){
	if($total != 0){
		$per[$i] = $rate=round($data[$i]/$total*1000)/10;
	}else{
		$per[$i] = 0;
	}
	$totalPer += $per[$i];
}

$max = 0;
for($j=0; $j < $data_size; $j++){
	if($data[$j] > $max){
		$max = $data[$j];
	}
}
$max = $max+10;
?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style> 
br{mso-data-placement:same-cell;}
.bg_color_white{background-color:#FFF;}
.bg_color_gray{background-color:#F3F3F3;}
.text_mark{font-weight:bold;}
table td{text-align:center;}
</style>
</head>
<body class="bg_color_white"> 
	<table border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col style="width:33.3%;" />
			<col style="width:33.3%;" />
			<col style="width:33.3%;" />
		</colgroup>
		<thead>
			<tr class="bg_color_gray">
				<th>상담시간(분)</th>
				<th>접속횟수 (회)</th>
				<th>백분율</th>
			</tr>
		</thead>
		<tbody>
		<?
		for($i=0; $i < $data_size; $i++){
		?>	
			<tr>
				<td><? echo $time[$i]; ?></td>
				<td><? echo $data[$i]; ?></td>
				<td><? echo sprintf("%.2f",$per[$i]);?>%</td>
			</tr>
		<?
		}
		?>
		</tbody>
	</table>
</body>
</html>