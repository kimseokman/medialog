<?php
/**
 * export_admin_statistics_os_list.php
 * 2015.08.18 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
$export_name = EXPORT_STAT_OS;
if( $utils_obj->is_ie() ) $export_name = $utils_obj->utf2euc($export_name);
$export_name = $utils_obj->GetFilenamePostFix($export_name);
header("Content-Type: application/vnd.ms-excel; charset=utf-8");   
header("Content-Disposition: attachment; filename=\"".$export_name.".xls\""); 
header("Content-Description: PHP4 Generated Data");

$start_time = $_POST['start_time'];
if(!$_POST['start_time']){
	$start_time = date("Y-m-d");
	$start_YYYY = date("Y");
	$start_MM = date("m");
	$start_DD = date("d");
}
else{
	$start_time_arr = explode("-", $start_time);
	$start_YYYY = $start_time_arr[0];
	$start_MM = $start_time_arr[1];
	$start_DD = $start_time_arr[2];
}

$end_time = $_POST['end_time'];
if(!$_POST['end_time']){
	$end_time = date("Y-m-d");
	$end_YYYY = date("Y");
	$end_MM = date("m");
	$end_DD = date("d");
}
else{
	$end_time_arr = explode("-", $end_time);
	$end_YYYY = $end_time_arr[0];
	$end_MM = $end_time_arr[1];
	$end_DD = $end_time_arr[2];
}

$result = $main_obj->GetReportByOS($start_time, $end_time);

$total = 0;

$os[0] = "";
$os[1] = OS_WIN_98;
$os[2] = OS_WIN_2000;
$os[3] = OS_WIN_XP;
$os[4] = OS_WIN_2003;
$os[5] = OS_WIN_VISTA;
$os[6] = OS_WIN_7;
$os[7] = OS_UNKNOWN;
$os[8] = OS_WIN_8;
$os[9] = OS_WIN_2008;
$os[10] = OS_WIN_2012;
$os[11] = OS_ANDROID;
$os[12] = OS_MAC;
$os[13] = OS_LINUX;
$os[14] = OS_WIN_8_1;
$os[15] = OS_WIN_10;
$os[16] = OS_WIN_SERVER;

$data_size = count($os);
$last_node = $data_size - 1;
for($i=0;$i<$data_size;$i++){
	$data[$i] = 0;
}

while($row = mysql_fetch_array($result)){


	$index = $row['os'];

	if($index == 100)
		$index  = 11;
	else if($index == 101)
		$index = 12;
	else if($index == 102)
		$index = 13;

	$data[$index] = $row['cnt'];

	$total += intVal($row['cnt']);

}
$data[7] += $data[0]; // 기존에 저장안되었던 os 정보는 기타 os에 저장한다.


$totalPer = 0;
for($i=1; $i < $data_size; $i++){
	if($total != 0){
		//$per[$i] = $rate=round($data[$i]/$total*1000)/10;
		$per[$i] = $rate = $data[$i]/$total*100;
	}else{
		$per[$i] = 0;
	}
	$totalPer += $per[$i];
}
?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style> 
br{mso-data-placement:same-cell;}
.bg_color_white{background-color:#FFF;}
.bg_color_gray{background-color:#F3F3F3;}
.text_mark{font-weight:bold;}
table td{text-align:center;}
</style>
</head>
<body class="bg_color_white"> 
	<table border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col style="width:33.3%;" />
			<col style="width:33.3%;" />
			<col style="width:33.3%;" />
		</colgroup>
		<thead>
			<tr class="bg_color_gray">
				<th>운영체제</th>
				<th>접속횟수 (회)</th>
				<th>백분율</th>
			</tr>
		</thead>
		<tbody>
		<?
		for($i=1;$i<$data_size;$i++){
		?>
			<tr>
				<td><? echo $os[$i]; ?></td>
				<td><? echo $data[$i]; ?></td>
				<td><? echo sprintf("%.2f",$per[$i]); ?>%</td>
			</tr>
		<?
		}//end of : for($i=1;$i<sizeof($data);$i++)
		?>
		</tbody>
	</table>
</body>
</html>