<?php
/**
 * export_admin_statistics_agent_list.php
 * 2015.08.18 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
$export_name = EXPORT_STAT_AGENT;
if( $utils_obj->is_ie() ) $export_name = $utils_obj->utf2euc($export_name);
$export_name = $utils_obj->GetFilenamePostFix($export_name);
header("Content-Type: application/vnd.ms-excel; charset=utf-8");   
header("Content-Disposition: attachment; filename=\"".$export_name.".xls\""); 
header("Content-Description: PHP4 Generated Data");

$start_time = $_POST['start_time'];
if(!$_POST['start_time']){
	$start_time = date("Y-m-d");
	$start_YYYY = date("Y");
	$start_MM = date("m");
	$start_DD = date("d");
}
else{
	$start_time_arr = explode("-", $start_time);
	$start_YYYY = $start_time_arr[0];
	$start_MM = $start_time_arr[1];
	$start_DD = $start_time_arr[2];
}

$end_time = $_POST['end_time'];
if(!$_POST['end_time']){
	$end_time = date("Y-m-d");
	$end_YYYY = date("Y");
	$end_MM = date("m");
	$end_DD = date("d");
}
else{
	$end_time_arr = explode("-", $end_time);
	$end_YYYY = $end_time_arr[0];
	$end_MM = $end_time_arr[1];
	$end_DD = $end_time_arr[2];
}

$result = $main_obj->GetReportBySpt($start_time, $end_time);

$total = 0;
$cnt = mysql_num_rows($result);

$i=0;
while($row = mysql_fetch_array($result)){
	$data[$i] = $row['cnt'];
	$name[$i] = $row['sptid'];
	$totaltime[$i] = $row['totaltime'];
	$total += $row['cnt'];
	$i++;
}

$totalPer = 0;
$data_size = count($data);
$last_node = $data_size - 1;
for($i=0; $i<$data_size; $i++){
	if($total != 0){
		$per[$i] = $rate = $data[$i]/$total*100;
	}else{
		$per[$i] = 0;
	}
	$totalPer += $per[$i];
}

function getTimeFromSeconds($seconds)
{
     $h = sprintf("%02d", intval($seconds) / 3600);
     $tmp = $seconds % 3600;
     $m = sprintf("%02d", $tmp / 60);
     $s = sprintf("%02d", $tmp % 60);
 
    return $h.':'.$m.':'.$s;
}

$totaltotaltime;
for($i=0; $i < $data_size; $i++){
	$totaltotaltime += $totaltime[$i];
}
?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style> 
br{mso-data-placement:same-cell;}
.bg_color_white{background-color:#FFF;}
.bg_color_gray{background-color:#F3F3F3;}
.text_mark{font-weight:bold;}
table td{text-align:center;}
</style>
</head>
<body class="bg_color_white"> 
	<table border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col style="width:90px;" />
			<col style="width:120px;" />
			<col style="width:*;" />
			<col style="width:220px;" />
			<col style="width:130px;" />
		</colgroup>
		<thead>
			<tr class="bg_color_gray">
				<th>상담원ID</th>
				<th>접속횟수 (회)</th>
				<th>총 이용 시간</th>
				<th>평균 이용 시간</th>
				<th>백분율</th>
			</tr>
		</thead>
		<tbody>
		<?
		for($i=0; $i < $data_size; $i++){	
		?>
			<tr>
				<td><? echo $name[$i]; ?></td>
				<td><? echo $data[$i]; ?></td>
				<td><? echo getTimeFromSeconds($totaltime[$i]); ?></td>
				<td><? echo getTimeFromSeconds($totaltime[$i]/$data[$i]); ?></td>
				<td><? echo sprintf("%.2f",$per[$i]); ?>%</td>
			</tr>
		<?
		}// end of :  for($i=0;$i<sizeof($data);$i++)
		?>	
		</tbody>
	</table>
</body>
</html>