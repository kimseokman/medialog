<?php
/**
 * export_admin_user_list.php
 * 2015.08.13 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
$export_name = EXPORT_SUPPORT_LOG;
if( $utils_obj->is_ie() ) $export_name = $utils_obj->utf2euc($export_name);
$export_name = $utils_obj->GetFilenamePostFix($export_name);
header("Content-Type: application/vnd.ms-excel; charset=utf-8");   
header("Content-Disposition: attachment; filename=\"".$export_name.".xls\""); 
header("Content-Description: PHP4 Generated Data");

$search_type = $_POST['search_type'];
if(!$_POST['search_type']){
	$search_type = "";
}

$search_val = $_POST['search_val'];
if(!$_POST['search_val']){
	$search_val = "";	
}

$start_time = $_POST['start_time'];
if(!$_POST['start_time']){
	$start_time = date("Y-m-d");
}

$end_time = $_POST['end_time'];
if(!$_POST['end_time']){
	$end_time = date("Y-m-d");
}

$isAdmin = $main_obj->ConfirmAdmin();
$exist_report = $main_obj->CheckReportExsit($search_type, $search_val, $start_time, $end_time);
$report_arr = $main_obj->GetReportExcel($search_type, $search_val, $start_time, $end_time);

$content = $report_arr["datainfo"];
$content_size = count($content);

function my_nl2br($str){
	$str = preg_replace("/\r/", "", $str);
	$str = preg_replace("/(\>[ ]*)\n/", ">\n", $str);
	$str = preg_replace("/((font|span|a)\>[ ]*|[^\>])\n/i", "\\1<br />\n", $str);
	return $str;
}
?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style> 
br{mso-data-placement:same-cell;}
.bg_color_white{background-color:#FFF;}
.bg_color_gray{background-color:#F3F3F3;}
.text_mark{font-weight:bold;}
table td{text-align:center;}
</style>
</head>
<body class="bg_color_white"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col width="40px" />
			<col width="90px" />
			<col width="90px" />
			<col width="90px" />
			<col width="90px" />
			<col width="80px" />
			<col width="90px" />
			<col width="90px" />
			<col width="90px" />
			<col width="90px" />
			<col width="*" />
			<col width="100px" />
			<col width="120px" />
			<col width="120px" />
			<col width="90px" />
			<col width="100px" />
			<col width="90px" />
			<col width="90px" />
		</colgroup>
		<thead>
			<tr class="bg_color_gray">
				<th>NO</th>
				<th>사용자ID</th>
				<th>사용자명</th>
				<th>사용자 IP</th>
				<th>사용자 PC명</th>
				<th>고객명</th>
				<th>고객 IP</th>
				<th>고객 PC명</th>
				<th>메일</th>
				<th>전화번호</th>
				<th>시작시각</th>
				<th>종료시각</th>
				<th>사용 시각</th>
				<th>고객OS</th>
				<th>지원유형</th>
				<th>지원내용</th>
				<th>해결여부</th>
				<th>만족도</th>
				<th>고객 코멘트</th>
			</tr>
		</thead>
		<tbody>
		<?
		if( ($exist_report == FALSE) || ($content_size == 0)){// not exist report
		?>
			<tr class="last">
				<td colspan="20">원격 지원 기록이 없습니다.</td>
			</tr>
		<?
		}
		else{//exist report
			for($i=0; $i < $content_size; $i++){
				$user_os = $utils_obj->GetOS($content[$i]["user_os"]);
				$user_oper = $utils_obj->GetOperationLog($content[$i]["operationlog"]);
				$user_satis = $utils_obj->GetSatisfaction($content[$i]["satisfaction"]);
				if($content[$i]["trbnum"] != 0){
					$trouble_name = $content[$i]["trbname"];
				}
				$report = my_nl2br($content[$i]["report"]);

				$sTime = mktime(intVal(substr($content[$i]["starttime"],11,2)),intVal(substr($content[$i]["starttime"],14,2)),intVal(substr($content[$i]["starttime"],17,2)),intVal(substr($content[$i]["starttime"],5,2)),intVal(substr($content[$i]["starttime"],8,2)),intVal(substr($content[$i]["starttime"],0,4)));
				$eTime = mktime(intVal(substr($content[$i]["endtime"],11,2)),intVal(substr($content[$i]["endtime"],14,2)),intVal(substr($content[$i]["endtime"],17,2)),intVal(substr($content[$i]["endtime"],5,2)),intVal(substr($content[$i]["endtime"],8,2)),intVal(substr($content[$i]["endtime"],0,4)));
				$use_time = intVal($eTime) - intVal($sTime);

				if($use_time < 0){
					$use_time = "알 수 없음";
				}else{
					$hours = intVal($use_time / 3600);//get hour
					$leftover = $use_time - ( $hours * 3600); 
					$min = intVal($leftover / 60);//get min
					$sec = $leftover - ( $min * 60);//get sec

					$use_time = str_pad($hours,"2","0",STR_PAD_LEFT).":".str_pad($min,"2","0",STR_PAD_LEFT).":".str_pad($sec,"2","0",STR_PAD_LEFT);
				}
		?>
			<tr>
				<td><? echo $i+1; ?></td>
				<td class="text_mark"><? echo $content[$i]['sptid']; ?></td>
				<td class="text_mark"><? echo $content[$i]["sptname"]; ?></td>
				<td class="text_mark"><? echo $content[$i]["spt_pub_ip"]." / ".$content[$i]["spt_pri_ip"]; ?></td>
				<td class="text_mark"><? echo $content[$i]["spt_com_name"]; ?></td>
				<td><? echo $content[$i]["name"]; ?></td>
				<td class="text_mark"><? echo $content[$i]["user_pub_ip"]." / ".$content[$i]["user_pri_ip"]; ?></td>
				<td class="text_mark"><? echo $content[$i]["user_com_name"]; ?></td>
				<td class="text_mark"><? echo $content[$i]["email"]; ?></td>
				<td class="text_mark"><? echo $content[$i]["tel"]; ?></td>
				<td><? echo $content[$i]["starttime"]; ?></td>
				<td><? echo $content[$i]["endtime"]; ?></td>
				<td><? echo $use_time; ?></td>
				<td><? echo $user_os; ?></td>
				<td class="text_mark"><? echo $trouble_name; ?></td>
				<td class="text_mark"><? echo $report; ?></td>
				<td><? echo $user_oper; ?></td>
				<td class="text_mark"><? echo $user_satis; ?></td>
				<td class="text_mark"><? echo $content[$i]["usercomment"]; ?></td>
			</tr>
		<?
			}
		}
		?>
		</tbody>
	</table>
</body> 
</html>