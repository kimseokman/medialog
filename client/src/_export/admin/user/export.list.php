<?php
/**
 * export_admin_user_list.php
 * 2015.08.13 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
$export_name = EXPORT_USER_INFO;
if( $utils_obj->is_ie() ) $export_name = $utils_obj->utf2euc($export_name);
$export_name = $utils_obj->GetFilenamePostFix($export_name);
header("Content-Type: application/vnd.ms-excel; charset=utf-8");   
header("Content-Disposition: attachment; filename=\"".$export_name.".xls\""); 
header("Content-Description: PHP4 Generated Data");

$spt_arr = $main_obj->GetSupporterList($search, $searchVal);
$spt_size = count($spt_arr);
?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style> 
br{mso-data-placement:same-cell;}
.bg_color_white{background-color:#FFF;}
.bg_color_gray{background-color:#F3F3F3;}
.text_mark{font-weight:bold;}
table td{text-align:center;}
</style>
</head>
<body class="bg_color_white"> 
	<table border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col style="width:70px;" />
			<col style="width:100px;" />
			<col style="width:100px;" />
			<col style="width:100px;" />
			<col style="width:80px;" />
			<col style="width:*;" />
			<col style="width:220px;" />
		</colgroup>
		<thead>
			<tr class="bg_color_gray">
				<th>NO</th>
				<th>사용자ID</th>
				<th>사용자명</th>
				<th>소속 부서</th>
				<th>상태</th>
				<th>최근접속시간</th>
				<th>지원권한</th>
			</tr>
		</thead>
		<tbody>
		<?
		if($spt_size == 0){
		?>
			<tr>
				<td colspan="7">상담원 정보가 없습니다. 상담원을 생성해 주세요.</td>
			</tr>
		<?
		}//end of : if($spt_size == 0)
		else{
			$last_node_num = $spt_size - 1;
			for($i = 0; $i < $spt_size; $i++){
				//set dept name 
				$user_dept_num = $spt_arr[$i]['depnum'];
				$dept = $main_obj->GetDepByDepnum($user_dept_num);
				$dep_name = $dept->depname;

				//set status
				$user_status = $spt_arr[$i]['logon'];
				$user_status_call = $spt_arr[$i]['cnt'];
				if($user_status == 0){
					$status = "오프라인";
				}
				else if($user_status == 1) {
					if($user_status_call == 0) {
						$status = "온라인";
					} 
					else {
						$status = "지원중";
					}
				} 
				else if($user_status == 2) {
					$status = "지원중";
				}
				
				//set auth
				$user_auth = $spt_arr[$i]['function'];
				if(($user_auth & PERMISSION_MOBILE_VIDEO_PAID) || ($user_auth & PERMISSION_MOBILE_VIDEO_FREE)){
					$auth = PRODUCT_MOBILE_VIDEO;
				}
				else if(($user_auth & PERMISSION_MOBILE_PAID) || ($user_auth & PERMISSION_MOBILE_FREE)){
					$auth = PRODUCT_MOBILE;
				}
				else if(($user_auth & PERMISSION_VIDEO_PAID) || ($user_auth & PERMISSION_VIDEO_FREE)){
					$auth = PRODUCT_VIDEO;
				}
				else{
					$auth = PRODUCT_PC;
				}
		?>
			<tr>
				<td><? echo $spt_arr[$i]['num']; ?></td>
				<td><? echo $spt_arr[$i]['sptid']; ?></td>
				<td><? echo $spt_arr[$i]['name']; ?></td>
				<td><? echo $dep_name; ?></td>
				<td><? echo $status; ?></td>
				<td><? echo $spt_arr[$i]['lastlogin']; ?></td>
				<td>
				<?
				if($auth == PRODUCT_MOBILE_VIDEO){
				?>
					PC + MOBILE + VIDEO
				<?
				}//end of : if($auth == PRODUCT_MOBILE_VIDEO)
				else if($auth == PRODUCT_MOBILE){
				?>
					PC + MOBILE
				<?
				}//end of : if($auth == PRODUCT_MOBILE)
				else if($auth == PRODUCT_VIDEO){
				?>
					PC + VIDEO
				<?
				}//end of : if($auth == PRODUCT_VIDEO)
				else if($auth == PRODUCT_PC){
				?>
					PC
				<?
				}//end of : if($auth == PRODUCT_PC)
				?>
				</td>
			</tr>
		<?
			}//end of : for($i = 0; $i < $dept_size; $i++)
		}//end of : if($spt_size != 0)
		?>
		</tbody>
	</table>
</body> 
</html>