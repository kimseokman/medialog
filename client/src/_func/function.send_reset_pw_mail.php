<?php
/**
 * send_reset_pw_mail.php
 * 2015.07.31 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
require($_SERVER['DOCUMENT_ROOT']."/_lib/_inc/phpmailer/class.phpmailer.php");

$user_id = $_POST['user_id'];
$user_eamil = $_POST['user_email'];
$reset_user_type = $_POST['reset_user_type'];

if($reset_user_type == "user"){
	$return_url = LOGIN_TRY;
}
else if($reset_user_type == "admin"){
	$return_url = ADMIN_LOGIN_TRY;
}
else{
	$return_url = LOGIN_TRY;
}

$config_path = array(
	"SEND_SUCCESS" => $return_url,
	"SEND_FAIL" => $return_url,
	"RESET_FAIL" => $return_url,
	"CHECK_FAIL" => $return_url
);

$config_msg = array(
	"RESET_FAIL" => "비밀번호 재설정에 실패했습니다.<br />다시 시도해주시기 바랍니다.<br />문제가 지속 된다면 U+Biz 원격지원 고객센터<br /><strong>(".CONTACT_US_TEL.")</strong>으로 문의주시기 바랍니다.",
	"CHECK_FAIL" => "등록된 메일 또는 관리자 아이디가 아닙니다.<br />다시 시도해주시기 바랍니다."
);

$check_res = $main_obj->CheckEmail($reset_user_type, $user_id, $user_eamil);

$reset_pw_res_pop_action = "close";
if($check_res == "FAIL"){
	$reset_pw_res_pop_action = "open";
?>
<form name="reset_pw_fail_form" method="post" action="<? echo $config_path['RESET_FAIL']; ?>">
	<input type="hidden" name="reset_pw_res_pop_action" value="<? echo $reset_pw_res_pop_action; ?>" />
	<input type="hidden" name="reset_pw_res_pop_msg" value="<? echo $config_msg['CHECK_FAIL']; ?>" />
</form>
<script>
	document.reset_pw_fail_form.submit();
</script>
<?
}
else if($check_res == "OK"){
	//create config password
	$pw_config_deciaml = "";
	for($i=48; $i <= 57; $i++){//숫자
		$pw_config_deciaml = $pw_config_deciaml . chr($i);
	}

	$pw_config_en = "";
	for($i=65; $i <= 90; $i++){//영문자 : 대문자 ( 소문자 제외 )
		$pw_config_en = $pw_config_en . chr($i);
	}

	//for($i=33; $i <= 27; $i++){//특수문자1
	//	$pw_config = $pw_config . chr($i);
	//}

	//for($i=58; $i <= 64; $i++){//특수문자2
	//	$pw_config = $pw_config . chr($i);
	//}

	//for($i=91; $i <= 96; $i++){//특수문자3
	//	$pw_config = $pw_config . chr($i);
	//}

	//for($i=123; $i <= 126; $i++){//특수문자4
	//	$pw_config = $pw_config . chr($i);
	//}
	
	//!@#$%^&* 까지 특수 문자로 인정
	$pw_config_special = "";
	$pw_config_special = $pw_config_special . chr(33);//!
	$pw_config_special = $pw_config_special . chr(64);//@
	$pw_config_special = $pw_config_special . chr(35);//#
	$pw_config_special = $pw_config_special . chr(36);//$
	$pw_config_special = $pw_config_special . chr(37);//%
	$pw_config_special = $pw_config_special . chr(94);//^
	$pw_config_special = $pw_config_special . chr(38);//&
	$pw_config_special = $pw_config_special . chr(42);//*

	$pw_length = PASSWORD_LENGTH;
	//$pw_chars_len = (strlen($pw_config) - 1);

	$new_pw = "";
	for($i = 0; $i < $pw_length; $i++){
		//$new_pw = $new_pw . $pw_config[rand(0, $pw_chars_len)];
		
		if( ($i >= 0) && ($i <= 3)){//1~4자리
			$new_pw = $new_pw . $pw_config_en[rand(0, (strlen($pw_config_en)-1))];
		}
		else if( ($i > 3) && ($i <= 5)){//1~4자리
			$new_pw = $new_pw . $pw_config_deciaml[rand(0, (strlen($pw_config_deciaml)-1))];
		}
		else{
			$new_pw = $new_pw . $pw_config_special[rand(0, (strlen($pw_config_special)-1))];
		}
		
	}

	$reset_res = $main_obj->ResetPassword($user_id, $user_eamil, $new_pw);

	if($reset_res != 'OK'){
		$reset_pw_res_pop_action = "open";
	?>
	<form name="reset_pw_fail_form" method="post" action="<? echo $config_path['RESET_FAIL']; ?>">
		<input type="hidden" name="reset_pw_res_pop_action" value="<? echo $reset_pw_res_pop_action; ?>" />
		<input type="hidden" name="reset_pw_res_pop_msg" value="<? echo $config_msg['RESET_FAIL']; ?>" />
	</form>
	<script>
		document.reset_pw_fail_form.submit();
	</script>
	<?	
	}
	else{
		//
		//	sender
		//
		$from = ML_MAIL_ADDR;
		$from_name = ML_MAIL_NAME;
		//
		//	recipient
		//
		$to = $user_eamil;
		$to_name = $user_id;

		//
		//	mail contents data
		//

		$home_url = REMOTE_HOME;
		$img_url = ML_URL.IMG_DIR;
		$new_password = $new_pw;
		$login_url = ML_URL.$return_url;
		$contact_us = CONTACT_US_TEL;

		//
		//	mail setting
		//
		$mail = new PHPMailer(); 
		$mail->IsSMTP(); // send via SMTP
		$mail->Host = ML_MAIL_HOST;
		$mail->Port = ML_MAIL_PORT;
		$mail->Mailer = ML_MAILER;
		$mail->CharSet = ML_MAIL_CHARSET;
		$mail->SMTPAuth = true; // turn on SMTP authentication
		$mail->Username = ML_MAIL_ID; // SMTP username
		$mail->Password = ML_MAIL_PW;// SMTP password

		//
		//	mail body
		//
		$mail->Subject = "[ ".SERVICE_NAME." ] 임시 비밀번호 안내 메일입니다.";
		$message = file_get_contents($_SERVER['DOCUMENT_ROOT'].MAIL_BODY_RESET_PW);
		$message = str_replace('<? echo $home_url; ?>', $home_url, $message); 
    		$message = str_replace('<? echo $img_url; ?>', $img_url, $message);
    		$message = str_replace('<? echo $new_password; ?>', $new_password, $message);
    		$message = str_replace('<? echo $login_url; ?>', $login_url, $message);
    		$message = str_replace('<? echo $contact_us; ?>', $contact_us, $message);
		$mail->MsgHTML($message);

		//
		//	mail ready
		//
		$mail->From = $from;
		$mail->FromName = $from_name;
		$mail->AddAddress($to, $to_name);
		$mail->AddReplyTo($from, $from_name);
		$mail->WordWrap = 50; // set word wrap
		$mail->IsHTML(TRUE); // send as HTML

		//
		//	send mail
		//
		$mail_res = $mail->Send();

		$reset_pw_send_mail_res_pop_action = "close";
		if(!$mail_res){//fail
			$reset_pw_send_mail_res_pop_action = "open";
		?>
		<form name="send_mail_fail_form" method="post" action="<? echo $config_path['SEND_FAIL']; ?>">
			<input type="hidden" name="send_mail_fail_pop_action" value="<? echo $reset_pw_send_mail_res_pop_action; ?>" />
			<input type="hidden" name="reset_pw_res_pop_msg" value="<? echo $config_msg['RESET_FAIL']; ?>" />
		</form>
		<script>
			document.send_mail_fail_form.submit();
		</script>
		<?
		}
		else{//success
			$reset_pw_send_mail_res_pop_action = "open";
		?>
		<form name="send_mail_success_form" method="post" action="<? echo $config_path['SEND_SUCCESS']; ?>">	
			<input type="hidden" name="send_mail_success_pop_action" value="<? echo $reset_pw_send_mail_res_pop_action; ?>" />
		</form>
		<script>
			document.send_mail_success_form.submit();
		</script>
		<?
		}
	}
}