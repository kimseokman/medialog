<?php
/**
 * common.php
 * 2015.07.29 | KSM | create
 */
session_start();
header('Content-Type: text/html; charset=utf-8');

include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.constants.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGUtils.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGMain.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGConfig.php");

$main_obj = new ASGMain();
$utils_obj = new ASGUtils();
$config_obj = new ASGConfig();

$login_flag = $main_obj->IsLogin();
$supporter_login_flag = $main_obj->SupporterIsLogin();
$admin_login_flag = $main_obj->AdminIsLogin();
$super_login_flag = $main_obj->OwnerIsLogin();

//validate auth
$access_auth = $utils_obj->GetAccessAuth();
$current_user = $utils_obj->GetCurrentUserType();

$session_valid = $utils_obj->ValidLastActivity();

if($access_auth == "Super"){
	if($current_user['type'] == "OWNER"){
		$utils_obj->UpdateLastActivity();// upate last activity
	}
	else if($current_user['type'] == "ADMIN"){
		$utils_obj->RedirectURL(ADMIN_INDEX);
	}
	else if($current_user['type'] == "SUPPORTER"){
		$utils_obj->RedirectURL(SUPPORTER_INDEX);
	}

	if(!$super_login_flag || !$session_valid){
		$utils_obj->RedirectURL(SUPERADMIN_LOGIN_TRY);
	}
}
else if($access_auth == "Admin"){
	if($current_user['type'] == "OWNER"){
		$utils_obj->RedirectURL(SUPERADMIN_INDEX);
	}
	else if($current_user['type'] == "ADMIN"){
		$admin = $main_obj->GetAdminInfo();//get Admin Info
		$admin_product = $utils_obj->GetProduct($admin->function);
		$utils_obj->UpdateLastActivity();// upate last activity
	}
	else if($current_user['type'] == "SUPPORTER"){
		$utils_obj->RedirectURL(SUPPORTER_INDEX);
	}

	if(!$admin_login_flag || !$session_valid){
		$utils_obj->RedirectURL(ADMIN_LOGIN_TRY);
	}
}
else if($access_auth == "Agent"){
	if($current_user['type'] == "OWNER"){
		$utils_obj->RedirectURL(SUPERADMIN_INDEX);
	}
	else if($current_user['type'] == "ADMIN"){
		$utils_obj->RedirectURL(ADMIN_INDEX);
	}
	else if($current_user['type'] == "SUPPORTER"){
		$utils_obj->UpdateLastActivity();// upate last activity
	}

	if(!$supporter_login_flag || !$session_valid){
		$utils_obj->RedirectURL(LOGIN_TRY);
	}
}
else if($access_auth == "Login"){
	if($admin_login_flag){
		if($current_user['type'] == "ADMIN"){
			$utils_obj->RedirectURL(ADMIN_INDEX);
		}
	}
	else if($supporter_login_flag){
		if($current_user['type'] == "SUPPORTER"){
			$utils_obj->RedirectURL(SUPPORTER_INDEX);
		}
	}
	else if($super_login_flag){
		if($current_user['type'] == "OWNER"){
			$utils_obj->RedirectURL(SUPERADMIN_INDEX);
		}
	}
	else{
		//not any more	
	}
}
else if($access_auth == "Customer"){
	$customer_auth = $utils_obj->GetCustomerAuth();
}
else{
	//not any more
}

$current_url = $utils_obj->GetCurrentPageURL();

return "Load common.php";