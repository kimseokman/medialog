<?php
/**
 * login_by_super.php
 * 2015.08.31 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$ip_block_use = SUPERADMIN_ACCESS_IP_BLOCK;  // ip 제한 사용 여부
$login_interval = LAST_LOGIN_INTERVAL;

$config_path = array(
     "LOGIN_SUCCESS_OWNER" => SUPERADMIN_INDEX,
     "LOGIN_FAIL" => SUPERADMIN_LOGIN_TRY
);

$alert = array(
     "action" => "close",
     "title" => "",
     "msg" => ""
);

$user_type = "OWNER";
$user_id = $_POST['super_id'];
$user_pw = $_POST['super_pw'];

$deny_flag = FALSE;

//
//   ip 제한
//
if($ip_block_use){
     $client_ip = $_SERVER["REMOTE_ADDR"];
     $block_flag = FALSE;
     $token = strtok($config_obj->AdminAllowedNetwork, ":;, ");

     while ($token != "") {
          if (strncmp($client_ip, $token, strlen($token)) == 0) {
               $block_flag = TRUE;
               $deny_flag = TRUE;
               break;
          }

          $token = strtok(":;, ");
     }
}


if(!$deny_flag){
     //
     //     Logic : Login
     //
     $access_log = $main_obj->OwnerLogin($user_id, $user_pw, $login_interval);
     if($access_log == "OWNER"){//case by login success admin
          $main_obj->SetLastLogin($user_type, $user_id);
          $utils_obj->UpdateLastActivity();
     ?>
          <form name="login_success_form" method="post" action="<? echo $config_path['LOGIN_SUCCESS_OWNER']; ?>">
          </from>
          <script>
               document.login_success_form.submit();
          </script>
     <?
     }
     else if($access_log == "OLD_OWNER"){//case by too old
               $alert['action'] = "open";
     ?>
     <form name="login_fail_form" method="post" action="<? echo $config_path['LOGIN_FAIL']; ?>">
          <input type="hidden" name="deny_old_super_action" value="<? echo $alert['action']; ?>" />
     </form>
     <script>
          document.login_fail_form.submit();
     </script>
     <?
     }
     else if($access_log == "NOID"){//case by no user id
          $alert['action'] = "open";
          $alert['title'] = "아이디 오류";
          $alert['msg'] = "아이디는 공백이 될 수 없습니다.";
     ?>
     <form name="login_fail_form" method="post" action="<? echo $config_path['LOGIN_FAIL']; ?>">
          <input type="hidden" name="alert_action" value="<? echo $alert['action']; ?>" />
          <input type="hidden" name="alert_title" value="<? echo $alert['title']; ?>" />
          <input type="hidden" name="alert_msg" value="<? echo $alert['msg']; ?>" />
     </form>
     <script>
          document.login_fail_form.submit();
     </script>
     <?     
     }
     else if($access_log == "NOPASSWORD"){//case by no user password
          $alert['action'] = "open";
          $alert['title'] = "비밀번호 오류";
          $alert['msg'] = "비밀번호는 공백이 될 수 없습니다.";
     ?>
     <form name="login_fail_form" method="post" action="<? echo $config_path['LOGIN_FAIL']; ?>">
          <input type="hidden" name="alert_action" value="<? echo $alert['action']; ?>" />
          <input type="hidden" name="alert_title" value="<? echo $alert['title']; ?>" />
          <input type="hidden" name="alert_msg" value="<? echo $alert['msg']; ?>" />
     </form>
     <script>
          document.login_fail_form.submit();
     </script>
     <?
     }
     else  if($access_log == "INVALIDACCOUNT"){//case by invalid user password
          $alert['action'] = "open";
          $alert['title'] = "접속 제한";
          $alert['msg'] = "암호가 틀렸거나 존재하지 않는 계정입니다. 다시 확인해주세요.";
     ?>
     <form name="login_fail_form" method="post" action="<? echo $config_path['LOGIN_FAIL']; ?>">
          <input type="hidden" name="alert_action" value="<? echo $alert['action']; ?>" />
          <input type="hidden" name="alert_title" value="<? echo $alert['title']; ?>" />
          <input type="hidden" name="alert_msg" value="<? echo $alert['msg']; ?>" />
     </form>
     <script>
          document.login_fail_form.submit();
     </script>
     <?
     }
}
else{
     if($block_flag){
          $alert['action'] = "open";
          $alert['title'] = "접속 제한";
          $alert['msg'] = "접근을 허락하지 않습니다.";
     ?>
          <form name="login_deny_form" method="post" action="<? echo $config_path['LOGIN_FAIL']; ?>">
               <input type="hidden" name="alert_action" value="<? echo $alert['action']; ?>" />
               <input type="hidden" name="alert_title" value="<? echo $alert['title']; ?>" />
               <input type="hidden" name="alert_msg" value="<? echo $alert['msg']; ?>" />
          </form>
          <script>
               document.login_deny_form.submit();
          </script>
     <?     
     }
}