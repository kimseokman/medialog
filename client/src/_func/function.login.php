<?php
/**
 * login.php
 * 2015.07.31 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$config_path = array(
	"LOGIN_SUCCESS_SUPPORTER" => SUPPORTER_INDEX,
	"LOGIN_FAIL" => LOGIN_TRY
);
$login_try_limit = LOGIN_TRY_COUNT_MAX;
$contact_us_tel = CONTACT_US_TEL;

$alert = array(
	"action" => "close",
	"title" => "",
	"msg" => ""
);

$user_id = $_POST['user_id'];
$user_pw = $_POST['user_pw'];

//
//	Validation : Login Try Count( MAX = 5 )
//

$try_cnt = $main_obj->GetLoginTryCount($user_id);

if($try_cnt >= $login_try_limit){
?>
	<form name="login_deny_form" method="post" action="<? echo $config_path['LOGIN_FAIL']; ?>">
		<input type="hidden" name="reset_pw_guide_popup_action" value="open" />
	</form>
	<script>
		document.login_deny_form.submit();
	</script>
<?
}//end of : if($try_cnt > $login_try_limit)
else if($try_cnt == -1){
	$alert['action'] = "open";
	$alert['title'] = "로그인 실패";
	$alert['msg'] = "입력하신 아이디는 존재하지 않습니다. ";
?>
	<form name="login_fail_form" method="post" action="<? echo $config_path['LOGIN_FAIL']; ?>">
		<input type="hidden" name="alert_action" value="<? echo $alert['action']; ?>" />
		<input type="hidden" name="alert_title" value="<? echo $alert['title']; ?>" />
		<input type="hidden" name="alert_msg" value="<? echo $alert['msg']; ?>" />
	</form>
	<script>
		document.login_fail_form.submit();
	</script>
<?
}
else{
	//
	//	Logic : Login
	//
	$access_log = $main_obj->LoginSupporter($user_id, $user_pw);
	
	if($access_log == "SUPPORTER") {//case by login success supporter
		$main_obj->SetLoginTryCount($user_id, 0);
		$utils_obj->UpdateLastActivity();
	?>
		<form name="login_success_form" method="post" action="<? echo $config_path['LOGIN_SUCCESS_SUPPORTER']; ?>">
		</from>
		<script>
			document.login_success_form.submit();
		</script>
	<?
	}
	else {//case by login fail supporter
		$try_cnt = $try_cnt + 1;
		$main_obj->SetLoginTryCount($user_id, $try_cnt);

		if($access_log == "NOID"){//case by no user id
			$alert['action'] = "open";
			$alert['title'] = "아이디 오류";
			$alert['msg'] = "아이디는 공백이 될 수 없습니다.";
		?>
		<form name="login_fail_form" method="post" action="<? echo $config_path['LOGIN_FAIL']; ?>">
			<input type="hidden" name="alert_action" value="<? echo $alert['action']; ?>" />
			<input type="hidden" name="alert_title" value="<? echo $alert['title']; ?>" />
			<input type="hidden" name="alert_msg" value="<? echo $alert['msg']; ?>" />
		</form>
		<script>
			document.login_fail_form.submit();
		</script>
		<?	
		}
		else if($access_log == "NOPASSWORD"){//case by no user password
			$alert['action'] = "open";
			$alert['title'] = "비밀번호 오류";
			$alert['msg'] = "비밀번호는 공백이 될 수 없습니다.";
		?>
		<form name="login_fail_form" method="post" action="<? echo $config_path['LOGIN_FAIL']; ?>">
			<input type="hidden" name="alert_action" value="<? echo $alert['action']; ?>" />
			<input type="hidden" name="alert_title" value="<? echo $alert['title']; ?>" />
			<input type="hidden" name="alert_msg" value="<? echo $alert['msg']; ?>" />
		</form>
		<script>
			document.login_fail_form.submit();
		</script>
		<?	
		}
		else if($access_log == "SUPPORTER_EXPIRED"){//case by expired user
			$main_obj->SetLoginTryCount($user_id, 0);

			$alert['action'] = "open";
			$alert['title'] = "라이센스 만료";
			$alert['msg'] = "로그인 하신 ID가 정상사용이 불가합니다.<br /> 자세한 사항은 ".$contact_us_tel."로 문의하여 주세요.";
		?>
		<form name="login_fail_form" method="post" action="<? echo $config_path['LOGIN_FAIL']; ?>">
			<input type="hidden" name="alert_action" value="<? echo $alert['action']; ?>" />
			<input type="hidden" name="alert_title" value="<? echo $alert['title']; ?>" />
			<input type="hidden" name="alert_msg" value="<? echo $alert['msg']; ?>" />
		</form>
		<script>
			document.login_fail_form.submit();
		</script>
		<?	
		}
		else  if($access_log == "INVALIDPASSWORD" || $access_log == "INVALIDACCOUNT"){//case by invalid user password
			$alert['action'] = "open";
			$alert['title'] = "비밀번호 오류: ".$try_cnt."회";
			$alert['msg'] = "입력하신 아이디 또는 비밀번호가 맞지않습니다. ".$login_try_limit."회 연속 틀릴경우 보안을 위해 서비스가 제한됩니다.";
		?>
			<form name="login_fail_form" method="post" action="<? echo $config_path['LOGIN_FAIL']; ?>">
				<input type="hidden" name="alert_action" value="<? echo $alert['action']; ?>" />
				<input type="hidden" name="alert_title" value="<? echo $alert['title']; ?>" />
				<input type="hidden" name="alert_msg" value="<? echo $alert['msg']; ?>" />
			</form>
			<script>
				document.login_fail_form.submit();
			</script>
		<?
		}
	}
}//end of : if($try_cnt <= $login_try_limit)