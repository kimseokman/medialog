<?php
/**
 * checkout_admin.php
 * 2015.08.31 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$config_path = array(
	"LOGIN_SUCCESS_ADMIN" => ADMIN_INDEX,
	"LOGIN_FAIL" => SUPER_GNB_CUSTOMER_PAID_INDEX
);

$id = $_POST['adid'];
$pw = $_POST['checkout_pw'];
$page = $_POST['page'];
$search = $_POST['search'];
$searchVal = $_POST['searchVal'];

$contact = CONTACT_US_TEL;
$pop_action_open = 'open';
$pop_action_close = 'close';

$logged = $main_obj->CheckoutAdmin($id, $pw);

$config_msg = array(
	"ADMIN_EXPIRED" => "사용기간이 만료된 아이디 입니다. 고객센터(". $contact .")로 연락 주시기 바랍니다.",
	"NOID" => "ID를 입력하지 않았습니다. 다시 로그인해주세요.",
	"NOPASSWORD" => "Password를 입력하지 않았습니다. 다시 로그인해주세요.",
	"ETC_ERROR" => "아이디 또는 비밀번호가 정확하지 않습니다. 다시 확인해 주시기 바랍니다."
);

if($logged == "ADMIN") {
?>
	<form name="checkout_form" method="post" action="<? echo $config_path['LOGIN_SUCCESS_ADMIN']; ?>">
	</from>
	<script>
		document.checkout_form.submit();
	</script>
<?
}
else if($logged == "ADMIN_EXPIRED") {
?>
<form name="checkout_form" method="post" action="<? echo $config_path['LOGIN_FAIL']; ?>">
	<input type="hidden" name="checkout_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="checkout_res_popup_msg" value="<? echo $config_msg['ADMIN_EXPIRED']; ?>" />
	<input type="hidden" name="remem_page" value="<? echo $page; ?>" />
	<input type="hidden" name="remem_search" value="<? echo $search; ?>" />
	<input type="hidden" name="remem_searchVal" value="<? echo $searchVal; ?>" />
</from>
<script>
	document.checkout_form.submit();
</script>
<?
}
else if($logged == "NOID") {
?>
<form name="checkout_form" method="post" action="<? echo $config_path['LOGIN_FAIL']; ?>">
	<input type="hidden" name="checkout_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="checkout_res_popup_msg" value="<? echo $config_msg['NOID']; ?>" />
	<input type="hidden" name="remem_page" value="<? echo $page; ?>" />
	<input type="hidden" name="remem_search" value="<? echo $search; ?>" />
	<input type="hidden" name="remem_searchVal" value="<? echo $searchVal; ?>" />
</from>
<script>
	document.checkout_form.submit();
</script>
<?
}
else if($logged == "NOPASSWORD") {
?>
<form name="checkout_form" method="post" action="<? echo $config_path['LOGIN_FAIL']; ?>">
	<input type="hidden" name="checkout_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="checkout_res_popup_msg" value="<? echo $config_msg['NOPASSWORD']; ?>" />
	<input type="hidden" name="remem_page" value="<? echo $page; ?>" />
	<input type="hidden" name="remem_search" value="<? echo $search; ?>" />
	<input type="hidden" name="remem_searchVal" value="<? echo $searchVal; ?>" />
</from>
<script>
	document.checkout_form.submit();
</script>
<?
}
else {
?>
<form name="checkout_form" method="post" action="<? echo $config_path['LOGIN_FAIL']; ?>">
	<input type="hidden" name="checkout_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="checkout_res_popup_msg" value="<? echo $config_msg['ETC_ERROR']; ?>" />
	<input type="hidden" name="remem_page" value="<? echo $page; ?>" />
	<input type="hidden" name="remem_search" value="<? echo $search; ?>" />
	<input type="hidden" name="remem_searchVal" value="<? echo $searchVal; ?>" />
</from>
<script>
	document.checkout_form.submit();
</script>
<?
}