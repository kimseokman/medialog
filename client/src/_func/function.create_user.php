<?php
/**
 * create_user.php
 * 2015.08.05 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$spt_id = $_POST['user_id'];
$spt_pw = $_POST['user_pw'];
$spt_pw2 = $_POST['user_pw2'];
$spt_name = $_POST['user_name'];
$spt_deptnum = $_POST['dept_num'];
$spt_tel = $_POST['user_tel'];
$spt_mail = $_POST['user_email'];
$spt_permit = $_POST['permit'];

$page = $_POST['page'];
$search = $_POST['search'];
$searchVal = $_POST['searchVal'];

$name_max_len = NAME_MAX_LENGTH;
$name_max_len_kr = NAME_MAX_LENGTH_KR;
$name_max_len_en = NAME_MAX_LENGTH_EN;
$phone_max_len = PHONE_MAX_LENGTH;
$mail_max_len = MAIL_ADDR_MAX_LENGTH;
$pw_length = PASSWORD_LENGTH;
$pw_length_min = PASSWORD_LENGTH_MIN;
$pw_length_max = PASSWORD_LENGTH_MAX;
$pw_safe_cnt = PASSWORD_SAFE_CNT;

$pop_action_open = 'open';
$pop_action_close = 'close';

$config_path = array(
	"CREATE_SUCCESS" => "/_func/function.send_create_user_mail.php",
	"CREATE_FAIL" => MANAGE_AGENT_USER_INDEX
);

$config_msg = array(
	"NO_AUTH" => "권한이 없습니다.",
	"EMPTY_ID"=> "ID를 입력해 주세요.",
	"OVER_FLOW" => "상담원 수를 초과하였습니다.",
	"DUPICATED"=> "이미 존재하는 ID입니다. 다른 ID를 사용해주세요.",
	"EMPTY_NAME"=> "이름을 입력해주세요.",
	"EMPTY_MAIL"=> "E-Mail을 입력해 주세요.<br>(상담원이 실제로 사용중인 이메일을 입력하세요)",
	"LONG_NAME"=> "이름은 한글 ".$name_max_len_kr."자, 영문 ".$name_max_len_en."자 이내로 입력해주세요.",
	"EMPTY_PASSWORD"=> "비밀번호를 입력 하세요.",
	"PASSWORD_NOT_MATCH"=> "비밀번호가 일치하지 않습니다.",
	"LONG_PHONE"=> "전화번호는 ".$phone_max_len."자 이내로 입력해주세요.",
	"LONG_EMAIL"=> "이메일 주소 길이는 ".$mail_max_len."글자 이내여야 합니다.",
	"CREATE_DEPARTMENT"=> "부서를 생성해주세요.",
	"EMPTY_DEPARTMENT"=> "부서를 선택해주세요.",
	"PW_CONFIG" => "비밀번호는<br /> 영문 대/소문자,특수문자,숫자 중 ".$pw_safe_cnt."가지 이상, <br />".$pw_length_min."자~".$pw_length_max."자로 설정해 주세요.",
	"OK"=>"저장이 완료되었습니다."
);

$isAdmin = $main_obj->ConfirmAdmin();

if(!$isAdmin){
?>
<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="user_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['NO_AUTH']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="input_spt_id" value="<? echo $spt_id; ?>" />
	<input type="hidden" name="input_spt_pw" value="<? echo $spt_pw; ?>" />
	<input type="hidden" name="input_spt_pw2" value="<? echo $spt_pw2; ?>" />
	<input type="hidden" name="input_spt_name" value="<? echo $spt_name; ?>" />
	<input type="hidden" name="input_spt_deptnum" value="<? echo $spt_deptnum; ?>" />
	<input type="hidden" name="input_spt_tel" value="<? echo $spt_tel; ?>" />
	<input type="hidden" name="input_spt_mail" value="<? echo $spt_mail; ?>" />
	<input type="hidden" name="input_spt_permit" value="<? echo $spt_permit; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.createUserResForm.submit();
</script>
<?
}
else{
	$admin = $main_obj->GetAdminInfo();
	$spt_company = $admin->company;
}

//validate password
$pw_arr = str_split($spt_pw);
$pw_size = count($pw_arr);

$safe_cnt = 0;
$decimal_cnt  = 0;
$eng_cnt = 0;
$special_cnt = 0;
$flag_config_error = FALSE;
for($i=0; $i < $pw_size; $i++){
	$acii_num = ord ($pw_arr[$i]);

	if( (48 <= $acii_num) && ($acii_num <= 57) ){//숫자
		$decimal_cnt  = 1;
	}
	else if( (65 <= $acii_num) && ($acii_num <= 90) ){//영문자 : 대문자
		$eng_cnt = 1;
	}
	else if( (97 <= $acii_num) && ($acii_num <= 122) ){//영문자 : 소문자
		$eng_cnt = 1;
	}
	else if( (33 <= $acii_num) && ($acii_num <= 47) ){//특수문자1
		$special_cnt = 1;
	}
	else if( (58 <= $acii_num) && ($acii_num <= 64) ){//특수문자2
		$special_cnt = 1;
	}
	else if( (91 <= $acii_num) && ($acii_num <= 96) ){//특수문자3
		$special_cnt = 1;
	}
	else if( (123 <= $acii_num) && ($acii_num <= 126) ){//특수문자4
		$special_cnt = 1;
	}
	else{
		$flag_config_error = TRUE;
	}
}
$safe_cnt = $decimal_cnt + $eng_cnt + $special_cnt;
//end validate password

//validate id
$id_arr = str_split($spt_id);
$id_size = count($id_arr);
$flag_config_error = FALSE;
for($i=0; $i < $id_size; $i++){
	$acii_num = ord ($id_arr[$i]);

	if( !((48 <= $acii_num) && ($acii_num <= 57)) && !((65 <= $acii_num) && ($acii_num <= 90)) && !((97 <= $acii_num) && ($acii_num <= 122))){//숫자, 영문자(대,소문자) 아니면
		$flag_config_error = TRUE;
	}
}
//end validate id


if(strlen($spt_id) == 0){//있을리 없는 id가 비어있는 경우
?>
<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="user_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['EMPTY_ID']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="input_spt_pw" value="<? echo $spt_pw; ?>" />
	<input type="hidden" name="input_spt_pw2" value="<? echo $spt_pw2; ?>" />
	<input type="hidden" name="input_spt_name" value="<? echo $spt_name; ?>" />
	<input type="hidden" name="input_spt_deptnum" value="<? echo $spt_deptnum; ?>" />
	<input type="hidden" name="input_spt_tel" value="<? echo $spt_tel; ?>" />
	<input type="hidden" name="input_spt_mail" value="<? echo $spt_mail; ?>" />
	<input type="hidden" name="input_spt_permit" value="<? echo $spt_permit; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.createUserResForm.submit();
</script>
<?
}
else if(strlen($spt_name) == 0) {// 이름을 입력하지 않았을 경우 에러
?>
<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="user_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['EMPTY_NAME']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="input_spt_id" value="<? echo $spt_id; ?>" />
	<input type="hidden" name="input_spt_pw" value="<? echo $spt_pw; ?>" />
	<input type="hidden" name="input_spt_pw2" value="<? echo $spt_pw2; ?>" />
	<input type="hidden" name="input_spt_deptnum" value="<? echo $spt_deptnum; ?>" />
	<input type="hidden" name="input_spt_tel" value="<? echo $spt_tel; ?>" />
	<input type="hidden" name="input_spt_mail" value="<? echo $spt_mail; ?>" />
	<input type="hidden" name="input_spt_permit" value="<? echo $spt_permit; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.createUserResForm.submit();
</script>
<?
}
else if(strlen($spt_name) > $name_max_len) {// 이름의 길이가 30자가 넘을 경우 에러
?>
<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="user_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['LONG_NAME']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="input_spt_id" value="<? echo $spt_id; ?>" />
	<input type="hidden" name="input_spt_pw" value="<? echo $spt_pw; ?>" />
	<input type="hidden" name="input_spt_pw2" value="<? echo $spt_pw2; ?>" />
	<input type="hidden" name="input_spt_deptnum" value="<? echo $spt_deptnum; ?>" />
	<input type="hidden" name="input_spt_tel" value="<? echo $spt_tel; ?>" />
	<input type="hidden" name="input_spt_mail" value="<? echo $spt_mail; ?>" />
	<input type="hidden" name="input_spt_permit" value="<? echo $spt_permit; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.createUserResForm.submit();
</script>
<?
}
else if(strlen($spt_pw) == 0) {// 패스워드를 입력하지 않았을 경우 에러
?>
<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="user_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['EMPTY_PASSWORD']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="input_spt_id" value="<? echo $spt_id; ?>" />
	<input type="hidden" name="input_spt_pw2" value="<? echo $spt_pw2; ?>" />
	<input type="hidden" name="input_spt_name" value="<? echo $spt_name; ?>" />
	<input type="hidden" name="input_spt_deptnum" value="<? echo $spt_deptnum; ?>" />
	<input type="hidden" name="input_spt_tel" value="<? echo $spt_tel; ?>" />
	<input type="hidden" name="input_spt_mail" value="<? echo $spt_mail; ?>" />
	<input type="hidden" name="input_spt_permit" value="<? echo $spt_permit; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.createUserResForm.submit();
</script>
<?
}
else if($flag_config_error == TRUE) {// 패스워드 입력 범위에서 벗어났을 경우 ( 특수문자,영문자,숫자 )
?>
<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="user_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['PW_CONFIG']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="input_spt_id" value="<? echo $spt_id; ?>" />
	<input type="hidden" name="input_spt_pw2" value="<? echo $spt_pw2; ?>" />
	<input type="hidden" name="input_spt_name" value="<? echo $spt_name; ?>" />
	<input type="hidden" name="input_spt_deptnum" value="<? echo $spt_deptnum; ?>" />
	<input type="hidden" name="input_spt_tel" value="<? echo $spt_tel; ?>" />
	<input type="hidden" name="input_spt_mail" value="<? echo $spt_mail; ?>" />
	<input type="hidden" name="input_spt_permit" value="<? echo $spt_permit; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.createUserResForm.submit();
</script>
<?
}
else if($safe_cnt < $pw_safe_cnt){//유효 카운트가 2개보다 적을 때
?>
<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="user_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['PW_CONFIG']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="input_spt_id" value="<? echo $spt_id; ?>" />
	<input type="hidden" name="input_spt_pw2" value="<? echo $spt_pw2; ?>" />
	<input type="hidden" name="input_spt_name" value="<? echo $spt_name; ?>" />
	<input type="hidden" name="input_spt_deptnum" value="<? echo $spt_deptnum; ?>" />
	<input type="hidden" name="input_spt_tel" value="<? echo $spt_tel; ?>" />
	<input type="hidden" name="input_spt_mail" value="<? echo $spt_mail; ?>" />
	<input type="hidden" name="input_spt_permit" value="<? echo $spt_permit; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.createUserResForm.submit();
</script>
<?
}
else if( ($pw_size < $pw_length_min) || ($pw_size > $pw_length_max) ){//password 길이가 10자리 보다 작을때
?>
<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="user_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['PW_CONFIG']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="input_spt_id" value="<? echo $spt_id; ?>" />
	<input type="hidden" name="input_spt_pw2" value="<? echo $spt_pw2; ?>" />
	<input type="hidden" name="input_spt_name" value="<? echo $spt_name; ?>" />
	<input type="hidden" name="input_spt_deptnum" value="<? echo $spt_deptnum; ?>" />
	<input type="hidden" name="input_spt_tel" value="<? echo $spt_tel; ?>" />
	<input type="hidden" name="input_spt_mail" value="<? echo $spt_mail; ?>" />
	<input type="hidden" name="input_spt_permit" value="<? echo $spt_permit; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.createUserResForm.submit();
</script>
<?
}
else if($spt_pw != $spt_pw2) {// 패스워드가 일치하지 않을 경우 에러
?>
<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="user_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['PASSWORD_NOT_MATCH']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="input_spt_id" value="<? echo $spt_id; ?>" />
	<input type="hidden" name="input_spt_pw" value="<? echo $spt_pw; ?>" />
	<input type="hidden" name="input_spt_name" value="<? echo $spt_name; ?>" />
	<input type="hidden" name="input_spt_deptnum" value="<? echo $spt_deptnum; ?>" />
	<input type="hidden" name="input_spt_tel" value="<? echo $spt_tel; ?>" />
	<input type="hidden" name="input_spt_mail" value="<? echo $spt_mail; ?>" />
	<input type="hidden" name="input_spt_permit" value="<? echo $spt_permit; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.createUserResForm.submit();
</script>
<?
}
else if(strlen($spt_tel) > $phone_max_len) {// 전화번호가 20자를 넘을 경우 에러
?>
<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="user_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['LONG_PHONE']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="input_spt_id" value="<? echo $spt_id; ?>" />
	<input type="hidden" name="input_spt_pw" value="<? echo $spt_pw; ?>" />
	<input type="hidden" name="input_spt_pw2" value="<? echo $spt_pw2; ?>" />
	<input type="hidden" name="input_spt_name" value="<? echo $spt_name; ?>" />
	<input type="hidden" name="input_spt_deptnum" value="<? echo $spt_deptnum; ?>" />
	<input type="hidden" name="input_spt_mail" value="<? echo $spt_mail; ?>" />
	<input type="hidden" name="input_spt_permit" value="<? echo $spt_permit; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.createUserResForm.submit();
</script>
<?
}
else if(strlen($spt_mail) == 0){
?>
<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="user_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['EMPTY_MAIL']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="input_spt_id" value="<? echo $spt_id; ?>" />
	<input type="hidden" name="input_spt_pw" value="<? echo $spt_pw; ?>" />
	<input type="hidden" name="input_spt_pw2" value="<? echo $spt_pw2; ?>" />
	<input type="hidden" name="input_spt_name" value="<? echo $spt_name; ?>" />
	<input type="hidden" name="input_spt_deptnum" value="<? echo $spt_deptnum; ?>" />
	<input type="hidden" name="input_spt_tel" value="<? echo $spt_tel; ?>" />
	<input type="hidden" name="input_spt_permit" value="<? echo $spt_permit; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.createUserResForm.submit();
</script>
<?
}
else if(strlen($spt_mail) > $mail_max_len) {// 이메일 주소가 50자를 넘을 경우 에러
?>
<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="user_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['LONG_EMAIL']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="input_spt_id" value="<? echo $spt_id; ?>" />
	<input type="hidden" name="input_spt_pw" value="<? echo $spt_pw; ?>" />
	<input type="hidden" name="input_spt_pw2" value="<? echo $spt_pw2; ?>" />
	<input type="hidden" name="input_spt_name" value="<? echo $spt_name; ?>" />
	<input type="hidden" name="input_spt_deptnum" value="<? echo $spt_deptnum; ?>" />
	<input type="hidden" name="input_spt_tel" value="<? echo $spt_tel; ?>" />
	<input type="hidden" name="input_spt_permit" value="<? echo $spt_permit; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.createUserResForm.submit();
</script>
<?
}
else if($spt_deptnum == "0" || $spt_deptnum == "") {// 부서를 선택하지 않았을 경우
	$isDeptList = $main_obj->GetDepList();
	if(count($isDeptList) == 0){
		$show_msg = $config_msg['CREATE_DEPARTMENT'];
	}
	else{
		$show_msg = $config_msg['EMPTY_DEPARTMENT'];
	}
?>
<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="user_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_add_res_popup_msg" value="<? echo $show_msg; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="input_spt_id" value="<? echo $spt_id; ?>" />
	<input type="hidden" name="input_spt_pw" value="<? echo $spt_pw; ?>" />
	<input type="hidden" name="input_spt_pw2" value="<? echo $spt_pw2; ?>" />
	<input type="hidden" name="input_spt_name" value="<? echo $spt_name; ?>" />
	<input type="hidden" name="input_spt_tel" value="<? echo $spt_tel; ?>" />
	<input type="hidden" name="input_spt_mail" value="<? echo $spt_mail; ?>" />
	<input type="hidden" name="input_spt_permit" value="<? echo $spt_permit; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.createUserResForm.submit();
</script>
<?
}
else{
	$spt_option = DEFAULT_PERMISSION_PC_SETTING;
	$spt_mobile_setting = DEFAULT_PERMISSION_MOBILE_FUNCTION;
	$spt_mobile_expired_date = DEFAULT_MOBILE_EXPIRED_DATE;

	$pc_spt_cnt = $main_obj->GetSupporterCnt('PC');						//only pc 상담원 수
	$pc_n_mobile_spt_cnt = $main_obj->GetSupporterCnt('PC_N_MOBILE');				//mobile(pc + mobile) 상담원 수
	$pc_n_vedio_spt_cnt = $main_obj->GetSupporterCnt('PC_N_VIDEO');				//video(pc + video) 상담원 수
	$pc_n_mobile_n_vedio_spt_cnt = $main_obj->GetSupporterCnt('PC_N_MOBILE_N_VIDEO');	//all(pc + mobile&video) 상담원 수

	$spt_volume = $admin->volume;
	$mobile_volume = $admin->mobile_volume;
	$pc_volume = $spt_volume - $mobile_volume;

	$overflow_flag = FALSE;

	if($spt_volume <= $pc_spt_cnt){
		$overflow_flag = TRUE;	
	}

	if($spt_permit == "p_pc"){
		$spt_function = DEFAULT_PERMISSION_PC_FUNCTION;
	}
	else if($spt_permit == "p_pcm"){
		$spt_function = DEFAULT_PERMISSION_PC_FUNCTION + PERMISSION_MOBILE_PAID;
		if($pc_n_mobile_spt_cnt >= $mobile_volume){
			$overflow_flag = TRUE;
		}
	}
	else if($spt_permit == "p_pcv"){
		$spt_function = DEFAULT_PERMISSION_PC_FUNCTION + PERMISSION_VIDEO_PAID;
		if($pc_n_vedio_spt_cnt >= $mobile_volume){
			$overflow_flag = TRUE;
		}
	}
	else if($spt_permit == "p_pcmv"){
		$spt_function = DEFAULT_PERMISSION_PC_FUNCTION + PERMISSION_MOBILE_VIDEO_PAID;
		if($pc_n_mobile_n_vedio_spt_cnt >= $mobile_volume){
			$overflow_flag = TRUE;
		}
	}

	if($overflow_flag == TRUE){
	?>
	<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
		<input type="hidden" name="user_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
		<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['OVER_FLOW']; ?>" />
		<input type="hidden" name="input_res" value="FAIL" />
		<input type="hidden" name="input_spt_id" value="<? echo $spt_id; ?>" />
		<input type="hidden" name="input_spt_pw" value="<? echo $spt_pw; ?>" />
		<input type="hidden" name="input_spt_pw2" value="<? echo $spt_pw2; ?>" />
		<input type="hidden" name="input_spt_name" value="<? echo $spt_name; ?>" />
		<input type="hidden" name="input_spt_tel" value="<? echo $spt_tel; ?>" />
		<input type="hidden" name="input_spt_mail" value="<? echo $spt_mail; ?>" />
		<input type="hidden" name="input_spt_permit" value="<? echo $spt_permit; ?>" />
		<input type="hidden" name="page" value="<? echo $page; ?>" />
		<input type="hidden" name="search" value="<? echo $search; ?>" />
		<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
	</form>
	<script>
		document.createUserResForm.submit();
	</script>
	<?
	}
	else{

		$sptdata = array(
			"depnum"=> $spt_deptnum, 
			"sptid"=> $spt_id, 
			"password"=> $spt_pw, 		
			"name"=> $spt_name, 
			"company"=> $spt_company,
			"phone"=> $spt_tel,
			"email"=> $spt_mail, 
			"fr"=>$spt_function, 
			"opt"=>$spt_option , 
			"mobile_expired_date"=>$spt_mobile_expired_date
		);

		$res = $main_obj->CreateSupporter($sptdata);

		if($res == "SUPPORTERALREADYEXISTS" || $res == "ADMINALREADYEXISTS") {// 이미 존재하는 ID인 경우
		?>
			<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
				<input type="hidden" name="user_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
				<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['DUPICATED']; ?>" />
				<input type="hidden" name="input_res" value="FAIL" />
				<input type="hidden" name="input_spt_pw" value="<? echo $spt_pw; ?>" />
				<input type="hidden" name="input_spt_pw2" value="<? echo $spt_pw2; ?>" />
				<input type="hidden" name="input_spt_name" value="<? echo $spt_name; ?>" />
				<input type="hidden" name="input_spt_deptnum" value="<? echo $spt_deptnum; ?>" />
				<input type="hidden" name="input_spt_tel" value="<? echo $spt_tel; ?>" />
				<input type="hidden" name="input_spt_mail" value="<? echo $spt_mail; ?>" />
				<input type="hidden" name="input_spt_permit" value="<? echo $spt_permit; ?>" />
				<input type="hidden" name="page" value="<? echo $page; ?>" />
				<input type="hidden" name="search" value="<? echo $search; ?>" />
				<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
			</form>
			<script>
				document.createUserResForm.submit();
			</script>
		<?
		}
		else {		

		?>
			<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_SUCCESS']; ?>">
				<input type="hidden" name="user_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
				<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['OK']; ?>" />
				<input type="hidden" name="input_spt_id" value="<? echo $spt_id; ?>" />
				<input type="hidden" name="input_spt_pw" value="<? echo $spt_pw; ?>" />
				<input type="hidden" name="input_spt_mail" value="<? echo $spt_mail; ?>" />
				<input type="hidden" name="input_res" value="SUCCESS" />
			</form>
			<script>
				document.createUserResForm.submit();
			</script>
		<?
		}
	}
}
