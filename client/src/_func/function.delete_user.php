<?php
/**
 * delete_user.php
 * 2015.08.05 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$sptnum = $_POST['sptnum'];
$page = $_POST['page'];
$search = $_POST['search'];
$searchVal = $_POST['searchVal'];

$config_path = array(
	"DEL_FAIL"=> MANAGE_AGENT_USER."/info.php",
	"DEL_SUCCESS"=> MANAGE_AGENT_USER_INDEX
);

$config_msg = array(
	"NO_AUTH" => "권한이 없습니다.",
	"NO_SUPPORTER"=> "해당 상담원이 존재하지 않습니다.",
	"DEL_OK" => "삭제가 완료되었습니다."
);

$pop_action_open = "open";
$pop_action_close = "close";

$isAdmin = $main_obj->ConfirmAdmin();
if(!$isAdmin){
?>
<form name="delUserResForm" method="post" action="<? echo $config_path['DEL_FAIL']; ?>">
	<input type="hidden" name="user_del_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_del_res_popup_msg" value="<? echo $config_msg['NO_AUTH']; ?>" />
	<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.delUserResForm.submit();
</script>
<?
}

if($main_obj->GetSupporterBySptnum($sptnum) == NULL) {
?>
<form name="delUserResForm" method="post" action="<? echo $config_path['DEL_FAIL']; ?>">
	<input type="hidden" name="user_del_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_del_res_popup_msg" value="<? echo $config_msg['NO_SUPPORTER']; ?>" />
	<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.delUserResForm.submit();
</script>
<?
}
else{
	$res = $main_obj->DeleteSupporter($sptnum);

	if($res == "NOSUPPORTER") {
	?>
	<form name="delUserResForm" method="post" action="<? echo $config_path['DEL_FAIL']; ?>">
		<input type="hidden" name="user_del_res_popup_action" value="<? echo $pop_action_open; ?>" />
		<input type="hidden" name="user_del_res_popup_msg" value="<? echo $config_msg['NO_SUPPORTER']; ?>" />
		<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
		<input type="hidden" name="page" value="<? echo $page; ?>" />
		<input type="hidden" name="search" value="<? echo $search; ?>" />
		<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
	</form>
	<script>
		document.delUserResForm.submit();
	</script>
	<?
	}//end of : if($res == "NOSUPPORTER")
	else{
	?>
	<form name="delUserResForm" method="post" action="<? echo $config_path['DEL_SUCCESS']; ?>">
		<input type="hidden" name="user_del_res_popup_action" value="<? echo $pop_action_open; ?>" />
		<input type="hidden" name="user_del_res_popup_msg" value="<? echo $config_msg['DEL_OK']; ?>" />
	</form>
	<script>
		document.delUserResForm.submit();
	</script>
	<?
	}
}

