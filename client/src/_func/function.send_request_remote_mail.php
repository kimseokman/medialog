<?php
/**
 * send_request_remote_mail.php
 * 2015.09.07 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
require($_SERVER['DOCUMENT_ROOT']."/_lib/_inc/phpmailer/class.phpmailer.php");

$config_path = array(
	"SEND_SUCCESS" => "/_app_cowork/send/mail.php",
	"SEND_FAIL" => "/_app_cowork/send/mail.php"
);

$config_msg = array(
	"EMPTY_MAIL" => "수신자 E-MAIL 주소를 입력해주세요.",
	"EMPTY_CODE" => "접속 코드가 존재하지 않습니다.",
	"OK" => "발송 완료"
);

$sptid = $_POST['sptid'];
$recevice_mail = $_POST['receive_mail'];
$access_code = $_POST['access_code'];
$access_url = $_POST['access_url'];
$public_access_url = $_POST['public_access_url'];

if(strlen($recevice_mail) == 0){
	$input_res = 'FAIL';
?>
<form name="requestRemoteSendMailForm" method="post" action="<? echo $config_path['SEND_FAIL']; ?>">
	<input type="hidden" name="input_res" value="<? echo $input_res; ?>" />
	<input type="hidden" name="guide_msg" value="<? echo $config_msg['EMPTY_MAIL']; ?>" />
	<input type="hidden" name="accessCode" value="<? echo $access_code; ?>" />
</form>
<script>
	document.requestRemoteSendMailForm.submit();
</script>
<?
}

if(strlen($access_code) == 0){
	$input_res = 'FAIL';
?>
<form name="requestRemoteSendMailForm" method="post" action="<? echo $config_path['SEND_FAIL']; ?>">
	<input type="hidden" name="input_res" value="<? echo $input_res; ?>" />
	<input type="hidden" name="guide_msg" value="<? echo $config_msg['EMPTY_CODE']; ?>" />
</form>
<script>
	document.requestRemoteSendMailForm.submit();
</script>
<?
}

//
//	sender
//
$from = ML_MAIL_ADDR;
$from_name = ML_MAIL_NAME;
//
//	recipient
//
$to = $recevice_mail;
$to_name = 'GUEST';

//
//	mail contents data
//
$home_url = REMOTE_HOME;
$img_url = ML_URL.IMG_DIR;
$contact_us = CONTACT_US_TEL;
$show_acc_code = $access_code;
$remote_url = $access_url;
$remote_url2 = $public_access_url;

$remote_url_css = "display:none;";
if(($admin->domain != null) && ($admin->domain != "")){
	$remote_url_css = "display:block;";
}

//
//	mail setting
//
$mail = new PHPMailer(); 
$mail->IsSMTP(); // send via SMTP
$mail->Host = ML_MAIL_HOST;
$mail->Port = ML_MAIL_PORT;
$mail->Mailer = ML_MAILER;
$mail->CharSet = ML_MAIL_CHARSET;
$mail->SMTPAuth = true; // turn on SMTP authentication
$mail->Username = ML_MAIL_ID; // SMTP username
$mail->Password = ML_MAIL_PW;// SMTP password

//
//	mail body
//
$mail->Subject = "[ ".SERVICE_NAME." ] 원격지원 상담 접속을 위한 안내 메일입니다.";
$message = file_get_contents($_SERVER['DOCUMENT_ROOT'].MAIL_BODY_REQUEST_REMOTE);
$message = str_replace('<? echo $home_url; ?>', $home_url, $message); 
$message = str_replace('<? echo $img_url; ?>', $img_url, $message);
$message = str_replace('<? echo $contact_us; ?>', $contact_us, $message);
$message = str_replace('<? echo $show_acc_code; ?>', $show_acc_code, $message);
$message = str_replace('<? echo $remote_url; ?>', $remote_url, $message);
$message = str_replace('<? echo $remote_url2; ?>', $remote_url2, $message);
$message = str_replace('<? echo $remote_url_css; ?>', $remote_url_css, $message);
$mail->MsgHTML($message);

//
//	mail ready
//
$mail->From = $from;
$mail->FromName = $from_name;
$mail->AddAddress($to, $to_name);
$mail->AddReplyTo($from, $from_name);
$mail->WordWrap = 50; // set word wrap
$mail->IsHTML(TRUE); // send as HTML

//
//	send mail
//
$mail_res = $mail->Send();

if(!$mail_res){//fail
	$input_res = 'FAIL';
?>
<form name="requestRemoteSendMailForm" method="post" action="<? echo $config_path['SEND_FAIL']; ?>">
	<input type="hidden" name="input_res" value="<? echo $input_res; ?>" />
</form>
<script>
	document.requestRemoteSendMailForm.submit();
</script>
<?
}
else{
	$input_res = 'SUCCESS';
	$pop_act = 'open';
?>
<form name="requestRemoteSendMailForm" method="post" action="<? echo $config_path['SEND_SUCCESS']; ?>">
	<input type="hidden" name="input_res" value="<? echo $input_res; ?>" />
	<input type="hidden" name="send_mail_pop_act" value="<? echo $pop_act; ?>" />
	<input type="hidden" name="send_mail_pop_msg" value="<? echo $config_msg['OK']; ?>" />
	<input type="hidden" name="accessCode" value="<? echo $access_code; ?>" />
	<input type="hidden" name="userid" value="<? echo $sptid; ?>" />
</form>
<script>
	document.requestRemoteSendMailForm.submit();
</script>
<?
}