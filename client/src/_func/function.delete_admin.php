<?php
/**
 * delete_admin.php
 * 2015.09.01 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$admin_act_type = $_POST['admin_act_type'];

$admin_id = $_POST['adid'];

$page = $_POST['page'];
$search = $_POST['search'];
$searchVal = $_POST['searchVal'];

$pop_action_open = 'open';
$pop_action_close = 'close';

if($admin_act_type == ADMIN_ACT_TYPE_FREE){
	$return_url = SUPER_GNB_CUSTOMER_FREE;
}
else if($admin_act_type == ADMIN_ACT_TYPE_PAID){
	$return_url = SUPER_GNB_CUSTOMER_PAID;
}

$config_path = array(
	"DEL_SUCCESS" => $return_url . "/list.php",
	"DEL_FAIL" => $return_url . "/info.php"
);

$config_msg = array(
	"NO_ADMIN" => "해당 관리자가 존재하지 않습니다. 다시 시도해주세요.",
	"DEL_OK" => "삭제가 완료되었습니다."
);

$res = $main_obj->DeleteAdminByAdid($admin_id);

if($res == "NOADMIN"){
?>
<form name="delAdminResForm" method="post" action="<? echo $config_path['DEL_FAIL']; ?>">
	<input type="hidden" name="adid" value="<? echo $admin_id; ?>" />

	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.delAdminResForm.submit();
</script>
<?
}
else{
?>
<form name="delAdminResForm" method="post" action="<? echo $config_path['DEL_SUCCESS']; ?>">
	<input type="hidden" name="admin_del_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_del_res_popup_msg" value="<? echo $config_msg['DEL_OK']; ?>" />
</form>
<script>
	document.delAdminResForm.submit();
</script>
<?
}
