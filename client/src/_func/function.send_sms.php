<?php
/**
 * send_sms.php
 * 2015.11.13 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$config_path = array(
	"SEND_SUCCESS" => "/_app_cowork/send/sms.php",
	"SEND_FAIL" => "/_app_cowork/send/sms.php"
);

$config_msg = array(
	"FAIL" => "접속 코드 전송이 실패하였습니다.<br /> 잠시후 이용해 주세요.",
	"OK" => "발송 완료"
);

$access_url = $_POST['access_url'];
$access_code = $_POST['access_code'];
$receive_num = $_POST['receive_num'];
$send_num = $_POST['send_num'];
$userid = $_POST['userid'];
$usernum = $_POST['usernum'];

/*
function rand_str()	{
	$count_n = 6;
	srand ((float) microtime() * 10000000);
	$input = array ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
	$input2 = array_rand( array_flip( $input ) , $count_n );
	$tnum = implode( "" , $input2 );
	return $tnum;
}

$today = date("s",mktime());
$SMS_STR = $today.rand_str();
*/

$dbconn = mysql_connect("61.42.12.47","anysms","iuone6707^^") || die("데이터베이스 연결에 실패하였습니다."); // 애니헬프 서버
$dbstatus = mysql_select_db("anysms");

//db mapping
$SMS_STR = $access_code;
$phone = $receive_num;
$to_message = "[LG U+] 원격지원 접속을 위한 안내입니다. URL [".$access_url."] 접속인증번호 [".$access_code."]";
$neturo="";
$userip = $_SERVER["REMOTE_ADDR"];
$reff = $_SERVER["HTTP_REFERER"];
$rec_phone = "00000000";

$sql = "INSERT INTO SC_TRAN ( TR_SENDDATE,TR_SERIALNUM,TR_PHONE,TR_MSG,TR_ETC1,TR_ETC2,TR_ETC3,TR_ETC4,TR_ETC5,TR_ETC6, TR_CALLBACK ) "
          ."VALUES(SYSDATE(),'$SMS_STR','$phone','$to_message','$neturo','$SMS_STR','$userid','$usernum','$reff','$userip', '$rec_phone')";

$pop_act = 'open';
$result = mysql_query($sql);

if(!$result) {
	//require_once("../../php/conn_sms_hosting.php");	//164.124.107.28 (장애sms발송서버로 임시발송)

	$query = "INSERT INTO em_tran(tran_pr, tran_phone, tran_callback, tran_status, tran_date, tran_msg, tran_etc1, tran_etc2, tran_etc3) VALUES(null, '$phone', '$rec_phone', 1, sysdate(), '$to_message', 'help', 'neturo', '$SMS_STR')";
	$result = mysql_query($query);
	
	if(!$result){
?>
		<form name="sendSMSResForm" method="post" action="<? echo $config_path['SEND_FAIL']; ?>">
			<input type="hidden" name="send_sms_pop_act" value="<? echo $pop_act; ?>" />
			<input type="hidden" name="send_sms_pop_msg" value="<? echo $config_msg['FAIL']; ?>" />
			<input type="hidden" name="accessCode" value="<? echo $access_code; ?>" />
			<input type="hidden" name="userid" value="<? echo $userid; ?>" />
		</form>
		<script>
			document.sendSMSResForm.submit();
		</script>
<?
	}
	else {
?>
		<form name="sendSMSResForm" method="post" action="<? echo $config_path['SEND_SUCCESS']; ?>">
			<input type="hidden" name="send_sms_pop_act" value="<? echo $pop_act; ?>" />
			<input type="hidden" name="send_sms_pop_msg" value="<? echo $config_msg['OK']; ?>" />
			<input type="hidden" name="accessCode" value="<? echo $access_code; ?>" />
			<input type="hidden" name="userid" value="<? echo $userid; ?>" />
		</form>
		<script>
			document.sendSMSResForm.submit();
		</script>
<?
	}

}
else{
	//require_once("../../php/conn_neturo.php");//$_SESSION[userid] $_SESSION[usernum]

	$sql = "INSERT INTO SC_TRAN(TR_SENDDATE,TR_SERIALNUM,TR_PHONE,TR_MSG,TR_ETC1,TR_ETC2,TR_ETC3,TR_ETC4,TR_ETC5,TR_ETC6) VALUES(SYSDATE(),'$SMS_STR','$phone','$to_message','$neturo','$SMS_STR','$userid','$usernum','$reff','$userip')";
	$ref = mysql_query($sql);

	if(!$ref){
?>
		<form name="sendSMSResForm" method="post" action="<? echo $config_path['SEND_FAIL']; ?>">
			<input type="hidden" name="send_sms_pop_act" value="<? echo $pop_act; ?>" />
			<input type="hidden" name="send_sms_pop_msg" value="<? echo $config_msg['FAIL']; ?>" />
			<input type="hidden" name="accessCode" value="<? echo $access_code; ?>" />
			<input type="hidden" name="userid" value="<? echo $userid; ?>" />
		</form>
		<script>
			document.sendSMSResForm.submit();
		</script>
<?
	}
	else {
?>
		<form name="sendSMSResForm" method="post" action="<? echo $config_path['SEND_SUCCESS']; ?>">
			<input type="hidden" name="send_sms_pop_act" value="<? echo $pop_act; ?>" />
			<input type="hidden" name="send_sms_pop_msg" value="<? echo $config_msg['OK']; ?>" />
			<input type="hidden" name="accessCode" value="<? echo $access_code; ?>" />
			<input type="hidden" name="userid" value="<? echo $userid; ?>" />
		</form>
		<script>
			document.sendSMSResForm.submit();
		</script>
<?
	}
}