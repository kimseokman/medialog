<?php
/**
 * logout.php
 * 2015.07.31 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$config_path = array(
	"LOGOUT_SUCCESS_DEFAULT" => GUEST_INDEX,
	"LOGOUT_SUCCESS_ADMIN" => ADMIN_INDEX,
	"LOGOUT_SUCCESS_SUPPORTER" => SUPPORTER_INDEX
);

$logout_url = $config_path['LOGOUT_SUCCESS_DEFAULT'];

if($admin_login_flag){
	$logout_url = $config_path['LOGOUT_SUCCESS_ADMIN'];
}
else if($supporter_login_flag){
	$logout_url = $config_path['LOGOUT_SUCCESS_SUPPORTER'];		
}

$main_obj->Logout();

$utils_obj->RedirectURL($logout_url);