<?php
/**
 * delete_support_type.php
 * 2015.08.18 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$trb_num = $_POST['trbnum'];

$config_path = array(
	"DEL_SUCCESS" => MANAGE_SUPPORT_TYPE_INDEX,
	"DEL_FAIL" => MANAGE_SUPPORT_TYPE_INDEX
);

$config_msg = array(
	"NO_TROUBLETYPE"=> "존재하지 않는 지원유형입니다.",
	"DEL_OK"=> "삭제가 완료되었습니다."
);

$pop_action_open = 'open';
$pop_action_close = 'close';

$res = $main_obj->DeleteTroubleType($trb_num);

if($res == "NOTROUBLETYPE"){
?>
<form name="delSupportTypeResForm" method="post" action="<? echo $config_path['DEL_FAIL']; ?>">
	<input type="hidden" name="type_del_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="type_del_res_popup_msg" value="<? echo $config_msg['NO_TROUBLETYPE']; ?>" />
</form>
<script>
	document.delSupportTypeResForm.submit();
</script>
<?
}
else{
?>
<form name="delSupportTypeResForm" method="post" action="<? echo $config_path['DEL_SUCCESS']; ?>">
	<input type="hidden" name="type_del_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="type_del_res_popup_msg" value="<? echo $config_msg['DEL_OK']; ?>" />
</form>
<script>
	document.delSupportTypeResForm.submit();
</script>
<?
}