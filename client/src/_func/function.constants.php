<?php
/**
 * constants.php
 * 2015.07.29 | KSM | create
 */

//-----------------------------------------------------------------------------------------------------------------------------------------
//     Dev 
//-----------------------------------------------------------------------------------------------------------------------------------------

//define("AES_KEY", "1ae49a1a1eb120723f07f1260b145526");              // old key
define("AES_KEY", "1fe49a101eb120763f07f1260b1f5526");                 // new key
//define("AES_IV", "2811da22377d62fcfdb02f29aad77d9e");                  // old key
define("AES_IV", "281eda22377d62fcfdb02f29aad77d9e");                     // new key

//     ENV
define("SERVICE_NAME", "U+Biz 원격지원");
//define("SERVER_ADDR", "http://".$_SERVER['SERVER_NAME']."");
define("SERVER_ADDR", "http://support.uplus.co.kr");
define("REMOTE_HOME", "http://support.uplus.co.kr/");                                          //원격 홈페이지
define("CONTACT_US_TEL", "1544-7078");                                                                    //문의 전화
define("HIDE_OPTION", TRUE);                                                                                        //요소 숨기기
define("SEARCH_TERM_YYYY", 2);                                                                                 //검색 가능 기간 2년 -> 1년으로 수정 15.11.02
define("APP_DEBUG", TRUE);
define("APP_DEBUG_ID", "wsjang1");
define("APP_DEBUG_REPORT_NUM", 1);
define("SESSION_TIMEOUT", 1800);                                                                                          //30 min ( 1800 sec )

//     MAIL
//     ver DEV
//define("ML_URL", SERVER_ADDR);                                                                         //Base URL
//define("ML_MAIL_HOST", "mail.koino.net");                                                         //보내는 메일 서버
//define("ML_MAIL_PORT", 25);                                                                                 //포트 번호
//define("ML_MAILER", "smtp");                                                                                 //프로토콜
//define("ML_MAIL_CHARSET", "utf-8");
//define("ML_MAIL_ID", "global@koino.net");                                                          //보내는 메일 계정 아이디
//define("ML_MAIL_PW", "ko2470153ino");                                                            //보내는 메일 계정 비밀번호
//define("ML_MAIL_ADDR", "global@koino.net");                                                  //관리자 메일
//define("ML_MAIL_NAME", "U+Biz 원격지원");                                                            //관리자 메일 이름
//     ver REAL
define("ML_URL", SERVER_ADDR);                                                                         //Base URL
define("ML_MAIL_HOST", "211.40.221.214");                                                         //보내는 메일 서버
define("ML_MAIL_PORT", 25);                                                                                 //포트 번호
define("ML_MAILER", "smtp");                                                                                 //프로토콜
define("ML_MAIL_CHARSET", "utf-8");
define("ML_MAIL_ID", "support@anyhelp.net");                                                          //보내는 메일 계정 아이디
define("ML_MAIL_PW", "doslgpfvm");                                                            //보내는 메일 계정 비밀번호
define("ML_MAIL_ADDR", "support@anyhelp.net");                                                  //관리자 메일
define("ML_MAIL_NAME", "U+Biz 원격지원");                                                            //관리자 메일 이름

//-----------------------------------------------------------------------------------------------------------------------------------------
//     SUPERADMIN
//-----------------------------------------------------------------------------------------------------------------------------------------

//     ENV
define("SUPERADMIN_ACCESS_IP_BLOCK", FALSE);
define("SUPERADMIN_CHECKOUT_PW", "koino1807303");
define("ID_MAX_LENGTH", 12);
define("SIGN_NUM_MAX_LENGTH", 12);
define("BILL_ACCOUNT_MAX_LENGTH", 12);
define("LAST_LOGIN_INTERVAL", 180);                                                                      //180일 (6개월)

//     URI
define("SUPER_GNB_SERVICE", "/super/manage/service");
define("SUPER_GNB_SERVICE_NOTICE", SUPER_GNB_SERVICE . "/notice");
define("SUPER_GNB_SERVICE_NOTICE_INDEX", SUPER_GNB_SERVICE_NOTICE . "/list.php");
define("SUPER_GNB_CUSTOMER", "/super/manage/customer");
define("SUPER_GNB_CUSTOMER_FREE", SUPER_GNB_CUSTOMER . "/free");
define("SUPER_GNB_CUSTOMER_FREE_INDEX", SUPER_GNB_CUSTOMER_FREE . "/list.php");
define("SUPER_GNB_CUSTOMER_PAID", SUPER_GNB_CUSTOMER . "/paid");
define("SUPER_GNB_CUSTOMER_PAID_INDEX", SUPER_GNB_CUSTOMER_PAID . "/list.php");

//     PATH
define("SUPERADMIN_INDEX", SUPER_GNB_CUSTOMER_PAID_INDEX);
define("SUPERADMIN_LOGIN_TRY", "/access/super_login.php");

//     CREAT ADMIN
define("CUSTOMER_ACCESS_URL_DEFAULT_HEADER", "http://");
define("CUSTOMER_ACCESS_URL_DEFAULT_FOOTER", "cs1472.com");
define("CUSTOMER_ACCESS_URL_DEFAULT", CUSTOMER_ACCESS_URL_DEFAULT_HEADER . CUSTOMER_ACCESS_URL_DEFAULT_FOOTER);
define("CUSTOMER_ACCESS_URL_LENGTH", 26);
define("CUSTOMER_ACCESS_URL_DOMAIN_LENGTH", 8);

define("ADMIN_ACTIVE_STATUS_DEFAULT", "Y");
define("PC_VOLUME_DEFAULT", 1);
define("M_N_V_VOLUME_DEFAULT", 0);
define("ALL_VOLUME_DEFAULT", PC_VOLUME_DEFAULT + M_N_V_VOLUME_DEFAULT);

define("CURRENT_DATE_DEFAULT", date("Y-m-d"));
define("SERVICE_FREE_TERM", 15);    //15일
define("ADMIN_ACT_TYPE_FREE", TRUE);    //isFree - TRUE
define("ADMIN_ACT_TYPE_PAID", FALSE);    //isFree - FALSE

define("BUSINESS_LIST", var_export(
              array(
                     0 => array(
                            "name" => "건설업"
                     ), 
                     1 => array(
                            "name" => "공공기관(사법/공공질서)"
                     ), 
                     2 => array(
                            "name" => "공공기관(외무/국방)"
                     ), 
                     3 => array(
                            "name" => "공공기관(입법/행정)"
                     ), 
                     4 => array(
                            "name" => "광업"
                     ), 
                     5 => array(
                            "name" => "교육업(기타/학원)"
                     ), 
                     6 => array(
                            "name" => "교육업(학교)"
                     ), 
                     7 => array(
                            "name" => "금융업(기타)"
                     ), 
                     8 => array(
                            "name" => "금융업(농,축,수협/신협/금고)"
                     ), 
                     9 => array(
                            "name" => "금융업(은행)"
                     ), 
                     10 => array(
                            "name" => "금융업(증권)"
                     ), 
                     11 => array(
                            "name" => "기계장비/소비용품 임대업"
                     ), 
                     12 => array(
                            "name" => "기타 서비스업"
                     ), 
                     13 => array(
                            "name" => "농업"
                     ), 
                     14 => array(
                            "name" => "보험/연금업"
                     ), 
                     15 => array(
                            "name" => "부동산업"
                     ), 
                     16 => array(
                            "name" => "사업지원 서비스업"
                     ), 
                     17 => array(
                            "name" => "사회복지사업"
                     ), 
                     18 => array(
                            "name" => "수리업"
                     ), 
                     19 => array(
                            "name" => "숙박업"
                     ), 
                     20 => array(
                            "name" => "어업"
                     ), 
                     21 => array(
                            "name" => "여행업"
                     ), 
                     22 => array(
                            "name" => "연구개발업"
                     ), 
                     23 => array(
                            "name" => "영화/방송/공연산업"
                     ), 
                     24 => array(
                            "name" => "오락/문화/운동관련(게임방)"
                     ), 
                     25 => array(
                            "name" => "오락/문화/운동관련(문화/오락)"
                     ), 
                     26 => array(
                            "name" => "오락/문화/운동관련(스포츠/레저)"
                     ), 
                     27 => array(
                            "name" => "외국기관"
                     ), 
                     28 => array(
                            "name" => "운송업(수상)"
                     ), 
                     29 => array(
                            "name" => "운송업(육상)"
                     ), 
                     30 => array(
                            "name" => "운송업(항공)"
                     ), 
                     31 => array(
                            "name" => "유통업(도/소매업)"
                     ), 
                     32 => array(
                            "name" => "음식점업"
                     ), 
                     33 => array(
                            "name" => "의료업"
                     ), 
                     34 => array(
                            "name" => "임업"
                     ), 
                     35 => array(
                            "name" => "전기/가스/수도업"
                     ), 
                     36 => array(
                            "name" => "전문/과학/기술서비스업(건축설계/엔지니어링)"
                     ), 
                     37 => array(
                            "name" => "전문/과학/기술서비스업(과학/기술)"
                     ), 
                     38 => array(
                            "name" => "전문/과학/기술서비스업(광고/디자인)"
                     ), 
                     39 => array(
                            "name" => "전문/과학/기술서비스업(법무/회계)"
                     ), 
                     40 => array(
                            "name" => "전문/과학/기술서비스업(시장조사/경영상담)"
                     ), 
                     41 => array(
                            "name" => "정보처리/컴퓨터운영관련(기타)"
                     ), 
                     42 => array(
                            "name" => "정보처리/컴퓨터운영관련(컨텐츠)"
                     ), 
                     43 => array(
                            "name" => "정보처리/컴퓨터운영관련(컴퓨터시스템/SI)"
                     ), 
                     44 => array(
                            "name" => "정보처리/컴퓨터운영관련(S/W)"
                     ), 
                     45 => array(
                            "name" => "제조업(금속)"
                     ), 
                     46 => array(
                            "name" => "제조업(기계/자동차/운송장비)"
                     ), 
                     47 => array(
                            "name" => "제조업(기타)"
                     ), 
                     48 => array(
                            "name" => "제조업(목재/가구)"
                     ), 
                     49 => array(
                            "name" => "제조업(섬유/가죽/신발)"
                     ), 
                     50 => array(
                            "name" => "제조업(음/식료)"
                     ), 
                     51 => array(
                            "name" => "제조업(전기)"
                     ), 
                     52 => array(
                            "name" => "제조업(전자)"
                     ), 
                     53 => array(
                            "name" => "제조업(종이/출판)"
                     ), 
                     54 => array(
                            "name" => "제조업(화학)"
                     ), 
                     55 => array(
                            "name" => "창고업"
                     ), 
                     56 => array(
                            "name" => "통신업(기타)"
                     ), 
                     57 => array(
                            "name" => "통신업(무선)"
                     ), 
                     58 => array(
                            "name" => "통신업(별정)"
                     ), 
                     59 => array(
                            "name" => "통신업(부가)"
                     ), 
                     60 => array(
                            "name" => "통신업(우편/소포송달)"
                     ), 
                     61 => array(
                            "name" => "통신업(유선)"
                     ), 
                     62 => array(
                            "name" => "폐기물처리/청소관련 서비스업"
                     ), 
                     63 => array(
                            "name" => "회원단체"
                     )
              ), true
));

//-----------------------------------------------------------------------------------------------------------------------------------------
//     APP
//-----------------------------------------------------------------------------------------------------------------------------------------

//     PATH
define("APP_DIR", "/_app_cowork");
define("APP_RECORD", APP_DIR . "/record");

//     HTML TITLE
define("APP_RECORD_TITLE", "record");

//     SETTING
define("APP_COWORK_LOGIN_PW", "modeWebLogin");                                         //APP 로그인 대조 값

//     ENV : app > record
define("RECORD_MAX_LENGTH", 5000);
define("RECORD_MAX_LENGTH_EN", 5000);
define("RECORD_MAX_LENGTH_KR", 1666);


//-----------------------------------------------------------------------------------------------------------------------------------------
//     ADMIN
//-----------------------------------------------------------------------------------------------------------------------------------------

//     URI
define("MANAGE_AGENT", "/admin/manage/agent");
define("MANAGE_AGENT_USER", MANAGE_AGENT . "/user");
define("MANAGE_AGENT_USER_INDEX", MANAGE_AGENT_USER . "/list.php");
define("MANAGE_AGENT_DEPARTMENT", MANAGE_AGENT . "/department");
define("MANAGE_AGENT_DEPARTMENT_INDEX", MANAGE_AGENT_DEPARTMENT . "/list.php");

define("MANAGE_SUPPORT", "/admin/manage/support");
define("MANAGE_SUPPORT_HISTORY", MANAGE_SUPPORT . "/history");
define("MANAGE_SUPPORT_HISTORY_INDEX", MANAGE_SUPPORT_HISTORY . "/list.php");
define("MANAGE_SUPPORT_TYPE", MANAGE_SUPPORT . "/type");
define("MANAGE_SUPPORT_TYPE_INDEX", MANAGE_SUPPORT_TYPE . "/list.php");

define("MANAGE_STAT", "/admin/manage/statistics");
define("MANAGE_STAT_TIME", MANAGE_STAT . "/time");
define("MANAGE_STAT_TIME_INDEX", MANAGE_STAT_TIME . "/list.php");
define("MANAGE_STAT_DAY", MANAGE_STAT . "/day");
define("MANAGE_STAT_DAY_INDEX", MANAGE_STAT_DAY . "/list.php");
define("MANAGE_STAT_MONTH", MANAGE_STAT . "/month");
define("MANAGE_STAT_MONTH_INDEX", MANAGE_STAT_MONTH . "/list.php");
define("MANAGE_STAT_AGENT", MANAGE_STAT . "/agent");
define("MANAGE_STAT_AGENT_INDEX", MANAGE_STAT_AGENT . "/list.php");
define("MANAGE_STAT_SUPPORT_TIME", MANAGE_STAT . "/support_time");
define("MANAGE_STAT_SUPPORT_TIME_INDEX", MANAGE_STAT_SUPPORT_TIME . "/list.php");
define("MANAGE_STAT_OS", MANAGE_STAT . "/os");
define("MANAGE_STAT_OS_INDEX", MANAGE_STAT_OS . "/list.php");
define("MANAGE_STAT_TROUBLE", MANAGE_STAT . "/trouble");
define("MANAGE_STAT_TROUBLE_INDEX", MANAGE_STAT_TROUBLE . "/list.php");

define("SETTING", "/admin/setting");
define("SETTING_SITE", SETTING . "/site");
define("SETTING_SITE_INDEX", SETTING_SITE . "/info.php");
define("SETTING_SITE_INDEX_AUTH", SETTING_SITE . "/info_auth.php");
define("SETTING_NOTICE", SETTING . "/notice");
define("SETTING_NOTICE_INDEX", SETTING_NOTICE . "/list.php");

//     PATH
define("GNB_USER_MANAGE", MANAGE_AGENT_DEPARTMENT_INDEX);
define("GNB_HISTORY_MANAGE", MANAGE_SUPPORT_HISTORY_INDEX);
define("GNB_STATISTICS_MANAGE", MANAGE_STAT_TIME_INDEX);
define("GNB_SITE_SETTING", SETTING_SITE_INDEX_AUTH);

define("ADMIN_LOGIN_TRY", "/access/admin_login.php");
define("LOGIN_TRY", "/access/login.php");
define("LOGIN_SUCCESS", "/");
define("ADMIN_INDEX", GNB_USER_MANAGE);
define("SUPPORTER_INDEX", "/agent/service.php");
define("CUSTOMER_INDEX", "/customer");
define("GUEST_INDEX", "/");

define("MAIL_CONTENTS_DIR", "/_mail_contents");                                                         //메일 내용 ROOT
define("MAIL_BODY_CREATE_USER", MAIL_CONTENTS_DIR . "/create_user_mail.php");     
define("MAIL_BODY_REQUEST_REMOTE", MAIL_CONTENTS_DIR . "/request_remote_mail.php");
define("MAIL_BODY_RESET_PW", MAIL_CONTENTS_DIR . "/reset_pw_mail.php");

define("PUBLIC_DIR", "/_public");                                                                                            //PUBLIC PATH
define("CSS_DIR", PUBLIC_DIR . "/css");
define("JS_DIR", PUBLIC_DIR . "/js");
define("BIZ_JS_DIR", JS_DIR . "/biz");
define("IMG_DIR", PUBLIC_DIR . "/images");

define("LIB_DIR", "/_lib");                                                                                                        //LIBRARY PATH
define("INC_DIR", LIB_DIR . "/_inc");

//     Export File(Excel) Names
define("EXPORT_USER_INFO", "user_info");
define("EXPORT_SUPPORT_LOG", "support_log");
define("EXPORT_SUPPORT_TYPE", "support_type");
define("EXPORT_STAT_TIME", "time_statistics");
define("EXPORT_STAT_SUPPORT_TIME", "support_time_statistics");
define("EXPORT_STAT_OS", "os_statistics");
define("EXPORT_STAT_MONTH", "monthly_statistics");
define("EXPORT_STAT_DAY", "daily_statistics");
define("EXPORT_STAT_AGENT", "agent_statistics");

define("EXPORT_APP_RECORD", "record");

//     HTML TITLE
define("LOGIN_TITLE", "U+biz Support User Admin 로그인화면");
define("ETC_TITLE", "U+biz admin");

//     LOGO
define("BASE_LOGO_DIR", IMG_DIR."/sub");
define("BASE_LOGO", "login_logo.gif");
define("CUSTOMER_LOGO_DIR", CUSTOMER_INDEX."/images");

//
//     ENV : login
//     
define("LOGIN_TRY_COUNT_MAX", 5);                                                                            //로그인 시도 횟수 제한
define("PASSWORD_SAFE_CNT", 3);        //비밀번호에 포함되어야 하는 영문자, 특수문자, 숫자 유효 카운트
define("PASSWORD_LENGTH", 8);
define("PASSWORD_LENGTH_MAX", 12);
define("PASSWORD_LENGTH_MIN", PASSWORD_LENGTH);
define("EDIT_PASSWORD_INTERVAL", 90);                                                                      //90일 (3개월)

//
//     ENV : manage agent > department
//     
define("DEPT_MAX_NUM", 20);
define("DEPT_NAME_MAX_LENGTH", 30);                                                                      //case by validate
define("DEPT_NAME_MAX_LENGTH_KR", 10);
define("DEPT_NAME_MAX_LENGTH_EN", 30);
define("DEPT_MEMO_MAX_LENGTH", 256);                                                                   //case by validate
define("DEPT_MEMO_MAX_LENGTH_KR", 85);
define("DEPT_MEMO_MAX_LENGTH_EN", 256);

//
//     ENV : manage agent > user
//     
define("NAME_MAX_LENGTH", 30);                                                                                 //case by validate
define("NAME_MAX_LENGTH_KR", 10);
define("NAME_MAX_LENGTH_EN", 30);
define("PHONE_MAX_LENGTH", 20);
define("MAIL_ADDR_MAX_LENGTH", 50);

//     PERMISSION define - PC
define("PERMISSION_PC_FUNCTION_1", 1);//                                                                                                                     1 / pc function   1 permission : 부분 공유
define("PERMISSION_PC_FUNCTION_2", 2);//                                                                                                                   10 / pc function   2 permission : 양방향 파일 전송
define("PERMISSION_PC_FUNCTION_3", 4);//                                                                                                                 100 / pc function   3 permission : 자동 재접속
define("PERMISSION_PC_FUNCTION_4", 8);//                                                                                                               1000 / pc function   4 permission : URL 전송
define("PERMISSION_PC_FUNCTION_5", 16);//                                                                                                         1 0000 / pc function   5 permission : 문자 채팅
define("PERMISSION_PC_FUNCTION_6", 32);//                                                                                                      10 0000 / pc function   6 permission : 시스템 정보 보기
define("PERMISSION_PC_FUNCTION_7", 64);//                                                                                                    100 0000 / pc function   7 permission : 프로세스 정보 보기
define("PERMISSION_PC_FUNCTION_8", 128);//                                                                                               1000 0000 / pc function   8 permission : 화면 캡처
define("PERMISSION_PC_FUNCTION_9", 256);//                                                                                            1 0000 0000 / pc function   9 permission : 상담원 화면 전송
define("PERMISSION_PC_FUNCTION_10", 512);//                                                                                       10 0000 0000 / pc function 10 permission : 즐겨찾기
define("PERMISSION_PC_FUNCTION_11", 1024);//                                                                                  100 0000 0000 / pc function 11 permission : 그리기
define("PERMISSION_PC_FUNCTION_12", 2048);//                                                                               1000 0000 0000 / pc function 12 permission : 양방향 그리기
define("PERMISSION_PC_FUNCTION_13", 4096);//                                                                            1 0000 0000 0000 / pc function 13 permission : 화면 녹화
define("PERMISSION_PC_FUNCTION_14", 8192);//                                                                         10 0000 0000 0000 / pc function 14 permission : 음성 채팅
define("PERMISSION_PC_FUNCTION_15", 16384);//                                                                    100 0000 0000 0000 / pc function 15 permission : 상담원 화면 제어
define("PERMISSION_PC_FUNCTION_16", 32768);//                                                                  1000 0000 0000 0000 / pc function 16 permission : 파일 보내기
define("PERMISSION_PC_FUNCTION_17", 65536);//                                                              1 0000 0000 0000 0000 / pc function 17 permission : 파일 가져오기
define("PERMISSION_PC_FUNCTION_18", 131072);//                                                         10 0000 0000 0000 0000 / pc function 18 permission : 상담원 초대
define("PERMISSION_PC_FUNCTION_19", 262144);//                                                       100 0000 0000 0000 0000 / pc function 19 permission : 안전모드 재부팅 후 자동 재접속
//     PERMISSION define - PC SETTING
define("PERMISSION_PC_SETTING_1", 1);//                                                                                                                     1 / pc setting permission : 상담원 강제 로그인
define("PERMISSION_PC_SETTING_2", 2);//                                                                                                                   10 / pc setting permission : 접속전 고객동의
define("PERMISSION_PC_SETTING_3", 4);//                                                                                                                 100 / pc setting permission : 고객 만족도 조사
define("PERMISSION_PC_SETTING_4", 8);//                                                                                                               1000 / pc setting permission : 드라이버 모드 사용
define("PERMISSION_PC_SETTING_5", 16);//                                                                                                         1 0000 / pc setting permission : 마우스 추적 사용
define("PERMISSION_PC_SETTING_6", 32);//                                                                                                      10 0000 / pc setting permission : 접속시 자동제어모드
define("PERMISSION_PC_SETTING_7", 64);//                                                                                                    100 0000 / pc setting permission : 상담원 제어 불가
define("PERMISSION_PC_SETTING_8", 128);//                                                                                               1000 0000 / pc setting permission : 파일 전송 뷰어 모드
define("PERMISSION_PC_SETTING_9", 256);//                                                                                            1 0000 0000 / pc setting permission : 수동 재부팅
define("PERMISSION_PC_SETTING_10", 512);//                                                                                       10 0000 0000 / pc setting permission : 상담원 초대전 고객 동의
define("PERMISSION_PC_SETTING_11", 1024);//                                                                                  100 0000 0000 / pc setting permission : Ctrl+C / Ctrl+V 파일 전송
define("PERMISSION_PC_SETTING_12", 2048);//                                                                               1000 0000 0000 / pc setting permission : 원격프린터
define("PERMISSION_PC_SETTING_13", 4096);//                                                                            1 0000 0000 0000 / pc setting permission : 원격 종료 후 세션 자동 생성
define("PERMISSION_PC_SETTING_14", 8192);//                                                                         10 0000 0000 0000 / pc setting permission : 바탕화면 공유
//     PERMISSION define - MOBILE FUNCTION
define("PERMISSION_MOBILE_FUNCTION_1", 1);//                                                                                                                     1 / mobile setting permission : 상담원 제어 불가
define("PERMISSION_MOBILE_FUNCTION_2", 2);//                                                                                                                   10 / mobile setting permission : 양방향 파일 전송
define("PERMISSION_MOBILE_FUNCTION_3", 4);//                                                                                                                 100 / mobile setting permission : 파일 전송전 고객동의
define("PERMISSION_MOBILE_FUNCTION_4", 8);//                                                                                                               1000 / mobile setting permission : 응용프로그램 공유 동의

//    PERMISSION - CAL
define("PERMISSION_PC_BASE", PERMISSION_PC_FUNCTION_1 + PERMISSION_PC_FUNCTION_2 + PERMISSION_PC_FUNCTION_3 + PERMISSION_PC_FUNCTION_4 +
PERMISSION_PC_FUNCTION_5 + PERMISSION_PC_FUNCTION_6 + PERMISSION_PC_FUNCTION_7 + PERMISSION_PC_FUNCTION_8 + 
//PERMISSION_PC_FUNCTION_9 + 
PERMISSION_PC_FUNCTION_10 + PERMISSION_PC_FUNCTION_11 + PERMISSION_PC_FUNCTION_12 + PERMISSION_PC_FUNCTION_13 + 
//PERMISSION_PC_FUNCTION_14 + 
//PERMISSION_PC_FUNCTION_15 + 
PERMISSION_PC_FUNCTION_16 + PERMISSION_PC_FUNCTION_17 + PERMISSION_PC_FUNCTION_18
// + PERMISSION_PC_FUNCTION_19
);
                                                                                                                                          //     2015.09.30 | KSM | 일부 기능 제거

//     PERMISSION
define("PERMISSION_PC", PERMISSION_PC_BASE);//                                                        011 1111 1111 1111 1111 / pc permission
define("PERMISSION_MOBILE_PAID", 524288);//                                                            1000 0000 0000 0000 0000 / mobile_paid permission 
define("PERMISSION_MOBILE_FREE", 1048576);//                                                      1 0000 0000 0000 0000 0000 / moible_free permission
define("PERMISSION_VIDEO_PAID", 2097152);//                                                      10 0000 0000 0000 0000 0000 / video_paid permission
define("PERMISSION_VIDEO_FREE", 4194304);//                                                    100 0000 0000 0000 0000 0000 / video_free permission
define("PERMISSION_MOBILE_VIDEO_PAID", 8388608);//                                1000 0000 0000 0000 0000 0000 / video_moible_paid permission		
define("PERMISSION_MOBILE_VIDEO_FREE", 16777216);//                           1 0000 0000 0000 0000 0000 0000 / video_moible_free permission

//     CODE
define("PRODUCT_CODE_PC", PERMISSION_PC);//                                                                                                                            011 1111 1111 1111 1111 / pc MAX permission
define("PRODUCT_CODE_MOBILE_PAID" , PERMISSION_PC + PERMISSION_MOBILE_PAID);//                                               1111 1111 1111 1111 1111 / pc + paid mobile
define("PRODUCT_CODE_MOBILE_FREE" , PERMISSION_PC + PERMISSION_MOBILE_FREE);//                                            1 0111 1111 1111 1111 1111 / pc + free mobile
define("PRODUCT_CODE_VIDEO_PAID" , PERMISSION_PC + PERMISSION_VIDEO_PAID);//                                              10 0111 1111 1111 1111 1111 / pc + paid video
define("PRODUCT_CODE_VIDEO_FREE" , PERMISSION_PC + PERMISSION_VIDEO_FREE);//                                            100 0111 1111 1111 1111 1111 / pc + free video
define("PRODUCT_CODE_MOBILE_VIDEO_PAID" , PERMISSION_PC + PERMISSION_MOBILE_VIDEO_PAID);//         1000 0111 1111 1111 1111 1111 / pc + paid mobile&video
define("PRODUCT_CODE_MOBILE_VIDEO_FREE" , PERMISSION_PC + PERMISSION_MOBILE_VIDEO_FREE);//      1 0000 0111 1111 1111 1111 1111 / pc + free mobile&video

//     Product Name
define("PRODUCT_PC", "PC");
define("PRODUCT_MOBILE", PRODUCT_PC."_MOBILE");
define("PRODUCT_VIDEO", PRODUCT_PC."_VIDEO");
define("PRODUCT_MOBILE_VIDEO", PRODUCT_PC."_MOBILE_VIDEO");

//     Set Default  PERMISSION ( case by create user )
define("DEFAULT_PERMISSION_PC_FUNCTION", PERMISSION_PC);
define("DEFAULT_PERMISSION_PC_SETTING", PERMISSION_PC_SETTING_4  + PERMISSION_PC_SETTING_6 + PERMISSION_PC_SETTING_11);
define("DEFAULT_PERMISSION_MOBILE_FUNCTION", PERMISSION_MOBILE_FUNCTION_2 + PERMISSION_MOBILE_FUNCTION_3);
define("DEFAULT_MOBILE_EXPIRED_DATE", "0000-00-00");

//     PC FUNCTIONS
define("PC_FUNCTION_LIST", var_export(
              array(
                     0  => array(
                            "tag" => "func1",
                            "permission" => PERMISSION_PC_FUNCTION_1,
                            "title" => "특정 사각영역 공유",
                            "memo" => "특정 사각영역 공유 기능을 사용 할 수 있습니다."
                     ), 
                     1  => array(
                            "tag" => "func2",
                            "permission" => PERMISSION_PC_FUNCTION_2,
                            "title" => "양방향 파일 전송",
                            "memo" => "양방향 파일 전송을 기능을 사용 할 수 있습니다."
                     ), 
                     2 => array(
                            "tag" => "func3",
                            "permission" => PERMISSION_PC_FUNCTION_3,
                            "title" => "리부팅 후 재접속",
                            "memo" => "고객 PC를 리부팅 시킨 후 자동 원격접속 기능을 사용 할 수 있습니다."
                     ), 
                     3 => array(
                            "tag" => "func4",
                            "permission" => PERMISSION_PC_FUNCTION_4,
                            "title" => "URL 전송",
                            "memo" => "특정 URL을 고객 PC에 전송합니다."
                     ), 
                     4 => array(
                            "tag" => "func5",
                            "permission" => PERMISSION_PC_FUNCTION_5,
                            "title" => "문자채팅",
                            "memo" => "고객과 채팅을 할 수 있습니다."
                     ), 
                     5 => array(
                            "tag" => "func6",
                            "permission" => PERMISSION_PC_FUNCTION_6,
                            "title" => "시스템 정보 보기",
                            "memo" => "고객 PC의 시스템 정보를 볼 수 있습니다."
                     ), 
                     6 => array(
                            "tag" => "func7",
                            "permission" => PERMISSION_PC_FUNCTION_7,
                            "title" => "프로세스 정보 보기",
                            "memo" => "고객 PC의 프로세스 정보를 볼 수 있습니다."
                     ), 
                     7 => array(
                            "tag" => "func8",
                            "permission" => PERMISSION_PC_FUNCTION_8,
                            "title" => "화면캡쳐",
                            "memo" => "현재 제어 화면을 캡처할 수 있습니다."
                     ), 
                     8 => array(
                            "tag" => "func9",
                            "permission" => PERMISSION_PC_FUNCTION_9,
                            "title" => "상담원 화면 전송",
                            "memo" => "고객 PC에 상담원 화면을 전송합니다.(제어불가)"
                     ), 
                     9 => array(
                            "tag" => "func10",
                            "permission" => PERMISSION_PC_FUNCTION_10,
                            "title" => "즐겨찾기",
                            "memo" => "자주 사용하는 URL, 프로그램, 폴더를 고객 PC에 띄울 수 있는 기능입니다."
                     ), 
                     10 => array(
                            "tag" => "func11",
                            "permission" => PERMISSION_PC_FUNCTION_11,
                            "title" => "그리기",
                            "memo" => "상담원이 고객 PC에 그리기를 할 수 있는 기능입니다."
                     ), 
                     11 => array(
                            "tag" => "func12",
                            "permission" => PERMISSION_PC_FUNCTION_12,
                            "title" => "양방향 그리기",
                            "memo" => "고객과 상담원이 동시에 그리기를 할 수 있는 기능입니다."
                     ), 
                     12 => array(
                            "tag" => "func13",
                            "permission" => PERMISSION_PC_FUNCTION_13,
                            "title" => "화면 녹화",
                            "memo" => "현재 제어 중인 화면을 녹화할 수 있습니다."
                     ), 
                     13 => array(
                            "tag" => "func14",
                            "permission" => PERMISSION_PC_FUNCTION_14,
                            "title" => "음성 채팅",
                            "memo" => "고객과 음성 채팅을 할 수 있습니다."
                     ), 
                     14 => array(
                            "tag" => "func15",
                            "permission" => PERMISSION_PC_FUNCTION_15,
                            "title" => "상담원 화면 제어",
                            "memo" => "고객 PC에 상담원 화면을 전송합니다.(제어 가능)"
                     ), 
                     15 => array(
                            "tag" => "func16",
                            "permission" => PERMISSION_PC_FUNCTION_16,
                            "title" => "파일 보내기",
                            "memo" => "파일 보내기 기능을 사용 할 수 있습니다."
                     ), 
                     16 => array(
                            "tag" => "func17",
                            "permission" => PERMISSION_PC_FUNCTION_17,
                            "title" => "파일 가져오기",
                            "memo" => "파일 가져오기 기능을 사용 할 수 있습니다."
                     ), 
                     17 => array(
                            "tag" => "func18",
                            "permission" => PERMISSION_PC_FUNCTION_18,
                            "title" => "세션 공유",
                            "memo" => "세션 공유 기능을 사용 할 수 있습니다."
                     ), 
                     18 => array(
                            "tag" => "func19",
                            "permission" => PERMISSION_PC_FUNCTION_19,
                            "title" => "안전모드 재부팅 후 자동 재접속",
                            "memo" => "고객 PC가 안전모드로 재부팅 된 후 자동으로 접속합니다."
                     )
              ), true
));
//     PC SETTINS
define("PC_SETTING_LIST", var_export(
              array(
                     0 => array(
                            "tag" => "sett1",
                            "permission" => PERMISSION_PC_SETTING_1,
                            "title" => "상담원 강제 로그인",
                            "memo" => "중복로그인시 후 사용자가 기존 접속을 끊을 수 있는 설정입니다."
                     ), 
                     1 => array(
                            "tag" => "sett2",
                            "permission" => PERMISSION_PC_SETTING_2,
                            "title" => "접속전 고객동의",
                            "memo" => "고객이 동의를 해야만 원격 접속이 되는 설정입니다."
                     ), 
                     2 => array(
                            "tag" => "sett3",
                            "permission" => PERMISSION_PC_SETTING_3,
                            "title" => "고객 만족도 조사",
                            "memo" => "고객 만족도 조사를 할 수 있습니다."
                     ), 
                     3 => array(
                            "tag" => "sett4",
                            "permission" => PERMISSION_PC_SETTING_4,
                            "title" => "드라이버 모드 사용",
                            "memo" => "드라이버 모드를 기본으로 설정합니다."
                     ), 
                     4 => array(
                            "tag" => "sett5",
                            "permission" => PERMISSION_PC_SETTING_5,
                            "title" => "마우스 추적 사용",
                            "memo" => "마우스 추적을 기본으로 설정합니다."
                     ), 
                     5 => array(
                            "tag" => "sett6",
                            "permission" => PERMISSION_PC_SETTING_6,
                            "title" => "접속시 자동제어모드",
                            "memo" => "원격 접속시 자동으로 제어가 되게 합니다."
                     ), 
                     6 => array(
                            "tag" => "sett7",
                            "permission" => PERMISSION_PC_SETTING_7,
                            "title" => "상담원 제어 불가",
                            "memo" => "상담원이 고객 PC를 제어하지 못하도록 설정합니다."
                     ), 
                     7 => array(
                            "tag" => "sett8",
                            "permission" => PERMISSION_PC_SETTING_8,
                            "title" => "파일 전송 뷰어 모드",
                            "memo" => "양방향 파일 전송일 경우 파일 전송 창을 뷰어에서 띄울 것인지 설정합니다."
                     ), 
                     8 => array(
                            "tag" => "sett9",
                            "permission" => PERMISSION_PC_SETTING_9,
                            "title" => "수동 재부팅",
                            "memo" => "부팅 후 자동 재접속시 수동으로 재부팅 합니다."
                     ), 
                     9 => array(
                            "tag" => "sett10",
                            "permission" => PERMISSION_PC_SETTING_10,
                            "title" => "세션공유 시 고객 동의",
                            "memo" => "원격지원 세션을 다른 상담원에게 공유 하기 전에 고객의 동의를 구합니다."
                     ), 
                     10 => array(
                            "tag" => "sett11",
                            "permission" => PERMISSION_PC_SETTING_11,
                            "title" => "Ctrl+C / Ctrl+V 파일 전송",
                            "memo" => "Ctrl+C/Ctrl+V를 이용하여 파일을 전송할 수 있습니다."
                     ), 
                     11 => array(
                            "tag" => "sett12",
                            "permission" => PERMISSION_PC_SETTING_12,
                            "title" => "원격프린터",
                            "memo" => "원격 프린터 기능을 사용합니다."
                     ), 
                     12 => array(
                            "tag" => "sett13",
                            "permission" => PERMISSION_PC_SETTING_13,
                            "title" => "원격 종료 후 세션 자동 생성",
                            "memo" => "원격종료 후 자동으로 새로운 세션을 생성합니다."
                     ), 
                     13 => array(
                            "tag" => "sett14",
                            "permission" => PERMISSION_PC_SETTING_14,
                            "title" => "바탕화면 공유",
                            "memo" => "고객의 바탕화면을 공유합니다."
                     )
              ), true
));
//     MOBIE FUNCTIONS
define("MOBILE_FUNCTION_LIST", var_export(
              array(
                     0 => array(
                            "tag" => "mob1",
                            "permission" => PERMISSION_MOBILE_FUNCTION_1,
                            "title" => "상담원 제어 불가",
                            "memo" => "상담원이 고객 모바일 단말을 제어하지 못하도록 설정합니다."
                     ), 
                     1 => array(
                            "tag" => "mob2",
                            "permission" => PERMISSION_MOBILE_FUNCTION_2,
                            "title" => "양방향 파일 전송",
                            "memo" => "양방향 파일 전송을 사용 할 수 있습니다."
                     ), 
                     2 => array(
                            "tag" => "mob3",
                            "permission" => PERMISSION_MOBILE_FUNCTION_3,
                            "title" => "파일 전송 전 고객동의",
                            "memo" => "파일전송시 고객의 동의를 얻어여만 파일 전송이 가능합니다."
                     ), 
                     3 => array(
                            "tag" => "mob4",
                            "permission" => PERMISSION_MOBILE_FUNCTION_4,
                            "title" => "응용프로그램 공유 동의",
                            "memo" => "고객의 응용프로그램 공유에 대한 동의 팝업창을 띄웁니다."
                     )
              ), true
));

//
//     ENV : manage support > history
//     
define("OS_UNKNOWN", "기타 OS");
define("OS_WIN_98", "Windows 98");
define("OS_WIN_2000", "Windows 2000");
define("OS_WIN_XP", "Windows XP");
define("OS_WIN_2003", "Windows 2003");
define("OS_WIN_VISTA", "Windows Vista");
define("OS_WIN_7", "Windows 7");
define("OS_WIN_8", "Windows 8");
define("OS_WIN_8_1", "Windows 8.1");
define("OS_WIN_2008", "Windows 2008");
define("OS_WIN_2012", "Windows 2012");
define("OS_WIN_10", "Windows 10 Inside Preview");
define("OS_WIN_SERVER", "Windows Server Technical Preview");
define("OS_ANDROID", "Android");
define("OS_MAC", "Mac");
define("OS_LINUX", "Linux");

define("RESOLVE", "해결");
define("HOLD", "보류");
define("NEED_DIRECT_SUPPORT", "지원 필요");
define("UNCHECKED", "미체크");

define("SATISFACTION", "만족");
define("NORMAL", "보통");
define("UNSATISFACTION", "불만족");
define("NO_ANSWER", "무응답");

//
//     ENV : manage support > type
//
define("TROUBLE_TYPE_MAX", 10);

define("TROUBLE_NAME_MAX_LENGTH", 40);
define("TROUBLE_NAME_MAX_LENGTH_KR", 13);
define("TROUBLE_NAME_MAX_LENGTH_EN", 40);

define("TROUBLE_NAME_NOT_SELECTED", "Not Selected");

//
//     ENV : setting > site
//
define("LOGO_FILE_LIMIT", 1000);
define("LOGO_FILE_SIZE", "" . (1024 * LOGO_FILE_LIMIT));
define("LOGO_FILE_TYPE_1", "jpg");
define("LOGO_FILE_TYPE_2", "gif");
define("LOGO_FILE_TYPE_3", "jpeg");
define("LOGO_FILE_TYPE_4", "png");
define("LOGO_FILE_TYPE", "image/pjpeg" . "," . "image/".LOGO_FILE_TYPE_2 . "," . "image/".LOGO_FILE_TYPE_3. "," . "image/".LOGO_FILE_TYPE_4);
define("LOGO_IMG_WIDTH", 262);
define("LOGO_IMG_HEIGHT", 74);
define("LOGO_IMG_RESIZE", FALSE);
