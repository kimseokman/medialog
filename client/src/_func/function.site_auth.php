<?php
/**
 * site_auth.php
 * 2015.11.03 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$config_path = array(
	"AUTH_SUCCESS" => SETTING_SITE_INDEX,
	"AUTH_FAIL" => SETTING_SITE_INDEX_AUTH
);

$password = $_POST['site_auth_password'];
$id = $_POST['adid'];

$access_log = $main_obj->LoginAdmin($id, $password);

if($access_log == "ADMIN"){//case by login success admin
	$_SESSION["site_auth"] = "PASS";
?>
<form name="auth_success_form" method="post" action="<? echo $config_path['AUTH_SUCCESS']; ?>">
</form>
<script>
	document.auth_success_form.submit();
</script>
<?
}
else{
	$_SESSION["site_auth"] = "NON_PASS";
	$msg = "비밀번호가 일치하지 않습니다.";
?>
<form name="auth_fail_form" method="post" action="<? echo $config_path['AUTH_FAIL']; ?>">
	<input type="hidden" name="msg" value="<? echo $msg; ?>" />
</form>
<script>
	document.auth_fail_form.submit();
</script>
<?
}