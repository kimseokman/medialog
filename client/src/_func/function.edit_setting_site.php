<?php
/**
 * edit_setting_site.php
 * 2015.08.19 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$edit_admin_name = $_POST["name"];
$edit_admin_company = $_POST["company"];
$edit_admin_mail = $_POST['email'];
$edit_admin_phone = $_POST['phone'];
$edit_admin_tel = $_POST['tel'];

$mail_addr_max_len = MAIL_ADDR_MAX_LENGTH;

$logo_file_limit = LOGO_FILE_LIMIT;
$logo_file_type1 = LOGO_FILE_TYPE_1;
$logo_file_type2 = LOGO_FILE_TYPE_2;
$logo_file_type3 = LOGO_FILE_TYPE_3;
$logo_file_type4 = LOGO_FILE_TYPE_4;

$pop_action_open = 'open';
$pop_action_close = 'close';
$log_action_show = 'show';
$log_action_hide = 'hide';

$config_path = array(
	"EDIT_SUCCESS" => SETTING_SITE_INDEX,
	"EDIT_FAIL" => SETTING_SITE_INDEX
);

$config_msg = array(
	"EMPTY_COMPANY" => "회사명을 입력해 주세요.",
	"EMPTY_NAME" => "담당자명을 입력해 주세요.",
	"FILE_SIZE_OVERFLOW" => "파일크기는 최대 ".$logo_file_limit."Kb 까지 가능합니다.",
	"FILE_TYPE_ERROR" => "파일형식은 ".$logo_file_type1.", ".$logo_file_type2.", ".$logo_file_type3.", ".$logo_file_type4." 만 가능합니다.",
	"UPLOAD_FAIL" => "파입업로드를 실패하였습니다.",
	"EMPTY_MAIL" => "이메일을 입력해주세요.",
	"INVALIDE_MAIL" => "이메일 입력 양식이 틀립니다.",
	"LONG_MAIL" => $mail_addr_max_len . "자 이내의 이메일 주소를 사용해주세요.",
	"NOADMIN" => "존재하지 않는 관리자입니다.",
	"EMPTY_MAIL" => "이메일을 입력해주세요.",
	"EMPTY_TEL" => "연락처를 입력해주세요.",
	"EMPTY_PHONE" => "연락처를 입력해주세요.",
	"EDIT_OK" => "저장이 완료되었습니다."
);

$mail_flag = $utils_obj->IsValidEmail($edit_admin_mail);

$logo_image =$_FILES["logo_url"]["name"];
$uploadedfile = $_FILES['logo_url']['tmp_name'];

if ( $logo_image ){
	$limit['size'] = LOGO_FILE_SIZE;
	$limit['type'] = LOGO_FILE_TYPE;

	$filename = stripslashes($_FILES['logo_url']['name']);
        	$extension = $utils_obj->GetExtension($filename);
        	
	$file_src = $_SERVER["DOCUMENT_ROOT"] . CUSTOMER_LOGO_DIR . "/";
	$imgname = explode(".",$_FILES['logo_url']['name']);
	$imgname = $imgname[count($imgname)-1];
	$imgname = $_SESSION['adid']."_".$_SESSION['adnum'].".".$imgname;

	$filetype = explode(",",$limit['type']);
	if ($_FILES['logo_url']['size'] > $limit['size']){
	?>
	<form name="editSettingSiteResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
		<input type="hidden" name="setting_site_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
		<input type="hidden" name="setting_site_edit_res_log_action" value="<? echo $log_action_hide; ?>" />
		<input type="hidden" name="setting_site_edit_res_popup_msg" value="<? echo $config_msg['FILE_SIZE_OVERFLOW']; ?>" />
	</form>
	<script>
		document.editSettingSiteResForm.submit();
	</script>
	<?
	}
	else {
		$flag = 0;
		for ($i=0;$i<count($filetype);$i++){
			if ($filetype[$i] == $_FILES["logo_url"]["type"]) $flag = 1;
		}

		if ($flag == 0){
		?>
		<form name="editSettingSiteResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
			<input type="hidden" name="setting_site_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
			<input type="hidden" name="setting_site_edit_res_log_action" value="<? echo $log_action_hide; ?>" />
			<input type="hidden" name="setting_site_edit_res_popup_msg" value="<? echo $config_msg['FILE_TYPE_ERROR']; ?>" />
		</form>
		<script>
			document.editSettingSiteResForm.submit();
		</script>
		<?
		}
		else{
			if (move_uploaded_file($uploadedfile, $file_src.$imgname)) {
				//echo "파일이 유효하고, 성공적으로 업로드 되었습니다.\n";
				//resize
				if(LOGO_IMG_RESIZE){
					list($width,$height) = getimagesize($uploadedfile);//original file

					$newwidth = LOGO_IMG_WIDTH;
					$newheight = LOGO_IMG_HEIGHT;
					if( ($width != $newwidth) || ($height != $newheight) ){
						$installGD = get_extension_funcs("gd");
						if(!$installGD){
							//echo "uninstalled GD";	
						}
						
						$path = $file_src.$imgname;//new image
						$canvas = imagecreatetruecolor($newwidth,$newheight);//지정된 크기의 검은색 이미지 만듬
						if($extension==LOGO_FILE_TYPE_1 || $extension==LOGO_FILE_TYPE_3){//JPG, JPEG
							$new_img = imagecreatefromjpeg($uploadedfile);
						}
						else if($extension==LOGO_FILE_TYPE_2){//GIF
							$new_img = imagecreatefromgif($uploadedfile);	
						}
						else if($extension==LOGO_FILE_TYPE_4){//PNG
							$new_img = imagecreatefrompng($uploadedfile);	
						}
						imagecopyresampled($canvas, $new_img, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

						//save
						if($extension==LOGO_FILE_TYPE_1 || $extension==LOGO_FILE_TYPE_3){//JPG, JPEG
							imagejpeg($canvas, $path);
						}
						else if($extension==LOGO_FILE_TYPE_2){//GIF
							imagegif($canvas, $path);	
						}
						else if($extension==LOGO_FILE_TYPE_4){//PNG
							imagepng($canvas, $path);	
						}

						imagedestroy($path);
						imagedestroy($canvas);
					}
				}
			} 
			else {
			?>
			<form name="editSettingSiteResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
				<input type="hidden" name="setting_site_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
				<input type="hidden" name="setting_site_edit_res_log_action" value="<? echo $log_action_hide; ?>" />
				<input type="hidden" name="setting_site_edit_res_popup_msg" value="<? echo $config_msg['UPLOAD_FAIL']; ?>" />
			</form>
			<script>
				document.editSettingSiteResForm.submit();
			</script>
			<?
			}
		}
	}

}
else {
	$admin = $main_obj->GetAdminInfo();
	$imgname = $admin->logo_url;
}


if(strlen($edit_admin_company) == 0){
?>
<form name="editSettingSiteResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="setting_site_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="setting_site_edit_res_log_action" value="<? echo $log_action_hide; ?>" />
	<input type="hidden" name="setting_site_edit_res_log" value="EMPTY_COMPANY" />
	<input type="hidden" name="setting_site_edit_res_popup_msg" value="<? echo $config_msg['EMPTY_COMPANY']; ?>" />
</form>
<script>
	document.editSettingSiteResForm.submit();
</script>
<?
}
else if(strlen($edit_admin_name) == 0){
?>
<form name="editSettingSiteResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="setting_site_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="setting_site_edit_res_log_action" value="<? echo $log_action_hide; ?>" />
	<input type="hidden" name="setting_site_edit_res_log" value="EMPTY_NAME" />
	<input type="hidden" name="setting_site_edit_res_popup_msg" value="<? echo $config_msg['EMPTY_NAME']; ?>" />
</form>
<script>
	document.editSettingSiteResForm.submit();
</script>
<?
}
else if(strlen($edit_admin_mail) == 0){
?>
<form name="editSettingSiteResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="setting_site_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="setting_site_edit_res_log_action" value="<? echo $log_action_hide; ?>" />
	<input type="hidden" name="setting_site_edit_res_log" value="EMPTY_MAIL" />
	<input type="hidden" name="setting_site_edit_res_popup_msg" value="<? echo $config_msg['EMPTY_MAIL']; ?>" />
</form>
<script>
	document.editSettingSiteResForm.submit();
</script>
<?
}
else if(strlen($edit_admin_mail) > $mail_addr_max_len){
?>
<form name="editSettingSiteResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="setting_site_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="setting_site_edit_res_log_action" value="<? echo $log_action_hide; ?>" />
	<input type="hidden" name="setting_site_edit_res_log" value="LONG_MAIL" />
	<input type="hidden" name="setting_site_edit_res_popup_msg" value="<? echo $config_msg['LONG_MAIL']; ?>" />
</form>
<script>
	document.editSettingSiteResForm.submit();
</script>
<?	
}
else if(!$mail_flag){
?>
<form name="editSettingSiteResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="setting_site_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="setting_site_edit_res_log_action" value="<? echo $log_action_hide; ?>" />
	<input type="hidden" name="setting_site_edit_res_log" value="INVALIDE_MAIL" />
	<input type="hidden" name="setting_site_edit_res_popup_msg" value="<? echo $config_msg['INVALIDE_MAIL']; ?>" />
</form>
<script>
	document.editSettingSiteResForm.submit();
</script>
<?
}
else if(strlen($edit_admin_tel) == 0){
?>
<form name="editSettingSiteResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="setting_site_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="setting_site_edit_res_log_action" value="<? echo $log_action_hide; ?>" />
	<input type="hidden" name="setting_site_edit_res_log" value="EMPTY_TEL" />
	<input type="hidden" name="setting_site_edit_res_popup_msg" value="<? echo $config_msg['EMPTY_TEL']; ?>" />
</form>
<script>
	document.editSettingSiteResForm.submit();
</script>
<?	
}
else if(strlen($edit_admin_phone) == 0){
?>
<form name="editSettingSiteResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="setting_site_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="setting_site_edit_res_log_action" value="<? echo $log_action_hide; ?>" />
	<input type="hidden" name="setting_site_edit_res_log" value="EMPTY_PHONE" />
	<input type="hidden" name="setting_site_edit_res_popup_msg" value="<? echo $config_msg['EMPTY_PHONE']; ?>" />
</form>
<script>
	document.editSettingSiteResForm.submit();
</script>
<?	
}
else{
	$admindata = array(
		"name"=>$edit_admin_name, 
		"company"=>$edit_admin_company, 
		"url"=>$admin->url, 
		"email"=>$edit_admin_mail, 
		"phone"=>$edit_admin_phone, 
		"tel"=>$edit_admin_tel,
		"logo_url"=>$imgname
	);

	$res = $main_obj->ModifyAdminInfo($admindata);
	if($res == "NOADMIN") {// 수정하려는 어드민이 존재하지 않을 경우
	?>
	<form name="editSettingSiteResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
		<input type="hidden" name="setting_site_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
		<input type="hidden" name="setting_site_edit_res_log_action" value="<? echo $log_action_hide; ?>" />
		<input type="hidden" name="setting_site_edit_res_log_msg" value="<? echo $config_msg['NOADMIN']; ?>" />
	</form>
	<script>
		document.editSettingSiteResForm.submit();
	</script>
	<?	
	}		
	else {// 성공적으로 수정. 메시지 띄운 후 이동
	?>
	<form name="editSettingSiteResForm" method="post" action="<? echo $config_path['EDIT_SUCCESS']; ?>">
		<input type="hidden" name="setting_site_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
		<input type="hidden" name="setting_site_edit_res_popup_msg" value="<? echo $config_msg['EDIT_OK']; ?>" />
		<input type="hidden" name="setting_site_edit_res_log_action" value="<? echo $log_action_hide; ?>" />
	</form>
	<script>
		document.editSettingSiteResForm.submit();
	</script>
	<?
	}
}