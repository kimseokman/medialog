<?php
/**
 * create_support_type.php
 * 2015.08.18 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$config_path = array(
	"CREATE_SUCCESS" => MANAGE_SUPPORT_TYPE_INDEX,
	"CREATE_FAIL" => MANAGE_SUPPORT_TYPE_INDEX
);

$main_obj->CreateTroubleType();
?>
<form name="createSupportTypeForm" method="post" action="<? echo $config_path['CREATE_SUCCESS']; ?>">
</form>
<script>
	document.createSupportTypeForm.submit();
</script>