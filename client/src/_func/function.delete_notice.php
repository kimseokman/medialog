<?php
/**
 * delete_noitce.php
 * 2015.09.03 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$seq = $_POST['seq'];

$config_path = array(
	"DEL_SUCCESS" => SUPER_GNB_SERVICE_NOTICE_INDEX,
	"DEL_FAIL" => SUPER_GNB_SERVICE_NOTICE_INDEX
);

$config_msg = array(
	"DEL_OK" => "삭제가 완료되었습니다.",
	"NO_NOTICE" => "존재하지 않습니다."
);

$pop_action_open = 'open';
$pop_action_close = 'close';

$res = $main_obj->DeleteNotice($seq);

if($res == "OK"){
?>
<form name="delNoticeResForm" method="post" action="<? echo $config_path['DEL_SUCCESS']; ?>">
	<input type="hidden" name="notice_del_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="notice_del_res_popup_msg" value="<? echo $config_msg['DEL_OK']; ?>" />
</form>
<script>
	document.delNoticeResForm.submit();
</script>
<?
}
else if($res == "NO_NOTICE"){
?>
<form name="delNoticeResForm" method="post" action="<? echo $config_path['DEL_FAIL']; ?>">
	<input type="hidden" name="notice_del_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="notice_del_res_popup_msg" value="<? echo $config_msg['NO_NOTICE']; ?>" />
</form>
<script>
	document.delNoticeResForm.submit();
</script>
<?
}