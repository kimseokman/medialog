<?php
/**
 * delete_dept.php
 * 2015.08.05 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$dept_num = $_POST['dept_num'];
$page = $_POST['page'];

$pop_action_open = 'open';
$pop_action_close = 'close';

$config_path = array(
	"DEL_SUCCESS" => MANAGE_AGENT_DEPARTMENT_INDEX,
	"DEL_FAIL" => MANAGE_AGENT_DEPARTMENT_INDEX
);

$config_msg = array(
	"DEL_OK" => "삭제가 완료되었습니다.",
	"EXIST_SUPPORTER" => "부서에 소속된 아이디가 있습니다.<br />아이디를 다른 부서로 이관하세요.",
	"NOT_EXIST_DEPT" => "존재하지 않는 부서명 입니다."
);

if($main_obj->GetDepByDepnum($dept_num) == NULL) {//case by do not exist department
?>
<form name="delDeptResForm" method="post" action="<? echo $config_path['DEL_FAIL']; ?>">
	<input type="hidden" name="dept_del_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="dept_del_res_popup_msg" value="<? echo $config_msg['NOT_EXIST_DEPT']; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
</form>
<script>
	document.delDeptResForm.submit();
</script>
<?
}
else if($main_obj->GetSupporterNumByDepnum($dept_num) != 0) {//case by exist servant supporter
?>
<form name="delDeptResForm" method="post" action="<? echo $config_path['DEL_FAIL']; ?>">
	<input type="hidden" name="dept_del_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="dept_del_res_popup_msg" value="<? echo $config_msg['EXIST_SUPPORTER']; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
</form>
<script>
	document.delDeptResForm.submit();
</script>
<?
}
else{//case by try to delete department
	$main_obj->DeleteDep($dept_num);
	$dept_total_size = count($main_obj->GetDepList());

	if($dept_total_size <= 10){//2번째 페이지에 자료가 남아있지 않을 경우 페이지 1 내려감
		$page--;
	}
	else if($dept_total_size <= 0){//자료가 하나도 없을 경우 1로 초기화
		$page = 1;	
	}
?>
<form name="delDeptResForm" method="post" action="<? echo $config_path['DEL_SUCCESS']; ?>">
	<input type="hidden" name="dept_del_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="dept_del_res_popup_msg" value="<? echo $config_msg['DEL_OK']; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
</form>
<script>
	document.delDeptResForm.submit();
</script>
<?
}