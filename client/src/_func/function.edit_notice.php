<?php
/**
 * edit_notice.php
 * 2015.09.03 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$notice_seq = $_POST['seq'];
$notice_title = $_POST['title'];
$notice_name = $_POST['name'];
$notice_content = $_POST['content'];

$remember_page = $_POST['remember_page'];
$remember_type = $_POST['remember_type'];
$remember_val = $_POST['remember_val'];

$config_path = array(
	"EDIT_SUCCESS" => SUPER_GNB_SERVICE_NOTICE . "/info.php"
);

$res = $main_obj->ModifyNotice($notice_seq, $notice_title, $notice_name, $notice_content);

if($res == "OK"){
?>
<form name="editNoticeResForm" method="post" action="<? echo $config_path['EDIT_SUCCESS']; ?>">
	<input type="hidden" name="seq" value="<? echo $notice_seq; ?>" />
	<input type="hidden" name="remember_page" value="<? echo $remember_page; ?>" />
	<input type="hidden" name="remember_type" value="<? echo $remember_type; ?>" />
	<input type="hidden" name="remember_val" value="<? echo $remember_val; ?>" />
</form>
<script>
	document.editNoticeResForm.submit();
</script>
<?
}