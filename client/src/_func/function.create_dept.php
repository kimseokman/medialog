<?php
/**
 * create_dept.php
 * 2015.08.05 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$dept_name = $_POST['dept_name'];
$dept_memo = $_POST['desc'];

$dept_name_max = DEPT_NAME_MAX_LENGTH;
$dept_name_max_kr = DEPT_NAME_MAX_LENGTH_KR;
$dept_name_max_en = DEPT_NAME_MAX_LENGTH_EN;
$dept_memo_max = DEPT_MEMO_MAX_LENGTH;
$dept_memo_max_kr = DEPT_MEMO_MAX_LENGTH_KR;
$dept_memo_max_en = DEPT_MEMO_MAX_LENGTH_EN;

$pop_action_open = 'open';
$pop_action_close = 'close';

$config_path = array(
	"CREATE_SUCCESS" => MANAGE_AGENT_DEPARTMENT_INDEX,
	"CREATE_FAIL" => MANAGE_AGENT_DEPARTMENT_INDEX
);

$config_msg = array(
	"EMPTY_DEPT_NAME" => "부서 이름을 입력해주세요.",
	"LONG_DEPT_NAME" => "부서 이름을 한글 ".$dept_name_max_kr."자<br /> 영문 ".$dept_name_max_en."자 이내로 입력해주세요.",
	"DEPT_NAME_DUPLICATE" => "중복된 부서명 입니다.",
	"LONG_DEPT_MEMO" => "부서 설명은 한글 ".$dept_memo_max_kr."자<br /> 영문 ".$dept_memo_max_en."자 이내로 입력해주세요.",
	"OK" => "생성이 완료되었습니다."
);

if(strlen($dept_name) == 0) {//case by department name is empty
?>
<form name="createDeptResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="dept_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="dept_add_res_popup_msg" value="<? echo $config_msg['EMPTY_DEPT_NAME']; ?>" />
</form>
<script>
	document.createDeptResForm.submit();
</script>
<?
}
else if(strlen($dept_name) > $dept_name_max) {//case by department name is too long
?>
<form name="createDeptResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="dept_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="dept_add_res_popup_msg" value="<? echo $config_msg['LONG_DEPT_NAME']; ?>" />
</form>
<script>
	document.createDeptResForm.submit();
</script>
<?
}
else if(strlen($dept_memo) > $dept_memo_max) {//case by department memo is too long
?>
<form name="createDeptResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="dept_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="dept_add_res_popup_msg" value="<? echo $config_msg['LONG_DEPT_MEMO']; ?>" />
</form>
<script>
	document.createDeptResForm.submit();
</script>
<?
}
else {//case by try to create department
	$create_res = $main_obj->CreateDep($dept_name, $dept_memo);

	if($create_res == 'OK'){
	?>
		<form name="createDeptResForm" method="post" action="<? echo $config_path['CREATE_SUCCESS']; ?>">
			<input type="hidden" name="dept_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
			<input type="hidden" name="dept_add_res_popup_label" value="OK" />
			<input type="hidden" name="dept_add_res_popup_msg" value="<? echo $config_msg['OK']; ?>" />
		</form>
		<script>
			document.createDeptResForm.submit();
		</script>
	<?	
	}
	else if($create_res == 'DEPNAMEALREADYEXISTS'){
	?>
		<form name="createDeptResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
			<input type="hidden" name="dept_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
			<input type="hidden" name="dept_add_res_popup_msg" value="<? echo $config_msg['DEPT_NAME_DUPLICATE']; ?>" />
		</form>
		<script>
			document.createDeptResForm.submit();
		</script>
	<?	
	}
}