<?php
/**
 * edit_dept.php
 * 2015.08.05 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$dept_num = $_POST['dept_num'];
$dept_name = $_POST['dept_name'];
$dept_memo = $_POST['dept_memo'];
$page = $_POST['page'];
$act_mode = $_POST['act_mode'];
$act_dept_num = $_POST['act_dept_num'];

$dept_name_max = DEPT_NAME_MAX_LENGTH;
$dept_name_max_kr = DEPT_NAME_MAX_LENGTH_KR;
$dept_name_max_en = DEPT_NAME_MAX_LENGTH_EN;
$dept_memo_max = DEPT_MEMO_MAX_LENGTH;
$dept_memo_max_kr = DEPT_MEMO_MAX_LENGTH_KR;
$dept_memo_max_en = DEPT_MEMO_MAX_LENGTH_EN;

$pop_action_open = 'open';
$pop_action_close = 'close';

$config_path = array(
	"EDIT_SUCCESS" => MANAGE_AGENT_DEPARTMENT_INDEX,
	"EDIT_FAIL" => MANAGE_AGENT_DEPARTMENT_INDEX
);

$config_msg = array(
	"EMPTY_DEPT_NAME" => "부서 이름을 입력해주세요.",
	"LONG_DEPT_NAME" => "부서 이름을 한글 ".$dept_name_max_kr."자<br /> 영문 ".$dept_name_max_en."자 이내로 입력해주세요.",
	"DEPT_NAME_DUPLICATE" => "중복된 부서명 입니다.",
	"LONG_DEPT_MEMO" => "부서 설명은 한글 ".$dept_memo_max_kr."자<br /> 영문 ".$dept_memo_max_en."자 이내로 입력해주세요.",
	"NOT_EXIST_DEPT" => "존재하지 않는 부서명 입니다."
);

if(strlen($dept_name) == 0) {//case by department name is empty
?>
<form name="editDeptResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="dept_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="dept_edit_res_popup_msg" value="<? echo $config_msg['EMPTY_DEPT_NAME']; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="act_mode" value="<? echo $act_mode; ?>" />
	<input type="hidden" name="act_dept_num" value="<? echo $act_dept_num; ?>" />
</form>
<script>
	document.editDeptResForm.submit();
</script>
<?
}
else if(strlen($dept_name) > $dept_name_max) {//case by department name is too long
?>
<form name="editDeptResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="dept_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="dept_edit_res_popup_msg" value="<? echo $config_msg['LONG_DEPT_NAME']; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="act_mode" value="<? echo $act_mode; ?>" />
	<input type="hidden" name="act_dept_num" value="<? echo $act_dept_num; ?>" />
</form>
<script>
	document.editDeptResForm.submit();
</script>
<?
}
else if(strlen($dept_memo) > $dept_memo_max) {//case by department memo is too long
?>
<form name="editDeptResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="dept_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="dept_edit_res_popup_msg" value="<? echo $config_msg['LONG_DEPT_MEMO']; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="act_mode" value="<? echo $act_mode; ?>" />
	<input type="hidden" name="act_dept_num" value="<? echo $act_dept_num; ?>" />
</form>
<script>
	document.editDeptResForm.submit();
</script>
<?
}
else if($main_obj->GetDepByDepnum($dept_num) == NULL) { //case by do not exist dept
?>
<form name="editDeptResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="dept_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="dept_edit_res_popup_msg" value="<? echo $config_msg['NOT_EXIST_DEPT']; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="act_mode" value="<? echo $act_mode; ?>" />
	<input type="hidden" name="act_dept_num" value="<? echo $act_dept_num; ?>" />
</form>
<script>
	document.editDeptResForm.submit();
</script>
<?
}
else {//case by try to edit department
	$edit_res = $main_obj->EditDeptInfo($dept_num, $dept_name, $dept_memo);

	if($edit_res =='OK'){//edit mode reset
		$act_mode = 'NORMAL';
		$act_dept_num = 0;
	?>
	<form name="editDeptResForm" method="post" action="<? echo $config_path['EDIT_SUCCESS']; ?>">
		<input type="hidden" name="dept_edit_res_popup_action" value="<? echo $pop_action_close; ?>" />
		<input type="hidden" name="page" value="<? echo $page; ?>" />
		<input type="hidden" name="act_mode" value="<? echo $act_mode; ?>" />
		<input type="hidden" name="act_dept_num" value="<? echo $act_dept_num; ?>" />
	</form>
	<script>
		document.editDeptResForm.submit();
	</script>
	<?
	}
	else if($edit_res =='INVALIDDEP'){
	?>
	<form name="editDeptResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
		<input type="hidden" name="dept_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
		<input type="hidden" name="dept_edit_res_popup_msg" value="<? echo $config_msg['NOT_EXIST_DEPT']; ?>" />
		<input type="hidden" name="page" value="<? echo $page; ?>" />
		<input type="hidden" name="act_mode" value="<? echo $act_mode; ?>" />
		<input type="hidden" name="act_dept_num" value="<? echo $act_dept_num; ?>" />
	</form>
	<script>
		document.editDeptResForm.submit();
	</script>
	<?	
	}
}