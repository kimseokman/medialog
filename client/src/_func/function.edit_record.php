<?php
/**
 * edit_record.php
 * 2015.08.25 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$sptid = $_POST['sptid'];
$pw = APP_COWORK_LOGIN_PW;
$report_num = $_POST['report_num'];
$cus_name = $_POST['cus_name'];
$cus_tel = $_POST['cus_tel'];
$cus_mail = $_POST['cus_mail'];
$cus_support_type = $_POST['cus_support_type'];
$cus_conts = $_POST['cus_conts'];
$choice_list = $_POST['choice_list'];

$main_obj->HistoryLogin($sptid, $pw);
$app_login_res = $main_obj->HistoryConfirmLogin();

if($app_login_res != "OK"){
?>
<form name="redirectBanForm" method="post" action="<? echo $config_path["NO_AUTH"]; ?>">
</form>
<script>
	document.redirectBanForm.submit();
</script>
<?	
}//end of : if($app_login_res == "")

$name_max_len = NAME_MAX_LENGTH;
$name_max_len_kr = NAME_MAX_LENGTH_KR;
$name_max_len_en = NAME_MAX_LENGTH_EN;
$phone_max_len = PHONE_MAX_LENGTH;
$mail_max_len = MAIL_ADDR_MAX_LENGTH;
$record_max_len = RECORD_MAX_LENGTH;
$record_max_len_kr = RECORD_MAX_LENGTH_KR;
$record_max_len_en = RECORD_MAX_LENGTH_EN;

$pop_action_open = 'open';
$pop_action_close = 'close';

$config_path = array(
	"EDIT_SUCCESS" => APP_RECORD . "/info.php",
	"EDIT_FAIL" => APP_RECORD . "/info.php"
);

$config_msg = array(
	"LONG_NAME" => "이름은 한글 ".$name_max_len_kr."자, 영문 ".$name_max_len_en."자 이내로 입력해주세요.",
	"LONG_PHONE" => "전화번호는 ".$phone_max."자 이내로 입력해주세요.",
	"LONG_EMAIL" => "이메일 주소 길이는 ".$mail_max."글자 이내여야 합니다.",
	"LONG_RECORD" => "내용은 ".$record_max_len."자를 넘길 수 없습니다. 한글 ".$record_max_len_kr."자, 영문 ".$record_max_len_en."자 이내로 입력해주세요.",
	"NO_RECORD" => "존재하지 않는 기록입니다. 다시 시도해주세요.",
	"OK" =>"지원 기록이 저장되었습니다."
);

if(strlen($cus_name) > $name_max_len) {// 이름의 길이가 30자가 넘을 경우 에러
?>
<form name="editRecordResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="record_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="record_edit_res_popup_msg" value="<? echo $config_msg['LONG_NAME']; ?>" />
	<input type="hidden" name="sptid" value="<? echo $sptid; ?>" />
	<input type="hidden" name="report_num" value="<? echo $report_num; ?>" />
</form>
<script>
	document.editRecordResForm.submit();
</script>
<?
}
else if(strlen($cus_tel) > $phone_max_len) {// 전화번호가 20자를 넘을 경우 에러
?>
<form name="editRecordResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="record_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="record_edit_res_popup_msg" value="<? echo $config_msg['LONG_PHONE']; ?>" />
	<input type="hidden" name="sptid" value="<? echo $sptid; ?>" />
	<input type="hidden" name="report_num" value="<? echo $report_num; ?>" />
</form>
<script>
	document.editRecordResForm.submit();
</script>
<?
}
else if(strlen($cus_mail) > $mail_max_len) {// 이메일 주소가 50자를 넘을 경우 에러
?>
<form name="editRecordResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="record_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="record_edit_res_popup_msg" value="<? echo $config_msg['LONG_EMAIL']; ?>" />
	<input type="hidden" name="sptid" value="<? echo $sptid; ?>" />
	<input type="hidden" name="report_num" value="<? echo $report_num; ?>" />
</form>
<script>
	document.editRecordResForm.submit();
</script>
<?
}
else if(strlen($cus_conts) > $record_max_len){
?>
<form name="editRecordResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="record_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="record_edit_res_popup_msg" value="<? echo $config_msg['LONG_RECORD']; ?>" />
	<input type="hidden" name="sptid" value="<? echo $sptid; ?>" />
	<input type="hidden" name="report_num" value="<? echo $report_num; ?>" />
</form>
<script>
	document.editRecordResForm.submit();
</script>
<?
}
else{
	$oper_log = 0;

	switch($choice_list){
		case RESOLVE: $oper_log = 1; break;
		case HOLD: $oper_log = 2; break;
		case NEED_DIRECT_SUPPORT: $oper_log = 3; break;
		default:
			$oper_log = 0;
	}

	$report_arr = array(
		"report_num" => $report_num,	
		"cus_name" => $cus_name,
		"cus_tel" => $cus_tel,
		"cus_mail" => $cus_mail,
		"cus_support_type" => $cus_support_type,
		"cus_conts" => $cus_conts,
		"oper_log" => $oper_log
	);

	$modify_res = $main_obj->HistoryModifyReport($report_arr);

	if($modify_res == "NOREPORT"){
	?>
		<form name="editRecordResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
			<input type="hidden" name="record_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
			<input type="hidden" name="record_edit_res_popup_msg" value="<? echo $config_msg['NO_RECORD']; ?>" />
			<input type="hidden" name="sptid" value="<? echo $sptid; ?>" />
			<input type="hidden" name="report_num" value="<? echo $report_num; ?>" />
		</form>
		<script>
			//document.editRecordResForm.submit();
		</script>
	<?
	}
	else{
	?>
		<form name="editRecordResForm" method="post" action="<? echo $config_path['EDIT_SUCCESS']; ?>">
			<input type="hidden" name="record_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
			<input type="hidden" name="record_edit_res_popup_msg" value="<? echo $config_msg['OK']; ?>" />
			<input type="hidden" name="sptid" value="<? echo $sptid; ?>" />
			<input type="hidden" name="report_num" value="<? echo $report_num; ?>" />
		</form>
		<script>
			document.editRecordResForm.submit();
		</script>
	<?
	}
}