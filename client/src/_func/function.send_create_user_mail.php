<?php
/**
 * send_create_user_mail.php
 * 2015.08.13 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
require($_SERVER['DOCUMENT_ROOT']."/_lib/_inc/phpmailer/class.phpmailer.php");

$config_path = array(
	"CREATE_SUCCESS" => MANAGE_AGENT_USER_INDEX
);

$user_add_res_popup_action = $_POST['user_add_res_popup_action'];
$user_add_res_popup_msg = $_POST['user_add_res_popup_msg'];
$user_id = $_POST['input_spt_id'];
$user_pw = $_POST['input_spt_pw'];
$user_mail = $_POST['input_spt_mail'];
$input_res = $_POST['input_res'];

//
//	sender
//
$from = ML_MAIL_ADDR;
$from_name = ML_MAIL_NAME;
//
//	recipient
//
$to = $user_mail;
$to_name = $user_id;

//
//	mail contents data
//
$home_url = REMOTE_HOME;
$img_url = ML_URL.IMG_DIR;
$show_id = $user_id;
$show_pw = $user_pw;
$login_url = ML_URL.LOGIN_TRY;
$contact_us = CONTACT_US_TEL;

//
//	mail setting
//
$mail = new PHPMailer(); 
$mail->IsSMTP(); // send via SMTP
$mail->Host = ML_MAIL_HOST;
$mail->Port = ML_MAIL_PORT;
$mail->Mailer = ML_MAILER;
$mail->CharSet = ML_MAIL_CHARSET;
$mail->SMTPAuth = true; // turn on SMTP authentication
$mail->Username = ML_MAIL_ID; // SMTP username
$mail->Password = ML_MAIL_PW;// SMTP password

//
//	mail body
//
$mail->Subject = "[ ".SERVICE_NAME." ] 사용자 계정 생성 안내";
$message = file_get_contents($_SERVER['DOCUMENT_ROOT'].MAIL_BODY_CREATE_USER);
$message = str_replace('<? echo $home_url; ?>', $home_url, $message); 
$message = str_replace('<? echo $img_url; ?>', $img_url, $message);
$message = str_replace('<? echo $contact_us; ?>', $contact_us, $message);
$message = str_replace('<? echo $show_id; ?>', $show_id, $message);
$message = str_replace('<? echo $show_pw; ?>', $show_pw, $message);
$message = str_replace('<? echo $login_url; ?>', $login_url, $message);
$mail->MsgHTML($message);

//
//	mail ready
//
$mail->From = $from;
$mail->FromName = $from_name;
$mail->AddAddress($to, $to_name);
$mail->AddReplyTo($from, $from_name);
$mail->WordWrap = 50; // set word wrap
$mail->IsHTML(TRUE); // send as HTML

//
//	send mail
//
$mail_res = $mail->Send();

?>
<form name="createUserSendMailForm" method="post" action="<? echo $config_path['CREATE_SUCCESS']; ?>">
	<input type="hidden" name="user_add_res_popup_action" value="<? echo $user_add_res_popup_action; ?>" />
	<input type="hidden" name="user_add_res_popup_msg" value="<? echo $user_add_res_popup_msg; ?>" />
	<input type="hidden" name="input_res" value="<? echo $input_res; ?>" />
</form>
<script>
	document.createUserSendMailForm.submit();
</script>