<?php
/**
 * create_notice.php
 * 2015.09.03 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$title = $_POST['title'];
$name = $_POST['name'];
$content = $_POST['content'];

$pop_action_open = 'open';
$pop_action_close = 'close';

$config_path = array(
	"CREATE_SUCCESS" => SUPER_GNB_SERVICE_NOTICE . "/info.php"
);

$config_msg = array(
	"OK"=>"저장이 완료되었습니다."
);

$res = $main_obj->SetNotice($title, $name, $content);

if($res == "OK"){
	$seq = $main_obj->GetNoticeLastest();
?>
<form name="createNoticeResForm" method="post" action="<? echo $config_path['CREATE_SUCCESS']; ?>">
	<input type="hidden" name="seq" value="<? echo $seq; ?>" />
</form>
<script>
	document.createNoticeResForm.submit();
</script>
<?
}