<?php
/**
 * edit_user.php
 * 2015.08.13 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$sptnum = $_POST['sptnum'];
$page = $_POST['page'];
$search = $_POST['search'];
$searchVal = $_POST['searchVal'];

$edit_user_name = $_POST['edit_user_name'];

$edit_user_pw = $_POST['edit_user_pw'];
$edit_user_pw2 = $_POST['edit_user_pw2'];
$is_password = FALSE;

if($edit_user_pw != NULL){
	$is_password = TRUE;
}

$edit_user_depnum = $_POST['edit_user_depnum'];
$edit_user_tel = $_POST['edit_user_tel'];
$edit_user_mail = $_POST['edit_user_mail'];

$func_arr = $_POST['func'];
$mobile_func = $_POST['mobile_func'];
$current_mobile_func = $_POST['current_mobile_func'];
$sett_arr = $_POST['sett'];
$mob_arr = $_POST['mob'];

$name_max_len = NAME_MAX_LENGTH;
$name_max_len_kr = NAME_MAX_LENGTH_KR;
$name_max_len_en = NAME_MAX_LENGTH_EN;
$phone_max_len = PHONE_MAX_LENGTH;
$mail_max_len = MAIL_ADDR_MAX_LENGTH;
$pw_length = PASSWORD_LENGTH;
$pw_length_min = PASSWORD_LENGTH_MIN;
$pw_length_max = PASSWORD_LENGTH_MAX;
$pw_safe_cnt = PASSWORD_SAFE_CNT;

$pop_action_open = 'open';
$pop_action_close = 'close';

$config_path = array(
	"EDIT_SUCCESS" => MANAGE_AGENT_USER."/info.php",
	"EDIT_FAIL" => MANAGE_AGENT_USER."/info.php"
);

$config_msg = array(
	"NO_AUTH" => "권한이 없습니다.",
	"EMPTY_ID" => "ID를 입력해 주세요.",
	"OVER_FLOW" => "모바일 지원 상담원 생성 수를 초과하였습니다.",
	"NO_SUPPORTER" => "존재하지 않는 사용자 입니다.",
	"EMPTY_NAME" => "이름을 입력해주세요.",
	"LONG_NAME" => "이름은 한글 ".$name_max_len_kr."자, 영문 ".$name_max_len_en."자 이내로 입력해주세요.",
	"PASSWORD_NOT_MATCH" => "비밀번호가 일치하지 않습니다.",
	"LONG_PHONE" => "전화번호는 ".$phone_max_len."자 이내로 입력해주세요.",
	"LONG_EMAIL" => "이메일 주소 길이는 ".$mail_max_len."글자 이내여야 합니다.",
	"CREATE_DEPARTMENT"=> "부서를 생성해주세요.",
	"EMPTY_DEPARTMENT"=> "부서를 선택해주세요.",
	"PW_CONFIG" => "비밀번호는 영문 대/소문자,특수문자,숫자 중".$pw_safe_cnt."가지 이상, <br />".$pw_length_min."자~".$pw_length_max."자로 설정해 주세요.",
	"OK" =>"저장이 완료되었습니다."
);

$isAdmin = $main_obj->ConfirmAdmin();

if(!$isAdmin){
?>
<form name="editUserResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="user_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_edit_res_popup_msg" value="<? echo $config_msg['NO_AUTH']; ?>" />
	<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.editUserResForm.submit();
</script>
<?
}
else{
	$admin = $main_obj->GetAdminInfo();
}

if($is_password == TRUE){
	//validate password
	$pw_arr = str_split($edit_user_pw);
	$pw_size = count($pw_arr);

	$safe_cnt = 0;
	$decimal_cnt  = 0;
	$eng_cnt = 0;
	$special_cnt = 0;
	$flag_config_error = FALSE;
	for($i=0; $i < $pw_size; $i++){
		$acii_num = ord ($pw_arr[$i]);

		if( (48 <= $acii_num) && ($acii_num <= 57) ){//숫자
			$decimal_cnt  = 1;
		}
		else if( (65 <= $acii_num) && ($acii_num <= 90) ){//영문자 : 대문자
			$eng_cnt = 1;
		}
		else if( (97 <= $acii_num) && ($acii_num <= 122) ){//영문자 : 소문자
			$eng_cnt = 1;
		}
		else if( (33 <= $acii_num) && ($acii_num <= 47) ){//특수문자1
			$special_cnt = 1;
		}
		else if( (58 <= $acii_num) && ($acii_num <= 64) ){//특수문자2
			$special_cnt = 1;
		}
		else if( (91 <= $acii_num) && ($acii_num <= 96) ){//특수문자3
			$special_cnt = 1;
		}
		else if( (123 <= $acii_num) && ($acii_num <= 126) ){//특수문자4
			$special_cnt = 1;
		}
		else{
			$flag_config_error = TRUE;
		}
	}
	$safe_cnt = $decimal_cnt + $eng_cnt + $special_cnt;
	//end validate password
}

if(strlen($edit_user_name) == 0) {// 이름을 입력하지 않았을 경우 에러
?>
<form name="editUserResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="user_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_edit_res_popup_msg" value="<? echo $config_msg['EMPTY_NAME']; ?>" />
	<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.editUserResForm.submit();
</script>
<?
}

else if(strlen($edit_user_name) > $name_max_len) {// 이름의 길이가 30자가 넘을 경우 에러
?>
<form name="editUserResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="user_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_edit_res_popup_msg" value="<? echo $config_msg['LONG_NAME']; ?>" />
	<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.editUserResForm.submit();
</script>
<?
}
else if(strlen($edit_user_tel) > $phone_max_len) {// 전화번호가 20자를 넘을 경우 에러
?>
<form name="editUserResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="user_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_edit_res_popup_msg" value="<? echo $config_msg['LONG_PHONE']; ?>" />
	<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.editUserResForm.submit();
</script>
<?
}
else if(strlen($edit_user_mail) > $mail_max_len) {// 이메일 주소가 50자를 넘을 경우 에러
?>
<form name="editUserResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="user_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_edit_res_popup_msg" value="<? echo $config_msg['LONG_EMAIL']; ?>" />
	<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.editUserResForm.submit();
</script>
<?
}
else if($edit_user_depnum == "0" || $edit_user_depnum == "") {// 부서를 선택하지 않았을 경우
	$isDeptList = $main_obj->GetDepList();
	if(count($isDeptList) == 0){
		$show_msg = $config_msg['CREATE_DEPARTMENT'];
	}
	else{
		$show_msg = $config_msg['EMPTY_DEPARTMENT'];
	}
?>
<form name="editUserResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="user_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="user_edit_res_popup_msg" value="<? echo $show_msg; ?>" />
	<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
</form>
<script>
	document.editUserResForm.submit();
</script>
<?
}
else{
	if($is_password == TRUE){
		if($pw_size == 0) {// 패스워드를 입력하지 않았을 경우 에러
		?>
		<form name="editUserResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
			<input type="hidden" name="user_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
			<input type="hidden" name="user_edit_res_popup_msg" value="<? echo $config_msg['EMPTY_PASSWORD']; ?>" />
			<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
			<input type="hidden" name="page" value="<? echo $page; ?>" />
			<input type="hidden" name="search" value="<? echo $search; ?>" />
			<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
		</form>
		<script>
			document.editUserResForm.submit();
		</script>
		<?
		}
		else if($flag_config_error == TRUE) {// 패스워드 입력 범위에서 벗어났을 경우 ( 특수문자,영문자,숫자 )
		?>
		<form name="editUserResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
			<input type="hidden" name="user_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
			<input type="hidden" name="user_edit_res_popup_msg" value="<? echo $config_msg['PW_CONFIG']; ?>" />
			<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
			<input type="hidden" name="page" value="<? echo $page; ?>" />
			<input type="hidden" name="search" value="<? echo $search; ?>" />
			<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
		</form>
		<script>
			document.editUserResForm.submit();
		</script>
		<?
		}
		else if($safe_cnt < $pw_safe_cnt){//유효 카운트가 2개보다 적을 때
		?>
		<form name="editUserResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
			<input type="hidden" name="user_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
			<input type="hidden" name="user_edit_res_popup_msg" value="<? echo $config_msg['PW_CONFIG']; ?>" />
			<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
			<input type="hidden" name="page" value="<? echo $page; ?>" />
			<input type="hidden" name="search" value="<? echo $search; ?>" />
			<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
		</form>
		<script>
			document.editUserResForm.submit();
		</script>
		<?
		}
		else if( ($pw_size < $pw_length_min) || ($pw_size > $pw_length_max) ){//password 길이가 10자리 보다 작을때
		?>
		<form name="editUserResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
			<input type="hidden" name="user_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
			<input type="hidden" name="user_edit_res_popup_msg" value="<? echo $config_msg['PW_CONFIG']; ?>" />
			<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
			<input type="hidden" name="page" value="<? echo $page; ?>" />
			<input type="hidden" name="search" value="<? echo $search; ?>" />
			<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
		</form>
		<script>
			document.editUserResForm.submit();
		</script>
		<?
		}
		else if($edit_user_pw != $edit_user_pw2) {// 패스워드가 일치하지 않을 경우 에러
		?>
		<form name="editUserResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
			<input type="hidden" name="user_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
			<input type="hidden" name="user_edit_res_popup_msg" value="<? echo $config_msg['PASSWORD_NOT_MATCH']; ?>" />
			<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
			<input type="hidden" name="page" value="<? echo $page; ?>" />
			<input type="hidden" name="search" value="<? echo $search; ?>" />
			<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
		</form>
		<script>
			document.editUserResForm.submit();
		</script>
		<?
		}
	}
	
	$spt_function = 0;
	$spt_setting = 0;
	$spt_mobile_setting = 0;

	for($i=0; $i < count($func_arr); $i++){
		$spt_function = $spt_function + $func_arr[$i];
	}

	$mobile_volume = $admin->mobile_volume;
	$currentMobileSupporters = $main_obj->GetMobileSupporterNum();

	if( !( ($current_mobile_func & PERMISSION_MOBILE_PAID) || 
	        ($current_mobile_func & PERMISSION_MOBILE_FREE) || 
	        ($current_mobile_func & PERMISSION_VIDEO_PAID) || 
	        ($current_mobile_func & PERMISSION_VIDEO_FREE) || 
	        ($current_mobile_func & PERMISSION_MOBILE_VIDEO_PAID) || 
	        ($current_mobile_func & PERMISSION_MOBILE_VIDEO_FREE)
	       ) && (
	        ($spt_function & PERMISSION_MOBILE_PAID) || 
	        ($spt_function & PERMISSION_MOBILE_FREE) || 
	        ($spt_function & PERMISSION_VIDEO_PAID) || 
	        ($spt_function & PERMISSION_VIDEO_FREE) || 
	        ($spt_function & PERMISSION_MOBILE_VIDEO_PAID) || 
	        ($spt_function & PERMISSION_MOBILE_VIDEO_FREE)
	       ) ){
		//원래 설정이 mobile 사용이 아니면서, mobile 사용으로 변경된 경우
		if($currentMobileSupporters >= $mobile_volume){//가용 수 validate
		?>
		<form name="editUserResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
			<input type="hidden" name="user_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
			<input type="hidden" name="user_edit_res_popup_msg" value="<? echo $config_msg['OVER_FLOW']; ?>" />
			<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
			<input type="hidden" name="page" value="<? echo $page; ?>" />
			<input type="hidden" name="search" value="<? echo $search; ?>" />
			<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
		</form>
		<script>
			document.editUserResForm.submit();
		</script>
		<?
		}
	}

	$spt_function = $spt_function + $mobile_func;

	for($i=0; $i < count($sett_arr); $i++){
		$spt_setting = $spt_setting + $sett_arr[$i];
	}

	for($i=0; $i < count($mob_arr); $i++){
		$spt_mobile_setting = $spt_mobile_setting + $mob_arr[$i];
	}

	$sptdata = array(
		"depnum"=>$edit_user_depnum, 
		"password"=>$edit_user_pw, 
		"name"=>$edit_user_name, 
		"phone"=>$edit_user_tel, 
		"email"=>$edit_user_mail, 
		"fr"=>$spt_function, 
		"opt"=>$spt_setting, 
		"mobile_setting"=>$spt_mobile_setting
	);


	$res = $main_obj->ModifySupporter($sptnum, $sptdata);

	if($res == "NOSUPPORTER") {// 수정하려는 상담자가 존재하지 않을 경우
	?>
	<form name="editUserResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
		<input type="hidden" name="user_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
		<input type="hidden" name="user_edit_res_popup_msg" value="<? echo $config_msg['NO_SUPPORTER']; ?>" />
		<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
		<input type="hidden" name="page" value="<? echo $page; ?>" />
		<input type="hidden" name="search" value="<? echo $search; ?>" />
		<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
	</form>
	<script>
		document.editUserResForm.submit();
	</script>
	<?
	}
	else { // 성공적으로 수정. 메시지 띄운 후 이동
	?>
	<form name="editUserResForm" method="post" action="<? echo $config_path['EDIT_SUCCESS']; ?>">
		<input type="hidden" name="user_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
		<input type="hidden" name="user_edit_res_popup_msg" value="<? echo $config_msg['OK']; ?>" />
		<input type="hidden" name="sptnum" value="<? echo $sptnum; ?>" />
		<input type="hidden" name="page" value="<? echo $page; ?>" />
		<input type="hidden" name="search" value="<? echo $search; ?>" />
		<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
	</form>
	<script>
		document.editUserResForm.submit();
	</script>
	<?
	}
}