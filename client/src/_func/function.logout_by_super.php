<?php
/**
 * logout_by_super.php
 * 2015.07.31 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$config_path = array(
	"LOGOUT_SUCCESS" => SUPERADMIN_LOGIN_TRY
);

$main_obj->Logout();

$utils_obj->RedirectURL($config_path['LOGOUT_SUCCESS']);