<?php
/**
 * create_admin.php
 * 2015.08.31 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$admin_act_type = $_POST['admin_act_type'];

$admin_id = $_POST['admin_id'];
$admin_enable = $_POST['admin_enable'];
$admin_pw = $_POST['admin_pw'];
$admin_pw2 = $_POST['admin_retype_pw'];
$admin_company = $_POST['admin_company'];
$admin_service = $_POST['admin_service'];
$admin_business_num = $_POST['admin_business_num'];
$admin_name = $_POST['admin_name'];
$admin_produc_type = $_POST['admin_product'];
$admin_volume_pc = $_POST['admin_pc_cnt'];
$admin_volume_mobile = $_POST['admin_pc_m_n_v_cnt'];
$admin_volume = $_POST['admin_all_cnt'];
$admin_domain = $_POST['admin_domain'];
if($admin_domain == null){
	$admin_domain = "";
	$admin_url = CUSTOMER_ACCESS_URL_DEFAULT;
}
else{
	$admin_url = CUSTOMER_ACCESS_URL_DEFAULT_HEADER . $admin_domain . "." . CUSTOMER_ACCESS_URL_DEFAULT_FOOTER;	
}
$admin_manage_num = $_POST['admin_manage_num'];
$admin_mail = $_POST['admin_mail'];
$admin_memo = $_POST['admin_memo'];
$admin_mobile_expired_date = $_POST['admin_mobile_expired_date'];
if(!$_POST['admin_mobile_expired_date']){
	$admin_mobile_expired_date = DEFAULT_MOBILE_EXPIRED_DATE;
}
$admin_business_sector = $_POST['admin_business_sector'];
$admin_phone = $_POST['admin_phone'];
$admin_tel = $_POST['admin_tel'];
$admin_service_join_date = $_POST['admin_service_join_date'];
if(!$_POST['admin_service_join_date']){
	$admin_service_join_date = DEFAULT_MOBILE_EXPIRED_DATE;
}
$admin_service_start_date = $_POST['admin_service_start_date'];
if(!$_POST['admin_service_start_date']){
	$admin_service_start_date = DEFAULT_MOBILE_EXPIRED_DATE;
}
$admin_service_expired_date = $_POST['admin_service_expired_date'];
if(!$_POST['admin_service_expired_date']){
	$admin_service_expired_date = DEFAULT_MOBILE_EXPIRED_DATE;
}
$admin_bill_account = $_POST['admin_bill_account'];
$admin_sign_num = $_POST['admin_sign_num'];

$page = $_POST['page'];
$search = $_POST['search'];
$searchVal = $_POST['searchVal'];

$p_pc = PRODUCT_CODE_PC;
$p_mobile_paid = PRODUCT_CODE_MOBILE_PAID;
$p_video_paid = PRODUCT_CODE_VIDEO_PAID;
$p_mobile_video_paid = PRODUCT_CODE_MOBILE_VIDEO_PAID;
$p_mobile_free = PRODUCT_CODE_MOBILE_FREE;
$p_video_free = PRODUCT_CODE_VIDEO_FREE;
$p_mobile_video_free = PRODUCT_CODE_MOBILE_VIDEO_FREE;

$name_max_len = NAME_MAX_LENGTH;
$name_max_len_kr = NAME_MAX_LENGTH_KR;
$name_max_len_en = NAME_MAX_LENGTH_EN;
$phone_max_len = PHONE_MAX_LENGTH;
$mail_max_len = MAIL_ADDR_MAX_LENGTH;
$pw_length = PASSWORD_LENGTH;
$pw_length_min = PASSWORD_LENGTH_MIN;
$pw_length_max = PASSWORD_LENGTH_MAX;
$pw_safe_cnt = PASSWORD_SAFE_CNT;
$sign_num_max_len = SIGN_NUM_MAX_LENGTH;
$bill_account_max_len = BILL_ACCOUNT_MAX_LENGTH;
$url_max_len = CUSTOMER_ACCESS_URL_LENGTH;
$url_domain_max_len = CUSTOMER_ACCESS_URL_DOMAIN_LENGTH;

$pop_action_open = 'open';
$pop_action_close = 'close';

if($admin_act_type == ADMIN_ACT_TYPE_FREE){
	$return_url = SUPER_GNB_CUSTOMER_FREE;
}
else if($admin_act_type == ADMIN_ACT_TYPE_PAID){
	$return_url = SUPER_GNB_CUSTOMER_PAID;
}

$config_path = array(
	"CREATE_SUCCESS" => $return_url . "/info.php",
	"CREATE_FAIL" => $return_url . "/add.php"
);

$config_msg = array(
	"NO_AUTH" => "권한이 없습니다.",
	"EMPTY_ID"=> "ID를 입력해 주세요.",
	"OVER_FLOW" => "상담원 수를 초과하였습니다.",
	"DUPICATED"=> "이미 존재하는 ID입니다. 다른 ID를 사용해주세요.",
	"DUPICATED_DOMAIN"=> "이미 존재하는 서브 도메인입니다. 다른 도메인를 사용해주세요.",
	"EMPTY_NAME"=> "이름을 입력해주세요.",
	"LONG_NAME"=> "이름은 한글 ".$name_max_len_kr."자, 영문 ".$name_max_len_en."자 이내로 입력해주세요.",
	"EMPTY_PASSWORD"=> "비밀번호를 입력 하세요.",
	"PASSWORD_NOT_MATCH"=> "비밀번호가 일치하지 않습니다.",
	"LONG_PHONE"=> "전화번호는 ".$phone_max_len."자 이내로 입력해주세요.",
	"LONG_EMAIL"=> "이메일 주소 길이는 ".$mail_max_len."글자 이내여야 합니다.",
	"LONG_URL"=> "URL 주소 길이는 ".$url_max_len."글자 이내여야 합니다.",
	"LONG_DOMAIN"=> "서브 도메인 길이는 ".$url_domain_max_len."글자 이내여야 합니다.",
	"VALID_DOMAIN"=> "서브 도메인은 숫자와 영문으로만 구성할 수 있습니다.",
	"LONG_SIGN_NUM"=> "가입번호는 ".$sign_num_max_len."자 이내로 입력해주세요.",
	"LONG_BILL_ACCOUNT"=> "청구계정번호는 ".$bill_account_max_len."자 이내로 입력해주세요.",
	"EMPTY_AGENT_CNT" => "생성할 상담원 수를 입력해주세요.",
	"PW_CONFIG" => "비밀번호는 영문 대/소문자,특수문자,숫자 중 ".$pw_safe_cnt."가지 이상, <br />".$pw_length_min."자~".$pw_length_max."자로 설정해 주세요.",
	"ONLY_USE_PC" => "PC 상담원만 지정 할 수 있습니다.",
	"MUST_USE_PC_M_N_V" => "애니시포트 PC + Mobile / Video 상담원 수 를 입력하세요.",
	"PROCESS_FAIL" => "관리자를 생성하는 도중 문제가 발생하였습니다. 다시 시도해주세요.",
	"OK"=>"저장이 완료되었습니다."
);

//validate password
$pw_arr = str_split($admin_pw);
$pw_size = count($pw_arr);

$safe_cnt = 0;
$decimal_cnt  = 0;
$eng_cnt = 0;
$special_cnt = 0;
$flag_config_error = FALSE;
for($i=0; $i < $pw_size; $i++){
	$acii_num = ord ($pw_arr[$i]);

	if( (48 <= $acii_num) && ($acii_num <= 57) ){//숫자
		$decimal_cnt  = 1;
	}
	else if( (65 <= $acii_num) && ($acii_num <= 90) ){//영문자 : 대문자
		$eng_cnt = 1;
	}
	else if( (97 <= $acii_num) && ($acii_num <= 122) ){//영문자 : 소문자
		$eng_cnt = 1;
	}
	else if( (33 <= $acii_num) && ($acii_num <= 47) ){//특수문자1
		$special_cnt = 1;
	}
	else if( (58 <= $acii_num) && ($acii_num <= 64) ){//특수문자2
		$special_cnt = 1;
	}
	else if( (91 <= $acii_num) && ($acii_num <= 96) ){//특수문자3
		$special_cnt = 1;
	}
	else if( (123 <= $acii_num) && ($acii_num <= 126) ){//특수문자4
		$special_cnt = 1;
	}
	else{
		$flag_config_error = TRUE;
	}
}
$safe_cnt = $decimal_cnt + $eng_cnt + $special_cnt;
//end validate password

//validate id
$id_arr = str_split($admin_id);
$id_size = count($id_arr);
$flag_config_error = FALSE;
for($i=0; $i < $id_size; $i++){
	$acii_num = ord ($id_arr[$i]);

	if( !((48 <= $acii_num) && ($acii_num <= 57)) && !((65 <= $acii_num) && ($acii_num <= 90)) && !((97 <= $acii_num) && ($acii_num <= 122))){//숫자, 영문자(대,소문자) 아니면
		$flag_config_error = TRUE;
	}
}
//end validate id

if(strlen($admin_id) == 0) {// ID를 입력하지 않았을 경우 에러
?>
<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['EMPTY_ID']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
	
	<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
	<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
	<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
	<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
	<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
	<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
	<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
	<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
	<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
	<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
	<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
	<input type="hidden" name="admin_mail" value="<? echo $admin_mail; ?>" />
	<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

	<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
	<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
	<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
	<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
	<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
	<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
	<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
	<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
</form>
<script>
	document.createAdminResForm.submit();
</script>
<?
}
else if(strlen($admin_name) == 0) {// 이름을 입력하지 않았을 경우 에러
?>
<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['EMPTY_NAME']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

	<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
	<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
	<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
	<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
	<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
	<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
	<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
	<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
	<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
	<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
	<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
	<input type="hidden" name="admin_mail" value="<? echo $admin_mail; ?>" />
	<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

	<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
	<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
	<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
	<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
	<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
	<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
	<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
	<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
</form>
<script>
	document.createAdminResForm.submit();
</script>
<?
}
else if(strlen($admin_name) > $name_max_len) {// 이름의 길이가 30자가 넘을 경우 에러
?>
<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['LONG_NAME']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

	<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
	<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
	<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
	<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
	<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
	<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
	<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
	<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
	<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
	<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
	<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
	<input type="hidden" name="admin_mail" value="<? echo $admin_mail; ?>" />
	<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

	<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
	<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
	<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
	<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
	<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
	<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
	<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
	<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
</form>
<script>
	document.createAdminResForm.submit();
</script>
<?
}
else if(strlen($admin_pw) == 0) {// 패스워드를 입력하지 않았을 경우 에러
?>
<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['EMPTY_PASSWORD']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

	<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
	<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
	<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
	<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
	<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
	<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
	<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
	<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
	<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
	<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
	<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
	<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
	<input type="hidden" name="admin_mail" value="<? echo $admin_mail; ?>" />
	<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

	<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
	<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
	<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
	<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
	<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
	<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
	<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
	<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
</form>
<script>
	document.createAdminResForm.submit();
</script>
<?
}
else if($flag_config_error == TRUE) {// 패스워드 입력 범위에서 벗어났을 경우 ( 특수문자,영문자,숫자 )
?>
<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['PW_CONFIG']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

	<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
	<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
	<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
	<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
	<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
	<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
	<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
	<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
	<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
	<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
	<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
	<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
	<input type="hidden" name="admin_mail" value="<? echo $admin_mail; ?>" />
	<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

	<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
	<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
	<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
	<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
	<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
	<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
	<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
	<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
</form>
<script>
	document.createAdminResForm.submit();
</script>
<?
}
else if($safe_cnt < $pw_safe_cnt){//유효 카운트가 2개보다 적을 때
?>
<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['PW_CONFIG']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

	<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
	<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
	<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
	<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
	<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
	<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
	<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
	<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
	<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
	<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
	<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
	<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
	<input type="hidden" name="admin_mail" value="<? echo $admin_mail; ?>" />
	<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

	<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
	<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
	<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
	<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
	<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
	<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
	<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
	<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
</form>
<script>
	document.createAdminResForm.submit();
</script>
<?
}
else if( ($pw_size < $pw_length_min) || ($pw_size > $pw_length_max) ){//password 길이가 10자리 보다 작을때
?>
<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['PW_CONFIG']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

	<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
	<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
	<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
	<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
	<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
	<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
	<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
	<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
	<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
	<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
	<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
	<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
	<input type="hidden" name="admin_mail" value="<? echo $admin_mail; ?>" />
	<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

	<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
	<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
	<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
	<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
	<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
	<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
	<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
	<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
</form>
<script>
	document.createAdminResForm.submit();
</script>
<?
}
else if($admin_pw != $admin_pw2) {// 패스워드가 일치하지 않을 경우 에러
?>
<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['PASSWORD_NOT_MATCH']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

	<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
	<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
	<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
	<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
	<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
	<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
	<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
	<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
	<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
	<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
	<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
	<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
	<input type="hidden" name="admin_mail" value="<? echo $admin_mail; ?>" />
	<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

	<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
	<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
	<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
	<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
	<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
	<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
	<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
	<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
</form>
<script>
	document.createAdminResForm.submit();
</script>
<?
}
else if(strlen($admin_tel) > $phone_max_len) {// 전화번호가 20자를 넘을 경우 에러
?>
<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['LONG_PHONE']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

	<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
	<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
	<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
	<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
	<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
	<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
	<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
	<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
	<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
	<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
	<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
	<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
	<input type="hidden" name="admin_mail" value="<? echo $admin_mail; ?>" />
	<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

	<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
	<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
	<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
	<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
	<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
	<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
	<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
	<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
</form>
<script>
	document.createAdminResForm.submit();
</script>
<?
}
else if(strlen($admin_mail) > $mail_max_len) {// 이메일 주소가 50자를 넘을 경우 에러
?>
<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['LONG_EMAIL']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

	<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
	<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
	<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
	<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
	<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
	<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
	<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
	<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
	<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
	<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
	<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
	<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
	<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

	<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
	<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
	<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
	<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
	<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
	<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
	<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
	<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
</form>
<script>
	document.createAdminResForm.submit();
</script>
<?
}
else if(strlen($admin_domain) > $url_domain_max_len) {// 고객 접속 URL 주소가 7자를 넘을 경우 에러
?>
<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['LONG_DOMAIN']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

	<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
	<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
	<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
	<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
	<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
	<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
	<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
	<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
	<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
	<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
	<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
	<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

	<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
	<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
	<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
	<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
	<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
	<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
	<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
	<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
</form>
<script>
	document.createAdminResForm.submit();
</script>
<?
}
else if ( preg_match('/[^A-Za-z0-9]/', $admin_domain) ) { // 영문이나 숫자 아닐 경우
?>
<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['VALID_DOMAIN']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

	<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
	<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
	<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
	<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
	<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
	<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
	<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
	<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
	<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
	<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
	<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
	<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

	<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
	<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
	<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
	<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
	<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
	<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
	<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
	<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
</form>
<script>
	document.createAdminResForm.submit();
</script>
<?
}
else if(strlen($admin_url) > $url_max_len) {// 고객 접속 URL 주소가 26자를 넘을 경우 에러
?>
<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['LONG_URL']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

	<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
	<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
	<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
	<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
	<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
	<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
	<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
	<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
	<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
	<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
	<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
	<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

	<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
	<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
	<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
	<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
	<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
	<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
	<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
	<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
</form>
<script>
	document.createAdminResForm.submit();
</script>
<?
}
else if(strlen($admin_bill_account) > $bill_account_max_len) {// 청구계정번호가 12자리 넘을때
?>
<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['LONG_BILL_ACCOUNT']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

	<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
	<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
	<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
	<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
	<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
	<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
	<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
	<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
	<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
	<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
	<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
	<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
	<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

	<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
	<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
	<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
	<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
	<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
	<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
	<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
</form>
<script>
	document.createAdminResForm.submit();
</script>
<?
}
else if(strlen($admin_sign_num) > $sign_num_max_len) {// 가입번호가 12자리 넘을때
?>
<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['LONG_SIGN_NUM']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

	<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
	<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
	<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
	<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
	<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
	<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
	<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
	<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
	<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
	<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
	<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
	<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
	<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

	<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
	<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
	<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
	<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
	<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
	<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
	<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
</form>
<script>
	document.createAdminResForm.submit();
</script>
<?
}
else if(strlen($admin_volume) == 0 || $admin_volume == 0){
?>
<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
	<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['EMPTY_AGENT_CNT']; ?>" />
	<input type="hidden" name="input_res" value="FAIL" />
	<input type="hidden" name="page" value="<? echo $page; ?>" />
	<input type="hidden" name="search" value="<? echo $search; ?>" />
	<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

	<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
	<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
	<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
	<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
	<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
	<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
	<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
	<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
	<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
	<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
	<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
	<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
	<input type="hidden" name="admin_mail" value="<? echo $admin_mail; ?>" />
	<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

	<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
	<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
	<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
	<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
	<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
	<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
	<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
	<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
</form>
<script>
	document.createAdminResForm.submit();
</script>
<?
}
else{
	if(strlen($admin_volume_pc) == 0) {
		$pc_volume = 0;
	}
	else{
		$pc_volume = $admin_volume_pc;
	}

	if(strlen($admin_volume_mobile) == 0) {
		$mobile_volume = 0;
	}
	else{
		$mobile_volume = $admin_volume_mobile;
	}

	if($admin_produc_type == $p_pc){//only pc
		$mobile_expired_date = DEFAULT_MOBILE_EXPIRED_DATE;

		if( $mobile_volume > 0  ){
		?>
			<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
				<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
				<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['ONLY_USE_PC']; ?>" />
				<input type="hidden" name="input_res" value="FAIL" />
				<input type="hidden" name="page" value="<? echo $page; ?>" />
				<input type="hidden" name="search" value="<? echo $search; ?>" />
				<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

				<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
				<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
				<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
				<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
				<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
				<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
				<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
				<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
				<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
				<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
				<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
				<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
				<input type="hidden" name="admin_mail" value="<? echo $admin_mail; ?>" />
				<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

				<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
				<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
				<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
				<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
				<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
				<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
				<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
				<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
			</form>
			<script>
				document.createAdminResForm.submit();
			</script>
		<?
		}
	}
	else if($admin_produc_type == $p_mobile_paid  || $admin_produc_type == $p_video_paid  || $admin_produc_type ==  $p_mobile_video_paid  ) {//paid
		$mobile_expired_date = DEFAULT_MOBILE_EXPIRED_DATE;
		
		if( $mobile_volume  < 0  ){
		?>
			<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
				<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
				<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['MUST_USE_PC_M_N_V']; ?>" />
				<input type="hidden" name="input_res" value="FAIL" />
				<input type="hidden" name="page" value="<? echo $page; ?>" />
				<input type="hidden" name="search" value="<? echo $search; ?>" />
				<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

				<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
				<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
				<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
				<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
				<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
				<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
				<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
				<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
				<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
				<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
				<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
				<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
				<input type="hidden" name="admin_mail" value="<? echo $admin_mail; ?>" />
				<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

				<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
				<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
				<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
				<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
				<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
				<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
				<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
				<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
			</form>
			<script>
				document.createAdminResForm.submit();
			</script>
		<?
		}
	}
	else if( $admin_produc_type == $p_mobile_free   || $admin_produc_type == $p_video_free   || $admin_produc_type ==  $p_mobile_video_free   ) {//free
		$mobile_expired_date = $admin_mobile_expired_date;

		if($mobile_volume < 1  ){
		?>
			<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
				<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
				<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['MUST_USE_PC_M_N_V']; ?>" />
				<input type="hidden" name="input_res" value="FAIL" />
				<input type="hidden" name="page" value="<? echo $page; ?>" />
				<input type="hidden" name="search" value="<? echo $search; ?>" />
				<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

				<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
				<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
				<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
				<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
				<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
				<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
				<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
				<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
				<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
				<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
				<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
				<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
				<input type="hidden" name="admin_mail" value="<? echo $admin_mail; ?>" />
				<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

				<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
				<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
				<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
				<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
				<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
				<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
				<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
				<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
			</form>
			<script>
				document.createAdminResForm.submit();
			</script>
		<?
		}
	}

	$admindata = array(
		"id"=>$admin_id, 
		"password"=>$admin_pw, 
		"company"=>$admin_company, 
		"name"=>$admin_name, 
		"email"=>$admin_mail, 
		"volume"=>$admin_volume, 
		"business_sector"=>$admin_business_sector,
		"phone"=>$admin_phone,
		"tel"=>$admin_tel,
		"url"=>$admin_url, 
		"domain"=>$admin_domain, 
		"others"=>$admin_memo, 
		"business_num"=>$admin_business_num, 
		"enable"=>$admin_enable, 
		"managenum"=>$admin_manage_num, 
		"service_name"=>$admin_service,
		"function"=>$admin_produc_type, 
		"mobile_volume"=>$admin_volume_mobile, 
		"service_join_date"=>$admin_service_join_date,
		"service_start_date"=>$admin_service_start_date,
		"service_expired_date"=>$admin_service_expired_date,
		"bill_account"=>$admin_bill_account,
		"sign_num"=>$admin_sign_num,
		"isFree"=>$admin_act_type,
		"mobile_expired_date"=>$mobile_expired_date
	);

	$res = $main_obj->CreateAdmin($admindata);
	if($res == "SUPPORTERALREADYEXISTS" || $res == "ADMINALREADYEXISTS") {// 이미 존재하는 ID인 경우
	?>
		<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
			<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
			<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['DUPICATED']; ?>" />
			<input type="hidden" name="input_res" value="FAIL" />
			<input type="hidden" name="page" value="<? echo $page; ?>" />
			<input type="hidden" name="search" value="<? echo $search; ?>" />
			<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

			<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
			<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
			<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
			<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
			<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
			<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
			<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
			<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
			<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
			<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
			<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
			<input type="hidden" name="admin_mail" value="<? echo $admin_mail; ?>" />
			<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

			<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
			<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
			<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
			<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
			<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
			<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
			<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
			<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
		</form>
		<script>
			document.createAdminResForm.submit();
		</script>
	<?
	}
	else if($res == "DOMAINALREADYEXISTS"){
	?>
		<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
			<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
			<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['DUPICATED_DOMAIN']; ?>" />
			<input type="hidden" name="input_res" value="FAIL" />
			<input type="hidden" name="page" value="<? echo $page; ?>" />
			<input type="hidden" name="search" value="<? echo $search; ?>" />
			<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
			
			<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
			<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
			<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
			<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
			<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
			<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
			<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
			<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
			<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
			<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
			<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
			<input type="hidden" name="admin_mail" value="<? echo $admin_mail; ?>" />
			<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

			<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
			<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
			<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
			<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
			<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
			<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
			<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
			<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
		</form>
		<script>
			document.createAdminResForm.submit();
		</script>
	<?	
	}
	else if($res == "OK"){// 성공적으로 생성
		$created = $main_obj->GetAdminByAdid($admin_id);
		$main_obj->CreateTroubleTypeDefault($created->adnum);
	?>
		<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_SUCCESS']; ?>">
			<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
			<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['OK']; ?>" />
			<input type="hidden" name="input_res" value="SUCCESS" />
			<input type="hidden" name="adid" value="<? echo $admin_id; ?>" />
			<input type="hidden" name="page" value="<? echo $page; ?>" />
			<input type="hidden" name="search" value="<? echo $search; ?>" />
			<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
		</form>
		<script>
			document.createAdminResForm.submit();
		</script>
	<?	
	}
	else {
	?>
		<form name="createAdminResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
			<input type="hidden" name="admin_add_res_popup_action" value="<? echo $pop_action_open; ?>" />
			<input type="hidden" name="admin_add_res_popup_msg" value="<? echo $config_msg['PROCESS_FAIL']; ?>" />
			<input type="hidden" name="input_res" value="FAIL" />
			<input type="hidden" name="page" value="<? echo $page; ?>" />
			<input type="hidden" name="search" value="<? echo $search; ?>" />
			<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />

			<input type="hidden" name="admin_id" value="<? echo $admin_id; ?>" />
			<input type="hidden" name="admin_enable" value="<? echo $admin_enable; ?>" />
			<input type="hidden" name="admin_company" value="<? echo $admin_company; ?>" />
			<input type="hidden" name="admin_service" value="<? echo $admin_service; ?>" />
			<input type="hidden" name="admin_business_num" value="<? echo $admin_business_num; ?>" />
			<input type="hidden" name="admin_name" value="<? echo $admin_name; ?>" />
			<input type="hidden" name="admin_product" value="<? echo $admin_produc_type; ?>" />
			<input type="hidden" name="admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
			<input type="hidden" name="admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
			<input type="hidden" name="admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
			<input type="hidden" name="admin_domain" value="<? echo $admin_domain; ?>" />
			<input type="hidden" name="admin_manage_num" value="<? echo $admin_manage_num; ?>" />
			<input type="hidden" name="admin_mail" value="<? echo $admin_mail; ?>" />
			<input type="hidden" name="admin_memo" value="<? echo $admin_memo; ?>" />

			<input type="hidden" name="admin_business_sector" value="<? echo $admin_business_sector; ?>" />
			<input type="hidden" name="admin_phone" value="<? echo $admin_phone; ?>" />
			<input type="hidden" name="admin_tel" value="<? echo $admin_tel; ?>" />
			<input type="hidden" name="admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
			<input type="hidden" name="admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
			<input type="hidden" name="admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
			<input type="hidden" name="admin_bill_account" value="<? echo $admin_bill_account; ?>" />
			<input type="hidden" name="admin_sign_num" value="<? echo $admin_sign_num; ?>" />
		</form>
		<script>
			document.createAdminResForm.submit();
		</script>
	<?	
	}
}
