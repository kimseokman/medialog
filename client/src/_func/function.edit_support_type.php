<?php
/**
 * edit_support_type.php
 * 2015.08.18 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$trb_num = $_POST['trbnum'];
$trb_name = $_POST['trbname'];

$trouble_name_max = TROUBLE_NAME_MAX_LENGTH;
$trouble_name_max_kr = TROUBLE_NAME_MAX_LENGTH_KR;
$trouble_name_max_en = TROUBLE_NAME_MAX_LENGTH_EN;

$config_path = array(
	"EDIT_SUCCESS" => MANAGE_SUPPORT_TYPE_INDEX,
	"EDIT_FAIL" => MANAGE_SUPPORT_TYPE_INDEX
);

$config_msg = array(
	"NO_TROUBLETYPE" => "존재하지 않는 지원유형 입니다.",
	"EMPTY_TYPE" => "지원유형을 입력하세요.",
	"LONG_TYPE" => "지원유형은 한글 ".$trouble_name_max_kr."자, <br />영어 ".$trouble_name_max_en."자 이내로 입력해주세요.",
	"EDIT_OK" => "지원유형이 수정 되었습니다."
);

$pop_action_open = 'open';
$pop_action_close = 'close';

if($main_obj->GetTroubleTypeByTrbnum($trb_num) == NULL){
?>
<form name="editSupportTypeResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="type_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="type_edit_res_popup_msg" value="<? echo $config_msg['NO_TROUBLETYPE']; ?>" />
</form>
<script>
	document.editSupportTypeResForm.submit();
</script>
<?
}
else if(strlen($trb_name) == 0) {
?>
<form name="editSupportTypeResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="type_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="type_edit_res_popup_msg" value="<? echo $config_msg['EMPTY_TYPE']; ?>" />
	<input type="hidden" name="type_edit_res" value="FAIL" />
	<input type="hidden" name="fail_trb_num" value="<? echo $trb_num; ?>" />
</form>
<script>
	document.editSupportTypeResForm.submit();
</script>
<?
}
else if(strlen($trb_name) > $trouble_name_max) {
?>
<form name="editSupportTypeResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="type_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="type_edit_res_popup_msg" value="<? echo $config_msg['LONG_TYPE']; ?>" />
	<input type="hidden" name="type_edit_res" value="FAIL" />
	<input type="hidden" name="fail_trb_num" value="<? echo $trb_num; ?>" />
</form>
<script>
	document.editSupportTypeResForm.submit();
</script>
<?
}
else {
	$res = $main_obj->ModifyTroubleType($trb_num, $trb_name);

	if($res == "NOTROUBLETYPE") {// 해당 장애유형이 존재하지 않을 때
	?>
	<form name="editSupportTypeResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
		<input type="hidden" name="type_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
		<input type="hidden" name="type_edit_res_popup_msg" value="<? echo $config_msg['NO_TROUBLETYPE']; ?>" />
	</form>
	<script>
		document.editSupportTypeResForm.submit();
	</script>
	<?	
	}
	else{
	?>
	<form name="editSupportTypeResForm" method="post" action="<? echo $config_path['EDIT_SUCCESS']; ?>">
		<input type="hidden" name="type_edit_res_popup_action" value="<? echo $pop_action_close; ?>" />
	</form>
	<script>
		document.editSupportTypeResForm.submit();
	</script>
	<?
	}
}