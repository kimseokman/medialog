<?php
/**
 * edit_support_type_order.php
 * 2015.08.18 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$trb_num = $_POST['trbnum'];
$trb_cnt = $_POST['trbcnt'];
$trb_order = $_POST['trborder'];
$trb_command = $_POST['trbcommand'];

$config_path = array(
	"EDIT_SUCCESS" => MANAGE_SUPPORT_TYPE_INDEX,
	"EDIT_FAIL" => MANAGE_SUPPORT_TYPE_INDEX
);

$config_msg = array(
	"MORE_TROUBLETYPE" => "지원유형이 두개이상 되어야 우선순위를 변경 할 수 있습니다.",
	"MAXIMUM_TROUBLETYPE" => "최고수준의 우선순위 입니다.",
	"MINIMUM_TROUBLETYPE" => "가장 낮은 우선순위 입니다."
);

$pop_action_open = 'open';
$pop_action_close = 'close';

if($trb_cnt == 1){
?>
<form name="editSupportTypeOrderResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="type_order_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="type_order_edit_res_popup_msg" value="<? echo $config_msg['MORE_TROUBLETYPE']; ?>" />
</form>
<script>
	document.editSupportTypeOrderResForm.submit();
</script>
<?
}
else if($trb_command == 'up' && $trb_order == 'FIRST'){
?>
<form name="editSupportTypeOrderResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="type_order_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="type_order_edit_res_popup_msg" value="<? echo $config_msg['MAXIMUM_TROUBLETYPE']; ?>" />
</form>
<script>
	document.editSupportTypeOrderResForm.submit();
</script>
<?
}
else if($trb_command == 'down' && $trb_order == 'LAST'){
?>
<form name="editSupportTypeOrderResForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="type_order_edit_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="type_order_edit_res_popup_msg" value="<? echo $config_msg['MINIMUM_TROUBLETYPE']; ?>" />
</form>
<script>
	document.editSupportTypeOrderResForm.submit();
</script>
<?
}
else{
	$main_obj->MoveTroubleType($trb_num,$trb_command);
?>
<form name="editSupportTypeOrderResForm" method="post" action="<? echo $config_path['EDIT_SUCCESS']; ?>">
	<input type="hidden" name="type_order_edit_res_popup_action" value="<? echo $pop_action_close; ?>" />
</form>
<script>
	document.editSupportTypeOrderResForm.submit();
</script>
<?
}