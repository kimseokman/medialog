<?php
/**
 * edit_password.php
 * 2015.08.19 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$pw_length = PASSWORD_LENGTH;
$pw_length_min = PASSWORD_LENGTH_MIN;
$pw_length_max = PASSWORD_LENGTH_MAX;
$pw_safe_cnt = PASSWORD_SAFE_CNT;

$config_path = array(
	"EDIT_SUCCESS" => SETTING_SITE_INDEX,
	"EDIT_FAIL" => SETTING_SITE_INDEX
);

$config_msg = array(
	"INVALID_NOW_PW" => "현재 비밀번호를 다시 확인해주세요.",
	"INVALID_NEW_PW" => "비밀번호는 영문 대/소문자,특수문자,숫자 중".$pw_safe_cnt."가지 이상, <br />".$pw_length_min."자~".$pw_length_max."자로 설정해 주세요.",
	"INVALID_RETYPE_PW" => "새로운 비밀번호를 다시 확인해주세요.",
	"ETC_FAIL" => "비밀번호 변경 실패"
);

$admin = $main_obj->GetAdminInfo();

$pop_action_open = 'open';
$pop_action_close = 'close';

$now_pw = $_POST['now_pw'];
$new_pw = $_POST['new_pw'];
$retype_pw = $_POST['retype_pw'];

$check_now_pw_res = $main_obj->CheckAdminPW($now_pw);

$check_now_pw_flag = FALSE;
$check_new_pw_flag = FALSE;
$check_retype_pw_flag = FALSE;

if($check_now_pw_res == 'OK'){
	$check_now_pw_flag = TRUE;
}
else{
	$check_now_pw_flag = FALSE;
}

if(!$check_now_pw_flag){
?>
<form name="editPWForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="edit_pw_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="edit_pw_popup_res" value="FAIL" />
	<input type="hidden" name="edit_pw_popup_msg" value="<? echo $config_msg['INVALID_NOW_PW']; ?>" />
</form>
<script>
	document.editPWForm.submit();
</script>
<?	
}

//validation new password
$pw_arr = str_split($new_pw);
$pw_size = count($pw_arr);

$safe_cnt = 0;
$decimal_cnt = 0;
$eng_cnt = 0;
$special_cnt = 0;
$flag_config_error = FALSE;
for($i=0; $i < $pw_size; $i++){
	$acii_num = ord ($pw_arr[$i]);

	if( (48 <= $acii_num) && ($acii_num <= 57) ){//숫자
		$decimal_cnt = 1;
	}
	else if( (65 <= $acii_num) && ($acii_num <= 90) ){//영문자 : 대문자
		$eng_cnt = 1;
	}
	else if( (97 <= $acii_num) && ($acii_num <= 122) ){//영문자 : 소문자
		$eng_cnt = 1;
	}
	else if( (33 <= $acii_num) && ($acii_num <= 47) ){//특수문자1
		$special_cnt = 1;
	}
	else if( (58 <= $acii_num) && ($acii_num <= 64) ){//특수문자2
		$special_cnt = 1;
	}
	else if( (91 <= $acii_num) && ($acii_num <= 96) ){//특수문자3
		$special_cnt = 1;
	}
	else if( (123 <= $acii_num) && ($acii_num <= 126) ){//특수문자4
		$special_cnt = 1;
	}
	else{
		$flag_config_error = TRUE;
	}
}

$safe_cnt = $decimal_cnt + $eng_cnt + $special_cnt;

if($flag_config_error == TRUE){
	$check_new_pw_flag = FALSE;
}
else{//입력한 패스워드 유효범위
	if($safe_cnt < $pw_safe_cnt){//유효 카운트가 2개보다 적을 때
		$check_new_pw_flag = FALSE;
	}
	else {//유효 카운트가 2개이상일때
		if( ($pw_size < $pw_length_min) || ($pw_size > $pw_length_max) ){//password 길이가 10자리 보다 작을때
			$check_new_pw_flag = FALSE;
		}
		else{
			$check_new_pw_flag = TRUE;
		}
	}	
}

if($check_new_pw_flag == FALSE){
?>
<form name="editPWForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="edit_pw_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="edit_pw_popup_res" value="FAIL" />
	<input type="hidden" name="edit_pw_popup_msg" value="<? echo $config_msg['INVALID_NEW_PW']; ?>" />
	<input type="hidden" name="edit_now_pw" value="<? echo $now_pw; ?>" />
</form>
<script>
	document.editPWForm.submit();
</script>
<?	
}

//validation retype password
if($new_pw != $retype_pw){
	$check_retype_pw_flag = FALSE;
}
else{
	$check_retype_pw_flag = TRUE;
}

if(!$check_retype_pw_flag){
?>
<form name="editPWForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
	<input type="hidden" name="edit_pw_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="edit_pw_popup_res" value="FAIL" />
	<input type="hidden" name="edit_pw_popup_msg" value="<? echo $config_msg['INVALID_RETYPE_PW']; ?>" />
	<input type="hidden" name="edit_now_pw" value="<? echo $now_pw; ?>" />
	<input type="hidden" name="edit_new_pw" value="<? echo $new_pw; ?>" />
</form>
<script>
	document.editPWForm.submit();
</script>
<?		
}

if($check_now_pw_flag && $check_new_pw_flag && $check_retype_pw_flag){
	$reset_res = $main_obj->ResetPassword($admin->adid, $admin->email, $new_pw);

	if($reset_res == 'OK'){
	?>
	<form name="editPWForm" method="post" action="<? echo $config_path['EDIT_SUCCESS']; ?>">
		<input type="hidden" name="edit_pw_popup_action" value="<? echo $pop_action_close; ?>" />
		<input type="hidden" name="edit_pw_popup_res" value="SUCCESS" />
	</form>
	<script>
		document.editPWForm.submit();
	</script>
	<?
	}
	else{
	?>
	<form name="editPWForm" method="post" action="<? echo $config_path['EDIT_FAIL']; ?>">
		<input type="hidden" name="edit_pw_popup_action" value="<? echo $pop_action_open; ?>" />
		<input type="hidden" name="edit_pw_popup_res" value="FAIL" />
		<input type="hidden" name="edit_pw_popup_msg" value="<? echo $config_msg['ETC_FAIL']; ?>" />
	</form>
	<script>
		document.editPWForm.submit();
	</script>
	<?	
	}	
}