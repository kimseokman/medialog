<?php
include_once("./_func/function.common.php");

if($login_flag == TRUE){
	if($admin_login_flag){
		$redirection_url = ADMIN_INDEX;
	}
	else{
		$redirection_url = SUPPORTER_INDEX;
	}

	$utils_obj->RedirectURL($redirection_url);
}
else{
	$utils_obj->RedirectURL(LOGIN_TRY);
}