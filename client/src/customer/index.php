<?php
include_once("../_func/function.common.php");

include_once("./info.php");

$charset = "utf-8";

if(getCountry() == "ko"){
	$title = $title_kr;
} 
else {
	$title = $title_en;
}

$mainPage = getMainPage();

function getCountry() {
	$country = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

	if (strstr($country, "ko"))  $country = "ko";
	else if (strstr($country, "kr")) $country = "ko";
	else if (strstr($country, "ja")) $country = "ja";
	else if (strstr($country, "en")) $country = "en";
	else $country = "en";

	return $country;
}

function isMacOS() {
	return strstr($_SERVER['HTTP_USER_AGENT'],'Mac');
}

function isLinuxOS() {
	return strstr($_SERVER['HTTP_USER_AGENT'],'Linux');
}

function getMainPage() {
	if(isLinuxOS()) {
		$page = "linux.html";
	} 
	else  if($is_using_ax == "0" || isMacOS()) {
		if(getCountry() == "ko") {
			$page = "manual.html";
		} 
		else  {
			$page = "manual_en.html";
		}
	} 
	else {
		if(getCountry() == "ko") {
			$page = "start.html";
		} 
		else  {
			$page = "start_en.html";
		}
	}

	return $page;
}

$code_validation_res = $_POST['VALIDATION_CODE_RES'];

include_once("./common/html_head.php");
include_once("./common/header.php");

//echo "===========>". $_SERVER['HTTP_HOST']. "<br>";
//echo "=======>". $SERVER_NAME."<br>";
?>
<div class="box_agree_wrap">
	<div class="box_agree_top"></div>
	<div class="box_endu">
		<div class="box_endu_content">
			<div class="endu_contents">
				<strong><img src="<? echo IMG_DIR; ?>/sub/endu_content_tit.gif" alt="전문 상담원에게 안내받으신 원격 지원 접속번호를 입력하신 후, 확인 버튼을 클릭하세요." /></strong>

				<!-- [D] End user가 접속번호를 입력하고 확인버튼을 클릭하면 원격지원 상담을 받을 수 있음 -->
				<div class="endu_num_chk">
					<form name="csform" action="download.php" method="post" onsubmit="return GetAccessCode();">
						<input type="hidden" id="downloadkey" name="downloadkey" value="2FBED37DE80A" />
						<input type="text" id="accesscode" name="accesscode" placeholder="접속번호" maxlength="7"  style="width:187px;ime-mode:disabled" onkeyPress="SplitAccesscode()" />
					</form>	
					<a href="#" onclick="Submit()"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm2.gif" alt="확인" /></a>
				</div>

				<!-- [D] 입력한 접속 번호에 이상이 있을 때, 오류메시지 입력 -->
				<?
				if($code_validation_res == 'FAIL'){
				?>
				<p class="endu_num_log imp_ico">
					입력하신 번호가 잘못되었습니다. 상담원에게 확인해주세요.
				</p>
				<?
				}//end of : if($code_validation_res == 'FAIL')
				?>
			</div>
		</div>
	</div>
	<div class="box_agree_btm"></div>
</div>
<?
include_once("./common/footer.php");
include_once("./common/html_foot.php");
