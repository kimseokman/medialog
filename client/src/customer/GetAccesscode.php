<?php
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGMain.php");

$asg_obj = new ASGMain();

$name = $_POST["name"];
$phone1 = $_POST["phone1"];
$phone2 = $_POST["phone2"];
$phone3 = $_POST["phone3"];
$email = $_POST["email"];
$save = $_POST["save"];
	
$accesscode = $_POST["accesscode"];
	
if(!$asg_obj->IsAccesscodeValid($accesscode)) { // livesupport_cs에 없는 accesscode
	echo "Invalid1";
}
else {
	$tel = $phone1."-".$phone2."-".$phone3;
	if($tel == "--") $tel = "";
	
	$res = $asg_obj->OpenCS($accesscode, $name, $tel, $email);
	
	if($res == "NOCS") { // livesupport_cs에 없는 accesscode
		echo "Invalid2";
	}
	else {
		echo "$accesscode";
	}
}	
