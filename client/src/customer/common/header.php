<!-- login_wrap -->
<div id="login_wrap">
<!-- agree_header -->
<div id="agree_header">
	<h1 class="agree_header_tit">
		<a href="#" class="login_logo">
			<!-- [D] 관리자사이트의 사이트 설정에서 이미지를 등록할 수 있고, 내용을 저장하면 업체 전용 접속사이트에 노출됨. -->
			<img src="<? echo $image_url; ?>" style="width:262px;height:74px;" alt="LG U+ 원격 지원 서비스" />
		</a>
	</h1>
	<div class="agree_header_cont">
		<img class="login_useradmin" src="<? echo IMG_DIR; ?>/sub/agree_hcont_txt.gif" alt="원격 지원 서비스는 전문 상담원이 고객의 PC화면을 원격으로 지원하여 PC의 문제를 빠르고 정확하게 문제를 해결해 주는 서비스입니다." />
		<div class="agree_header_cont_btn">
			<!-- [D] 원격상담 접속 프로그램이 실행됨 -->
			<a href="#" class="program_download"><img src="<? echo IMG_DIR; ?>/sub/btn_agree_pstart.gif" alt="프로그램 실행" /></a>
			<!-- [D] 서비스 홈페이지 링크 -->
			<a href="<? echo $home_url; ?>" target="_blank"><img src="<? echo IMG_DIR; ?>/sub/btn_agree_sinfo.gif" alt="서비스안내" /></a>
		</div>
	</div>
</div>
<!-- //agree_header -->
<!-- login_content -->
<div id="login_content">