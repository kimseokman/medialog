<?php
	$home_url = REMOTE_HOME;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=960" />
	<meta name="apple-mobile-web-app-title" content="UPLUS" />
	<title><? echo $title; ?></title>
	<link rel="stylesheet" type="text/css" href="<? echo CSS_DIR; ?>/common.css" />
	<link rel="stylesheet" type="text/css" href="<? echo CSS_DIR; ?>/popup.css" />
	<link rel="stylesheet" type="text/css" href="<? echo CSS_DIR; ?>/board.css" />
	<link rel="stylesheet" type="text/css" href="<? echo CSS_DIR; ?>/select.css" />
	<link rel="stylesheet" type="text/css" href="<? echo CSS_DIR; ?>/biz_common.css" />

	<script type="text/javascript" src="<? echo INC_DIR; ?>/jquery/1.11.3/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="<? echo INC_DIR; ?>/jquery-cookie/1.4.1/jquery.cookie.js"></script>
	<script type="text/javascript" src="<? echo INC_DIR; ?>/prototype/1.5.0/prototype.js"></script>
	<script>
	
	var bRunManualLauncher = false; // ManualLauncher를 실행시킬건지 결정하는 변수

	// ------------------------------------------------------------------
	// Function    : GetAccessCode()
	// Description : GetAccessCode.php를 호출하여 Accesscode를 얻어옴
	// ------------------------------------------------------------------
	function GetAccessCode() {
		var url = "GetAccesscode.php";
		var pars = "accesscode=" + csform.accesscode.value + "&name=" + csform.name.value;

		pars = compactTrim(pars);
		//var pars = "accesscode=" + csform.accesscode.value+"&name=232"; 	
		
		bRunManualLauncher = false;	
	
		new Ajax.Request( url, 
		{
		     method: 'post',
		     parameters: pars,
		     asynchronous : false,
		     onSuccess: CheckAccessCode,
		     error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('some error');
		     }
	 	 }, true);
	 	  
	 	 return bRunManualLauncher;
	}

	function compactTrim(str) {
		return str.replace( /(\s*)/g, "" );
	}

	// ------------------------------------------------------------------
	// Function    : CheckAccessCode()
	// Description : 얻어온 Accesscode를 검사하고 Title을 변경한다 
	// ------------------------------------------------------------------
	function CheckAccessCode(transport) {
	
		var accesscode = transport.responseText;
		if(accesscode.indexOf("Invalid1") >= 0 || accesscode.indexOf("Invalid2") >= 0) {
			bRunManualLauncher = false;

			// form
			var form = document.createElement("form");     
			form.setAttribute("method","post");                    
			form.setAttribute("action","");        
			document.body.appendChild(form);

			//input
			var input_valid_code_res = document.createElement("input");  
			input_valid_code_res.setAttribute("type", "hidden");                 
			input_valid_code_res.setAttribute("name", "VALIDATION_CODE_RES");                        
			input_valid_code_res.setAttribute("value", "FAIL");                          
			form.appendChild(input_valid_code_res);
			 
			//폼전송
			form.submit();	
		}
		else {
			parent.document.title = "AnyHelp Neturo Launcher "+transport.responseText+"1111111111111111111111111111111111111";
			this.document.title = "AnyHelp Neturo Launcher "+transport.responseText+"1111111111111111111111111111111111111";
			//parent.document.title = "Koino AnySupport ActiveX "+transport.responseText+"1111111111111111111111111111111111111";
			//this.document.title = "Koino AnySupport ActiveX "+transport.responseText+"1111111111111111111111111111111111111";
			bRunManualLauncher = true;
		}	
	}

	function CallAgent(accesscode) {
		//url = "cs.html?access=" + accesscode;
		document.csform.accesscode.value = accesscode;
		GetAccessCode();
		document.csform.submit();
	}

	function Submit() {

		var val =  GetAccessCode();
		
		if (val) {
			document.csform.submit();
		}
	}

	function ManualExecute() {
		//ManualLauncher에 group code 전달
		<?

			echo "parent.document.title = \"AnyHelp Neturo Launcher - ".$groupid."\" ;";
			echo "this.document.title = \"AnyHelp Neturo Launcher - ".$groupid."\" ;";

		?>
		location.href = "download.php";
	}

	function RefreshPage() {
		window.location.reload(true);
	}

	function InitFocus() {
	<?
	if($operation_type == "0" || $operation_type == "2") {//인증번호 넣는 타입만..
	?>
		document.csform.accesscode.focus();
	<?
	} 
	?>
	}

	function AddFavorite(name) {
		loc = location.href;
		pos = loc.lastIndexOf("/");
		url = loc.substr(0, pos);
		window.external.AddFavorite(url, name); 
	}

	function CreateShortcut() {
		if(KoinoLoader) {
			loc = location.href;
			pos = loc.lastIndexOf("/");
			url = loc.substr(0, pos);
			KoinoLoader.CreateShortcut("애니헬프 고객접속페이지", url);
			alert("바탕화면에 바로가기가 추가되었습니다");
		}
	}

	function SplitAccesscode(){
		var code = document.getElementById("accesscode");//fname아이디의 값을 가져 온다.

		//숫자열 0 ~ 9 : 48 ~ 57 , 8 : backspace , 13 : enter
		//alert(event.keyCode);
		if(event.keyCode >= 48 && event.keyCode <= 57 || event.keyCode == 8){
			if(code.value.length == 3){//세글자 뒤 공백 추가
				code.value = code.value + " ";
			}
	    		}
	    		else if(event.keyCode == 13){
	    			var val =  GetAccessCode();
			if (val) {
				document.csform.submit();
			}
	    		}
	    		else{
			if(event.preventDefault){
				event.preventDefault();
			} 
			else {
				event.returnValue = false;
			}
		}

	}

	function popup_help(url) {
		
		var locationStr = url;
		var optionStr = "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0,copyhistory=0,width=695,height=700";
		window.open(locationStr, 'AnySupport', optionStr);
	}
	</script>
</head>
<body onload="InitFocus()" oncontextmenu="return false">
