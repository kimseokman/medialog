﻿<?php
	$title_kr = "";
	$title_en = "";
	
	if($customer_auth == 'PUBLIC'){
		$image_url = BASE_LOGO_DIR . "/" . BASE_LOGO;
	}
	else if($customer_auth == 'PRIVATE'){
		$subdomain = $utils_obj->GetSubdomain();
		$company = $main_obj->GetSubDomainInfo($subdomain);
		$image_url = CUSTOMER_LOGO_DIR . "/" . $company['logo'];

		$logo_exist_flag = file_exists($_SERVER['DOCUMENT_ROOT'].$image_url);
		if( (!$logo_exist_flag) || ($company==null)){
			$image_url = BASE_LOGO_DIR . "/" . BASE_LOGO;
		}
	}

	/* 접속 페이지 속성 : ActiveX를 사용하는지 여부 , ActiveX 사용 (1) , 접속기로 만(0) */
	$is_using_ax = "0";
	
	/* 접속 페이지 속성 : 인증 암호만(0) , 원클릭만(1) , 원클릭 + 인증암호(2) */
	$operation_type = "0";

	$country = $_SERVER["HTTP_ACCEPT_LANGUAGE"];

	if (strstr($country, "ko")) $country = "ko";
	else if (strstr($country, "ja")) $country = "ja";
	else if (strstr($country, "en")) $country = "en";
	else $country = "en";
