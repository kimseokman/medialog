<?php
/**
 * ajax_admin_support_log_list.php
 * 2015.08.14 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$page = $_POST['page'];
if(!$_POST['page']){
	$page = 1;
}

$search_type = $_POST['search_type'];
if(!$_POST['search_type']){
	$search_type = "";
}

$search_val = $_POST['search_val'];
if(!$_POST['search_val']){
	$search_val = "";	
}

$start_time = $_POST['start_time'];
if(!$_POST['start_time']){
	$start_time = date("Y-m-d");
	$start_YYYY = date("Y");
	$start_MM = date("m");
	$start_DD = date("d");
}
else{
	$start_time_arr = explode("-", $start_time);
	$start_YYYY = $start_time_arr[0];
	$start_MM = $start_time_arr[1];
	$start_DD = $start_time_arr[2];
}

$end_time = $_POST['end_time'];
if(!$_POST['end_time']){
	$end_time = date("Y-m-d");
	$end_YYYY = date("Y");
	$end_MM = date("m");
	$end_DD = date("d");
}
else{
	$end_time_arr = explode("-", $end_time);
	$end_YYYY = $end_time_arr[0];
	$end_MM = $end_time_arr[1];
	$end_DD = $end_time_arr[2];
}

$exist_report = $main_obj->CheckReportExsit($search_type, $search_val, $start_time, $end_time);
$report_arr = $main_obj->GetReportList($page, $search_type, $search_val, $start_time, $end_time);

$content = $report_arr["datainfo"];
$pagination = $report_arr["pageInfo"];
$content_size = count($content);

function print_last($base, $current){
	$class_name = "";

	if($base == $current){
		$class_name = "last";
	}

	return $class_name;
}

function print_imp($print_flag){
	if($print_flag)
		return "imp";
}
?>
<table class="style_board_default" summary="번호, 부서명, 설명, 수정/삭제가 있는 게시판" width="100%">
	<caption><span>번호, 부서명, 설명, 수정/삭제가 있는 게시판</span></caption>
	<colgroup>
		<col style="width:70px;" />
		<col style="width:100px;" />
		<col style="width:100px;" />
		<col style="width:100px;" />
		<col style="width:80px;" />
		<col style="width:*;" />
		<col style="width:120px;" />
	</colgroup>
	<thead>
		<tr>
			<th>NO</th>
			<th>사용자ID</th>
			<th>사용자명</th>
			<th>고객명</th>
			<th>시작시각</th>
			<th>종료시각</th>
			<th>고객OS</th>
			<th class="last">해결여부</th>
		</tr>
	</thead>
	<tbody>
	<?
	if( ($exist_report == FALSE) || ($content_size == 0)){// not exist report
	?>
		<tr class="last">
			<td colspan="8">원격 지원 기록이 없습니다.</td>
		</tr>
	<?
	}
	else{//exist report
		$page_num = $report_arr["pageNum"];
		$last_node_num = $content_size - 1;
		for($i=0; $i < $content_size; $i++){
			$user_os = $utils_obj->GetOS($content[$i]["user_os"]);
			$user_oper = $utils_obj->GetOperationLog($content[$i]["operationlog"]);
	?>
		<tr class="<? echo print_last($last_node_num, $i)?>">
			<td><? echo $content[$i]["num"]; ?></td>
			<td><a href="#" onClick="open_pop_detail_report(event, <? echo $content[$i]["reportnum"]; ?>, <? echo $page; ?>)"><? echo $content[$i]['sptid']; ?></a></td>
			<td><? echo $content[$i]["sptname"]; ?></td>
			<td><? echo $content[$i]["name"]; ?></td>
			<td><? echo $content[$i]["starttime"]; ?></td>
			<td><? echo $content[$i]["endtime"]; ?></td>
			<td><? echo $user_os; ?></td>
			<td><? echo $user_oper; ?></td>
		</tr>
	<?
			$page_num--;
		}
	}
	?>	
	</tbody>
</table>
<?
$page_prev = $pagination->GetIsPrev();
$page_list = $pagination->GetPageList();
$page_next = $pagination->GetIsNext();

if( ($exist_report != FALSE) && ($content_size != 0)){// not exist report
?>
<!-- [D] 한 화면의 지원기록의 내역수가 15개를 초과하면 페이징 처리 -->
<div class="paging">
	<?
	if($page_prev['is_older_page']){
	?>
	<a href="#" class="btn_arrow ar_fst" onClick="support_history_pagination(event, <? echo $page_prev['older_page']; ?>)">첫페이지</a>
	<?
	}
	else{
	?>
	<a href="#" class="btn_arrow ar_fst">첫페이지</a>
	<?
	}

	if($page_prev['is_prev_page']){
	?>
	<a href="#" class="btn_arrow ar_prev" onClick="support_history_pagination(event, <? echo $page_prev['prev_page']; ?>)">이전페이지</a>
	<?
	}
	else{
	?>
	<a href="#" class="btn_arrow ar_prev">이전페이지</a>
	<?
	}

	while(list($k, $v) = each($page_list)){
	?>
	<a href="#" onClick="support_history_pagination(event, <? echo $k; ?>)"><span class="<? echo print_imp($v['is_current']); ?>"><? echo $k; ?></span></a>
	<?
	}//end of : while(list($k, $v) = each($page_list))

	if($page_next['is_next_page']){
	?>
	<a href="#" class="btn_arrow ar_next" onClick="support_history_pagination(event, <? echo $page_next['next_page']; ?>)"><span class="hidden">다음페이지</span></a>
	<?
	}
	else{
	?>
	<a href="#" class="btn_arrow ar_next"><span class="hidden">다음페이지</span></a>
	<?
	}

	if($page_next['is_newer_page']){
	?>
	<a href="#" class="btn_arrow ar_last" onClick="support_history_pagination(event, <? echo $page_next['newer_page']; ?>)"><span class="hidden">마지막페이지</span></a>
	<?
	}
	else{
	?>
	<a href="#" class="btn_arrow ar_last"><span class="hidden">마지막페이지</span></a>
	<?
	}
	?>
</div>
<?
}//end of : if($exist_report != FALSE)
?>