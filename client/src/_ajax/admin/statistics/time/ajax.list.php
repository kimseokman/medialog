<?php
/**
 * ajax_admin_statistics_time_list.php
 * 2015.08.18 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$start_time = $_POST['start_time'];
if(!$_POST['start_time']){
	$start_time = date("Y-m-d");
	$start_YYYY = date("Y");
	$start_MM = date("m");
	$start_DD = date("d");
}
else{
	$start_time_arr = explode("-", $start_time);
	$start_YYYY = $start_time_arr[0];
	$start_MM = $start_time_arr[1];
	$start_DD = $start_time_arr[2];
}

$end_time = $_POST['end_time'];

if(!$_POST['end_time']){
	$end_time = date("Y-m-d");
	$end_YYYY = date("Y");
	$end_MM = date("m");
	$end_DD = date("d");
}
else{
	$end_time_arr = explode("-", $end_time);
	$end_YYYY = $end_time_arr[0];
	$end_MM = $end_time_arr[1];
	$end_DD = $end_time_arr[2];
}

$result = $main_obj->GetReportByTime($start_time, $end_time);

$total = 0;

for($i=0;$i<24;$i++){
	$data[$i]->ccount = 0;
	$data[$i]->totaltime = 0;
}

while($row = mysql_fetch_array($result)){
	$t = intVal(substr($row['starttime'],11,2));
	$z = $t;

	$data[$z]->ccount++;
	$total++;
	
	//zisako 2014.1.23 통계데이터 -값 오류 수정
	$intervaltime = $row['intervaltime'];
	if($intervaltime<0) $intervaltime = 0;
	$data[$z]->totaltime +=$intervaltime;
}

$totalPer = 0;
$data_size = count($data);
for($i=0; $i < $data_size; $i++){
	if($total != 0){
		$per[$i] = $rate = $data[$i]->ccount/$total*100;
	}else{
		$per[$i] = 0;
	}
	$totalPer += $per[$i];
}

$last_node = $data_size - 1;

function print_last($base, $current){
	$class_name = "";

	if($base == $current){
		$class_name = "last";
	}

	return $class_name;
}

function getTimeFromSeconds($seconds){
	$h = sprintf("%02d", intval($seconds) / 3600);
	$tmp = $seconds % 3600;
	$m = sprintf("%02d", $tmp / 60);
	$s = sprintf("%02d", $tmp % 60);
	return $h.':'.$m.':'.$s;
}

$totaltotaltime;
for($i=0;$i<sizeof($data);$i++){
	$totaltotaltime += $data[$i]->totaltime;
}
?>
<table class="style_board_default" summary="번호, 부서명, 설명, 수정/삭제가 있는 게시판" width="100%">
	<caption><span>번호, 부서명, 설명, 수정/삭제가 있는 게시판</span></caption>
	<colgroup>
		<col style="width:90px;" />
		<col style="width:120px;" />
		<col style="width:*;" />
		<col style="width:220px;" />
		<col style="width:130px;" />
	</colgroup>
	<thead>
		<tr>
			<th>시간</th>
			<th>접속횟수</th>
			<th>총 이용 시간</th>
			<th>평균 이용 시간</th>
			<th class="last">백분율</th>
		</tr>
	</thead>
	<tbody>
	<?
	for($i=0; $i < $data_size; $i++){
	?>
		<tr class="<? echo print_last($last_node, $i); ?>">
			<td><? echo str_pad($i,"2","0",STR_PAD_LEFT).":00"; ?>~<? echo str_pad($i,"2","0",STR_PAD_LEFT).":59"; ?></td>
			<td><? echo $data[$i]->ccount; ?></td>
			<td><? echo getTimeFromSeconds($data[$i]->totaltime); ?></td>
			<td><? echo getTimeFromSeconds($data[$i]->totaltime/$data[$i]->ccount); ?></td>
			<td><? echo sprintf("%.2f",$per[$i]); ?>%</td>
		</tr>
	<?
	}//end of : for($i=0; $i < $data_size; $i++)
	?>	
	</tbody>
</table>