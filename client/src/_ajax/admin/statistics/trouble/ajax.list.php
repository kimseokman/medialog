<?php
/**
 * ajax_admin_statistics_trouble_list.php
 * 2015.08.19 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$start_time = $_POST['start_time'];
if(!$_POST['start_time']){
	$start_time = date("Y-m-d");
	$start_YYYY = date("Y");
	$start_MM = date("m");
	$start_DD = date("d");
}
else{
	$start_time_arr = explode("-", $start_time);
	$start_YYYY = $start_time_arr[0];
	$start_MM = $start_time_arr[1];
	$start_DD = $start_time_arr[2];
}

$end_time = $_POST['end_time'];
if(!$_POST['end_time']){
	$end_time = date("Y-m-d");
	$end_YYYY = date("Y");
	$end_MM = date("m");
	$end_DD = date("d");
}
else{
	$end_time_arr = explode("-", $end_time);
	$end_YYYY = $end_time_arr[0];
	$end_MM = $end_time_arr[1];
	$end_DD = $end_time_arr[2];
}

$stats_arr = $main_obj->GetReportByTroubleType($start_time, $end_time);
$stats_size = count($stats_arr);
$last_node = $stats_size - 1;

$total = 0;
for($i=0; $i < $stats_size; $i++){

	$total += $stats_arr[$i]["res1"] + $stats_arr[$i]["res2"] + $stats_arr[$i]["res3"] + $stats_arr[$i]["res0"];
}

$totalPer = 0;
for($i=0; $i < $stats_size; $i++){

	$stats_sum = $stats_arr[$i]["res1"] + $stats_arr[$i]["res2"] + $stats_arr[$i]["res3"] + $stats_arr[$i]["res0"];
	if($total != 0){
		$per[$i] = $rate = $stats_sum/$total*100;
	}
	else{
		$per[$i] = 0;
	}
	$totalPer += $per[$i];
}

function print_last($base, $current){
	$class_name = "";

	if($base == $current){
		$class_name = "last";
	}

	return $class_name;
}
?>
<table class="style_board_default" summary="번호, 부서명, 설명, 수정/삭제가 있는 게시판" width="100%">
	<caption><span>번호, 부서명, 설명, 수정/삭제가 있는 게시판</span></caption>
	<colgroup>
		<col style="width:18%;" />
		<col style="width:15%;" />
		<col style="width:15%;" />
		<col style="width:%;" />
		<col style="width:15%;" />
		<col style="width:15%;" />
	</colgroup>
	<thead>
		<tr>
			<th>장애유형</th>
			<th>해결</th>
			<th>보류</th>
			<th>직접지원 필요</th>
			<th>미체크</th>
			<th class="last">백분율</th>
		</tr>
	</thead>
	<tbody>
	<?
	for($i=0; $i < $stats_size; $i++){
	?>
		<tr class="<? echo print_last($last_node, $i); ?>">
			<td><? echo $stats_arr[$i]['trbname']; ?></td>
			<td><? echo $stats_arr[$i]['res1']; ?></td>
			<td><? echo $stats_arr[$i]['res2']; ?></td>
			<td><? echo $stats_arr[$i]['res3']; ?></td>
			<td><? echo $stats_arr[$i]['res0']; ?></td>
			<td><? echo sprintf("%.2f",$per[$i]); ?>%</td>
		</tr>
	<?
	}
	?>	
	</tbody>
</table>