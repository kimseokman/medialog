<?php
/**
 * ajax_admin_statistics_month_list.php
 * 2015.08.18 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$search_year = $_POST['search_year'];
if(!$_POST['search_year']){
	$search_year = date("Y");
	$start_YYYY = date("Y");
}

$result = $main_obj->GetReportByMonth($search_year);

$year_arr = $utils_obj->GetYears();
$YYYY_cnt = count($year_arr);
$month_arr = $utils_obj->GetMonths();

$total = 0;
for($i=0;$i<12;$i++){
	$data[$i] = 0;
}

while($row = mysql_fetch_array($result)){
	$tmp = explode("-",$row['starttime']);
	$t = intVal($tmp[1]);
	$z = --$t;
	$data[$z]++;
	$total++;
}


$totalPer = 0;
$data_size = count($data);
$last_node = $data_size - 1;
for($i=0; $i < $data_size; $i++){
	if($total != 0){
		$per[$i] = $rate = $data[$i]/$total*100;
	}else{
		$per[$i] = 0;
	}

	$totalPer += $per[$i];
}

function print_last($base, $current){
	$class_name = "";

	if($base == $current){
		$class_name = "last";
	}

	return $class_name;
}
?>
<table class="style_board_default" summary="번호, 부서명, 설명, 수정/삭제가 있는 게시판" width="100%">
	<caption><span>번호, 부서명, 설명, 수정/삭제가 있는 게시판</span></caption>
	<colgroup>
		<col style="width:15%;" />
		<col style="width:*;" />
		<col style="width:20%;" />
	</colgroup>
	<thead>
		<tr>
			<th>날짜(월)</th>
			<th>접속횟수(회)</th>
			<th class="last">백분율</th>
		</tr>
	</thead>
	<tbody>
	<?
	for($i=0; $i < $data_size; $i++){
	?>	
		<tr class="<? echo print_last($last_node, $i); ?>">
			<td><? echo $month_arr[$i]; ?></td>
			<td><? echo $data[$i]; ?></td>
			<td><? echo sprintf("%.2f",$per[$i]); ?>%</td>
		</tr>
	<?
	}
	?>
	</tbody>
</table>