<?php
/**
 * ajax_admin_user_list.php
 * 2015.08.06 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$page = $_POST['page'];
if(!$_POST['page']){
	$page = 1;	
}

$search = $_POST['search'];
if(!$_POST['search']){
	$search = "";
}

$searchVal = $_POST['searchVal'];
if(!$_POST['searchVal']){
	$searchVal = "";
}

$spt_arr = $main_obj->GetSupporterListPage($page, $search, $searchVal);

$pagination = $spt_arr['pagination'];
$content = $spt_arr['content'];

$spt_size = count($content);

function print_last($current_node, $last_node){
	$class_name = "";

	if($current_node == $last_node){
		$class_name = "last";
	}

	return $class_name;
}

function print_imp($print_flag){
	if($print_flag)
		return "imp";
}
?>
<table class="style_board_default" summary="번호, 부서명, 설명, 수정/삭제가 있는 게시판" width="100%">
<caption><span>번호, 부서명, 설명, 수정/삭제가 있는 게시판</span></caption>
<colgroup>
	<col style="width:70px;" />
	<col style="width:100px;" />
	<col style="width:100px;" />
	<col style="width:100px;" />
	<col style="width:80px;" />
	<col style="width:*;" />
	<col style="width:120px;" />
</colgroup>
<thead>
	<tr>
		<th>NO</th>
		<th>사용자ID</th>
		<th>사용자명</th>
		<th>소속 부서</th>
		<th>상태</th>
		<th>최근접속시간</th>
		<th class="last">지원권한</th>
	</tr>
</thead>
<tbody>
<?
if( ($spt_size == 1) && ($content[0]['sptnum']=='') ){
?>
	<tr class="last">
		<td colspan="7">상담원 정보가 없습니다.</td>
	</tr>
<?
}//end of : if($spt_size == 0)
else{
	$last_node_num = $spt_size - 1;
	for($i = 0; $i < $spt_size; $i++){
		//set dept name 
		$user_dept_num = $content[$i]['depnum'];
		$dept = $main_obj->GetDepByDepnum($user_dept_num);
		$dep_name = $dept->depname;

		//set status
		$user_status = $content[$i]['logon'];
		$user_status_call = $content[$i]['cnt'];
		if($user_status == 0){
			$status = "오프라인";
		}
		else if($user_status == 1) {
			if($user_status_call == 0) {
				$status = "온라인";
			} 
			else {
				$status = "지원중";
			}
		} 
		else if($user_status == 2) {
			$status = "지원중";
		}
		
		//set auth
		$user_auth = $content[$i]['function'];
		if(($user_auth & PERMISSION_MOBILE_VIDEO_PAID) || ($user_auth & PERMISSION_MOBILE_VIDEO_FREE)){
			$auth = PRODUCT_MOBILE_VIDEO;
		}
		else if(($user_auth & PERMISSION_MOBILE_PAID) || ($user_auth & PERMISSION_MOBILE_FREE)){
			$auth = PRODUCT_MOBILE;
		}
		else if(($user_auth & PERMISSION_VIDEO_PAID) || ($user_auth & PERMISSION_VIDEO_FREE)){
			$auth = PRODUCT_VIDEO;
		}
		else{
			$auth = PRODUCT_PC;
		}
?>
	<tr class="<? echo print_last($i, $last_node_num); ?>">
		<td><? echo $content[$i]['num']; ?></td>
		<td><a href="#" onClick="user_detail(event, <? echo $content[$i]['sptnum']; ?>, <? echo $page; ?>, '<? echo $search; ?>', '<? echo $searchVal; ?>')"><? echo $content[$i]['sptid']; ?></a></td>
		<td><? echo $content[$i]['name']; ?></td>
		<td><? echo $dep_name; ?></td>
		<td><? echo $status; ?></td>
		<td><? echo $content[$i]['lastlogin']; ?></td>
		<td>
		<?
		if($auth == PRODUCT_MOBILE_VIDEO){
		?>
			<a href="#"><img src="<? echo IMG_DIR; ?>/sub/ico_pc.gif" alt="PC" /></a>
			<a href="#"><img src="<? echo IMG_DIR; ?>/sub/ico_mob.gif" alt="모바일" /></a>
			<a href="#"><img src="<? echo IMG_DIR; ?>/sub/ico_vid.gif" alt="비디오" /></a>
		<?
		}//end of : if($auth == PRODUCT_MOBILE_VIDEO)
		else if($auth == PRODUCT_MOBILE){
		?>
			<a href="#"><img src="<? echo IMG_DIR; ?>/sub/ico_pc.gif" alt="PC" /></a>
			<a href="#"><img src="<? echo IMG_DIR; ?>/sub/ico_mob.gif" alt="모바일" /></a>
		<?
		}//end of : if($auth == PRODUCT_MOBILE)
		else if($auth == PRODUCT_VIDEO){
		?>
			<a href="#"><img src="<? echo IMG_DIR; ?>/sub/ico_pc.gif" alt="PC" /></a>
			<a href="#"><img src="<? echo IMG_DIR; ?>/sub/ico_vid.gif" alt="비디오" /></a>
		<?
		}//end of : if($auth == PRODUCT_VIDEO)
		else if($auth == PRODUCT_PC){
		?>
			<a href="#"><img src="<? echo IMG_DIR; ?>/sub/ico_pc.gif" alt="PC" /></a>
		<?
		}//end of : if($auth == PRODUCT_PC)
		?>
		</td>
	</tr>
<?
	}//end of : for($i = 0; $i < $dept_size; $i++)
}//end of : if($spt_size != 0)
?>
</tbody>
</table>
<?
$page_prev = $pagination->GetIsPrev();
$page_list = $pagination->GetPageList();
$page_next = $pagination->GetIsNext();
?>
<!-- [D] 사용자 항목이 15개를 초과하면 페이징 처리 -->
<div class="paging">
<?
if($page_prev['is_older_page']){
?>
<a href="#" class="btn_arrow ar_fst" onClick="user_pagination(event, <? echo $page_prev['older_page']; ?>)">첫페이지</a>
<?
}
else{
?>
<a href="#" class="btn_arrow ar_fst">첫페이지</a>
<?
}

if($page_prev['is_prev_page']){
?>
<a href="#" class="btn_arrow ar_prev" onClick="user_pagination(event, <? echo $page_prev['prev_page']; ?>)">이전페이지</a>
<?
}
else{
?>
<a href="#" class="btn_arrow ar_prev">이전페이지</a>
<?
}

while(list($k, $v) = each($page_list)){
?>
<a href="#" onClick="user_pagination(event, <? echo $k; ?>)"><span class="<? echo print_imp($v['is_current']);?>"><? echo $k; ?></span></a>
<?
}//end of : while(list($k, $v) = each($page_list))

if($page_next['is_next_page']){
?>
<a href="#" class="btn_arrow ar_next" onClick="user_pagination(event, <? echo $page_next['next_page']; ?>)"><span class="hidden">다음페이지</span></a>
<?
}
else{
?>
<a href="#" class="btn_arrow ar_next"><span class="hidden">다음페이지</span></a>
<?
}

if($page_next['is_newer_page']){
?>
<a href="#" class="btn_arrow ar_last" onClick="user_pagination(event, <? echo $page_next['newer_page']; ?>)"><span class="hidden">마지막페이지</span></a>
<?
}
else{
?>
<a href="#" class="btn_arrow ar_last"><span class="hidden">마지막페이지</span></a>
<?
}
?>
</div>