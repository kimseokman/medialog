<?php
/**
 * validate_retype_pw.php
 * 2015.08.11 | KSM | create
 */
session_start();
header('Content-Type: application/json');

include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.constants.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGUtils.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGMain.php");

$guide = array('flag' => FALSE, 'msg' => '&nbsp');

$password = $_POST['user_pw'];
$retype_password = $_POST['user_retype_pw'];

if($password == $retype_password){
	$guide['flag'] = TRUE;
	$guide['msg'] = NULL;
}
else{
	$guide['flag'] = TRUE;
	$guide['msg'] = '비밀번호를 다시 확인 해주세요.';
}

$output = json_encode($guide);

echo urldecode($output);