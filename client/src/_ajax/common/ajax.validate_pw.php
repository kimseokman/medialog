<?php
/**
 * validate_pw.php
 * 2015.08.11 | KSM | create
 */
session_start();
header('Content-Type: application/json');

include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.constants.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGUtils.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGMain.php");

$guide = array('flag' => FALSE, 'msg' => '&nbsp');

$password = $_POST['user_pw'];

$pw_arr = str_split($password);
$pw_size = count($pw_arr);
$pw_length = PASSWORD_LENGTH;
$pw_length_min = PASSWORD_LENGTH_MIN;
$pw_length_max = PASSWORD_LENGTH_MAX;
$pw_safe_cnt = PASSWORD_SAFE_CNT;

$safe_cnt = 0;
$decimal_cnt = 0;
$eng_cnt = 0;
$special_cnt = 0;
$flag_config_error = FALSE;

for($i=0; $i < $pw_size; $i++){
	$acii_num = ord ($pw_arr[$i]);
	$temp = $acii_num;

	if( (48 <= $acii_num) && ($acii_num <= 57) ){//숫자
		$decimal_cnt  = 1;
	}
	else if( (65 <= $acii_num) && ($acii_num <= 90) ){//영문자 : 대문자
		$eng_cnt = 1;
	}
	else if( (97 <= $acii_num) && ($acii_num <= 122) ){//영문자 : 소문자
		$eng_cnt = 1;
	}
	else if( (33 <= $acii_num) && ($acii_num <= 47) ){//특수문자1
		$special_cnt = 1;
	}
	else if( (58 <= $acii_num) && ($acii_num <= 64) ){//특수문자2
		$special_cnt = 1;
	}
	else if( (91 <= $acii_num) && ($acii_num <= 96) ){//특수문자3
		$special_cnt = 1;
	}
	else if( (123 <= $acii_num) && ($acii_num <= 126) ){//특수문자4
		$special_cnt = 1;
	}
	else{
		$flag_config_error = TRUE;
	}
}

$safe_cnt = $decimal_cnt + $eng_cnt + $special_cnt;

if($flag_config_error == TRUE){
	$guide['flag'] = TRUE;
	$guide['msg'] = "영문 대/소문자,특수문자,숫자만 입력해주십시오.";
}
else{//입력한 패스워드 유효범위
	if($safe_cnt < $pw_safe_cnt){//유효 카운트가 2개보다 적을 때
		$guide['flag'] = TRUE;
		$guide['msg'] = "영문 대/소문자,특수문자,숫자 중 ".$pw_safe_cnt."가지 이상으로 설정해 주세요.";
	}
	else {//유효 카운트가 2개이상일때
		if( ($pw_size < $pw_length_min) || ($pw_size > $pw_length_max) ){//password 길이가 10자리 보다 작을때
			$guide['flag'] = TRUE;
			$guide['msg'] = "영문 대/소문자,특수문자,숫자 중".$pw_safe_cnt."가지 이상, ".$pw_length_min."자~".$pw_length_max."자로 설정해 주세요.";
		}
		else{
			$guide['flag'] = TRUE;
			$guide['msg'] = "안전한 비밀번호 입니다.";	
		}
	}	
}

$output = json_encode($guide);

echo urldecode($output);