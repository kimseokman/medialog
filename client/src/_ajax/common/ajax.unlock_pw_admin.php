<?php
/**
 * unlock_pw.php
 * 2015.11.03 | KSM | create
 */
session_start();
header('Content-Type: application/json');

include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.constants.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGUtils.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGMain.php");

$login_try_limit = LOGIN_TRY_COUNT_MAX;

$guide = array('flag' => FALSE, 'msg' => '&nbsp');

$id = $_POST['id'];

$main_obj = new ASGMain();

$res = $main_obj->SetLoginTryCount($id, 0);

$guide['flag'] = TRUE;
if($res == "ADMIN"){
	$guide['msg'] = "UNLOCK";
}
else{
	$try_cnt = $main_obj->GetLoginTryCount($id);

	if($try_cnt >= $login_try_limit){
		$guide['msg'] = "LOCK";
	}
	else{
		$guide['msg'] = "UNLOCK";	
	}
}

$output = json_encode($guide);

echo urldecode($output);