<?php
/**
 * ajax_notice_detail.php
 * 2015.07.31 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
$seq = $_POST['seq'];
$data = $main_obj->GetNotice($seq);
?>
<!-- 공지사항 뷰페이지 -->
<table class="style_board_view" summary="글제목, 날짜가 있는 게시판" width="100%">
	<caption><span>글제목, 날짜가 있는 게시판</span></caption>
	<colgroup>
		<col style="width:*;" />
		<col style="width:35px;" />
		<col style="width:65px;" />
	</colgroup>
	<thead>
		<tr>
			<th class="title"><? echo $data['title']; ?></th>
			<th class="date"><span>날짜</span></th>
			<th><? echo $data['regdate']; ?></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="3">
				<div id="" class="notice_view">
					<? echo preg_replace("/\r\n/", "<br />", $data['content']); ?>
				</div>
			</td>
		</tr>
	</tbody>
</table>