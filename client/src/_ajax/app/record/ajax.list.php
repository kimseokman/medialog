<?php
/**
 * ajax_app_record_list.php
 * 2015.08.25 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
include_once($_SERVER['DOCUMENT_ROOT'] . APP_DIR . "/common/config.php");

$sptid = $_POST["sptid"];
$pw = APP_COWORK_LOGIN_PW;

$main_obj->HistoryLogin($sptid, $pw);
$app_login_res = $main_obj->HistoryConfirmLogin();

if($app_login_res != "OK"){
?>
<form name="redirectBanForm" method="post" action="<? echo $config_path["NO_AUTH"]; ?>">
</form>
<script>
	document.redirectBanForm.submit();
</script>
<?	
}//end of : if($app_login_res == "")

$page = $_POST['page'];
if(!$_POST['page']){
	$page = 0;
}

$search_type = 'sptid';
$search_val = $sptid;

$start_time = $_POST['start_time'];
if(!$_POST['start_time']){
	$start_time = date("Y-m-d");
	$start_YYYY = date("Y");
	$start_MM = date("m");
	$start_DD = date("d");
}
else{
	$start_time_arr = explode("-", $start_time);
	$start_YYYY = $start_time_arr[0];
	$start_MM = $start_time_arr[1];
	$start_DD = $start_time_arr[2];
}

$end_time = $_POST['end_time'];
if(!$_POST['end_time']){
	$end_time = date("Y-m-d");
	$end_YYYY = date("Y");
	$end_MM = date("m");
	$end_DD = date("d");
}
else{
	$end_time_arr = explode("-", $end_time);
	$end_YYYY = $end_time_arr[0];
	$end_MM = $end_time_arr[1];
	$end_DD = $end_time_arr[2];
}


$exist_report = $main_obj->CheckReportExsit($search_type, $search_val, $start_time, $end_time);
$report_arr = $main_obj->GetReportList($page, $search_type, $search_val, $start_time, $end_time);

$content = $report_arr["datainfo"];
$pagination = $report_arr["pageInfo"];
$content_size = count($content);
$last_node = $content_size - 1;

$year_arr = $utils_obj->GetYears();
$YYYY_cnt = count($year_arr);
$month_arr = $utils_obj->GetMonths();
$MM_cnt = count($month_arr);
$this_last_day = $utils_obj->GetLastDay(date('m'), date('Y'));
$day_arr = $utils_obj->GetDays($this_last_day);
$DD_cnt = count($day_arr);

function print_last_bot($base, $current){
	$class_name = "";

	if($base == $current){
		$class_name = "last_bot";
	}

	return $class_name;
}

function print_point($base, $current){
	$class_name = "";

	if($base == $current){
		$class_name = "point";
	}

	return $class_name;
}

function print_selected($base, $current){
	$str_value = "";

	if($base == $current){
		$str_value = "selected";
	}

	return $str_value;
}
?>
<table class="style_board_default first_tbl manager_tbl"style="width:100%;" summary="날짜, 고객명,시작시간,종료시간,고객os,해결여부표">
	<caption><span>지원기록 관리</span></caption>
	<colgroup>
		<col style="width:80px;" />
		<col style="width:60px;" />
		<col style="width:70px;" />
		<col style="width:70px;" />
		<col style="width:70px;" />
		<col style="width:*;" />
	</colgroup>
	<tbody>
	<?
	if($exist_report == FALSE){// not exist report
	?>
		<tr class="last_bot">
			<td colspan="6">원격 지원 기록이 없습니다.</td>
		</tr>
	<?
	}
	else{//exist report
		for($i=0; $i < $content_size; $i++){
			$user_os = $utils_obj->GetOS($content[$i]["user_os"]);
			$user_oper = $utils_obj->GetOperationLog($content[$i]["operationlog"]);

			$start_time_arr = explode(' ', $content[$i]['starttime']);
			$end_time_arr = explode(' ', $content[$i]['endtime']);

			$date = $start_time_arr[0];
			$support_start_time = $start_time_arr[1];
			$support_end_time = $end_time_arr[1];
	?>
		<tr class="is_data <? echo print_last_bot($last_node, $i); ?>" onClick="pageTitleSet(event, '<? echo APP_RECORD_TITLE; ?>', <? echo $content[$i]['reportnum']; ?>)">
			<td><? echo $date; ?></td>
			<td><? echo $content[$i]['name']; ?></td>
			<td><? echo $support_start_time; ?></td>
			<td><? echo $support_end_time; ?></td>
			<td>
				<div class="overflow_wrap_70">
					<span class="overflow_dot">
						<? echo $user_os; ?>
					</span>
				</div>
			</td>
			<td class="<? echo print_point(RESOLVE, $user_oper); ?>"><? echo $user_oper; ?></td>
		</tr>
	<?						
		}
	}//end of : for($i=0; $i < $content_size; $i++)
	?>
	</tbody>
</table>