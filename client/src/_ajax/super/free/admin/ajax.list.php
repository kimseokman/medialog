<?php
include_once("../../../../_func/function.common.php");

$admin_act_type = ADMIN_ACT_TYPE_FREE;

$page = $_POST['page'];
if(!$_POST['page']){
	$page = 1;
}

$search = $_POST['search'];
if(!$_POST['search']){
	$search = "";
}

$searchVal = $_POST['searchVal'];
if(!$_POST['searchVal']){
	$searchVal = "";
}

$check_out_pw = SUPERADMIN_CHECKOUT_PW;
$admin_arr = $main_obj->SuperGetAdminList($page, $search, $searchVal, $admin_act_type);

$pagination = $admin_arr['pagination'];
$content = $admin_arr['content'];
$content_size = count($content);
$last_node = $content_size - 1;

function print_last($current_node, $last_node){
	$class_name = "";

	if($current_node == $last_node){
		$class_name = "last";
	}

	return $class_name;
}

function print_imp($print_flag){
	if($print_flag)
		return "imp";
}
?>
<input type="hidden" class="page" name="page" value="<? echo $page; ?>" />
<input type="hidden" class="search" name="search" value="<? echo $search; ?>" />
<input type="hidden" class="searchVal" name="searchVal" value="<? echo $searchVal; ?>" />

<table class="style_board_default" summary="번호, 부서명, 설명, 수정/삭제가 있는 게시판" width="100%">
	<caption><span>번호, 부서명, 설명, 수정/삭제가 있는 게시판</span></caption>
	<colgroup>
		<col style="width:70px;" />
		<col style="width:100px;" />
		<col style="width:*;" />
		<col style="width:80px;" />
		<col style="width:80px;" />
		<col style="width:80px;" />
		<col style="width:120px;" />
		<col style="width:120px;" />
		<col style="width:70px;" />
	</colgroup>
	<thead>
		<tr>
			<th>NO</th>
			<th>ID</th>
			<th>회사</th>
			<th>PC</th>
			<th>Mobile</th>
			<th>Video</th>
			<th>청구계정번호</th>
			<th>가입번호</th>
			<th class="last">활성</th>
		</tr>
	</thead>
	<tbody>
	<?
	if($content_size == 0){
	?>
		<tr class="last">
			<td colspan="9">관리자 정보가 없습니다.</td>
		</tr>
	<?
	}
	else{
		for($i=0; $i < $content_size; $i++){
			$pc_cnt = $content[$i]['using'] - $content[$i]['mobile_using'];
			$pc_all_cnt = $content[$i]['volume'] - $content[$i]['mobile_volume'];

			$video_cnt = 0;
			$video_volume = 0;

			$mobile_cnt = 0;
			$mobile_volume = 0;

			$user_auth = $content[$i]['function'];
			if(($user_auth & PERMISSION_MOBILE_VIDEO_PAID) || ($user_auth & PERMISSION_MOBILE_VIDEO_FREE)){
				$mobile_cnt = $content[$i]['mobile_using'];
				$mobile_volume = $content[$i]['mobile_volume'];
			}
			else if(($user_auth & PERMISSION_MOBILE_PAID) || ($user_auth & PERMISSION_MOBILE_FREE)){
				$mobile_cnt = $content[$i]['mobile_using'];
				$mobile_volume = $content[$i]['mobile_volume'];
			}
			else if(($user_auth & PERMISSION_VIDEO_PAID) || ($user_auth & PERMISSION_VIDEO_FREE)){
				$video_cnt = $content[$i]['mobile_using'];
				$video_volume = $content[$i]['mobile_volume'];
			}
			else{
				$video_cnt = 0;
				$video_volume = 0;

				$mobile_cnt = 0;
				$mobile_volume = 0;
			}
	?>
		<tr class="<? echo print_last($last_node, $i); ?>">
			<td><? echo $content[$i]['num']; ?></td>
			<td><a href="#" onClick="info_admin(event, '<? echo $content[$i]['adid']; ?>')"><? echo $content[$i]['adid']; ?></a></td>
			<td><? echo $content[$i]['company']; ?></td>
			<td><? echo $pc_cnt . " / " . $pc_all_cnt; ?></td>
			<td><? echo $mobile_cnt . " / " . $mobile_volume; ?></td>
			<td><? echo $video_cnt . " / " . $video_volume; ?></td>
			<td><? echo $content[$i]['bill_account']; ?></td>
			<td><? echo $content[$i]['sign_num']; ?></td>
			<td><? echo $content[$i]['enable']; ?></td>
			<!--td>
				<a href="#" onClick="checkout_admin(event, '<? echo $content[$i]['adid']; ?>', '<? echo $check_out_pw; ?>')">
					<img src="<? echo IMG_DIR; ?>/super/login.gif" alt="해당 관리자로 로그인" />
				</a>
			</td-->
		</tr>
	<?
		}//end of : for($i=0; $i < $content_size; $i++)
	}//end of : if($content_size == 0)	
	?>
	</tbody>
</table>

<?
$page_prev = $pagination->GetIsPrev();
$page_list = $pagination->GetPageList();
$page_next = $pagination->GetIsNext();
?>
<!-- [D] 사용자 항목이 15개를 초과하면 페이징 처리 -->
<div class="paging">
<?
if($page_prev['is_older_page']){
?>
	<a href="#" class="btn_arrow ar_fst" onClick="admin_pagination(event, <? echo $page_prev['older_page']; ?>)">첫페이지</a>
<?
}
else{
?>
	<a href="#" class="btn_arrow ar_fst">첫페이지</a>
<?
}

if($page_prev['is_prev_page']){
?>
	<a href="#" class="btn_arrow ar_prev" onClick="admin_pagination(event, <? echo $page_prev['prev_page']; ?>)">이전페이지</a>
<?
}
else{
?>
	<a href="#" class="btn_arrow ar_prev">이전페이지</a>
<?
}

while(list($k, $v) = each($page_list)){
?>
	<a href="#" onClick="admin_pagination(event, <? echo $k; ?>)"><span class="<? echo print_imp($v['is_current']);?>"><? echo $k; ?></span></a>
<?
}//end of : while(list($k, $v) = each($page_list))

if($page_next['is_next_page']){
?>
	<a href="#" class="btn_arrow ar_next" onClick="admin_pagination(event, <? echo $page_next['next_page']; ?>)"><span class="hidden">다음페이지</span></a>
<?
}
else{
?>
	<a href="#" class="btn_arrow ar_next"><span class="hidden">다음페이지</span></a>
<?
}

if($page_next['is_newer_page']){
?>
	<a href="#" class="btn_arrow ar_last" onClick="admin_pagination(event, <? echo $page_next['newer_page']; ?>)"><span class="hidden">마지막페이지</span></a>
<?
}
else{
?>
	<a href="#" class="btn_arrow ar_last"><span class="hidden">마지막페이지</span></a>
<?
}
?>
</div>