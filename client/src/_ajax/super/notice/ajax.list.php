<?php
/**
 * ajax_admin_setting_notice_list.php
 * 2015.08.20 | KSM | create
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$notice_page = $_POST['page'];
if(!$_POST['page']){
	$notice_page = 1;//page : 0 -> total	
}

$notice_search_type = $_POST['search_type'];
if(!$_POST['search_type']){
	$notice_search_type = "";
}

$notice_search_val = $_POST['search_val'];
if(!$_POST['search_val']){
	$notice_search_val = "";
}

$notice_arr = $main_obj->GetNoticeList($notice_page, $notice_search_type, $notice_search_val);

$notice_pagination = $notice_arr['pagination'];
$notice_content = $notice_arr['content'];
$notice_page_num = $notice_arr['page_num'];

function print_last($row_index, $last_index){
	if($row_index == $last_index)
		return "last";
}

function print_imp($print_flag){
	if($print_flag)
		return "imp";
}
?>

<input type="hidden" class="remember_page" value="<? echo $notice_page; ?>" />
<input type="hidden" class="remember_type" value="<? echo $notice_search_type; ?>" />
<input type="hidden" class="remember_val" value="<? echo $notice_search_val; ?>" />
<!-- 공지사항 테이블 -->
<!-- [D] 제목을 클릭하면 해당공지사항의 상세화면(공지사항 뷰페이지)이 보여짐. -->
<table class="style_board_default" summary="글번호, 분류, 제목, 작성자, 날짜가 있는 게시판" width="100%">
	<caption><span>글번호, 분류, 제목, 작성자, 날짜가 있는 게시판</span></caption>
	<colgroup>
		<col style="width:37px;" />
		<col style="width:*;" />
		<col style="width:56px;" />
		<col style="width:75px;" />
	</colgroup>
	<thead>
		<tr>
			<th>NO</th>
			<th>제목</th>
			<th>작성자</th>
			<th class="last">날짜</th>
		</tr>
	</thead>
	<tbody>
	<?
	$notice_content_size = sizeof($notice_content);
	if($notice_content_size == 0){
	?>
		<tr class="last">
			<td colspan="4">검색 결과 없음</td>
		</tr>
	<?
	}//end of : if($notice_content_size == 0)
	else{
		$notice_last_node_num = $notice_content_size - 1;
		for ($i = 0; $i < $notice_content_size; $i++){
	?>
		<tr class="<? echo print_last($i, $notice_last_node_num); ?>">
			<td><? echo $notice_content[$i]['num']; ?></td>
			<td class="title">
				<a href="#" onClick="notice_detail(event, <? echo $notice_content[$i]['seq']; ?>, <? echo $notice_page; ?>, '<? echo $notice_search_type; ?>', '<? echo $notice_search_val; ?>')"><? echo $notice_content[$i]['title']; ?></a>
			</td>
			<td><? echo $notice_content[$i]['name']; ?></td>
			<td><? echo $notice_content[$i]['regdate']; ?></td>
		</tr>
	<?
		}//end of : for ($i = 0; $i < sizeof($notice_content); $i++)
	}//end of : if($notice_content_size != 0)
	?>
	</tbody>
</table>

<!-- [D] 공지 항목이 10개가 초과되면 페이징 처리 -->
<?
$page_prev = $notice_pagination->GetIsPrev();
$page_list = $notice_pagination->GetPageList();
$page_next = $notice_pagination->GetIsNext();
?>
<div class="paging">
<?
if($page_prev['is_older_page']){
?>
	<a href="#" class="btn_arrow ar_fst" onClick="notice_pagination(event, <? echo $page_prev['older_page']; ?>)">첫페이지</a>
<?
}
else{
?>
	<a href="#" class="btn_arrow ar_fst">첫페이지</a>
<?
}

if($page_prev['is_prev_page']){
?>
	<a href="#" class="btn_arrow ar_prev" onClick="notice_pagination(event, <? echo $page_prev['prev_page']; ?>)">이전페이지</a>
<?
}
else{
?>
	<a href="#" class="btn_arrow ar_prev">이전페이지</a>
<?
}

while(list($k, $v) = each($page_list)){
?>
	<a href="#" onClick="notice_pagination(event, <? echo $k; ?>)"><span class="<? echo print_imp($v['is_current']);?>"><? echo $k; ?></span></a>
<?
}//end of : while(list($k, $v) = each($page_list))

if($page_next['is_next_page']){
?>
	<a href="#" class="btn_arrow ar_next" onClick="notice_pagination(event, <? echo $page_next['next_page']; ?>)"><span class="hidden">다음페이지</span></a>
<?
}
else{
?>
	<a href="#" class="btn_arrow ar_next"><span class="hidden">다음페이지</span></a>
<?
}

if($page_next['is_newer_page']){
?>
	<a href="#" class="btn_arrow ar_last" onClick="notice_pagination(event, <? echo $page_next['newer_page']; ?>)"><span class="hidden">마지막페이지</span></a>
<?
}
else{
?>
	<a href="#" class="btn_arrow ar_last"><span class="hidden">마지막페이지</span></a>
<?
}
?>
</div>