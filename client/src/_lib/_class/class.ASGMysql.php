<?php
/**
 * Description
 * 	ASGMysql()		DB 선택 및 연결
 * 	Close()			Mysql 연결 닫기
 * 	Query($sql)		Query만 수행
 * 	QueryNum($sql)	Query 후 라인수 리턴
 * 	QueryFetch($sql)	Query 후 라인수 & Fetch된 열 리턴
 * 	NextFetch()		다음 데이터 Fetch
 * 	Encryption()		암호화된 문자열을 구하기
 * 	QueryCount()		개수 구하기
 *  	
 * @ KSM | 2015.06.22 | CMysql.php modified
 */

class ASGMysql {
	var $connect = null;
	var $num;
	var $result;
	var $row;

	public function ASGMysql() {
		
		$fp = fopen(dirname( __FILE__ )."\db_addr.txt" , "r");
			
		if(!$fp) {
			$current_dbaddr = "localhost";	
		}
		else {
			$current_dbaddr = fread($fp , 16);
			fclose($fp);
		}
	
		//$current_dbaddr = "localhost";	
		$this->connect = mysql_connect($current_dbaddr , "root", "koinoanysupportfighting")
			or die("Cannot connect to MySQL Server : ".mysql_error());

		mysql_query("set names utf8", $this->connect);

		if($this->connect)
			mysql_select_db("livewebsupport", $this->connect);
			//or die("Cannot select DB! : ".mysql_error());
	}
	
	public function Close() {
		mysql_close($this->connect) 
			or die("Cannot close MySQL! : " . mysql_error());
	}

	public function Query($sql) {
		$this->result = mysql_query($sql, $this->connect) 
			or die(" $sql <br> 잘못된 쿼리입니다. 관리자에게 문의하십시오."); 
	}

	public function QueryNum($sql) {
		$this->Query($sql);
		$this->num = mysql_num_rows($this->result);
	}

	public function QueryFetch($sql) {
		$this->QueryNum($sql);

		if($this->num == 0){
			return NULL;	
		}
		else {
			$this->row = mysql_fetch_object($this->result);
			return $this->row;
		}
	}

	public function NextFetch() {
		if($this->row != NULL) {
			$this->row = mysql_fetch_object($this->result);
			return $this->row;
		}
		
		return NULL;
	}

	public function Encryption($str) {
		$sql = "select sha2('$str', 512) as enc";
		$this->QueryFetch($sql);
		return $this->row->enc;
	}
		
	public function QueryCount($sql) {
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
	
		return $row[0];
	}
}
