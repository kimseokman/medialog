<?php
	include dirname(__FILE__)."/class.ASGMysql.php";

/**
 * Description
 *
 * CreateAdmin($adminarray)				유료 관리자 생성
 * GetAdminByAdid($adid)				성리자 Primary Key Get
 * CreateTroubleTypeDefault($adnum)		해당 관리자 Default Trouble Type 생성
 *
 * @see 151027 | KSM | create
 */

class LGU {
	var $cmysql;
	
	public function LGU() {
		$this->cmysql = new ASGMysql();
	}

	public function CreateAdmin($adminarray){
		$sql = "SELECT sptnum "
		          ."FROM livesupport_supporter_info "
		          ."WHERE sptid = '".$adminarray["id"]."'";
		$this->cmysql->QueryNum($sql);
		
		// 같은 id의 supporter가 이미 존재할 경우 에러
		if($this->cmysql->num != 0) return "SUPPORTERALREADYEXISTS";
		
		$sql = "SELECT adnum "
		          ."FROM livesupport_admin_info "
		          ."WHERE adid = '".$adminarray["id"]."'";
		$this->cmysql->QueryNum($sql);
		
		// 같은 id의 admin이 이미 존재할 경우도 에러 (같이 로긴하므로)
		if($this->cmysql->num != 0) return "ADMINALREADYEXISTS";

		$admin_act_type = 0;
		
		$sql = "INSERT INTO livesupport_admin_info (adid, name, password, lasteditpassword, company, volume, url, email, tel, phone, others, business_num, enable, managenum , function , bill_account, sign_num, service_name, service_join_date, service_start_date, service_expired_date, mobile_volume , isFree , mobile_expired_date) "
		          ."VALUES ("
		          	."'".$adminarray["id"]."', "
		          	."'".$adminarray["name"]."', "
		          	."sha1('".$adminarray["password"]."'), "
					."now(), "
		          	."'".$adminarray["company"]."', "
		          	."'".$adminarray["volume"]."', "
		          	."'".$adminarray["url"]."', "
		          	."'".$adminarray["email"]."', "
		          	."'".$adminarray["tel"]."', "
		          	."'".$adminarray["phone"]."', "
		          	."'".$adminarray["others"]."', "
		          	."'".$adminarray["business_num"]."', "
		          	."'".$adminarray["enable"]."', "
		          	."'".$adminarray["managenum"]."', "
		          	."".$adminarray["function"].", "
		          	."'".$adminarray["bill_account"]."', "
		          	."'".$adminarray["sign_num"]."', "
		          	."'".$adminarray["service_name"]."', "
		          	."'".$adminarray["service_join_date"]."', "
		          	."'".$adminarray["service_start_date"]."', "
		          	."'".$adminarray["service_expired_date"]."', "
		          	."'".$adminarray["mobile_volume"]."', "
		          	."".$admin_act_type.", "
		          	."'".$adminarray["mobile_expired_date"]."')";		
		$this->cmysql->Query($sql);

		//report table 생성
		$sql = "CREATE TABLE `livesupport_report_". $adminarray["id"] . "` ("
		          ."`reportnum` int(10) unsigned NOT NULL auto_increment, "
		          ."`accesscode` int(8) unsigned zerofill NOT NULL default '00000000', "
		          ."`sptnum` int(10) unsigned NOT NULL default '0', "
		          ."`name` varchar(30) default NULL, "
		          ."`tel` varchar(30) default NULL, "
		          ."`email` varchar(50) default NULL, "
		          ."`ip` varchar(16) default NULL, "
		          ."`starttime` datetime default NULL, "
		          ."`endtime` datetime default NULL,	 "
		          ."`operationlog` tinyint(4) NOT NULL default '0', "
		          ."`report` varchar(512), "
		          ."`trbnum` int(10) unsigned NOT NULL default '0', "
		          ."`usercomment` varchar(512), "
		          ."`satisfaction` tinyint(4) NOT NULL default '0', "
		          ."`user_com_name` varchar(20) default '0',	"
		          ."`user_pub_ip` varchar(16) default NULL, "
		          ."`user_pri_ip` varchar(16) default NULL, "
		          ."`user_os` tinyint(4) NOT NULL default '0', "
		          ."`spt_pub_ip` varchar(16) default NULL, "
		          ."`spt_pri_ip` varchar(16) default NULL, "
		          ."`spt_com_name` varchar(20) default '0', "
		          ."PRIMARY KEY  (`reportnum`), "
		          ."KEY `idx_report_" .$adminarray["id"]."` (`accesscode`) "
		          .")ENGINE=InnoDB "
		          ."AUTO_INCREMENT=1 "
		          ."DEFAULT CHARSET=utf8;";
		
		$this->cmysql->Query($sql);

		//$adnum = $this->GetAdminByAdid($adminarray["id"]);
		//$this->CreateTroubleTypeDefault($adnum);
		return "OK";
	}

	public function GetAdminByAdid($adid){
		$sql = "SELECT adnum "
		       ."FROM livesupport_admin_info "
		       ."WHERE adid = '".$adid."'";
		
		$this->cmysql->QueryFetch($sql);
		
		return $this->cmysql->row;
	}

	public function CreateTroubleTypeDefault($adnum){
		$sql = "INSERT INTO livesupport_trouble_type (adnum, trbname, trborder) "
		          ."VALUES ('".$adnum."', '기능 오류', 0)";
		$this->cmysql->Query($sql);
		
		$sql = "INSERT INTO livesupport_trouble_type (adnum, trbname, trborder) "
		          ."VALUES ('".$adnum."', '사용 불편', 1)";
		$this->cmysql->Query($sql);

		$sql = "INSERT INTO livesupport_trouble_type (adnum, trbname, trborder) "
		          ."VALUES ('".$adnum."', '사용자 교육', 2)";
		$this->cmysql->Query($sql);

		$sql = "INSERT INTO livesupport_trouble_type (adnum, trbname, trborder) "
		          ."VALUES ('".$adnum."', '업무 지원', 3)";
		$this->cmysql->Query($sql);
	}
}
