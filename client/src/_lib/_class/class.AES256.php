<?php
/**
 * Description
 * 	ASGConfig()				생성자. 설정 읽어오기
 * 	WriteConfig()				설정 내용 기록
 * 	
 * @ KSM | 2015.06.22 | CConfig.php modified	
 */

class AES256 {
	var $key;
	var $iv;
	public function AES256() {
		$this->key = $this->hex2bin(AES_KEY);
		$this->iv =  $this->hex2bin(AES_IV);
	}

	public function hex2bin($hexdata) {
		$bindata="";

		for ($i=0;$i<strlen($hexdata);$i+=2) {
			$bindata.=chr(hexdec(substr($hexdata,$i,2)));
		}

		return $bindata;
	}
	        
	public function toPkcs7 ($value) {
		if ( is_null ($value) )
			$value = "" ;

		$padSize = 16 - (strlen ($value) % 16) ;

		return $value . str_repeat (chr ($padSize), $padSize) ;
	}
	        
	public function fromPkcs7 ($value) {
		$valueLen = strlen ($value) ;
		if ( $valueLen % 16 > 0 )
			$value = "";
		$padSize = ord ($value{$valueLen - 1}) ;
		if ( ($padSize < 1) or ($padSize > 16) )
			$value = "";
		// Check padding.
		for ($i = 0; $i < $padSize; $i++) {
			if ( ord ($value{$valueLen - $i - 1}) != $padSize )
				$value = "";
		}

		return substr ($value, 0, $valueLen - $padSize) ;
	}
	        
	public function encrypt ($key, $iv, $value) {
		
		if ( is_null ($value) )
			$value = "" ;
		
		$value = $this->toPkcs7 ($value) ;
		$output = mcrypt_encrypt (MCRYPT_RIJNDAEL_128, $key, $value, MCRYPT_MODE_CBC, $iv) ;

		return base64_encode ($output) ;
	}

	public function decrypt ($key, $iv, $value) {    
		if ( is_null ($value) )
			$value = "" ;   

		$value = base64_decode ($value) ;
		$output = mcrypt_decrypt (MCRYPT_RIJNDAEL_128, $key, $value, MCRYPT_MODE_CBC, $iv) ;

		return $this->fromPkcs7 ($output) ;
	}
}