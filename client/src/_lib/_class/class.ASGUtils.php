<?php
/**
 * Description
 * 	ASGUtils()							생성자
 * 	GetCurrentPageURL()						현재 페이지 URL 구하기
 * 	IsURL($url, $search_url)						url 에서 문자(url) 찾기ㅏ
 * 	GetJSPath($url)							각 페이지별로 script  파일 링크하기
 *  	RedirectURL($req_url)						해당 URL로 리다이렉트
 *  	GetAccessAuth()						해당 URL에 대한 사용자 권한 구하기
 *  	GetLangFromURL($url)						intro 페이지에서 현재 언어 구하기(intro 에서 사용)
 *  	GetSiteLang()							Session에서 언어를 String 형태로 반환(admin 에서 사용)
 *  	GetRequestURL()						Request URL 구하기
 *  	GetIndexPart($url)						현재 URL 기준으로 Index Part 나누기
 *  	GetAdminPart($url)						현재 URL 기준으로 Admin Part 나누기
 *  	GetIsEven($number)						해당 숫자 짝수 인지 판단
 *  	GetProduct($function)						상품 권한으로 상품 문자열 구하기
 *   	GetCodeProduct($code)						상품 코드로 상품 이름 얻기
 *    	GetCodeTechnician($code)					상품 코드로 상담원 볼륨 얻기
 *  	GetLangs()							정의된 언어 목록 구하기
 *  	SetBasePage()							User Type에 따른 기본 페이지 설정
 *  	GetReportPage()						URL로 Report 페이지 분기
 *  	GetCurrentUserType()						현재 User Type 구하기
 *  	GetSearchDate($constant_date_type)				정의된 날짜 상수 type 따른 날짜 구하기
 *  	GetYears()							year 목록 구하기
 *  	GetMonths($maxMonth=12)					month 목록 구하기(max month 는 default 12월)
 *  	GetDays($maxDay)						1일부터 maxDay 까지 day 목록 구하기
 *  	GetLastDay($base_m, $base_y)					해당 년, 월말 일 구하기
 *  	PrintDisabled($flag)						disabled 문자열 리턴
 *  	PrintSelected($base, $val)					selected 문자열 리턴
 *  	PrintSelectedLang($base_c = "en", $val_c = "en")			언어 match 후 selected 문자열 리턴
 *  	PrintSelectedCurrentYear($val_y)				현재 해 비교 후 selected 문자열 리턴
 *  	PrintSelectedCurrentMonth($val_m)				현재 월 비교 후 selected 문자열 리턴
 *  	PrintSelectedCurrentDay($val_d)					현재 일 비교 후 selected 문자열 리턴
 *  	GetSearchType()						상수로 정의된 Search Type List 리턴
 *  	GetOperationLog($oper_type)					상수로 정의된 Operation log 타입에 따른 문자열 리턴
 *  	GetOS($os_type)						상수로 정의된 OS 타입에 따른 문자열 리턴
 *  	GetSatisfaction($satisfaction_type)				상수로 정의된 만족도 타입에 따른 문자열 리턴
 *  	IsValidID($str)							ID 문자열 validate
 *  	IsValidPassword($str)						Password 문자열 validate
 *  	IsValidEmail($str)						Email 문자열 validate
 *  	
 * @ KSM | 2015.06.22 | For AnySupportGlobal
 */

class ASGUtils{

	public function ASGUtils(){
		//empty
	}

	public function GetCurrentPageURL() {// get current page url
	 	$pageURL = 'http';
	 	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	 	$pageURL .= "://";
	 	if ($_SERVER["SERVER_PORT"] != "80") {
	 		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	 	} else {
	 		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	 	}
 	
 		return $pageURL;
	}

	public function IsURL($url, $search_url){
		return stristr($url, $search_url);
	}

	public function GetJSPath($url){
		$relative_path = preg_replace("`\/[^/]*\.php$`i", "/", $_SERVER['PHP_SELF']);
		$split_url = explode($relative_path, $url);
		$php_filename = $split_url[1];
		
		if(!$php_filename){
			$php_filename = 'index.php';
		}
		$js_filename = str_replace(".php", ".js", $php_filename);
		
		if(strpos($js_filename, '?') !== FALSE){
			$split_js_filename = explode('?', $js_filename);
			$js_filename = $split_js_filename[0];
		}

		return $relative_path . $js_filename;
	}

	public function RedirectURL($req_url){
		header("Location: ".$req_url);
		exit(0);
	}

	public function GetAccessAuth(){// get site Auth
		$request_url = $_SERVER["REQUEST_URI"];
		$request_tag = explode("/", $request_url);
		$access_auth_tag = $request_tag[1];

		$web_access_auth = "All";
		
		if(strpos($access_auth_tag, 'access') !== FALSE){	
			$web_access_auth = "Login";
		}
		else if(strpos($access_auth_tag, 'super') !== FALSE){	
			$web_access_auth = "Super";
		}
		else if(strpos($access_auth_tag, 'admin') !== FALSE){
			$web_access_auth = "Admin";
		}
		else if(strpos($access_auth_tag, 'agent') !== FALSE){	
			$web_access_auth = "Agent";
		}
		else if(strpos($access_auth_tag, 'customer') !== FALSE){	
			$web_access_auth = "Customer";
		}
		else{
			$web_access_auth = "All";
		}

		return $web_access_auth;
	}

	public function GetCustomerAuth(){// get customer location
		$request_url = $_SERVER["SERVER_NAME"];
		//$request_url = "cs1472.com";
		//$request_url = "test.cs1472.com";
		$web_access_auth = "PUBLIC";
		
		if(strpos($request_url, CUSTOMER_ACCESS_URL_DEFAULT_FOOTER) !== false){
			$request_tag = explode("/", $request_url);
			$access_page_tag = explode(CUSTOMER_ACCESS_URL_DEFAULT_FOOTER, $request_tag[0]);

			if($access_page_tag[0] == null){
				$web_access_auth = "PUBLIC";
			}
			else{
				$web_access_auth = "PRIVATE";
			}
		}
		else{
			$web_access_auth = "PUBLIC";
		}	

		return $web_access_auth;	
	}

	public function GetSubdomain(){
		$request_url = $_SERVER["SERVER_NAME"];
		//$request_url = "cs1472.com";
		//$request_url = "test.cs1472.com";

		preg_match('/(?:http[s]*\:\/\/)*(.*?)\.(?=[^\/]*\..{2,5})/i', $request_url, $match);

		return $match[1];
	}

	public function GetLangFromURL($intro_url){// get language in use index page
		$split_url = explode("/", $intro_url);
		return $split_url[4];
	}

	public function GetSiteLang(){//get site language in use admin page
		if($_SESSION['language'] == 'en'){
			return 'English';
		}else if($_SESSION['language'] == 'kr'){
			return 'Korean';
		}else if($_SESSION['language'] == 'jp'){
			return 'Japanese';
		}else{
			return 'NO_SESSION';
		}
	}

	public function GetRequestURL(){// use admin page
		return $_SERVER["REQUEST_URI"];
	}

	public function GetIsEven($number){
		$number%=2;
		if($number > 0){//isOdd
			return FALSE;
		}else{//isEven
			return TRUE;
		}
	}

	public function GetCodeProduct($code){//get product
		if($code == CODE_ALL_IN_ONE){
			return "Anysupport All In One";
		}
		else if($code == CODE_PC){
			return "Anysupport 1+2";
		}

		return "UNKNOWN";
	}

	public function GetCodeTechnician($code){//get product
		if($code == CODE_ALL_IN_ONE){
			return 1;
		}
		else if($code == CODE_PC){
			return 3;
		}

		return "UNKNOWN";
	}

	public function GetProduct($function){//get product
		if($function & PERMISSION_MOBILE_VIDEO_PAID || $function & PERMISSION_MOBILE_VIDEO_FREE){
			return PRODUCT_MOBILE_VIDEO;
		}
		else if($function & PERMISSION_MOBILE_PAID || $function & PERMISSION_MOBILE_FREE){
			return PRODUCT_MOBILE;
		}
		else if($function & PERMISSION_VIDEO_PAID || $function & PERMISSION_VIDEO_FREE){
			return PRODUCT_VIDEO;
		}
		else{
			return PRODUCT_PC;
		}

		return "UNKNOWN";
	}

	public function GetLangs(){//get constant :: ASG_LANGUAGE
		return eval('return ' . ASG_LANGUAGE . ';');
	}

	public function SetBasePage($type){
		if($type == "ADMIN"){
			return ADMIN_INDEX;
		}else if($type == "SUPPORTER"){
			return SUPPORTER_INDEX;
		}else if($type == "GUEST"){
			return GUEST_INDEX;
		}
	}

	public function GetPageTag($num){
		$request_url = $_SERVER["REQUEST_URI"];
		$request_tag = explode("/", $request_url);
		
		return $request_tag[$num];
	}

	public function GetReportPage(){
		$request_url = $_SERVER["REQUEST_URI"];
		$request_tag = explode("/", $request_url);
		
		return $request_tag[3];
	}

	public function GetCurrentUserType(){// 2015.06.05 | KSM | Check User
		$user = array(
			"type" => "GUEST",
			"id" => "GUEST"
		);

		if($_SESSION["usertype"] != ""){
			$user['type'] = $_SESSION["usertype"];

			if($user['type'] == "OWNER"){
				$user['id'] = $_SESSION["owid"];
			}
			else if($user['type'] == "ADMIN" && $user['type'] != "SUPERADMIN"){
				$user['id'] = $_SESSION["adid"];
			}else if($user['type'] == "SUPPORTER"){
				$user['id'] = $_SESSION["sptid"];
			}
		}
		
		return $user;
	}

	public function GetSearchDate($constant_date_type){
		$date_arr = array();
		$type_str = explode('_', $constant_date_type);

		if($type_str[0] == "0"){
			$date_arr["Y"] = date("Y");
			$date_arr["M"] = date("m");
			$date_arr["D"] = date("d");
		}else{
			if($type_str[1] == "DAY"){
				$strtotime_sub_str = "days";	
			}else if($type_str[1] == "MONTH"){
				$strtotime_sub_str = "month";
			}else if($type_str[1] == "YEAR"){
				$strtotime_sub_str = "year";
			}

			$strtotime_str = "-".$type_str[0]." ".$strtotime_sub_str;
			
			$date_arr["Y"] = date('Y', strtotime($strtotime_str));
			$date_arr["M"] = date("m", strtotime($strtotime_str));
			$date_arr["D"] = date("d", strtotime($strtotime_str));
		}

		return $date_arr;
	}

	public function GetYears(){
		$year_arr = array();

		$interval = SEARCH_TERM_YYYY-1;

		$index = 0;
		for($i=$interval; $i >= 0; $i--){
			$year_arr[$index] = date("Y", strtotime("-" . $i . " YEAR"));
			$index++;
		}

		return $year_arr;
	}

	public function GetMonths($maxMonth=12){
		$minMonth = 1;

		$month_arr = array();

		for($i=0; $i < $maxMonth; $i++){
			$month_arr[$i] = $minMonth + $i;
		}
		
		return $month_arr;
	}

	public function GetDays($maxDay){
		$minDay = 1;
		$day_arr = array();

		for($i=0; $i < $maxDay; $i++){
			$day_arr[$i] = $minDay + $i;	
		}

		return $day_arr;
	}

	public function GetLastDay($base_m, $base_y){
		$lastDay = date("t", mktime(0, 0, 0, $base_m,1, $base_y));

		return $lastDay;	
	}

	public function PrintDisabled($flag){
		if($flag == TRUE){
			return "disabled";
		}
		return "";
	}

	public function PrintSelected($base, $val){
		if($base == $val){
			return "selected";
		}

		return "";
	}

	public function PrintSelectedLang($base_c = "en", $val_c = "en"){
		if($base_c == $val_c){
			return "selected";
		}

		return "";
	}

	public function PrintSelectedCurrentYear($val_y){
		$thisYear = date("Y");
		if($thisYear == $val_y){
			return "selected";
		}

		return "";
	}

	public function PrintSelectedCurrentMonth($val_m){
		$thisMonth = date("m");
		if($thisMonth == $val_m){
			return "selected";
		}

		return "";
	}

	public function PrintSelectedCurrentDay($val_d){
		$thisDay = date("d");
		if($thisDay == $val_d){
			return "selected";
		}

		return "";
	}

	public function GetSearchType(){
		if($_SESSION['language'] == 'en'){
			return eval('return ' . SEARCH_TYPE_LIST_EN . ';');	
		}else if($_SESSION['language'] == 'jp'){
			return eval('return ' . SEARCH_TYPE_LIST_JP . ';');	
		}else if($_SESSION['language'] == 'kr'){
			return eval('return ' . SEARCH_TYPE_LIST_KR . ';');
		}else{
			return eval('return ' . SEARCH_TYPE_LIST_EN . ';');	
		}
		
	}

	public function GetOperationLog($oper_type){
		$oper_log = UNCHECKED;

		switch($oper_type){
			case 1: $oper_log = RESOLVE; break;
			case 2: $oper_log = HOLD; break;
			case 3: $oper_log = NEED_DIRECT_SUPPORT; break;
			default:
			$oper_log = UNCHECKED;
		}

		return $oper_log;
	}

	public function GetOS($os_type){
		$os_str = OS_UNKNOWN;

		switch($os_type){
			case 1: $os_str = OS_WIN_98; break;
			case 2: $os_str = OS_WIN_2000; break;
			case 3: $os_str = OS_WIN_XP; break;
			case 4: $os_str = OS_WIN_2003; break;
			case 5: $os_str = OS_WIN_VISTA; break;
			case 6: $os_str = OS_WIN_7; break;
			case 7: $os_str = OS_UNKNOWN; break;
			case 8: $os_str = OS_WIN_8; break;
			case 9: $os_str = OS_WIN_2008; break;
			case 10: $os_str = OS_WIN_2012; break;
			case 14: $os_str = OS_WIN_8_1; break;
			case 15: $os_str = OS_WIN_10; break;
			case 16: $os_str = OS_WIN_SERVER; break;
			case 100: $os_str = OS_ANDROID; break;
			case 101: $os_str = OS_MAC; break;
			case 102: $os_str = OS_LINUX; break;
			default:
				$os_str = OS_UNKNOWN;
		}

		return $os_str;
	}

	public function GetSatisfaction($satisfaction_type){
		$satisfaction_log = NO_ANSWER;

		switch($satisfaction_type){
			case 1: $satisfaction_log = SATISFACTION; break;
			case 2: $satisfaction_log = NORMAL; break;
			case 3: $satisfaction_log = UNSATISFACTION; break;
			default: $satisfaction_log = NO_ANSWER; 
		}

		return $satisfaction_log;
	}

	public function GetIndexPart($url){
		$url_split = explode("/", $url);

		$inc_file_name = "";
		if($url_split[5] == NULL){
			$inc_file_name = "index";
		}
		else{
			$inc_file_name = str_replace(".php", "", $url_split[5]);//remove .php
		}
		return $inc_file_name;
	}

	public function GetAdminPart($url){
		$url_split = explode("/", $url);

		$inc_file_name = "";
		if($url_split[5] == NULL){
			$inc_file_name = "supporter_index";
		}
		else{	
			if($url_split[4] == "notice"){
				$inc_file_name = str_replace(".php", "", $url_split[4]);//remove .php
			}
			else{
				$inc_file_name = str_replace(".php", "", $url_split[4]."_".$url_split[5]);//remove .php	
			}
		}
		return $inc_file_name;
	}

	public function GetAgentPart($url){
		$url_split = explode("/", $url);

		$inc_file_name = "";
		if($url_split[4] == NULL){
			$inc_file_name = "index";
		}
		else{
			$inc_file_name = str_replace(".php", "", $url_split[4]);//remove .php
		}
		return $inc_file_name;
	}

	public function IsValidID($str){
		$id_length_min = ID_MIN_LENGTH;
		$id_length_max = ID_MAX_LENGTH;
		return ereg("(^[0-9a-zA-Z]{".$id_length_min.",".$id_length_max."}$)", $str);
	}

	public function IsValidPassword($str){
		
		$pw_length_min = PW_MIN_LENGTH;
		$pw_length_max = PW_MAX_LENGTH;
		return ereg("(^[0-9a-zA-Z]{".$pw_length_min.",".$pw_length_max."}$)", $str);
	}

	public function IsValidEmail($str){
		return ereg("(^[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+)*@[0-9a-zA-Z-]+(\.[0-9a-zA-Z-]+)*$)", $str);
	}

	public function GetBusinessList(){
		return eval('return ' . BUSINESS_LIST . ';');
	}

	public function GetFunctionList($type){
		if($type == "PC_FUNCTION_LIST"){
			return eval('return ' . PC_FUNCTION_LIST . ';');
		}
		else if($type == "PC_SETTING_LIST"){
			return eval('return ' . PC_SETTING_LIST . ';');
		}
		else if($type == "MOBILE_FUNCTION_LIST"){
			return eval('return ' . MOBILE_FUNCTION_LIST . ';');
		}
	}

	public function GetExtension($str) {
		$i = strrpos($str,".");
		if (!$i) { return ""; } 
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
	}

	public function utf2euc($str) { return iconv("UTF-8","cp949//IGNORE", $str); }

	public function is_ie() {
		if(!isset($_SERVER['HTTP_USER_AGENT']))return false;
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false) return true; // IE8
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'Windows NT 6.1') !== false) return true; // IE11
		return false;
	}

	public function GetFilenamePostFix($str){
		$str .= "_" . date("Y") . date("m") . date("d") .date("H") . date("i") .date("s");
		return $str;
	}

	public function ValidLastActivity(){
		if (isset($_SESSION['LAST_ACTIVITY'])) {
			if((time() - $_SESSION['LAST_ACTIVITY'] > SESSION_TIMEOUT)){
				session_unset();
		    		session_destroy();		
		    		return False;
			}
			else{
				return True;
			}
		}
		
		return False;

	}

	public function UpdateLastActivity(){
		$_SESSION['LAST_ACTIVITY'] = time();
	}

	public function fileCopy($odir,$ndir) { 
	      if(filetype($odir) === 'dir') { 
	           clearstatcache(); 

	           if($fp = @opendir($odir)) { 
	                  while(false !== ($ftmp = readdir($fp))){ 
	                        if(($ftmp !== ".") && ($ftmp !== "..") && ($ftmp !== "")) {
	                              if(filetype($odir.'/'.$ftmp) === 'dir') { 
	                                   clearstatcache(); 
	       
	                                   @mkdir($ndir.'/'.$ftmp); 
	                                   echo ($ndir.'/'.$ftmp."<br />\n"); 
	                                   set_time_limit(0); 
	                                   fileCopy($odir.'/'.$ftmp,$ndir.'/'.$ftmp); 
	                              } else { 
	                                   copy($odir.'/'.$ftmp,$ndir.'/'.$ftmp); 
	                              } 
	                        } 
	                  } 
	           } 
	           if(is_resource($fp)){ 
	                 closedir($fp); 
	           } 
	      } else { 
	            echo $ndir."<br />\n";      
	            copy($odir,$ndir); 
	      } 
	 } // end func 
}