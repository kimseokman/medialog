<?php
	include dirname(__FILE__)."/class.ASGMysql.php";
	include dirname(__FILE__)."/class.ASGPage.php";
	include dirname(__FILE__)."/class.AES256.php";
/**
 * Description
 * 	ASGMain()																생성자. mysql 연결
 * 	Login($id, $password)															로그인
 * 	Logout()																로그아웃
 * 	IsLogin()																현재 로그인 되어 있는지 검사
 * 	AdminIsLogin()																ADMIN 현재 로그인 되어 있는지 검사
 * 	SupporterIsLogin()															SUPPORTER 현재 로그인 되어 있는지 검사
 * 	GetAdminInfo()																세션 정보의 adnum 을 기준으로 ADMIN 정보 얻기
 * 	ConfirmAdmin()															ADMIN 확인
 * 	LoginAdmin($id, $password)														ADMIN 로그인
 * 	LoginSupporter($id, $password)														SUPPORTER 로그인
 * 	GenerateAuthCodeForResetAdminPassword(&$code, &$adid, $email)									ADMIN 비밀번호 초기화 관련 인증 code 생성
 * 	CheckEmailValidationForResetPassword($code)												ADMIN 비밀번호 초기화 관련 code validation
 * 	ResetAdminPassword($code , $password)												ADMIN 비밀번호 초기화
 * 	SetContactUsHistory($name, $phone, $mail, $message, $company, $department, $status)						CUSTOMER claim DB 저장
 * 	DuplicatedCheckID($user_type, $id)													ID 중복 체크시 사용 ($user_type : 'ADMIN' / 'SUPPORTER' / 'SUPERADMIN')
 * 	GenerateAuthCodeForFreeJoin(&$code , $email , $adid)											무료 가입 관련 인증 code 생성
 * 	CheckEmailAuthCodeForFreeJoin($email , $adid , $code)										무료 가입 관련 인증 code validation
 * 	CreateFreeAdmin($email, $adid, $name, $password, $free_duration, $product_code, $technician_volum, $customer_url , $code)		무료 ADMIN 생성
 * 	GetAdminByID($adid)															ADMIN ID 로 ADMIN 정보 얻기
 * 	CheckAdminPW($password)														ADMIN Password Check
 * 	ChangeAdminInfo($admindata)														ADMIN Information Change
 * 	GetDepList()																Get Department List 
 * 	GetDepListPage($page)															Get Department List ( Paging )
 * 	GetDepByDepnum($depnum)														Get Department Information
 * 	RenameDep($depnum, $depname)													Edit Department name
 * 	EditDeptInfo($depnum, $depname, $depmemo)											Edit Department Information
 * 	GetSupporterNum()															해당 ADMIN에 속한 서포터수 구하기
 * 	GetMobileSupporterNum()														해당 ADMIN에 속한 Mobile 서포터수 구하기
 * 	GetSupporterListPage($page, $search, $searchVal)											해당 ADMIN에 속한 서포터 목록 구하기( Paging )
 * 	GetSupporterNumByDepnum($depnum)													부서에 속한 서포터수 구하기
 * 	DeleteDep($depnum)															부서 삭제
 * 	GetSupporterBySptnum($sptnum)													서포터 Num으로 서포터 구하기
 * 	CreateSupporter($sptarray)														서포터 생성
 * 	ModifySupporter($sptnum, $sptarray)													서포터 정보 수정
 * 	DeleteSupporter($sptnum)														서포터 삭제
 * 	GetTroubleTypeCount()															장애 유형 개수
 * 	GetTroubleTypeList()															장애 유형 목록
 * 	GetTroubleTypeByTrbnum($trbnum)													장애 유형 정보
 * 	ModifyTroubleType($trbnum, $trbname)													장애 유형 수정
 * 	DeleteTroubleType($trbnum)														장애 유형 삭제
 * 	CreateTroubleType()															장애 유형 생성
 * 	CheckReportExsit($search_type, $search_val, $start_time, $end_time)													Report 있는지 검사
 * 	GetReportList($page, $search, $searchVal, $starttime, $endtime)										Report 목록 조회
 * 	GetReport($reportnum)															Report 조회
 * 	GetReportExcel($search, $searchVal, $starttime, $endtime)										Excel 용 Report 조회
 * 	GetReportByMonth($year)														월별 Report 조회
 * 	GetReportByDay($year, $month)														일별 Report 조회
 * 	GetReportByTime($sDate, $eDate)													시간별 Report 조회
 * 	GetReportBySpt($sDate,$eDate)														상담원별 Report 조회
 * 	GetReportByOS($sDate, $eDate)														OS별 Report 조회
 * 	GetReportByTroubleType($date1, $date2)												장애유형별 Report 조회
 * 	GetReportBySatisfactionOfTroubleType($date1, $date2)											장애유형의 만조도별 Report 조회
 * 	GetNoticeList($page, $search_type, $search_val)												공지사항 목록 조회
 * 	GetNotice($seq)																공지사항 조회
 * 	GenerateTitleBar(&$str, $sptnum)													타이틀바 생성
 * 	IsAccesscodeValid(&$accesscode)													run.php 사용
 * 	OpenCS($accesscode, $name, $tel, $email)												run.php 사용
 * 	HistoryLogin($id, $password)														history 보기 위해 로그인
 * 	HistoryConfirmLogin()															history 로그인되었는지 확인
 * 	HistoryGetReportList($date1, $date2)													history 목록 가져오기
 * 	HistoryGetStatsByTrbType($date1, $date2)												history 에서 장애유형 통계
 * 	HistoryGetStatsByFixing($date1, $date2)													history 에서 해결여부 통계
 * 	HistoryGetStatsBySatisfaction($date1, $date2)												history 에서 만족도별 통계
 * 	HistoryModifyReportContent($reportnum, $report)											history 에서 report 내용 수정
 * 	HistoryGetReportForExcel($date1, $date2)												history 에서 excel 데이터 받기
 *  
 *  GetSptIdByTitlekey($titlekey)												KMS  다중화
 *	RouteKms($sptid)															로그인 할 kms 라우팅
 *  CheckKms($accesscode)														 accesscode 로 접속할 kms 라우팅
 * @ KSM | 2015.06.22 | CLiveSupport.php modified
 */

class ASGMain {
	var $cmysql;
	var $cpage;
	var $aes_256;
	
	public function ASGMain() {
		$this->cmysql = new ASGMysql();
		$this->aes_256 = new AES256();
	}

	public function GetLoginTryCount($id){
		//validate Admin ID
		$sql = "SELECT adnum, company , enable, login_try_count "
		          ."FROM livesupport_admin_info "
		          ."WHERE adid = '".$id."' ";

		$this->cmysql->QueryFetch($sql);
		
		// 해당 id/password가 없을 경우
		if($this->cmysql->num == 0) {
			//validate Suppoter ID
			$sql = "SELECT a.sptnum AS sptnum , a.adnum AS adnum , a.company AS company , b.enable AS enable , a.login_try_count AS login_try_count "
			          ."FROM livesupport_supporter_info a , livesupport_admin_info b  "
			          ."WHERE a.sptid = '".$id."' "
			          ."AND a.deleted = '' "
			          ."AND a.adnum = b.adnum";
			$this->cmysql->QueryFetch($sql);
			// 서포터 중에도 해당 id/password가 없을 경우
			if($this->cmysql->num == 0) {
				return -1;
			}
			else {
				return $this->cmysql->row->login_try_count;
			}
		}
		else{
			return $this->cmysql->row->login_try_count;
		}
	}

	// 회사 로그 , 이름 , 전화 번호 추출
	public function GetCompanyInfo($adminid){
		$cpnarray = array();
		
		$sql = "SELECT service_name , phone , logo_url "
		          ."FROM livesupport_admin_info "
		          ."WHERE adid = '" .$adminid. "'";
		
		//echo $sql;
		if($this->cmysql->QueryFetch($sql) != NULL) {
			$cpnarray = array("company"=>$this->cmysql->row->service_name, "phone"=>$this->cmysql->row->phone , "logo"=>$this->cmysql->row->logo_url);
		}
		return $cpnarray;
	}

	public function GetSubDomainInfo($domain){
		$cpnarray = array();
		
		$sql = "SELECT service_name , adid, phone , logo_url "
		          ."FROM livesupport_admin_info "
		          ."WHERE domain = '" .$domain. "'";
		
		//echo $sql;
		if($this->cmysql->QueryFetch($sql) != NULL) {
			$cpnarray = array("company"=>$this->cmysql->row->service_name, "phone"=>$this->cmysql->row->phone , "logo"=>$this->cmysql->row->logo_url);
		}
		return $cpnarray;
	}

	public function SetLoginTryCount($id, $count){
		if($this->cmysql->connect == null) return "DB_ERROR";
		if($id == "") return "NOID";

		$sql = "SELECT adnum, company , enable "
		          ."FROM livesupport_admin_info "
		          ."WHERE adid = '".$id."' ";

		$this->cmysql->QueryFetch($sql);
		
		// 해당 id/password가 없을 경우
		if($this->cmysql->num == 0) {
			// 이젠 서포터 중에 있나 검사해야 한다.
			$sql = "SELECT a.sptnum AS sptnum , a.adnum AS adnum , a.company AS company , b.enable AS enable "
			          ."FROM livesupport_supporter_info a , livesupport_admin_info b "
			          ."WHERE a.sptid = '".$id."' "
			          ."AND a.deleted = '' "
			          ."AND a.adnum = b.adnum";
			$this->cmysql->QueryFetch($sql);
			
			// 서포터 중에도 해당 id/password가 없을 경우
			if($this->cmysql->num == 0) {
				return "INVALIDACCOUNT";
			}
			else {// 해당 계정이 있을 경우, 세션 등록 및 "SUPPORTER" 리턴
				$sql = "UPDATE livesupport_supporter_info a "
				          ."SET login_try_count = ".$count." "
				          ."WHERE a.sptid = '".$id."' "
				          ."AND a.deleted = '' ";

				$this->cmysql->QueryFetch($sql);

				return "SUPPORTER";          
			}
		}
		else {// ADMIN
			$sql = "UPDATE livesupport_admin_info "
			          ."SET login_try_count = ".$count." "
			          ."WHERE adid = '".$id."' ";

			$this->cmysql->QueryFetch($sql);          

			return "ADMIN";
		}		
	}

	public function ResetPassword($id, $mail, $password){
		if($this->cmysql->connect == null) return "DB_ERROR";
		if($id == "") return "NOID";

		$sql = "SELECT adnum, company , enable "
		          ."FROM livesupport_admin_info "
		          ."WHERE adid = '".$id."' "
		          ."AND email = '".$mail."' ";

		$this->cmysql->QueryFetch($sql);
		
		// 해당 id/password가 없을 경우
		if($this->cmysql->num == 0) {
			// 이젠 서포터 중에 있나 검사해야 한다.
			$sql = "SELECT a.sptnum AS sptnum , a.adnum AS adnum , a.company AS company , b.enable AS enable "
			          ."FROM livesupport_supporter_info a , livesupport_admin_info b "
			          ."WHERE a.sptid = '".$id."' "
			          ."AND a.deleted = '' "
			          ."AND a.adnum = b.adnum "
			          ."AND a.email = '".$mail."' ";
			$this->cmysql->QueryFetch($sql);
			
			// 서포터 중에도 해당 id/password가 없을 경우
			if($this->cmysql->num == 0) {
				return "FAIL";
			}
			else {// 해당 계정이 있을 경우, 세션 등록 및 "SUPPORTER" 리턴
				$sql = "UPDATE livesupport_supporter_info a "
				          ."SET a.password = sha2('".$password."', 512), "
				          ."WHERE a.sptid = '".$id."' "
				          ."AND a.deleted = '' ";

				$this->cmysql->QueryFetch($sql);

				return "OK";          
			}
		}
		else {// ADMIN
			$sql = "UPDATE livesupport_admin_info "
			          ."SET password = sha2('".$password."', 512), "
			          ."lasteditpassword = now() "
			          ."WHERE adid = '".$id."' ";

			$this->cmysql->QueryFetch($sql);          

			return "OK";
		}
	}

	public function Login($id, $password) {
		if($this->cmysql->connect == null) return "DB_ERROR";
		if($id == "") return "NOID";
		if($password == "") return "NOPASSWORD";
		
		$sql = "SELECT adnum, company , enable "
		          ."FROM livesupport_admin_info "
		          ."WHERE adid = '".$id."' "
		          ."AND password = sha2('".$password."', 512) ";

		$this->cmysql->QueryFetch($sql);
		
		// 해당 id/password가 없을 경우
		if($this->cmysql->num == 0) {
			// 이젠 서포터 중에 있나 검사해야 한다.
			$sql = "SELECT a.sptnum AS sptnum , a.adnum AS adnum , a.company AS company , b.enable AS enable "
			          ."FROM livesupport_supporter_info a , livesupport_admin_info b "
			          ."WHERE a.sptid = '".$id."' "
			          ."AND a.password = sha2('".$password."', 512) "
			          ."AND a.deleted = '' "
			          ."AND a.adnum = b.adnum";
			$this->cmysql->QueryFetch($sql);
			
			// 서포터 중에도 해당 id/password가 없을 경우
			if($this->cmysql->num == 0) {
				return "INVALIDPASSWORD";
			}
			else {// 해당 계정이 있을 경우, 세션 등록 및 "SUPPORTER" 리턴
				if($this->cmysql->row->enable == "Y") {
					$_SESSION["usertype"] = "SUPPORTER";
					$_SESSION["sptnum"] = $this->cmysql->row->sptnum;
					$_SESSION["adnum"] = $this->cmysql->row->adnum;
					$_SESSION["sptid"] = $id;
					$_SESSION["company"] = $this->cmysql->row->company;
					
					session_register(usertype);
					session_register(sptnum);
					session_register(adnum);
					session_register(sptid);
					session_register(company);
								
					return "SUPPORTER";
				}
				else {
					return "SUPPORTER_EXPIRED";
				}
			}
		}
		else {// 해당 계정이 있을 경우, 세션 등록 및 "ADMIN" 리턴
			if($this->cmysql->row->enable == "Y") {
				$_SESSION["usertype"] = "ADMIN";
				$_SESSION["adnum"] = $this->cmysql->row->adnum;
				$_SESSION["adid"] = $id;
				$_SESSION["company"] = $this->cmysql->row->company;
				
				session_register(usertype);
				session_register(adnum);
				session_register(adid);
				session_register(company);

				return "ADMIN";
			}
			else {
				return "ADMIN_EXPIRED";
			}
		}
	}
	
	public function Logout() {
		session_destroy();		
	}

	public function IsLogin(){
		if(($_SESSION["usertype"] != "ADMIN" || $_SESSION["adnum"] == "" || $_SESSION["adid"] == "") && ($_SESSION["usertype"] != "SUPPORTER" || $_SESSION["sptnum"] == "" || $_SESSION["adnum"] == "" ||$_SESSION["sptid"] == "")) 
			return FALSE;
		else 
			return TRUE;
	}

	public function AdminIsLogin(){
		if($_SESSION["usertype"] != "ADMIN" || $_SESSION["adnum"] == "" || $_SESSION["adid"] == "")
			return FALSE;
		else 
			return TRUE;
	}

	public function SupporterIsLogin(){
		if($_SESSION["usertype"] != "SUPPORTER" || $_SESSION["sptnum"] == "" || $_SESSION["adnum"] == "" ||$_SESSION["sptid"] == "")
			return FALSE;
		else 
			return TRUE;
	}

	public function GetAdminInfo() {
		$adnum = $_SESSION["adnum"];

		$sql = "SELECT adid, name, service_expired_date, lasteditpassword, company, volume, url, email, function, setting, mobile_setting, phone, tel, logo_url , service_name , mobile_volume , mobile_expired_date, pay_count "
		            ."FROM livesupport_admin_info "
		            ."WHERE adnum = '".$adnum."'";

		$this->cmysql->QueryFetch($sql);
		
		$this->cmysql->row->name = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->name);
		$this->cmysql->row->email = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->email);
		$this->cmysql->row->tel = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->tel);
		$this->cmysql->row->phone = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->phone);

		return $this->cmysql->row;
	}

	public function SetLoginTime($id){
		if($this->cmysql->connect == null) return "DB_ERROR";
		if($id == "") return "NOID";

		$sql = "SELECT adnum, company , enable "
		          ."FROM livesupport_admin_info "
		          ."WHERE adid = '".$id."' ";

		$this->cmysql->QueryFetch($sql);
		
		// 해당 id/password가 없을 경우
		if($this->cmysql->num == 0) {
			// 이젠 서포터 중에 있나 검사해야 한다.
			$sql = "SELECT a.sptnum AS sptnum , a.adnum AS adnum , a.company AS company , b.enable AS enable "
			          ."FROM livesupport_supporter_info a , livesupport_admin_info b "
			          ."WHERE a.sptid = '".$id."' "
			          ."AND a.deleted = '' "
			          ."AND a.adnum = b.adnum";
			$this->cmysql->QueryFetch($sql);
			
			// 서포터 중에도 해당 id/password가 없을 경우
			if($this->cmysql->num == 0) {
				return "INVALIDACCOUNT";
			}
			else {// 해당 계정이 있을 경우, 세션 등록 및 "SUPPORTER" 리턴
				$sql = "UPDATE livesupport_supporter_info a "
				          ."SET lastlogin = now() "
				          ."WHERE a.sptid = '".$id."' "
				          ."AND a.deleted = '' ";

				$this->cmysql->QueryFetch($sql);

				return "SUPPORTER";          
			}
		}
		else {// ADMIN
			$sql = "UPDATE livesupport_admin_info "
			          ."SET lastlogin = now() "
			          ."WHERE adid = '".$id."' ";

			$this->cmysql->QueryFetch($sql);          

			return "ADMIN";
		}
	}

	public function LoginAdmin($id, $password){
		if($this->cmysql->connect == null)
			return "DB_ERROR";
		
		if($id == "") return "NOID";
		if($password == "") return "NOPASSWORD";
		
		$sql = "SELECT adnum, company , enable "
		          ."FROM livesupport_admin_info "
		          ."WHERE adid = '".$id."' "
		          ."AND password = sha2('".$password."', 512) ";

		$this->cmysql->QueryFetch($sql);
		
		if($this->cmysql->num != 0) {//login success
			if($this->cmysql->row->enable == "Y"){
				$_SESSION["usertype"] = "ADMIN";
				$_SESSION["adnum"] = $this->cmysql->row->adnum;
				$_SESSION["adid"] = $id;
				$_SESSION["company"] = $this->cmysql->row->company;
				
				//session_register(usertype);
				//session_register(adnum);
				//session_register(adid);
				//session_register(company);
			}else{//login success & expired
				return "ADMIN_EXPIRED";
			}
			return "ADMIN";
		}else{//login fail
			return "INVALIDACCOUNT";
		}
	}

	public function LoginSupporter($id, $password){// 2015.06.04 | KSM | Only Suppoter Login
		if($this->cmysql->connect == null)
			return "DB_ERROR";
		
		if($id == "") return "NOID";
		if($password == "") return "NOPASSWORD";

		$sql = "SELECT a.sptnum AS sptnum , a.adnum AS adnum , a.company AS company , b.enable AS enable "
		          ."FROM livesupport_supporter_info a , livesupport_admin_info b  "
		          ."WHERE a.sptid = '".$id."' "
		          ."AND a.password = sha2('".$password."', 512) "
		          ."AND a.deleted = '' "
		          ."AND a.adnum = b.adnum";

		$this->cmysql->QueryFetch($sql);

		if($this->cmysql->num != 0) {//login success
			if($this->cmysql->row->enable == "Y"){
				$_SESSION["usertype"] = "SUPPORTER";
				$_SESSION["sptnum"] = $this->cmysql->row->sptnum;
				$_SESSION["adnum"] = $this->cmysql->row->adnum;
				$_SESSION["sptid"] = $id;
				$_SESSION["company"] = $this->cmysql->row->company;
				
				//session_register(usertype);
				//session_register(sptnum);
				//session_register(adnum);
				//session_register(sptid);
				//session_register(company);
							
				return "SUPPORTER";
			}else{//login success & expired
				return "SUPPORTER_EXPIRED";
			}
		}else {//login fail
			return "INVALIDACCOUNT";
		}
	}

	public function ConfirmAdmin(){
		if( ($_SESSION["usertype"] != "ADMIN" && $_SESSION["usertype"] != "SUPERADMIN") || $_SESSION["adnum"] == "" || $_SESSION["adid"] == "" ) 
			return FALSE;
		return TRUE;
	}

	public function GenerateAuthCodeForResetAdminPassword(&$code, &$adid, $email) {
		$sql = "SELECT adnum, adid "
		          ."FROM livesupport_admin_info "
		          ."WHERE email = '".$email."'";

		$this->cmysql->QueryFetch($sql);

		if($this->cmysql->num == 0) 
			return FALSE; // 없는 이메일
		
		$row = $this->cmysql->row;
		$adnum = $row->adnum;
		$adid = $row->adid;
		
		$titlekey = crypt($adnum).crypt(time()).crypt($adid).crypt(time()).crypt($adid);
		$code = substr(crypt($titlekey).$titlekey, 0, 45);

		$sql = "INSERT INTO livesupport_email_auth (adid , email , auth_code , make_date) "
		          ."VALUES('".$adid."' , '".$email."' , '".$code."' , now() )";

		$this->cmysql->Query($sql);
		return TRUE;
	}

	public function CheckEmail($user_type, $id, $mail){
		if($user_type == "admin"){
			$get_colume = "adnum";
			$table_name  = "livesupport_admin_info";
			$com_colume = "adid";
			$mail_colume = "email";
		}

		if($user_type == "user"){
			$get_colume = "sptnum";
			$table_name  = "livesupport_supporter_info";
			$com_colume = "sptid";
			$mail_colume = "email";
		}
		
		$sql = "SELECT ".$get_colume." "
		          ."FROM ".$table_name." "
		          ."WHERE ".$com_colume." = '".$id."' "
		          ."AND ".$mail_colume." = '".$mail."' ";
		
		$this->cmysql->QueryFetch($sql);
		
		if($this->cmysql->num == 0){
			return "FAIL";
		}

		return "OK";
	}

	public function CheckEmailValidationForResetPassword($code){
		$sql = "SELECT adid "
		          ."FROM livesupport_email_auth "
		          ."WHERE auth_code = '".$code."' "
		          ."AND enable = 'Y'";

		$this->cmysql->QueryFetch($sql);

		if($this->cmysql->num == 0) 
			return FALSE; 

		return TRUE;
	}

	public function ResetAdminPassword($code , $password) {
		$sql = "SELECT adid "
		          ."FROM livesupport_email_auth "
		          ."WHERE auth_code = '".$code."' "
		          ."AND enable = 'Y'";

		$this->cmysql->QueryFetch($sql);

		if($this->cmysql->num == 0) 
			return FALSE; // 없는 인증코드
		
		$row = $this->cmysql->row;
		$adid = $row->adid;
		
		$sql = "UPDATE livesupport_admin_info "
		          ."SET password = sha2('".$password."', 512) "
		          ."WHERE adid = '".$adid."'";

		$this->cmysql->QueryFetch($sql);

		$sql = "UPDATE livesupport_email_auth "
		          ."SET enable = 'N' , process_date = now() "
		          ."WHERE auth_code = '".$code."' "
		          ."AND adid = '".$adid."'";

		$this->cmysql->Query($sql);
		return TRUE;
	}

	public function SetContactUsHistory($name, $phone, $mail, $message, $company, $department, $status) {
		//$status : 'Resolved', 'Hold', 'Support', 'Unresolved'
		$sql = "INSERT INTO livesupport_customer_claim "
		          ."(name, phone, mail, message, company, department, request_date , status) "
		          ."VALUES ('".$name."', '".$phone."', '".$mail."', '".$message."' , '".$company."', '".$department."' , now() , '".$status."')";

		$this->cmysql->Query($sql);

		return "OK";       
	}

	public function DuplicatedCheckID($user_type, $id){//2015.06.09 | KSM | User ID Duplicated check
		if($user_type == "ADMIN"){
			$get_colume = "adnum";
			$table_name  = "livesupport_admin_info";
			$com_colume = "adid";
		}

		if($user_type == "SUPPORTER"){
			$get_colume = "sptnum";
			$table_name  = "livesupport_supporter_info";
			$com_colume = "sptid";
		}

		if($user_type == "SUPERADMIN"){
			$get_colume = "sadnum";
			$table_name  = "livesupport_super_admin_info";
			$com_colume = "sadid";
		}
		
		$sql = "SELECT ".$get_colume." "
		          ."FROM ".$table_name." "
		          ."WHERE ".$com_colume." = '".$id."'";	
		
		$this->cmysql->QueryFetch($sql);

		if($this->cmysql->num != 0){
			return "DUPLICATED";
		}

		return "OK";
	}

	public function GenerateAuthCodeForFreeJoin(&$code , $email , $adid) {
		$sql = "SELECT sptnum "
		          ."FROM livesupport_supporter_info "
		          ."WHERE sptid = '".$adid."'";

		$this->cmysql->QueryNum($sql);
		
		// 같은 id의 supporter가 이미 존재할 경우 에러
		if($this->cmysql->num != 0) 
			return "DUPLECATE_ADID";
		
		$sql = "SELECT adnum "
		          ."FROM livesupport_admin_info "
		          ."WHERE adid = '".$adid."'";

		$this->cmysql->QueryNum($sql);
		
		// 같은 id의 admin이 이미 존재할 경우도 에러 (같이 로긴하므로)
		if($this->cmysql->num != 0)
			return "DUPLECATE_ADID";

		$sql = "SELECT adnum "
		          ."FROM livesupport_admin_info "
		          ."WHERE email = '".$email."'";

		$this->cmysql->QueryNum($sql);
		
		// 이미 등록된 이메일
		if($this->cmysql->num != 0)
			return "DUPLECATE_EMAIL";

					
		$titlekey = crypt($adid).crypt(time()).crypt($email).crypt(time()).crypt($email);
		$code = substr(crypt($titlekey).$titlekey, 0, 45);

		$sql = "INSERT INTO livesupport_email_auth (adid , email , auth_code , make_date) "
		          ."VALUES('".$adid."' , '".$email."' , '".$code."' , now() )";

		$this->cmysql->Query($sql);

		return "OK";
	}

	public function CheckEmailAuthCodeForFreeJoin($email , $adid , $code){
		$sql = "SELECT adid "
		          ."FROM livesupport_email_auth "
		          ."WHERE auth_code = '".$code."' "
		          ."AND enable = 'Y' "
		          ."AND email = '".$email."' "
		          ."AND adid = '".$adid."'";

		$this->cmysql->QueryFetch($sql);

		if($this->cmysql->num == 0) 
			return "INVALID_AUTHCODE"; 

		return "OK";

	}

	public function CreateFreeAdmin($email, $adid, $name, $password, $free_duration, $product_code, $technician_volum, $customer_url , $code) {
		$sql = "SELECT adid "
		          ."FROM livesupport_email_auth "
		          ."WHERE auth_code = '".$code."' "
		          ."AND enable = 'Y' "
		          ."AND email = '".$email."'";
		$this->cmysql->QueryFetch($sql);

		if($this->cmysql->num == 0) 
			return "INVALID_AUTHCODE"; 

		
		$sql = "SELECT sptnum "
		          ."FROM livesupport_supporter_info "
		          ."WHERE sptid = '".$adid."'";
		$this->cmysql->QueryNum($sql);
		
		// 같은 id의 supporter가 이미 존재할 경우 에러
		if($this->cmysql->num != 0) 
			return "DUPLECATE_ADID";
		
		$sql = "SELECT adnum "
		          ."FROM livesupport_admin_info "
		          ."WHERE adid = '".$adid."'";
		$this->cmysql->QueryNum($sql);
		
		// 같은 id의 admin이 이미 존재할 경우도 에러 (같이 로긴하므로)
		if($this->cmysql->num != 0)
			return "DUPLECATE_ADID";

		$sql = "SELECT adnum "
		          ."FROM livesupport_admin_info "
		          ."WHERE email = '".$email."'";

		$this->cmysql->QueryNum($sql);
		
		// 이미 등록된 이메일
		if($this->cmysql->num != 0)
			return "DUPLECATE_EMAIL";

		
		$expired_date = date("Y-m-d", mktime(0 , 0 , 0 , date("m") , date("d") + $free_duration , date("Y") ) );

		$function = $product_code;
		$volume = $technician_volum;
		
		// 무료가입
		$sql = "INSERT INTO livesupport_admin_info (adid, name, password, volume, email , service_expired_date , function , url) "
		          ."VALUES ('".$adid."', '".$name."', sha2('".$password."', 512), ".$volume." , '".$email."', '".$expired_date."' , ".$function." , '".$customer_url."')";
		$this->cmysql->Query($sql);
		
		//기본 부서 생성하기
		$sql = "SELECT adnum "
		          ."FROM livesupport_admin_info "
		          ."WHERE adid = '".$adid."'";
		$this->cmysql->QueryFetch($sql);
		if($this->cmysql->num != 0) {
			$adnum = $this->cmysql->row->adnum;
		}

		$sql = "SELECT depnum "
		          ."FROM livesupport_dep_info "
		          ."WHERE depname = '".$adid."'";
		$this->cmysql->QueryNum($sql);

		if($this->cmysql->num == 0) {
			$sql = "INSERT INTO livesupport_dep_info (adnum , depname) "
			          ."VALUES (".$adnum." , '".$adid."')";
			$this->cmysql->Query($sql);
		}
		else {
			//부서가 이미 있다면..
			$i = 0;
			while(1) {
				$sql = "SELECT depnum "
				          ."FROM livesupport_dep_info "
				          ."WHERE depname = '".$adid.$i."'";
				$this->cmysql->QueryNum($sql);

				if($this->cmysql->num != 0) 
				{
					$i++;
					continue;
				}
				else{
					break;
				}
				
			}

			$sql = "INSERT INTO livesupport_dep_info (adnum , depname) "
			          ."VALUES (".$adnum." , '".$adid.$i."')";
			$this->cmysql->Query($sql);
		}

		//email 인증 테이블 처리
		$sql = "UPDATE livesupport_email_auth "
		          ."SET enable = 'N' , process_date = now() "
		          ."WHERE auth_code = '".$code."' AND adid = '".$adid."'";
		$this->cmysql->Query($sql);

		//report table 생성
		$sql = "CREATE TABLE `livesupport_report_". $adid . "` ("
		          ."`reportnum` int(10) unsigned NOT NULL auto_increment, "
		          ."`accesscode` int(8) unsigned zerofill NOT NULL default '00000000', "
		          ."`sptnum` int(10) unsigned NOT NULL default '0', "
		          ."`name` varchar(30) default NULL, "
		          ."`tel` varchar(30) default NULL, "
		          ."`email` varchar(50) default NULL, "
		          ."`ip` varchar(16) default NULL, "
		          ."`starttime` datetime default NULL, "
		          ."`endtime` datetime default NULL,	 "
		          ."`operationlog` tinyint(4) NOT NULL default '0', "
		          ."`report` varchar(512), "
		          ."`trbnum` int(10) unsigned NOT NULL default '0', "
		          ."`usercomment` varchar(512), "
		          ."`satisfaction` tinyint(4) NOT NULL default '0', "
		          ."`user_com_name` varchar(20) default '0',	"
		          ."`user_pub_ip` varchar(16) default NULL, "
		          ."`user_pri_ip` varchar(16) default NULL, "
		          ."`user_os` tinyint(4) NOT NULL default '0', "
		          ."`spt_pub_ip` varchar(16) default NULL, "
		          ."`spt_pri_ip` varchar(16) default NULL, "
		          ."`spt_com_name` varchar(20) default '0', "
		          ."PRIMARY KEY  (`reportnum`), "
		          ."KEY `idx_report_" .$adid."` (`accesscode`) "
		          .")ENGINE=InnoDB "
		          ."AUTO_INCREMENT=1 "
		          ."DEFAULT CHARSET=utf8;";
		
		$this->cmysql->Query($sql);
		
		return "OK";
	}

	public function GetAdminByID($adid) {
		$sql = "SELECT adid, name, service_expired_date, company, volume, url, email,function, setting, mobile_setting, phone, logo_url , service_name , mobile_volume , mobile_expired_date, pay_count "
		          ."FROM livesupport_admin_info "
		          ."WHERE adid = '".$adid."'";
		$this->cmysql->QueryFetch($sql);
		
		return $this->cmysql->row;
	}

	public function CheckAdminPW($password){
		$adnum = $_SESSION["adnum"];

		$sql = "SELECT adnum "
		          ."FROM livesupport_admin_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND password = sha2('".$password."', 512) ";
		$this->cmysql->QueryNum($sql);

		
		if($this->cmysql->num == 0) return "INCORRECT";

		return "OK";
	}

	public function ChangeAdminInfo($admindata){
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT adnum "
		          ."FROM livesupport_admin_info "
		          ."WHERE adnum = '".$adnum."'";
		$this->cmysql->QueryNum($sql);

		// 해당 Admin이 존재하지 않을 경우 에러
		if($this->cmysql->num == 0) return "NOADMIN";

		if($admindata['password'] != NULL || strlen($admindata["password"]) != 0 ){
			$sql = "UPDATE livesupport_admin_info "
			          ."SET  password = sha2('".$admindata["password"]."', 512) "
			          ."WHERE adnum = '".$adnum."'";

			$this->cmysql->Query($sql);

			return "OK";
		}
		else {
			return "FAIL";
		}
	}

	public function GetDepList() {
		$adnum = $_SESSION["adnum"];

		$deparray = array();
		
		$sql = "SELECT depnum, depname "
		          ."FROM livesupport_dep_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."ORDER BY depnum";
		
		if($this->cmysql->QueryFetch($sql) != NULL) {// dep가 존재한다.
			$num = 0;
			do {
				$cmysql2 = new ASGMysql();
				$sql2 = "SELECT sptid,name "
				            ."FROM livesupport_supporter_info "
				            ."WHERE adnum = '".$adnum."' "
				            ."AND depnum = '".$this->cmysql->row->depnum."' "
				            ."AND deleted = '' ORDER BY sptnum";
				
				$sptarray = array();
				$sptname = array();
				if($cmysql2->QueryFetch($sql2) != NULL) {// dep에 속한 supporter 존재
					do {
						$sptarray[] = $cmysql2->row->sptid;
						$sptname[] = $cmysql2->row->name;
					} while($cmysql2->NextFetch() != NULL);
				}
				else {
					// dep에 속한 supporter 없을 때
				}

				$num++;
				$deparray[] = array(
					"num" => $num,
					"depnum"=>$this->cmysql->row->depnum, 
					"depname"=>$this->cmysql->row->depname, 
					"numofspt"=>$cmysql2->num, 
					"sptarray"=>$sptarray, 
					"sptname"=>$sptname
				);
			} while($this->cmysql->NextFetch() != NULL);
			
			//reserve sorting
			rsort($deparray);

			return $deparray;
		}
		else {// dep가 하나도 존재하지 않는다. 빈 배열 리턴
			return $deparray;
		}
	}

	public function GetDepListPage($page){
		$adnum = $_SESSION["adnum"];

		$tblname = "livesupport_dep_info";
		$deparray = array();
		$content_arr = array();
		$content_row_arr = array();
		
		$dept_select = "SELECT depnum, depname, memo FROM";
		$dept_table = " ".$tblname." ";
		$dept_where = "WHERE adnum = '".$adnum."' ";
		$dept_order = "ORDER BY depnum";
		$dept_sub_sql = $dept_table . $dept_where . $dept_order;

		$pView = new ASGPage(10,10);	//페이지당 글갯수 , 페이지그룹당 페이지수 
		$pView->SetTotalNum($dept_sub_sql);	// 총 데이타 수를 구함. 
		$pView->SetPage($page);

		$this_page_first = $pView->total_num - ((intVal($page)-1)*9);

		$dept_sql = $dept_select . $dept_sub_sql;
		$dept_limit = " LIMIT ".$pView->page_first_data.", ".$pView->page_per_num;
		$sql = $dept_sql . $dept_limit;

		//query
		$this->cmysql->QueryFetch($sql);

		//data
		if($this->cmysql->row != NULL){
			//table numbering
			$num = (($page-1) * 10);
			do {
				$cmysql2 = new ASGMysql();
				$sql2 = "SELECT sptid,name "
				            ."FROM livesupport_supporter_info "
				            ."WHERE adnum = '".$adnum."' "
				            ."AND depnum = '".$this->cmysql->row->depnum."' "
				            ."AND deleted = '' ORDER BY sptnum";
				
				$sptarray = array();
				$sptname = array();
				if($cmysql2->QueryFetch($sql2) != NULL) {// dep에 속한 supporter 존재
					do {
						$sptarray[] = $cmysql2->row->sptid;
						$sptname[] = $cmysql2->row->name;
					} while($cmysql2->NextFetch() != NULL);
				}
				else {
					// dep에 속한 supporter 없을 때
				}

				$num++;
				$deparray[] = array(
					"num" => $num,
					"depnum"=>$this->cmysql->row->depnum, 
					"depname"=>$this->cmysql->row->depname, 
					"memo"=>$this->cmysql->row->memo, 
					"numofspt"=>$cmysql2->num, 
					"sptarray"=>$sptarray, 
					"sptname"=>$sptname
				);
			}while($this->cmysql->NextFetch());
		}

		$dept_arr["pagination"] = $pView;
		$dept_arr["content"] = $deparray;
		$dept_arr["page_num"] = $this_page_first;
		
		//retrun
		return $dept_arr;
	}

	public function GetDepByDepnum($depnum) {
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT depnum, adnum, depname "
		          ."FROM livesupport_dep_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND depnum = '".$depnum."'";

		$this->cmysql->QueryFetch($sql);
		
		if($this->cmysql->num == 0) {// 해당 dep가 없는 경우
			return NULL;
		}
		else {// 해당 dep가 있는 경우
			return $this->cmysql->row;
		}
	}

	public function RenameDep($depnum, $depname) {
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT depname "
		          ."FROM livesupport_dep_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND depnum = '".$depnum."'";
		$this->cmysql->QueryNum($sql);
		
		// 해당 depnum이 없을 경우
		if($this->cmysql->num == 0) return "INVALIDDEP";
		
		// 그 외에는 수정
		$sql = "UPDATE livesupport_dep_info "
		          ."SET depname = '".$depname."' "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND depnum = '".$depnum."'";
		$this->cmysql->Query($sql);
		return "OK";
	}

	public function EditDeptInfo($depnum, $depname, $depmemo) {
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT depname "
		          ."FROM livesupport_dep_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND depnum = '".$depnum."'";
		$this->cmysql->QueryNum($sql);
		
		// 해당 depnum이 없을 경우
		if($this->cmysql->num == 0) return "INVALIDDEP";
		
		// 그 외에는 수정
		$sql = "UPDATE livesupport_dep_info "
		          ."SET depname = '".$depname."', memo = '".$depmemo."' "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND depnum = '".$depnum."'";
		$this->cmysql->Query($sql);
		return "OK";
	}

	public function GetSupporterNum() {
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT sptnum "
		          ."FROM livesupport_supporter_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND deleted = ''";
		$this->cmysql->QueryNum($sql);
		
		return $this->cmysql->num;
	}

	public function GetMobileSupporterNum() {
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT sptnum "
		          ."FROM livesupport_supporter_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND deleted = '' "
		          ."AND  (function & 524288 or function & 1048576 or function & 2097152 or function & 4194304 or function & 8388608 or function & 16777216) ";
		$this->cmysql->QueryNum($sql);
		
		return $this->cmysql->num;
	}

	public function GetSupporterCnt($product_case){
		$adnum = $_SESSION["adnum"];

		$sql = "SELECT sptnum "
		          ."FROM livesupport_supporter_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND deleted = '' ";

		if($product_case == "PC"){
			$sql = $sql."AND  function <= ".PERMISSION_PC." ";
		}
		else if($product_case == "PC_N_MOBILE"){
			$sql = $sql."AND  (function & ".PERMISSION_MOBILE_PAID." or function & ".PERMISSION_MOBILE_FREE.") ";
		}
		else if($product_case == "PC_N_VIDEO"){
			$sql = $sql."AND  (function & ".PERMISSION_VIDEO_PAID." or function & ".PERMISSION_VIDEO_FREE.") ";
		}
		else if($product_case == "PC_N_MOBILE_N_VIDEO"){
			$sql = $sql."AND  (function & ".PERMISSION_MOBILE_VIDEO_PAID." or function & ".PERMISSION_MOBILE_VIDEO_FREE.") ";
		}

		$this->cmysql->QueryNum($sql);
		
		return $this->cmysql->num;
	}

	public function GetSupporterNumByDepnum($depnum) {
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT sptnum "
		          ."FROM livesupport_supporter_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND depnum = '".$depnum."' "
		          ."AND deleted = ''";
		$this->cmysql->QueryNum($sql);
		
		return $this->cmysql->num;
	}

	public function DeleteDep($depnum) {
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT depname "
		          ."FROM livesupport_dep_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND depnum = '".$depnum."'";
		$this->cmysql->QueryNum($sql);
		
		// 해당 depnum이 없을 경우
		if($this->cmysql->num == 0) return "INVALIDDEP";
		
		// 그 외에는 삭제
		$sql = "DELETE FROM livesupport_dep_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND depnum = '".$depnum."'";
		$this->cmysql->Query($sql);
		return "OK";
	}

	public function CreateDep($depname, $depmemo) {
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT depnum "
		          ."FROM livesupport_dep_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND depname = '".$depname."'";
		$this->cmysql->QueryNum($sql);
		
		// 같은 Admin이 같은 이름의 부서를 이미 갖고있을 경우
		if($this->cmysql->num != 0) return "DEPNAMEALREADYEXISTS";
		
		// 제대로 될 경우 생성
		$sql = "INSERT INTO livesupport_dep_info (adnum, depname, memo) "
		          ."VALUES ('".$adnum."', '".$depname."', '".$depmemo."')";
		$this->cmysql->Query($sql);
		return "OK";
	}

	public function GetSupporterList($search, $searchVal, $order) {
		$cmysql2 = new ASGMysql();

		$adnum = $_SESSION["adnum"];

		if($search != ""){
			$where = " and ".$search." like '%".$searchVal."%'";
		}
		else if($search == "name"){
			$spt_where = $spt_where . "AND a.".$search." like '%".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $searchVal)."%'";	
		}
		else{
			$where = "";
		}

		$sptarray = array();
		
		$sql = "SELECT sptnum, depnum, sptid, company, name, phone, email, deleted, logon,lastlogin, spt_pub_ip, spt_pri_ip , function "
		          ."FROM livesupport_supporter_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND deleted = ''".$where." "
		          ."ORDER BY sptid ".$order;
		
		$num = 0;
		if($this->cmysql->QueryFetch($sql) != NULL) {// dep가 존재한다.
			do {
				$sql = "SELECT status AS cnt "
				          ."FROM livesupport_cs "
				          ."WHERE sptid = '".$this->cmysql->row->sptid."' "
				          ."AND status ='Connected'";
				$cmysql2->QueryNum($sql);
				$num++;
				$sptarray[] = array(
					"num" => $num,
					"cnt"=>$cmysql2->num , 
					"sptnum"=>$this->cmysql->row->sptnum, 
					"depnum"=>$this->cmysql->row->depnum, 
					"sptid"=>$this->cmysql->row->sptid, 
					"company"=>$this->cmysql->row->company, 
					"name"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->name), 
					"phone"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->phone), 
					"email"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->email), 
					"deleted"=>$this->cmysql->row->deleted, 
					"logon"=>$this->cmysql->row->logon, 
					"lastlogin"=>$this->cmysql->row->lastlogin, 
					"spt_pub_ip"=>$this->cmysql->row->spt_pub_ip,
					"spt_pri_ip"=>$this->cmysql->row->spt_pri_ip ,
					"function"=>$this->cmysql->row->function);
			} while($this->cmysql->NextFetch() != NULL);
			
			return $sptarray;
		}
		else {// dep가 하나도 존재하지 않는다. 빈 배열 리턴
			return $sptarray;
		}
	}

	public function GetSupporterListPage($page, $search, $searchVal) {
		$cmysql2 = new ASGMysql();

		$adnum = $_SESSION["adnum"];

		$deparray = array();
		$content_arr = array();
		$content_row_arr = array();

		$spt_select = "SELECT a.sptnum, a.depnum, a.sptid, a.company, a.name, a.phone, a.email, a.deleted, a.logon,a.lastlogin, a.spt_pub_ip, a.spt_pri_ip , a.function FROM";
		$spt_table = " livesupport_supporter_info AS a ";
		$spt_where = "WHERE a.adnum = '".$adnum."' "
			         ."AND a.deleted = '' ";

		if($search == ""){
			$spt_where = $spt_where . "";
		}
		else{
			if($search == "depname"){
				$spt_where = " JOIN livesupport_dep_info AS b ON a.depnum = b.depnum"
				                      . " ". $spt_where . " "
				                      . "AND b.".$search." like '%".$searchVal."%'";	
			}
			else if($search == "name"){
				$spt_where = $spt_where . "AND a.".$search." like '%".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $searchVal)."%'";	
			}
			else{
				$spt_where = $spt_where . "AND a.".$search." like '%".$searchVal."%'";	
			}
		}

		$spt_order = "ORDER BY a.sptid";
		$spt_sub_sql = $spt_table . $spt_where . $spt_order;
		
		$pView = new ASGPage(15,10);	//페이지당 글갯수 , 페이지그룹당 페이지수 
		$pView->SetTotalNum($spt_sub_sql);	// 총 데이타 수를 구함. 
		$pView->SetPage($page);

		$this_page_first = $pView->total_num - ((intVal($page)-1)*9);

		$spt_sql = $spt_select . $spt_sub_sql;
		$spt_limit = " LIMIT ".$pView->page_first_data.", ".$pView->page_per_num;
		$sql = $spt_sql . $spt_limit;

		//query
		$this->cmysql->QueryFetch($sql);

		//data
		if($this->cmysql->row != NULL){
			//table numbering
			$num = (($page-1) * 15);

			do {
				$sql = "SELECT status AS cnt "
				          ."FROM livesupport_cs "
				          ."WHERE sptid = '".$this->cmysql->row->sptid."' "
				          ."AND status ='Connected'";
				$cmysql2->QueryNum($sql);
				
				$num++;
				$sptarray[] = array(
					"num" => $num,
					"cnt"=>$cmysql2->num , 
					"sptnum"=>$this->cmysql->row->sptnum, 
					"depnum"=>$this->cmysql->row->depnum, 
					"sptid"=>$this->cmysql->row->sptid, 
					"company"=>$this->cmysql->row->company, 
					"name"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->name), 
					"phone"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->phone), 
					"email"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->email), 
					"deleted"=>$this->cmysql->row->deleted, 
					"logon"=>$this->cmysql->row->logon, 
					"lastlogin"=>$this->cmysql->row->lastlogin, 
					"spt_pub_ip"=>$this->cmysql->row->spt_pub_ip,
					"spt_pri_ip"=>$this->cmysql->row->spt_pri_ip ,
					"function"=>$this->cmysql->row->function);
			} while($this->cmysql->NextFetch());
		}

		$spt_arr["pagination"] = $pView;
		$spt_arr["content"] = $sptarray;
		$spt_arr["page_num"] = $this_page_first;
		
		//retrun
		return $spt_arr;
	}

	public function GetSupporterBySptnum($sptnum) {
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT * "
		          ."FROM livesupport_supporter_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND sptnum = '".$sptnum."'";
		$this->cmysql->QueryFetch($sql);
		
		$this->cmysql->row->name = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->name);
		$this->cmysql->row->phone = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->phone);
		$this->cmysql->row->email = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->email);

		return $this->cmysql->row;
	}

	public function CreateSupporter($sptarray) {
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT sptnum "
		          ."FROM livesupport_supporter_info "
		          ."WHERE sptid = '".$sptarray["sptid"]."'";
		$this->cmysql->QueryNum($sql);
		
		// 같은 id의 supporter가 이미 존재할 경우 에러
		if($this->cmysql->num != 0) return "SUPPORTERALREADYEXISTS";

		$sql = "SELECT adnum "
		          ."FROM livesupport_admin_info "
		          ."WHERE adid = '".$sptarray["sptid"]."'";
		$this->cmysql->QueryNum($sql);
		
		// 같은 id의 admin이 이미 존재할 경우도 에러 (같이 로긴하므로)
		if($this->cmysql->num != 0) return "ADMINALREADYEXISTS";
					
		// 가입한 날짜를 저장(hchkim)
		$joindate = date("Y-m-d"); 
		
		// 아닐 경우 입력
		$sql = "INSERT INTO livesupport_supporter_info (adnum, depnum, sptid, password, company, name, phone, email, deleted, join_date, function, setting , mobile_expired_date) "
		          ."VALUES ('".$adnum."', '".$sptarray["depnum"]."', '".$sptarray["sptid"]."', sha2('".$sptarray["password"]."', 512), '".$sptarray["company"]."', 
		          	'".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $sptarray["name"])."', 
		          	'".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $sptarray["phone"])."', 
		          	'".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $sptarray["email"])."', 
		          	'', '".$joindate."', '".$sptarray["fr"]."', '".$sptarray["opt"]."' , '".$sptarray["mobile_expired_date"]."')";
		$this->cmysql->Query($sql);
		
		// 상담원 화면 보여주기를 위한 데이터를 입력한다.
		$sql = "INSERT INTO neturo_user (userid, password) "
		          ."VALUES ('".$sptarray["sptid"]."', sha2('koino', 512))";
		$this->cmysql->Query($sql);
		return "OK";
	}

	public function ModifySupporter($sptnum, $sptarray) {
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT sptnum "
		          ."FROM livesupport_supporter_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND sptnum = '".$sptnum."' "
		          ."AND deleted = ''";
		$this->cmysql->QueryNum($sql);
		
		// 해당 supporter가 존재하지 않을 경우 에러
		if($this->cmysql->num == 0) return "NOSUPPORTER";
		
		if(strlen($sptarray["password"]) == 0) {
			// 패스워드가 없을 경우, 패스워드는 그냥 유지
			$sql = "UPDATE livesupport_supporter_info "
			          ."SET "
			          ."depnum = '".$sptarray["depnum"]."', "
			          ."company = '".$sptarray["company"]."', "
			          ."name = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $sptarray["name"])."', "
			          ."phone = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $sptarray["phone"])."', "
			          ."email = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $sptarray["email"])."', "
			          ."function = '".$sptarray["fr"]."', "
			          ."setting='".$sptarray["opt"]."', "
			          ."mobile_setting='".$sptarray["mobile_setting"]."' "
			          ."WHERE sptnum = '".$sptnum."' "
			          ."AND adnum = '".$adnum."'";
		}
		else {
			$sql = "UPDATE livesupport_supporter_info "
			          ."SET "
			          ."depnum = '".$sptarray["depnum"]."', "
			          ."password = sha2('".$sptarray["password"]."', 512), "
			          ."login_try_count = 0, "
			          ."company = '".$sptarray["company"]."', "
			          ."name = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $sptarray["name"])."', "
			          ."phone = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $sptarray["phone"])."', "
			          ."email = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $sptarray["email"])."', "
			          ."function = '".$sptarray["fr"]."', "
			          ."setting='".$sptarray["opt"]."', "
			          ."mobile_setting='".$sptarray["mobile_setting"]."' "
			          ."WHERE sptnum = '".$sptnum."' "
			          ."AND adnum = '".$adnum."'";
		}

		$this->cmysql->Query($sql);
		return "OK";
	}

	public function DeleteSupporter($sptnum) {
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT sptnum, sptid "
		          ."FROM livesupport_supporter_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND sptnum = '".$sptnum."' "
		          ."AND deleted = ''";
		$this->cmysql->QueryFetch($sql);
		
		// 해당 supporter가 존재하지 않을 경우 에러
		if($this->cmysql->num == 0) return "NOSUPPORTER";
		
		// 아닐 경우, 유니크 생성키를 만들어서 id에 넣고, deleted에 원래 id를 넣는다.
		
		$originalid = $this->cmysql->row->sptid;
		$uniqueid = "";
		
		do {
			$uniqueid = "del_".mt_rand(10000000, 99999999);
			$sql = "SELECT sptnum "
			          ."FROM livesupport_supporter_info "
			          ."WHERE sptid = '".$uniqueid."'";
			$this->cmysql->QueryNum($sql);
		} while($this->cmysql->num != 0);
		
		// 업데이트.
		// 주민등록번호도 지워준다.(hchkim)
		$sql = "UPDATE livesupport_supporter_info "
		          ."SET sptid = '".$uniqueid."', deleted = '".$originalid."', ssn = 'deleted' "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND sptnum = '".$sptnum."'";
		$this->cmysql->Query($sql);
		return "OK";
	}

	public function GetTroubleTypeCount() {
		$adnum = $_SESSION["adnum"];

		$sql = "SELECT count(*) AS cnt "
		          ."FROM livesupport_trouble_type "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND deleted <> 1 ";
		$this->cmysql->QueryFetch($sql);
		
		return $this->cmysql->row->cnt;
	}

	public function GetTroubleTypeList() {
		$adnum = $_SESSION["adnum"];

		$trbtypearray = array();
		
		$sql = "SELECT trbnum, trbname, deleted, `trborder` "
		          ."FROM livesupport_trouble_type "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND deleted <> 1 "
		          ."ORDER BY `trborder` asc";
		

		if($this->cmysql->QueryFetch($sql) != NULL) {// trouble type이 존재한다.			
			do {
				$trbtypearray[] = array(
					"trbnum"=>$this->cmysql->row->trbnum, 
					"trbname"=>$this->cmysql->row->trbname, 
					"deleted"=>$this->cmysql->row->deleted, 
					"trborder"=>$this->cmysql->row->trborder
				);
			} while($this->cmysql->NextFetch() != NULL);
			
			return $trbtypearray;
		}
		else {// dep가 하나도 존재하지 않는다. 빈 배열 리턴
			return $trbtypearray;
		}
	}

	public function GetTroubleTypeByTrbnum($trbnum) {
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT trbname, deleted "
		          ."FROM livesupport_trouble_type "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND trbnum = '".$trbnum."'";
		$this->cmysql->QueryFetch($sql);

		return $this->cmysql->row;
	}

	public function ModifyTroubleType($trbnum, $trbname) {
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT trbnum "
		          ."FROM livesupport_trouble_type "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND trbnum = '".$trbnum."'";
		$this->cmysql->QueryNum($sql);
		
		// 해당 장애유형이 존재하지 않을 경우 에러
		if($this->cmysql->num == 0) return "NOTROUBLETYPE";
		
		// 아닐 경우 변경
		$sql = "UPDATE livesupport_trouble_type "
		          ."SET trbname = '".$trbname."' "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND trbnum = '".$trbnum."'";
		$this->cmysql->Query($sql);
		return "OK";
	}

	public function DeleteTroubleType($trbnum) {
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT `trborder` "
		          ."FROM livesupport_trouble_type "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND trbnum = '".$trbnum."'";

		$this->cmysql->QueryFetch($sql);
		
		// 해당 장애유형이 존재하지 않을 경우 에러
		if($this->cmysql->num == 0) return "NOTROUBLETYPE";
		
		// 아닐 경우 삭제
		$sql = "UPDATE livesupport_trouble_type "
		          ."SET `trborder`=`trborder`-1 "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND `trborder` > ".$this->cmysql->row->trborder;
		$this->cmysql->Query($sql);

		$sql = "UPDATE livesupport_trouble_type "
		          ."SET deleted = '1', `trborder`='-1' "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND trbnum = '".$trbnum."'";
		$this->cmysql->Query($sql);
		return "OK";
	}

	public function CreateTroubleTypeDefault($adnum){
		$sql = "INSERT INTO livesupport_trouble_type (adnum, trbname, trborder) "
		          ."VALUES ('".$adnum."', '기능 오류', 0)";
		$this->cmysql->Query($sql);
		
		$sql = "INSERT INTO livesupport_trouble_type (adnum, trbname, trborder) "
		          ."VALUES ('".$adnum."', '사용 불편', 1)";
		$this->cmysql->Query($sql);

		$sql = "INSERT INTO livesupport_trouble_type (adnum, trbname, trborder) "
		          ."VALUES ('".$adnum."', '사용자 교육', 2)";
		$this->cmysql->Query($sql);

		$sql = "INSERT INTO livesupport_trouble_type (adnum, trbname, trborder) "
		          ."VALUES ('".$adnum."', '업무 지원', 3)";
		$this->cmysql->Query($sql);
	}

	public function CreateTroubleType() {
		$sel_row;
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT max(`trborder`) AS `trborder` "
		          ."FROM livesupport_trouble_type "
		          ."WHERE adnum=".$adnum." "
		          ."AND deleted != '1' "
		          ."GROUP BY adnum";

		if($sel_row = $this->cmysql->QueryFetch($sql)){
			$next_order = $sel_row->trborder+1;
			$sql = "INSERT INTO livesupport_trouble_type (adnum, trbname,`trborder`) "
			          ."VALUES ('".$adnum."', '새로운 지원 유형', '".$next_order."')";
		}else{
			$sql = "INSERT INTO livesupport_trouble_type (adnum, trbname) "
			          ."VALUES ('".$adnum."', '새로운 지원 유형')";
		}
		$this->cmysql->Query($sql);
	}

	public function CheckReportExsit($search, $searchVal, $date1, $date2){
		$tempdate = explode("-", $date2);// !!! date2는 편의상 하루 더 연기하여 계산하도록 한다.
		
		$temptimestamp = mktime(0, 0, 0, $tempdate[1], $tempdate[2], $tempdate[0]);
		$temptimestamp += 24*60*60;
		
		$date2 = date("Y-m-d", $temptimestamp);
		
		$adnum = $_SESSION["adnum"];

		$where = " ";
		if($search != "" && $searchVal != ""){

			if($search == "sptid"){
				$where = " AND b.".$search." = '".$searchVal."' ";
			}
			else{
				$where = " ";
			}

		}
		else{
			$where = " ";
		}
		
		$sql = "SELECT a.reportnum "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " a, livesupport_supporter_info b "
		          ."WHERE a.starttime >= '".$date1."' "
		          ."AND a.starttime < '".$date2."' "
		          ."AND a.sptnum = b.sptnum "
		          .$where
		          ."AND b.adnum = '".$adnum."' "
		          ."AND a.operationlog != '0'";

		$this->cmysql->QueryNum($sql);
		if($this->cmysql->num != 0) return TRUE;
		else return FALSE;
	}

	public function GetReportList($page, $search, $searchVal, $starttime, $endtime){
		$adnum = $_SESSION["adnum"];
		
		$reportarray = array(); // array(pageinfo array, dataarray)
		$dataarray = array();
		
		$numOfArticlePerPage = 10; // 한 페이지에 보여줄 report 수
		$numOfPageToShow = 15; // 한번에 보여줄 페이지 목록 수

		if($search != "" && $searchVal != ""){
			
			if($search == "sptid"){
				$where = " and b.".$search." = '".$searchVal."'";
			}else if($search == "sptname"){
				$where = " and b.name like '%".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $searchVal)."%'";
			}else if($search == "name"){
				$where = " and a.".$search." like '%".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $searchVal)."%'";
			}else{
				$where = " and a.".$search." like '%".$searchVal."%'";
			}
		}
		else{
			$where = "";
		}

		$tmp = explode("-",$endtime);
		$endTime = mktime(0,0,1,$tmp[1],$tmp[2],$tmp[0]);
		$endTime = $endTime + (60*60*24);
		$endDate = date("Y-m-d",$endTime);
		$where .= " and starttime >= '".$starttime."' and endtime < '".$endDate."'";

		$board_tab = "livesupport_report_" . $_SESSION["adid"] . " a, livesupport_supporter_info b where a.sptnum = b.sptnum and b.adnum = '".$adnum."' ".$where." order by a.reportnum desc";
		$pView = new ASGPage(15,10);	//페이지당 글갯수 , 페이지그룹당 페이지수
		$pView->SetTotalNum($board_tab);	// 총 데이타 수를 구함. 
		$pView->SetPage($page);

		$this_page_first = $pView->total_num - ((intVal($page)-1)*9);

		$sql = "SELECT a.reportnum AS reportnum, b.sptid AS sptid, b.name AS sptname, b.deleted AS deleted, a.name AS name, a.starttime AS starttime, a.operationlog AS operationlog, a.endtime AS endtime, a.user_os AS user_os "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " a, livesupport_supporter_info b "
		          ."WHERE a.sptnum = b.sptnum "
		          ."AND b.adnum = '".$adnum."' " .$where." "
		          ."ORDER BY a.starttime DESC "
		          ."LIMIT " . $pView->page_first_data . ", " . $pView->page_per_num;
		
		$this->cmysql->QueryFetch($sql);

		if($this->cmysql->row != NULL){
			$num = (($page-1) * 10);
			do{
				$num++;
				if($this->cmysql->row->deleted == "") {// 현존하는 상담원인 경우
					$dataarray[] = array(
						"num"=>$num, 
						"reportnum"=>$this->cmysql->row->reportnum, 
						"sptid"=>$this->cmysql->row->sptid, 
						"sptname"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->sptname),
						"name"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->name), 
						"trbname"=>$this->cmysql->row->trbname, 
						"starttime"=>$this->cmysql->row->starttime, 
						"operationlog"=>$this->cmysql->row->operationlog, 
						"endtime"=>$this->cmysql->row->endtime, 
						"user_os"=>$this->cmysql->row->user_os
					);
				}
				else {// 지워진 상담원 아이디인 경우, 아이디 대신 deleted
					$dataarray[] = array(
						"num"=>$num, 
						"reportnum"=>$this->cmysql->row->reportnum, 
						"sptid"=>$this->cmysql->row->deleted, 
						"sptname"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->sptname),
						"name"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->name), 
						"trbname"=>$this->cmysql->row->trbname, 
						"starttime"=>$this->cmysql->row->starttime, 
						"operationlog"=>$this->cmysql->row->operationlog, 
						"endtime"=>$this->cmysql->row->endtime, 
						"user_os"=>$this->cmysql->row->user_os
					);
				}
			}while($this->cmysql->NextFetch());
		}
		
		$reportarray["pageInfo"] = $pView;
		$reportarray["datainfo"] = $dataarray;
		$reportarray["pageNum"] = $this_page_first;
		
		return $reportarray;
	}

	public function GetReport($reportnum){
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT trbnum "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " "
		          ."WHERE reportnum = '".$reportnum."'";
		$this->cmysql->QueryFetch($sql);

		if($this->cmysql->row->trbnum == 0) {// 장애 유형 선택 안되었을 경우
			$sql = "SELECT a.reportnum AS reportnum, a.accesscode AS accesscode, a.sptnum AS sptnum, a.name AS name, a.tel AS tel, a.email AS email, a.ip AS ip, a.starttime AS starttime, a.endtime AS endtime, a.trbnum AS trbnum, a.report AS report, a.operationlog AS operationlog, a.satisfaction AS satisfaction, a.usercomment AS usercomment, b.sptid AS sptid, b.deleted AS deleted, b.name AS sptname, b.lastlogin AS lastlogin, a.user_os AS user_os, a.spt_pub_ip AS spt_pub_ip, a.spt_pri_ip AS spt_pri_ip, a.user_pub_ip AS user_pub_ip, a.user_pri_ip AS user_pri_ip, a.user_com_name AS user_com_name, a.spt_com_name AS spt_com_name "
			          ."FROM livesupport_report_" . $_SESSION["adid"] . " a, livesupport_supporter_info b "
			          ."WHERE b.adnum = '".$adnum."' "
			          ."AND reportnum = '".$reportnum."' "
			          ."AND a.sptnum = b.sptnum";
			$this->cmysql->QueryFetch($sql);
		}
		else {
			$sql = "SELECT a.reportnum AS reportnum, a.accesscode AS accesscode, a.sptnum AS sptnum, a.name AS name, a.tel AS tel, a.email AS email, a.ip AS ip, a.starttime AS starttime, a.endtime AS endtime, a.trbnum AS trbnum, a.report AS report, a.operationlog AS operationlog, a.satisfaction AS satisfaction, a.usercomment AS usercomment, b.sptid AS sptid, b.deleted AS deleted, b.name AS sptname, c.trbname AS trbname, c.trbnum AS trbnum,b.lastlogin AS lastlogin, a.user_os AS user_os, a.spt_pub_ip AS spt_pub_ip, a.spt_pri_ip AS spt_pri_ip, a.user_pub_ip AS user_pub_ip, a.user_pri_ip AS user_pri_ip, a.user_com_name AS user_com_name, a.spt_com_name AS spt_com_name "
			          ."FROM livesupport_report_" . $_SESSION["adid"] . " a, livesupport_supporter_info b, livesupport_trouble_type c "
			          ."WHERE b.adnum = '".$adnum."' "
			          ."AND reportnum = '".$reportnum."' "
			          ."AND a.sptnum = b.sptnum "
			          ."AND a.trbnum = c.trbnum";	
			$this->cmysql->QueryFetch($sql);
		}
		
		$this->cmysql->row->name = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->name);
		$this->cmysql->row->tel = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->tel);
		$this->cmysql->row->email = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->email);
		$this->cmysql->row->sptname = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->sptname);

		return $this->cmysql->row;
	}

	public function GetReportByHistory($reportnum){
		$adnum = $_SESSION["adnum"];
		$sptnum = $_SESSION["sptnum"];
		
		$sql = "SELECT trbnum "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " "
		          ."WHERE reportnum = '".$reportnum."'"
		          ."AND sptnum = '".$sptnum."' ";
		$this->cmysql->QueryFetch($sql);

		if($this->cmysql->num == 0){
			return "NO_AUTH";
		}
		else{
			if($this->cmysql->row->trbnum == 0) {// 장애 유형 선택 안되었을 경우
				$sql = "SELECT a.reportnum AS reportnum, a.accesscode AS accesscode, a.sptnum AS sptnum, a.name AS name, a.tel AS tel, a.email AS email, a.ip AS ip, a.starttime AS starttime, a.endtime AS endtime, a.trbnum AS trbnum, a.report AS report, a.operationlog AS operationlog, a.satisfaction AS satisfaction, a.usercomment AS usercomment, b.sptid AS sptid, b.deleted AS deleted, b.name AS sptname, b.lastlogin AS lastlogin, a.user_os AS user_os, a.spt_pub_ip AS spt_pub_ip, a.spt_pri_ip AS spt_pri_ip, a.user_pub_ip AS user_pub_ip, a.user_pri_ip AS user_pri_ip, a.user_com_name AS user_com_name, a.spt_com_name AS spt_com_name "
				          ."FROM livesupport_report_" . $_SESSION["adid"] . " a, livesupport_supporter_info b "
				          ."WHERE b.adnum = '".$adnum."' "
				          ."AND reportnum = '".$reportnum."' "
				          ."AND a.sptnum = b.sptnum";
				$this->cmysql->QueryFetch($sql);
			}
			else {
				$sql = "SELECT a.reportnum AS reportnum, a.accesscode AS accesscode, a.sptnum AS sptnum, a.name AS name, a.tel AS tel, a.email AS email, a.ip AS ip, a.starttime AS starttime, a.endtime AS endtime, a.trbnum AS trbnum, a.report AS report, a.operationlog AS operationlog, a.satisfaction AS satisfaction, a.usercomment AS usercomment, b.sptid AS sptid, b.deleted AS deleted, b.name AS sptname, c.trbname AS trbname, c.trbnum AS trbnum,b.lastlogin AS lastlogin, a.user_os AS user_os, a.spt_pub_ip AS spt_pub_ip, a.spt_pri_ip AS spt_pri_ip, a.user_pub_ip AS user_pub_ip, a.user_pri_ip AS user_pri_ip, a.user_com_name AS user_com_name, a.spt_com_name AS spt_com_name "
				          ."FROM livesupport_report_" . $_SESSION["adid"] . " a, livesupport_supporter_info b, livesupport_trouble_type c "
				          ."WHERE b.adnum = '".$adnum."' "
				          ."AND reportnum = '".$reportnum."' "
				          ."AND a.sptnum = b.sptnum "
				          ."AND a.trbnum = c.trbnum";	
				$this->cmysql->QueryFetch($sql);
			}
		}
		
		$this->cmysql->row->sptname = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->sptname);
		$this->cmysql->row->name = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->name);
		$this->cmysql->row->phone = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->phone);
		$this->cmysql->row->tel = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->tel);
		$this->cmysql->row->email = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->email);

		return $this->cmysql->row;
	}

	public function GetMaxOrder(){
		$adnum = $_SESSION["adnum"];

		$sql = "SELECT max('trborder') AS 'trborder' "
		          ."FROM livesupport_trouble_type "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND deleted <> 1 group by adnum";
		$this->cmysql->QueryFetch($sql);

		return $this->cmysql->row->trborder;
	}

	public function MoveTroubleType($trbnum,$str){
		$adnum = $_SESSION["adnum"];

		$sql = "SELECT `trborder` "
		          ."FROM livesupport_trouble_type "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND trbnum = '".$trbnum."'";
		$this->cmysql->QueryFetch($sql);
		
		if($this->cmysql->num == 0) return "NOTROUBLETYPE";// 해당 장애유형이 존재하지 않을 경우 에러
		
		//우선순위를 높일때
		if($str == "up"){
			$current_order = $this->cmysql->row->trborder;

			$prev_order = $this->cmysql->row->trborder - 1;

			$sql = "SELECT trbnum "
			          ."FROM livesupport_trouble_type "
			          ."WHERE adnum = '".$adnum."' "
			          ."AND `trborder` = '".$prev_order."'";

			$this->cmysql->QueryFetch($sql);
			$prev_num = $this->cmysql->row->trbnum;

			$sql = "UPDATE livesupport_trouble_type "
			          ."SET `trborder`=".$prev_order." "
			          ."WHERE adnum = '".$adnum."' "
			          ."AND trbnum='".$trbnum."'";			

			$this->cmysql->Query($sql);
			
			$sql = "UPDATE livesupport_trouble_type "
			          ."SET `trborder`=".$current_order." "
			          ."WHERE adnum = '".$adnum."' "
			          ."AND trbnum='".$prev_num."'";

			$this->cmysql->Query($sql);

		}
		else if($str == "down"){ //우선순위를 낮출때
			$current_order = $this->cmysql->row->trborder;
			$next_order = $this->cmysql->row->trborder + 1;

			$sql = "SELECT trbnum "
			          ."FROM livesupport_trouble_type "
			          ."WHERE adnum = '".$adnum."' "
			          ."AND `trborder` = '".$next_order."'";

			$this->cmysql->QueryFetch($sql);
			$next_num = $this->cmysql->row->trbnum;

			$sql = "UPDATE livesupport_trouble_type "
			          ."SET `trborder`=".$next_order." "
			          ."WHERE adnum = '".$adnum."' "
			          ."AND trbnum='".$trbnum."'";
			$this->cmysql->Query($sql);
			
			$sql = "UPDATE livesupport_trouble_type "
			          ."SET `trborder`=".$current_order." "
			          ."WHERE adnum = '".$adnum."' "
			          ."AND trbnum='".$next_num."'";
			$this->cmysql->Query($sql);
		}
		return 1;
	}

	public function GetReportExcel($search, $searchVal, $starttime, $endtime){
		$adnum = $_SESSION["adnum"];
		$reportarray = array(); // array(pageinfo array, dataarray)
		$dataarray = array();
		
		$numOfArticlePerPage = 10; // 한 페이지에 보여줄 report 수
		$numOfPageToShow = 5; // 한번에 보여줄 페이지 목록 수

		if($search != "" && $searchVal != ""){
			if($search == "sptid"){
				$where = " and b.".$search." like '%".$searchVal."%'";
			}else if($search == "sptname"){
				$where = " and b.name like '%".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $searchVal)."%'";
			}else if($search == "name"){
				$where = " and a.".$search." like '%".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $searchVal)."%'";
			}else{
				$where = " and a.".$search." like '%".$searchVal."%'";
			}
		}else{
			$where = "";
		}

		$tmp = explode("-",$endtime);
		$endTime = mktime(0,0,1,$tmp[1],$tmp[2],$tmp[0]);
		$endTime = $endTime + (60*60*24);
		$endDate = date("Y-m-d",$endTime);
		$where .= " and starttime >= '".$starttime."' and starttime < '".$endDate."'";

		$sql = "SELECT a.reportnum AS reportnum, a.accesscode AS accesscode, a.sptnum AS sptnum, a.name AS name, a.tel AS tel, a.email AS email, a.ip AS ip, a.starttime AS starttime, a.endtime AS endtime, a.trbnum AS trbnum, a.report AS report, a.operationlog AS operationlog, a.satisfaction AS satisfaction, a.usercomment AS usercomment, b.sptid AS sptid, b.deleted AS deleted, b.name AS sptname, b.lastlogin AS lastlogin, a.user_os AS user_os, a.spt_pub_ip AS spt_pub_ip, a.spt_pri_ip AS spt_pri_ip, a.spt_com_name AS spt_com_name, a.user_pub_ip AS user_pub_ip, a.user_pri_ip AS user_pri_ip, a.user_com_name AS user_com_name, c.trbname AS trbname, a.trbnum AS trbnum "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " a, livesupport_supporter_info b, livesupport_trouble_type c "
		          ."WHERE a.sptnum = b.sptnum "
		          ."AND b.adnum = '".$adnum."'  "
		          ."AND (a.trbnum = c.trbnum or a.trbnum = 0) ".$where." "
		          ."GROUP BY a.reportnum "
		          ."ORDER BY a.reportnum DESC";

		$this->cmysql->QueryFetch($sql);
		
		if($this->cmysql->row != NULL){
			do{
				if($this->cmysql->row->deleted == "") {// 현존하는 상담원인 경우
					$dataarray[] = array(
						"num"=>$i, 
						"reportnum"=>$this->cmysql->row->reportnum, 
						"sptid"=>$this->cmysql->row->sptid, 
						"name"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->name), 
						"trbname"=>$this->cmysql->row->trbname, 
						"starttime"=>$this->cmysql->row->starttime, 
						"operationlog"=>$this->cmysql->row->operationlog, 
						"endtime"=>$this->cmysql->row->endtime, 
						"user_os"=>$this->cmysql->row->user_os, 
						"accesscode"=>$this->cmysql->row->accesscode, 
						"tel"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->tel), 
						"report"=>$this->cmysql->row->report, 
						"satisfaction"=>$this->cmysql->row->satisfaction, 
						"usercomment"=>$this->cmysql->row->usercomment, 
						"sptname"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->sptname), 
						"lastlogin"=>$this->cmysql->row->lastlogin, 
						"spt_pub_ip"=>$this->cmysql->row->spt_pub_ip, 
						"spt_pri_ip"=>$this->cmysql->row->spt_pri_ip, 
						"spt_com_name"=>$this->cmysql->row->spt_com_name, 
						"user_pub_ip"=>$this->cmysql->row->user_pub_ip, 
						"user_pri_ip"=>$this->cmysql->row->user_pri_ip, 
						"user_com_name"=>$this->cmysql->row->user_com_name, 
						"email"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->email), 
						"trbname"=>$this->cmysql->row->trbname, 
						"trbnum"=>$this->cmysql->row->trbnum
					);
				}
				else {// 지워진 상담원 아이디인 경우, 아이디 대신 deleted
					$dataarray[] = array(
						"num"=>$i, 
						"reportnum"=>$this->cmysql->row->reportnum, 
						"sptid"=>$this->cmysql->row->deleted, 
						"name"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->name), 
						"trbname"=>$this->cmysql->row->trbname, 
						"starttime"=>$this->cmysql->row->starttime, 
						"operationlog"=>$this->cmysql->row->operationlog, 
						"endtime"=>$this->cmysql->row->endtime, 
						"user_os"=>$this->cmysql->row->user_os,
						"accesscode"=>$this->cmysql->row->accesscode, 
						"tel"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->tel), 
						"report"=>$this->cmysql->row->report, 
						"satisfaction"=>$this->cmysql->row->satisfaction, 
						"usercomment"=>$this->cmysql->row->usercomment, 
						"sptname"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->sptname), 
						"lastlogin"=>$this->cmysql->row->lastlogin, 
						"spt_pub_ip"=>$this->cmysql->row->spt_pub_ip, 
						"spt_pri_ip"=>$this->cmysql->row->spt_pri_ip, 
						"spt_com_name"=>$this->cmysql->row->spt_com_name,  
						"user_pub_ip"=>$this->cmysql->row->user_pub_ip, 
						"user_pri_ip"=>$this->cmysql->row->user_pri_ip, 
						"user_com_name"=>$this->cmysql->row->user_com_name, 
						"email"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->email), 
						"trbname"=>$this->cmysql->row->trbname, 
						"trbnum"=>$this->cmysql->row->trbnum
					);
				}
			}while($this->cmysql->NextFetch());
		}
		
		$reportarray["datainfo"] = $dataarray;
		
		return $reportarray;
	}

	public function GetReportByMonth($year){
		$adnum = $_SESSION["adnum"];

		$sql = "SELECT starttime "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " AS a "
		          ."JOIN livesupport_supporter_info AS b "
		          ."ON a.sptnum=b.sptnum "
		          ."WHERE adnum='".$adnum."'";

		if($year != ""){
			$sql .= " AND a.starttime >= '".$year."'";
			$sql .= " AND a.starttime < '".++$year."'";
		}
		else{
			$current = mktime();
			$year = date("Y",$current);
			$sql .= " AND a.starttime >= '".$year."'";
			$sql .= " AND a.starttime < '".++$year."'";
		}

		$this->cmysql->Query($sql);
		return $this->cmysql->result;
	}

	public function GetReportByDay($year, $month){
		$adnum = $_SESSION["adnum"];

		$sql = "SELECT starttime "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " AS a "
		          ."JOIN livesupport_supporter_info AS b "
		          ."ON a.sptnum=b.sptnum "
		          ."WHERE adnum='".$adnum."'";

		if($year != ""){
			$chkDate = $year."-".sprintf("%02d",$month);
			$tmpTime = mktime(0,0,0,$month,1,$year);
			$tmpTime = $tmpTime + (60*60*24*32);
			$nextDate = date("Y-m",$tmpTime);
			$sql .= " AND a.starttime >= '".$chkDate."'";
			$sql .= " AND a.starttime < '".$nextDate."'";
		}
		else{
			$current = mktime();
			$year = date("Y",$current);
			$month = date("m",$current);
			$chkDate = $year."-".$month;
			$tmpTime = mktime(0,0,0,$month,1,$year);
			$tmpTime = $tmpTime + (60*60*24*32);
			$nextDate = date("Y-m",$tmpTime);
			$sql .= " AND a.starttime >= '".$chkDate."'";
			$sql .= " AND a.starttime < '".$nextDate."'";
		}

		$this->cmysql->Query($sql);
		return $this->cmysql->result;
	}

	public function GetReportByTime($sDate, $eDate){
		$adnum = $_SESSION["adnum"];

		$sql = "SELECT starttime, IF(starttime>endtime, 0,UNIX_TIMESTAMP(endtime)-UNIX_TIMESTAMP(starttime)) AS intervaltime "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " AS a "
		          ."JOIN livesupport_supporter_info AS b "
		          ."ON a.sptnum=b.sptnum "
		          ."WHERE adnum='".$adnum."'";

		if($sDate != ""){
			$tmp = explode("-",$eDate);
			$endTime = mktime(0,0,1,$tmp[1],$tmp[2],$tmp[0]);
			$endTime = $endTime + (60*60*24);
			$endDate = date("Y-m-d",$endTime);
			$sql .= " AND a.starttime >= '".$sDate."'";
			$sql .= " AND a.starttime < '".$endDate."'";
		}
		else{
			$current = mktime();
			$sDate = date("Y-m-d",$current);
			$endTime = $current + (60*60*24);
			$endDate = date("Y-m-d",$endTime);
			$sql .= " AND a.starttime >= '".$sDate."'";
			$sql .= " AND a.starttime < '".$endDate."'";
		}

		$this->cmysql->Query($sql);
		return $this->cmysql->result;
	}

	public function GetReportBySpt($sDate,$eDate){
		$adnum = $_SESSION["adnum"];

		$sql = "SELECT b.sptid AS sptid, count(b.sptid) AS cnt, sum( IF(starttime>endtime, 0,UNIX_TIMESTAMP(endtime)-UNIX_TIMESTAMP(starttime))  ) AS totaltime "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " AS a "
		          ."JOIN livesupport_supporter_info AS b "
		          ."ON a.sptnum=b.sptnum "
		          ."WHERE b.deleted='' "
		          ."AND adnum='".$adnum."' ";

		if($sDate != ""){
			$tmp = explode("-",$eDate);
			$endTime = mktime(0,0,1,$tmp[1],$tmp[2],$tmp[0]);
			$endTime = $endTime + (60*60*24);
			$endDate = date("Y-m-d",$endTime);
			$sql .= " AND a.starttime >= '".$sDate."'";
			$sql .= " AND a.starttime < '".$endDate."'";
		}
		else{
			$current = mktime();
			$sDate = date("Y-m-d",$current);
			$endTime = $current + (60*60*24);
			$endDate = date("Y-m-d",$endTime);
			$sql .= " AND a.starttime >= '".$sDate."'";
			$sql .= " AND a.starttime < '".$endDate."'";
		}
		$sql .= " GROUP BY b.sptid";

		$this->cmysql->Query($sql);

		return $this->cmysql->result;
	}

	public function GetReportByUseTime($sDate,$eDate){
		$adnum = $_SESSION["adnum"];
		$adid = $_SESSION["adid"];

		$sql = "SELECT starttime,endtime "
		          ."FROM livesupport_report_".$adid." AS a "
		          ."JOIN livesupport_supporter_info AS b "
		          ."ON a.sptnum=b.sptnum "
		          ."WHERE adnum='".$adnum."'";

		if($sDate != ""){
			$tmp = explode("-",$eDate);
			$endTime = mktime(0,0,1,$tmp[1],$tmp[2],$tmp[0]);
			$endTime = $endTime + (60*60*24);
			$endDate = date("Y-m-d",$endTime);
			$sql .= " and a.starttime >= '".$sDate."'";
			$sql .= " and a.starttime < '".$endDate."'";
		}else{
			$current = mktime();
			$sDate = date("Y-m-d",$current);
			$endTime = $current + (60*60*24);
			$endDate = date("Y-m-d",$endTime);
			$sql .= " and a.starttime >= '".$sDate."'";
			$sql .= " and a.starttime < '".$endDate."'";
		}
		
		$this->cmysql->Query($sql);
		return $this->cmysql->result;
	}

	public function GetReportByOS($sDate, $eDate){
		$adnum = $_SESSION["adnum"];

		$sql = "SELECT a.user_os AS os,count(a.user_os) AS cnt "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " AS a "
		          ."JOIN livesupport_supporter_info AS b "
		          ."ON a.sptnum=b.sptnum "
		          ."WHERE adnum='".$adnum."'";

		if($sDate != ""){
			$tmp = explode("-",$eDate);
			$endTime = mktime(0,0,1,$tmp[1],$tmp[2],$tmp[0]);
			$endTime = $endTime + (60*60*24);
			$endDate = date("Y-m-d",$endTime);
			$sql .= " AND a.starttime >= '".$sDate."'";
			$sql .= " AND a.starttime < '".$endDate."'";
		}
		else{
			$current = mktime();
			$sDate = date("Y-m-d",$current);
			$endTime = $current + (60*60*24);
			$endDate = date("Y-m-d",$endTime);
			$sql .= " AND a.starttime >= '".$sDate."'";
			$sql .= " AND a.starttime < '".$endDate."'";
		}
		$sql .= " GROUP BY a.user_os";

		$this->cmysql->Query($sql);
		return $this->cmysql->result;
	}

	public function GetReportByTroubleType($date1, $date2){
		if($date1 == ""){
			$date1 = date("Y-m-d",mktime());
			$date2 = date("Y-m-d",mktime());
		}

		$tempdate = explode("-", $date2);
		$temptimestamp = mktime(0, 0, 0, $tempdate[1], $tempdate[2], $tempdate[0]);
		$temptimestamp += 24*60*60;
		
		$date2 = date("Y-m-d", $temptimestamp);
		
		$adnum = $_SESSION["adnum"];

		$statsarray = array();
		
		$trbtypearray = $this->GetTroubleTypeList();
		
		while(list($key, $val) = each($trbtypearray)) {// 각각의 장애 유형마다 기록을 구한다.
			if($val["deleted"] != 0) continue;
			
			$temparray = array(0, 0, 0, 0);
			$sql = "SELECT operationlog, count(reportnum) AS count "
			          ."FROM livesupport_report_" . $_SESSION["adid"] . " "
			          ."WHERE trbnum = '".$val["trbnum"]."' "
			          ."AND starttime >= '".$date1."' "
			          ."AND starttime < '".$date2."' "
			          ."GROUP BY operationlog";
			
			if($this->cmysql->QueryFetch($sql) != NULL) {
				do {
					$temparray[$this->cmysql->row->operationlog] = $this->cmysql->row->count;
				} while($this->cmysql->NextFetch());
			};
			
			$statsarray[] = array(
				"trbname"=>$val["trbname"], 
				"res1"=>$temparray[1], 
				"res2"=>$temparray[2], 
				"res3"=>$temparray[3], 
				"res0"=>$temparray[0]
			);
		}
		
		$sql = "SELECT operationlog, count(reportnum) AS count "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " "
		          ."WHERE trbnum = 0 "
		          ."AND starttime >= '".$date1."' "
		          ."AND starttime < '".$date2."' "
		          ."GROUP BY operationlog";
		
		if($this->cmysql->QueryFetch($sql) != NULL) {
			do {
				$temparray[$this->cmysql->row->operationlog] = $this->cmysql->row->count;
			} while($this->cmysql->NextFetch());
		}

		$trb_name_string = TROUBLE_NAME_NOT_SELECTED;

		$statsarray[] = array(
			"trbname"=>$trb_name_string, 
			"res1"=>$temparray[1], 
			"res2"=>$temparray[2], 
			"res3"=>$temparray[3], 
			"res0"=>$temparray[0]
		);
		return $statsarray;
	}

	public function ModifyAdminInfo($admindata) {		
		$adnum = $_SESSION["adnum"];
		
		$sql = "SELECT adnum "
		          ."FROM livesupport_admin_info "
		          ."WHERE adnum = '".$adnum."'";
		$this->cmysql->QueryNum($sql);
		
		// 해당 Admin이 존재하지 않을 경우 에러
		if($this->cmysql->num == 0) return "NOADMIN";
		
		// 아닐 경우 변경

		if(strlen($admindata["password"]) == 0) 
		{
			$sql = "UPDATE livesupport_admin_info "
			          ."SET name = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $admindata["name"])."', "
			          ."company = '".$admindata["company"]."', "
			          ."service_name =  '".$admindata["service_name"]."', "
			          ."email = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $admindata["email"])."' , "
			          ."phone = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $admindata["phone"])."' , "
			          ."tel = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $admindata["tel"])."' , "
			          ."logo_url = '".$admindata["logo_url"]."' "
			          ."WHERE adnum = '".$adnum."'";
		}
		else 
		{
			$sql = "UPDATE livesupport_admin_info "
			          ."SET name = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $admindata["name"])."', "
			          ."password = sha2('".$admindata["password"]."', 512), "
			          ."company = '".$admindata["company"]."', "
			          ."service_name =  '".$admindata["service_name"]."' , "
			          ."email = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $admindata["email"])."'  , "
			          ."phone = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $admindata["phone"])."' , "
			          ."tel = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $admindata["tel"])."' , "
			          ."logo_url = '".$admindata["logo_url"]."' "
			          ."WHERE adnum = '".$adnum."'";
		}
		$this->cmysql->Query($sql);
		return "OK";
	}

	public function GetReportBySatisfactionOfTroubleType($date1, $date2){
		// !!! date2는 편의상 하루 더 연기하여 계산하도록 한다.
		if($date1 == ""){
			$date1 = date("Y-m-d",mktime());
			$date2 = date("Y-m-d",mktime());
		}
		$tempdate = explode("-", $date2);
		$temptimestamp = mktime(0, 0, 0, $tempdate[1], $tempdate[2], $tempdate[0]);
		$temptimestamp += 24*60*60;
		
		$date2 = date("Y-m-d", $temptimestamp);
		
		$adnum = $_SESSION["adnum"];

		$statsarray = array();
		
		$trbtypearray = $this->GetTroubleTypeList();
		// array(trbnum, trbname, deleted)

		while(list($key, $val) = each($trbtypearray)) {
			// 각각의 장애 유형마다 기록을 구한다.
			
			if($val["deleted"] != 0) continue;
			
			$temparray = array(0, 0, 0, 0);
			$sql = "SELECT satisfaction, count(reportnum) AS count "
			          ."FROM livesupport_report_" . $_SESSION["adid"] . " "
			          ."WHERE trbnum = '".$val["trbnum"]."' "
			          ."AND starttime >= '".$date1."' "
			          ."AND starttime < '".$date2."' "
			          ."AND operationlog != '0' "
			          ."GROUP BY satisfaction";
			
			if($this->cmysql->QueryFetch($sql) != NULL) {
				do {
					$temparray[$this->cmysql->row->satisfaction] = $this->cmysql->row->count;
				} while($this->cmysql->NextFetch());
			};
			
			$statsarray[] = array(
				"trbname"=>$val["trbname"], 
				"res1"=>$temparray[1], 
				"res2"=>$temparray[2], 
				"res3"=>$temparray[3], 
				"res0"=>$temparray[0]
			);
		}
		
		$sql = "SELECT satisfaction, count(reportnum) AS count "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " "
		          ."WHERE trbnum = '0' "
		          ."AND starttime >= '".$date1."' "
		          ."AND starttime < '".$date2."' "
		          ."GROUP BY satisfaction";
			
		if($this->cmysql->QueryFetch($sql) != NULL) {
			do {
				$temparray[$this->cmysql->row->satisfaction] = $this->cmysql->row->count;
			} while($this->cmysql->NextFetch());
		};

		$trb_name_string = TROUBLE_NAME_EN;
		if($_SESSION['language'] == 'en'){
			$trb_name_string = TROUBLE_NAME_EN;
		}
		else if($_SESSION['language'] == 'kr'){
			$trb_name_string = TROUBLE_NAME_KR;
		}
		else if($_SESSION['language'] == 'jp'){
			$trb_name_string = TROUBLE_NAME_JP;
		}	

		$statsarray[] = array(
			"trbname"=>$trb_name_string, 
			"res1"=>$temparray[1], 
			"res2"=>$temparray[2], 
			"res3"=>$temparray[3], 
			"res0"=>$temparray[0]
		);
		
		return $statsarray;
	}

	public function GetNoticeList($page, $search_type, $search_val){
		//case by $page == 0 : total
		$tblname = "b_notice";
		$notice_arr = array();
		$content_arr = array();
		$content_row_arr = array();

		if($page == 0){
			$page = 1;
		}

		//set sql
		$notice_select = "SELECT seq, title, depth, name, regdate, readcount, filename FROM";
		$notice_table = " ".$tblname." ";
		if($search_val == ""){
			$notice_where = "";
			$notice_order = "ORDER BY listorder DESC";
		}
		else if($search_type == "name"){
			$notice_where = "WHERE ".$search_type." like '%". $this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $search_val) ."%' ";
			$notice_order = "ORDER BY seq DESC";
		}
		else{
			//$search_type  : title, seq, regdate
			$notice_where = "WHERE ".$search_type." like '%".$search_val."%' ";
			$notice_order = "ORDER BY seq DESC";
		}
		$notice_sub_sql = $notice_table . $notice_where . $notice_order;

		$pView = new ASGPage(10,10);	//페이지당 글갯수 , 페이지그룹당 페이지수 
		$pView->SetTotalNum($notice_sub_sql);	// 총 데이타 수를 구함. 
		$pView->SetPage($page);

		$this_page_first = $pView->total_num - ((intVal($page)-1)*9);

		$notice_sql = $notice_select . $notice_sub_sql;
		$notice_limit = " LIMIT ".$pView->page_first_data.", ".$pView->page_per_num;
		$sql = $notice_sql . $notice_limit;

		//query
		$this->cmysql->QueryFetch($sql);

		$num = (($page-1) * 10);
		//data
		if($this->cmysql->row != NULL){
			
			do{
				$num++;
				$content_row_arr["num"] = $num;
				$content_row_arr["seq"] = $this->cmysql->row->seq;
				$content_row_arr["title"] = $this->cmysql->row->title;
				$content_row_arr["depth"] = $this->cmysql->row->depth;
				$content_row_arr["name"] = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->name);
				$content_row_arr["regdate"] = $this->cmysql->row->regdate;
				$content_row_arr["readcount"] = $this->cmysql->row->readcount;
				$content_row_arr["filename"] = $this->cmysql->row->filename;
				$content_arr[] = $content_row_arr;//content $key == seq
			}while($this->cmysql->NextFetch());
		}

		$notice_arr["pagination"] = $pView;
		$notice_arr["content"] = $content_arr;
		$notice_arr["page_num"] = $this_page_first;
		
		//retrun
		return $notice_arr;
	}

	public function GetNotice($seq){
		$tblname = "b_notice";
		$content_row_arr = array();

		//set sql
		$notice_select = "SELECT title, name, email, regdate, content, filename FROM";
		$notice_table = " ".$tblname." ";
		$notice_where = "WHERE seq='".$seq."'";

		$sql = $notice_select . $notice_table . $notice_where;

		//query
		$this->cmysql->QueryFetch($sql);

		if($this->cmysql->row != NULL){
			$content_row_arr["title"] = $this->cmysql->row->title;
			$content_row_arr["name"] = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->name);
			$content_row_arr["regdate"] = $this->cmysql->row->regdate;
			$content_row_arr["content"] = $this->cmysql->row->content;
			$content_row_arr["filename"] = $this->cmysql->row->filename;
		}

		return $content_row_arr;
	}

	public function GenerateTitleBar(&$str, $sptnum) {
		$sql = "SELECT sptid "
		          ."FROM livesupport_supporter_info "
		          ."WHERE sptnum = '".$sptnum."'";
		$this->cmysql->QueryFetch($sql);

		if($this->cmysql->num == 0) 
			return FALSE; // 잘못된접속
		
		$row = $this->cmysql->row;
		
		$titlekey = crypt($sptnum).crypt(time()).crypt($row->sptid).crypt(time()).crypt($row->sptid);
		$str = substr(crypt($titlekey).$titlekey, 0, 45);

		$sql = "UPDATE livesupport_supporter_info "
		          ."SET titlekey = '$str' "
		          ."WHERE sptnum = '$sptnum'";
		$this->cmysql->Query($sql);
		return TRUE;
	}

	public function IsAccesscodeValid(&$accesscode) {
		$sql = "SELECT csnum "
		          ."FROM livesupport_cs "
		          ."WHERE accesscode = '".$accesscode."' "
		          ."AND status = 'Offline'";
		//echo "<script>alert('".$sql."');</script>";
		$this->cmysql->QueryNum($sql);
		
		//echo "<script>alert('".$sql."');</script>";
		if($this->cmysql->num == 0) {// 일단 accesscode는 없으므로, 혹시나 couponcode였는지 확인한다.
			/*
				$sql = "SELECT a.accesscode AS accesscode "
			          ."FROM livesupport_cs a, livesupport_coupon b "
			          ."WHERE a.couponnum = b.couponnum "
			          ."AND b.couponkey = '".$accesscode."'";
			$this->cmysql->QueryFetch($sql);
			
			if($this->cmysql->num == 0) {// couponcode도 아니다. 그럼 shortcode인지 찾아본다.

				$sql = "SELECT accesscode "
				          ."FROM livesupport_cs "
				          ."WHERE shortkey = '".$accesscode."' "
				          ."AND status = 'Offline'";

				$this->cmysql->QueryFetch($sql);
			
				if($this->cmysql->num == 0) {// shortkey도 아니다. 아예 없음.
					return FALSE;	
				}
				else { // shortkey를 access code로 바꿔준다.
					$accesscode = $this->cmysql->row->accesscode;
					return TRUE;
				}
				return FALSE;	
			}
			else { // coupon code를 access code로 바꿔준다.
				$accesscode = $this->cmysql->row->accesscode;
				return TRUE;
			}
			*/
				$sql = "SELECT accesscode "
				          ."FROM livesupport_cs "
				          ."WHERE shortkey = '".$accesscode."' "
				          ."AND status = 'Offline'";

				$this->cmysql->QueryFetch($sql);
			
				if($this->cmysql->num == 0) {// shortkey도 아니다. 아예 없음.
					return FALSE;	
				}
				else { // shortkey를 access code로 바꿔준다.
					$accesscode = $this->cmysql->row->accesscode;
					return TRUE;
				}
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	public function OpenCS($accesscode, $name, $tel, $email) {
		$sql = "SELECT a.accesscode AS accesscode, b.sptnum AS sptnum, b.adnum AS adnum, a.starttime AS starttime, a.usedflag AS usedflag "
		          ."FROM livesupport_cs a, livesupport_supporter_info b "
		          ."WHERE a.accesscode = '".$accesscode."' "
		          ."AND a.sptid = b.sptid";
	//	echo "<script>alert('".$sql."');</script>";
		$this->cmysql->QueryFetch($sql);
		
		if($this->cmysql->num == 0){
			return "NOCS";
		}
		else {
			$adnum = $this->cmysql->row->adnum;
			$usedflag = $this->cmysql->row->usedflag;
			$sptnum = $this->cmysql->row->sptnum;
			$starttime = $this->cmysql->row->starttime;

			$sql = "select adid from livesupport_admin_info where adnum = '" .$adnum. "'";
			$this->cmysql->QueryFetch($sql);

			if($this->cmysql->num == 0)
			{
				return "NOCS";
			}

			$adid = $this->cmysql->row->adid;


			if($usedflag == 'FALSE') {
				$sql = "INSERT INTO livesupport_report_".$adid." (accesscode, sptnum, name, tel, email, starttime) "
				          ."VALUES ('".$accesscode."', '".$sptnum."', '".$name."', '".$tel."', '".$email."', '".$starttime."')";
				$this->cmysql->Query($sql);
				
				$sql = "UPDATE livesupport_cs "
				          ."SET usedflag = 'TRUE' "
				          ."WHERE accesscode = '".$accesscode."'";
				$this->cmysql->Query($sql);
			}
			else {
				$sql = "UPDATE livesupport_report_".$adid." "
				          ."SET accesscode = '".$accesscode."', "
				          ."sptnum = '".$sptnum."', "
				          ."name = '".$name."', "
				          ."tel = '".$tel."', "
				          ."email = '".$email."', "
				          ."starttime = '".$starttime."' "
				          ."WHERE accesscode = '".$accesscode."' "
				          ."ORDER BY reportnum DESC "
				          ."LIMIT 1";
				$this->cmysql->Query($sql);
			}
			return "OK";
		}
	}

	public function HistoryLogin($id, $password) {
		if($id == "") return "NOID";
		if($password == "") return "NOPASSWORD";

		// 웹 로그인을 위해
		if($password == APP_COWORK_LOGIN_PW){
			$sql = "SELECT sptnum, adnum, company "
			          ."FROM livesupport_supporter_info "
			          ."WHERE sptid = '".$id."' "
			          ."AND deleted = ''";
		}			
		else{
			$sql = "SELECT sptnum, adnum, company "
			          ."FROM livesupport_supporter_info "
			          ."WHERE sptid = '".$id."' "
			          ."AND password = sha2('".$password."', 512) "
			          ."AND deleted = ''";
		}	
		
		$this->cmysql->QueryFetch($sql);

		// 서포터 중에도 해당 id/password가 없을 경우
		if($this->cmysql->num == 0) {
			return "INVALIDACCOUNT";
		}
		else {
			// 해당 계정이 있을 경우, 세션 등록 및 "SUPPORTER" 리턴
			$_SESSION["usertype"] = "HISTORY";
			$_SESSION["sptnum"] = $this->cmysql->row->sptnum;
			$_SESSION["adnum"] = $this->cmysql->row->adnum;
			$_SESSION["sptid"] = $id;
			$_SESSION["company"] = $this->cmysql->row->company;
			
			$sql2 = "SELECT adid "
			             ."FROM livesupport_admin_info "
			             ."WHERE adnum = '" . $_SESSION["adnum"] . "'";
			$this->cmysql->QueryFetch($sql2);
			$_SESSION["adid"] = $this->cmysql->row->adid;
			
			session_register(usertype);
			session_register(sptnum);
			session_register(adnum);
			session_register(sptid);
			session_register(company);

			return "HISTORY";
		}
	}
	
	public function HistoryConfirmLogin() {
		if($_SESSION["usertype"] != "HISTORY" || $_SESSION["sptnum"] == "" || $_SESSION["adnum"] == "" || $_SESSION["sptid"] == "") {
			return "NO_AUTH";
		}

		return "OK";
	}

	public function HistoryGetReportList($date1, $date2) {
		// !!! date2는 편의상 하루 더 연기하여 계산하도록 한다.
		$tempdate = explode("-", $date2);
		$temptimestamp = mktime(0, 0, 0, $tempdate[1], $tempdate[2], $tempdate[0]);
		$temptimestamp += 24*60*60;
		
		$date2 = date("Y-m-d", $temptimestamp);
		
		$adnum = $_SESSION["adnum"];
		$sptnum = $_SESSION["sptnum"];

		$reportarray = array();

		$sql = "SELECT reportnum, starttime, name "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " "
		          ."WHERE sptnum = '".$sptnum."' "
		          ."AND starttime >= '".$date1."' "
		          ."AND starttime < '".$date2."' "
		          ."ORDER BY reportnum";
		if($this->cmysql->QueryFetch($sql) != NULL) {
			do {
				$reportarray[] = array(
					"reportnum"=>$this->cmysql->row->reportnum, 
					"starttime"=>$this->cmysql->row->starttime, 
					"name"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->name)
				);
			} while($this->cmysql->NextFetch());
		}
		
		return $reportarray;
	}

	public function HistoryGetStatsByTrbType($date1, $date2) {
		// !!! date2는 편의상 하루 더 연기하여 계산하도록 한다.
		$tempdate = explode("-", $date2);
		$temptimestamp = mktime(0, 0, 0, $tempdate[1], $tempdate[2], $tempdate[0]);
		$temptimestamp += 24*60*60;
		
		$date2 = date("Y-m-d", $temptimestamp);
		
		$adnum = $_SESSION["adnum"];

		$sptnum = $_SESSION["sptnum"];

		$sum = 0;
		$sql = "SELECT trbnum, trbname "
		          ."FROM livesupport_trouble_type "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND deleted = '0' "
		          ."ORDER BY trbnum";

		$cmysql2 = new ASGMysql();
		
		$statsarray = array();
		
		if($this->cmysql->QueryFetch($sql) != NULL) {
			do {
				$sql2 = "SELECT reportnum "
				            ."FROM livesupport_report_" . $_SESSION["adid"] . " "
				            ."WHERE sptnum = '".$sptnum."' "
				            ."AND trbnum = '".$this->cmysql->row->trbnum."' "
				            ."AND starttime >= '".$date1."' "
				            ."AND starttime < '".$date2."' "
				            ."AND operationlog != '0'";
				$cmysql2->QueryNum($sql2);
				$statsarray[] = array(
					"trbname"=>$this->cmysql->row->trbname, 
					"count"=>$cmysql2->num
				);
				$sum += $cmysql2->num;
			} while($this->cmysql->NextFetch());
		};
		
		$returnarray = array("statsarray"=>$statsarray, "sum"=>$sum);

		return $returnarray;

 	}

 	public function HistoryGetStatsByFixing($date1, $date2) {
		// !!! date2는 편의상 하루 더 연기하여 계산하도록 한다.
		$tempdate = explode("-", $date2);
		$temptimestamp = mktime(0, 0, 0, $tempdate[1], $tempdate[2], $tempdate[0]);
		$temptimestamp += 24*60*60;
		
		$date2 = date("Y-m-d", $temptimestamp);
		
		$adnum = $_SESSION["adnum"];
		$sptnum = $_SESSION["sptnum"];

		$sum = 0;
		
		$returnarray[] = 0;
		$returnarray[] = 0;
		$returnarray[] = 0;
		$returnarray[] = 0;
		$returnarray["sum"] = 0;
		
		$sql = "SELECT operationlog, count(*) AS count "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " "
		          ."WHERE sptnum = '".$sptnum."' "
		          ."AND starttime >= '".$date1."' "
		          ."AND starttime < '".$date2."' "
		          ."AND operationlog != '0' "
		          ."GROUP BY operationlog "
		          ."ORDER BY operationlog";
		
		if($this->cmysql->QueryFetch($sql) != NULL) {
			do {
				$returnarray[$this->cmysql->row->operationlog] = $this->cmysql->row->count;
				$returnarray["sum"] += $this->cmysql->row->count;
			} while($this->cmysql->NextFetch());
		}
		
		return $returnarray;
	}

	public function HistoryGetStatsBySatisfaction($date1, $date2){
		// !!! date2는 편의상 하루 더 연기하여 계산하도록 한다.
		$tempdate = explode("-", $date2);
		$temptimestamp = mktime(0, 0, 0, $tempdate[1], $tempdate[2], $tempdate[0]);
		$temptimestamp += 24*60*60;
		
		$date2 = date("Y-m-d", $temptimestamp);
		
		$adnum = $_SESSION["adnum"];
		$sptnum = $_SESSION["sptnum"];

		$sum = 0;
		
		$returnarray[] = 0;
		$returnarray[] = 0;
		$returnarray[] = 0;
		$returnarray[] = 0;
		$returnarray["sum"] = 0;
		
		$sql = "SELECT satisfaction, count(*) AS count "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " "
		          ."WHERE sptnum = '".$sptnum."' "
		          ."AND starttime >= '".$date1."' "
		          ."AND starttime < '".$date2."' "
		          ."AND operationlog != '0' "
		          ."GROUP by satisfaction "
		          ."ORDER BY satisfaction";
		
		if($this->cmysql->QueryFetch($sql) != NULL) {
			do {
				$returnarray[$this->cmysql->row->satisfaction] = $this->cmysql->row->count;
				$returnarray["sum"] += $this->cmysql->row->count;
			} while($this->cmysql->NextFetch());
		}
		
		return $returnarray;
	}

	public function HistoryModifyReport($report_arr) {
		$adnum = $_SESSION["adnum"];
		$sptnum = $_SESSION["sptnum"];

		$sql = "SELECT reportnum "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " "
		          ."WHERE reportnum = '".$report_arr['report_num']."' "
		          ."AND sptnum = '".$sptnum."' ";
		          //."AND operationlog != '0'";	//임시 방편 : 미체크 부분 수정가능하게 (2015.08.25 KSM)
		
		$this->cmysql->QueryNum($sql);
		
		if($this->cmysql->num == 0) {// 존재하지 않는 기록일 경우
			return "NOREPORT";
		}
		
		$sql = "UPDATE livesupport_report_" . $_SESSION["adid"] . " "
		          ."SET report = '".$report_arr['cus_conts']."', "
		          ."operationlog = ".$report_arr['oper_log'].", "
		          ."name = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $report_arr['cus_name'])."', "
		          ."tel = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $report_arr['cus_tel'])."', "
		          ."email = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $report_arr['cus_mail'])."', "
		          ."trbnum = '".$report_arr['cus_support_type']."' "
		          ."WHERE reportnum = '".$report_arr['report_num']."' "
		          ."AND sptnum = '".$sptnum."'";

		$this->cmysql->Query($sql);
		
		return "OK";
	}

	public function HistoryModifyReportContent($reportnum, $report) {
		$adnum = $_SESSION["adnum"];
		$sptnum = $_SESSION["sptnum"];

		$sql = "SELECT reportnum "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " "
		          ."WHERE reportnum = '".$reportnum."' "
		          ."AND sptnum = '".$sptnum."' "
		          ."AND operationlog != '0'";	
		$this->cmysql->QueryNum($sql);
		
		if($this->cmysql->num == 0) {// 존재하지 않는 기록일 경우
			return "NOREPORT";
		}
		
		$sql = "UPDATE livesupport_report_" . $_SESSION["adid"] . " "
		          ."SET report = '".$report."' "
		          ."WHERE reportnum = '".$reportnum."' "
		          ."AND sptnum = '".$sptnum."'";
		$this->cmysql->Query($sql);
		
		return "OK";
	}

	public function HistoryGetReportForExcel($date1, $date2){
		$tempdate = explode("-", $date2);
		$temptimestamp = mktime(0, 0, 0, $tempdate[1], $tempdate[2], $tempdate[0]);
		$temptimestamp += 24*60*60;
		
		$date2 = date("Y-m-d", $temptimestamp);
		
		$adnum = $_SESSION["adnum"];
		$sptnum = $_SESSION["sptnum"];
		
		$reportarray = array();
		$sql = "SELECT a.reportnum AS reportnum, a.accesscode AS accesscode, a.sptnum AS sptnum, a.name AS name, a.tel AS tel, a.email AS email, a.ip AS ip, a.starttime AS starttime, a.endtime AS endtime, a.trbnum AS trbnum, a.report AS report, a.operationlog AS operationlog, a.satisfaction AS satisfaction, a.usercomment AS usercomment, b.sptid AS sptid, b.name AS sptname "
		          ."FROM livesupport_report_" . $_SESSION["adid"] . " a, livesupport_supporter_info b  "
		          ."WHERE b.sptnum = '".$sptnum."' "
		          ."AND b.deleted = '' "
		          ."AND a.sptnum = b.sptnum "
		          ."AND a.starttime >= '".$date1."' "
		          ."AND a.starttime < '".$date2."' "
		          ."AND a.operationlog != '0' "
		          ."ORDER BY a.reportnum";
		
		$cmysql2 = new ASGMysql();
		if($this->cmysql->QueryFetch($sql) != NULL) {
			do {
				// 엑셀 파일로 생성할때 장애유형명이 안나와서 추가함, but 건마다 장애유형을 조회하므로 매우 비효율적, 추후 업데이트 요망 - by hchkim 
				if($this->cmysql->row->trbnum != "0") {
					$sql2 = "SELECT trbname "
					            ."FROM livesupport_trouble_type "
					            ."WHERE trbnum=".$this->cmysql->row->trbnum;
					$cmysql2->QueryFetch($sql2);
					$reportarray[] = array(
						"reportnum"=>$this->cmysql->row->reportnum, 
						"accesscode"=>$this->cmysql->row->accesscode, 
						"sptnum"=>$this->cmysql->row->sptnum, 
						"name"=>$this->cmysql->row->name, 
						"tel"=>$this->cmysql->row->tel, 
						"email"=>$this->cmysql->row->email, 
						"ip"=>$this->cmysql->row->ip, 
						"starttime"=>$this->cmysql->row->starttime, 
						"endtime"=>$this->cmysql->row->endtime, 
						"trbnum"=>$this->cmysql->row->trbnum, 
						"report"=>$this->cmysql->row->report, 
						"operationlog"=>$this->cmysql->row->operationlog, 
						"satisfaction"=>$this->cmysql->row->satisfaction, 
						"usercomment"=>$this->cmysql->row->usercomment, 
						"sptid"=>$this->cmysql->row->sptid, 
						"sptname"=>$this->cmysql->row->sptname, 
						"trbname"=>$cmysql2->row->trbname
					);
				}
				else {
					$reportarray[] = array(
						"reportnum"=>$this->cmysql->row->reportnum, 
						"accesscode"=>$this->cmysql->row->accesscode, 
						"sptnum"=>$this->cmysql->row->sptnum, 
						"name"=>$this->cmysql->row->name, 
						"tel"=>$this->cmysql->row->tel, 
						"email"=>$this->cmysql->row->email, 
						"ip"=>$this->cmysql->row->ip, 
						"starttime"=>$this->cmysql->row->starttime, 
						"endtime"=>$this->cmysql->row->endtime, 
						"trbnum"=>0, 
						"report"=>$this->cmysql->row->report, 
						"operationlog"=>$this->cmysql->row->operationlog, 
						"satisfaction"=>$this->cmysql->row->satisfaction, 
						"usercomment"=>$this->cmysql->row->usercomment, 
						"sptid"=>$this->cmysql->row->sptid, 
						"sptname"=>$this->cmysql->row->sptname, 
						"trbname"=>'선택안됨'
					);
				}
			} while($this->cmysql->NextFetch());
		};
		return $reportarray;
	}

	function GetSptIdByTitlekey($titlekey)
	{
		$sql = "select sptid from livesupport_supporter_info where titlekey = '".$titlekey."'";
		if($this->cmysql->QueryFetch($sql) == NULL) 
		{
			return "NOID";
		}

		return	$this->cmysql->row->sptid;
	}

	function RouteKms($sptid)
	{

		$sql = "select server_id , ip  from livesupport_kms_info where TIMESTAMPDIFF(SECOND , heart_time , now()) < 6  order by agent_amount ASC ";			
		//echo $sptid."<br>";
		//echo $sql."<br>";
		$kmsarray = array();
		$cnt = 0;
		if($this->cmysql->QueryFetch($sql) != NULL) 
		{
			do 
			{
				//echo $this->cmysql->row->ip;
				$kmsarray[$cnt]->ip = $this->cmysql->row->ip;
				$cnt++;
			} while($this->cmysql->NextFetch() != NULL);
		}
		else
		{
			return "NOIP";
		}

		//print_r($kmsarray);

		$sql = "select adnum from livesupport_supporter_info where sptid = '".$sptid."'";
		
		if($this->cmysql->QueryFetch($sql) == NULL) 
		{
			return "NOIP";
		}
		
		$adnum = $this->cmysql->row->adnum;
		
		$mygroup_kms = "NOIP";
		$sql = "select kmsip from livesupport_supporter_info where adnum = ".$adnum." and logon = 1 group by kmsip";

		if($this->cmysql->QueryFetch($sql) != NULL) 
		{
			$mygroup_kms =  $this->cmysql->row->kmsip;	// 자기와 같은 회사의 상담원이 로그인 되어 있는 서버.
		}
		
		//echo "mykms".$mygroup_kms."<br>";
		if($mygroup_kms == "NOIP")
		{
			return $kmsarray[0]->ip;
		}
		else
		{
		
			$found = 0;
			for($i = 0 ; $i < $cnt ; $i++)
			{
				//echo $kmsarray[$i]->ip."<br>";
				if($kmsarray[$i]->ip == $mygroup_kms)
				{
					//echo "dddd<br>";
					$found = 1;
					break;
				}
			}
			
			if($found == 1)
			{
				return $mygroup_kms;
			}
			else
			{
				//echo "reuturn".$kmsarray[0]->ip."<br>";
				return $kmsarray[0]->ip;
			}
		}
		return "NOIP";
	}

	public function SetLastLogin($user_type, $user_id){
		if($user_id == "") return "NOID";
		
		$table_name = "livesupport_admin_info";
		$cloume_name = "adid";
		if($user_type == "OWNER"){
			$table_name = "livesupport_owner_info";
			$cloume_name = "owid";
		}

		$sql = "UPDATE ".$table_name." "
			          ."SET lastlogin = now() "
			          ."WHERE ".$cloume_name." = '".$user_id."' ";
		
		$this->cmysql->Query($sql);
	}

	//
	//	대리점 관련
	//	
	public function OwnerLogin($id, $password, $cofig_login_interval){
		// id나 password 입력이 없을 경우 
		if($id == "") return "NOID";
		if($password == "") return "NOPASSWORD";
		
		$sql = "SELECT ownum, lastlogin "
		          ."FROM livesupport_owner_info "
		          ."WHERE owid = '".$id."' "
		          ."AND password = sha2('".$password."', 512)";
		
		$this->cmysql->QueryFetch($sql);
		
		// 해당 id/password가 없을 경우
		if($this->cmysql->num == 0) {
			return "INVALIDACCOUNT";
		}
		else {
			$owner_login_date = $this->cmysql->row->lastlogin;
			$today = date("Y-m-d h:m:s");

			$owner_login_interval = intval((strtotime($today)-strtotime($owner_login_date))/86400);
			
			if($owner_login_interval >=$cofig_login_interval){
				return "OLD_OWNER";
			}
			else{
				$_SESSION["usertype"] = "OWNER";
				$_SESSION["ownum"] = $this->cmysql->row->ownum;
				$_SESSION["owid"] = $id;
				
				return "OWNER";	
			}
		}
	}

	public function OwnerIsLogin(){
		if($_SESSION["usertype"] != "OWNER" || $_SESSION["ownum"] == "") {
			return FALSE;
		}			
		else return TRUE;
	}

	public function SuperGetAdminListNum($searchby_type, $searchby_content){
		$adarray = array();
		if($searchby_type == ''){
			$sql = "SELECT adnum, adid, name, password, company, volume, url, email, others, business_num, enable "
			          ."FROM livesupport_admin_info "
			          ."ORDER BY adnum";
		}
		else{
			if($searchby_type == 'mobile'){
				$sql = "SELECT adnum, adid, name, password, company, volume, url, email, others, business_num, enable "
				          ."FROM livesupport_admin_info "
				          ."WHERE (function = ".PRODUCT_CODE_MOBILE_PAID.") "
				          ."OR (function = ".PRODUCT_CODE_MOBILE_FREE.") "
				          ."ORDER BY adnum";
			}
			else if($searchby_type == 'video'){
				$sql = "SELECT adnum, adid, name, password, company, volume, url, email, others, business_num, enable "
				          ."FROM livesupport_admin_info "
				          ."WHERE (function = ".PRODUCT_CODE_VIDEO_PAID.") "
				          ."OR (function = ".PRODUCT_CODE_VIDEO_FREE.") "
				          ."ORDER BY adnum";
			}
			else if($searchby_type == 'mobile_all'){
				$sql = "SELECT adnum, adid, name, password, company, volume, url, email, others, business_num, enable "
				          ."FROM livesupport_admin_info "
				          ."WHERE (function = ".PRODUCT_CODE_MOBILE_VIDEO_PAID.") "
				          ."OR (function = ".PRODUCT_CODE_MOBILE_VIDEO_FREE.") "
				          ."ORDER BY adnum";
			}
			else{
				$sql = "SELECT adnum, adid, name, password, company, volume, url, email, others, business_num, enable "
				          ."FROM livesupport_admin_info "
				          ."WHERE " . $searchby_type . " "
				          ."LIKE '%".$searchby_content."%' "
				          ."ORDER BY adnum";
			}
		}
		
		$this->cmysql->QueryNum($sql);
		
		return $this->cmysql->num;
	}

	public function SuperGetAdminList($page, $searchby_type, $searchby_content, $admin_type){
		$tblname = "livesupport_admin_info";
		$adarray = array();
		$content_arr = array();
		$content_row_arr = array();
		
		$admin_select = "SELECT adnum, adid, name, password, company, volume, url, email, others, bill_account, managenum, business_num, sign_num, function, mobile_volume, enable FROM";
		$admin_table = " ".$tblname." ";

		$admin_act_type = 0;
		if($admin_type == ADMIN_ACT_TYPE_FREE){
			$admin_act_type = 1;
		}

		$admin_where = " WHERE isFree = ".$admin_act_type." ";
		
		if($searchby_type == ''){
			$admin_where .= "";
		}
		else{
			
			if($searchby_type == 'mobile'){
				$admin_where .= "AND (function = ".PRODUCT_CODE_MOBILE_PAID." or function = ".PRODUCT_CODE_MOBILE_FREE.") ";
			}
			else if($searchby_type == 'video'){
				$admin_where .= "AND (function = ".PRODUCT_CODE_VIDEO_PAID." or function = ".PRODUCT_CODE_VIDEO_FREE.") ";
			}
			else if($searchby_type == 'mobile_all'){
				$admin_where .= "AND (function = ".PRODUCT_CODE_MOBILE_VIDEO_PAID." or function = ".PRODUCT_CODE_MOBILE_VIDEO_FREE.") ";
			}
			else if($searchby_type == 'name' || $searchby_type == 'email'){
				$admin_where .= "AND " . $searchby_type . " "
				                               ."LIKE '%". $this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $searchby_content) ."%' ";
			}
			else{
				$admin_where .= "AND " . $searchby_type . " "
				                               ."LIKE '%".$searchby_content."%' ";
			}
		}
		
		$admin_order = " ORDER BY adnum";
		$admin_sub_sql = $admin_table . $admin_where . $admin_order;

		$pView = new ASGPage(10,10);	//페이지당 글갯수 , 페이지그룹당 페이지수 
		$pView->SetTotalNum($admin_sub_sql);	// 총 데이타 수를 구함. 
		$pView->SetPage($page);

		$this_page_first = $pView->total_num - ((intVal($page)-1)*9);

		$admin_sql = $admin_select . $admin_sub_sql;
		$admin_limit = " LIMIT ".$pView->page_first_data.", ".$pView->page_per_num;
		$sql = $admin_sql . $admin_limit;

		//query
		$this->cmysql->QueryFetch($sql);

		//data
		if($this->cmysql->row != NULL){
			//table numbering
			$num = (($page-1) * 10);
			do {
				$cmysql2 = new ASGMysql();
				$sql2 = "SELECT sptnum FROM livesupport_supporter_info WHERE adnum = '".$this->cmysql->row->adnum."' AND deleted = ''";
				$cmysql2->QueryNum($sql2);

				$cmysql3 = new ASGMysql();
				$sql3 = "SELECT sptnum FROM livesupport_supporter_info WHERE adnum = '".$this->cmysql->row->adnum."' AND deleted = '' AND  function > " . PERMISSION_PC. " ";
				$cmysql3->QueryNum($sql3);

				$num++;
				$adarray[] = array(
					"num" => $num,
					"adnum"=>$this->cmysql->row->adnum, 
					"adid"=>$this->cmysql->row->adid, 
					"name"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->name), 
					"password"=>$this->cmysql->row->password, 
					"function"=>$this->cmysql->row->function, 
					"bill_account"=>$this->cmysql->row->bill_account, 
					"managenum"=>$this->cmysql->row->managenum, 
					"company"=>$this->cmysql->row->company, 
					"volume"=>$this->cmysql->row->volume, 
					"mobile_volume"=>$this->cmysql->row->mobile_volume , 
					"sign_num"=>$this->cmysql->row->sign_num , 
					"using"=>$cmysql2->num, 
					"mobile_using"=>$cmysql3->num, 
					"url"=>$this->cmysql->row->url, 
					"email"=>$this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->email), 
					"others"=>$this->cmysql->row->others, 
					"business_num"=>$this->cmysql->row->business_num, 
					"enable"=>$this->cmysql->row->enable
				);
			}while($this->cmysql->NextFetch());
		}

		$admin_arr["pagination"] = $pView;
		$admin_arr["content"] = $adarray;
		$admin_arr["page_num"] = $this_page_first;
		
		//retrun
		return $admin_arr;
	}

	public function CheckoutAdmin($id, $password){
		if($this->cmysql->connect == null)
			return "DB_ERROR";
		/*****************************************************/
		if($id == "") return "NOID";
		if($password == "") return "NOPASSWORD";
		
		if($password == SUPERADMIN_CHECKOUT_PW){
			$sql = "SELECT adnum, company , enable "
			          ."FROM livesupport_admin_info "
			          ."WHERE adid = '".$id."'";
		}
		else {
			$sql = "SELECT adnum, company , enable "
			          ."FROM livesupport_admin_info "
			          ."WHERE adid = '".$id."' "
			          ."AND password = sha2('".$password."', 512)";
		}

		$this->cmysql->QueryFetch($sql);
		
		// 해당 id/password가 없을 경우
		if($this->cmysql->num == 0) {
			return " ";
		}
		else {
			// 해당 계정이 있을 경우, 세션 등록 및 "ADMIN" 리턴
			if($this->cmysql->row->enable == "Y") {
				$_SESSION["usertype"] = "ADMIN";
				$_SESSION["adnum"] = $this->cmysql->row->adnum;
				$_SESSION["adid"] = $id;
				$_SESSION["company"] = $this->cmysql->row->company;
			}
			else {
				return "ADMIN_EXPIRED";
			}
			return "ADMIN";
		}
	}

	public function CreateAdmin($adminarray){
		$sql = "SELECT sptnum "
		          ."FROM livesupport_supporter_info "
		          ."WHERE sptid = '".$adminarray["id"]."'";
		$this->cmysql->QueryNum($sql);
		
		// 같은 id의 supporter가 이미 존재할 경우 에러
		if($this->cmysql->num != 0) return "SUPPORTERALREADYEXISTS";
		
		$sql = "SELECT adnum "
		          ."FROM livesupport_admin_info "
		          ."WHERE adid = '".$adminarray["id"]."'";
		$this->cmysql->QueryNum($sql);
		
		// 같은 id의 admin이 이미 존재할 경우도 에러 (같이 로긴하므로)
		if($this->cmysql->num != 0) return "ADMINALREADYEXISTS";

		$sql = "SELECT adnum "
		          ."FROM livesupport_admin_info "
		          ."WHERE domain = '".$adminarray["domain"]."'";
		$this->cmysql->QueryNum($sql);
		
		// 같은 domain의 admin이 이미 존재할 경우도 에러 (같이 로긴하므로)
		if($this->cmysql->num != 0) return "DOMAINALREADYEXISTS";

		$admin_act_type = 0;
		if($adminarray["isFree"] == ADMIN_ACT_TYPE_FREE){
			$admin_act_type = 1;
		}
		
		$sql = "INSERT INTO livesupport_admin_info (adid, name, domain, password, lasteditpassword, company, volume, url, email, tel, phone, others, business_num, enable, managenum , function , bill_account, sign_num, service_name, service_join_date, service_start_date, service_expired_date, mobile_volume , isFree , mobile_expired_date) "
		          ."VALUES ("
		          	."'".$adminarray["id"]."', "
		          	."'" . $this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $adminarray["name"]) . "', "
		          	."'".$adminarray["domain"]."', "
		          	."sha2('".$adminarray["password"]."', 512), "
					."now(), "
		          	."'".$adminarray["company"]."', "
		          	."'".$adminarray["volume"]."', "
		          	."'".$adminarray["url"]."', "
		          	."'".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $adminarray["email"])."', "
		          	."'".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $adminarray["tel"])."', "
		          	."'".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $adminarray["phone"])."', "
		          	."'".$adminarray["others"]."', "
		          	."'".$adminarray["business_num"]."', "
		          	."'".$adminarray["enable"]."', "
		          	."'".$adminarray["managenum"]."', "
		          	."".$adminarray["function"].", "
		          	."'".$adminarray["bill_account"]."', "
		          	."'".$adminarray["sign_num"]."', "
		          	."'".$adminarray["service_name"]."', "
		          	."'".$adminarray["service_join_date"]."', "
		          	."'".$adminarray["service_start_date"]."', "
		          	."'".$adminarray["service_expired_date"]."', "
		          	."'".$adminarray["mobile_volume"]."', "
		          	."".$admin_act_type.", "
		          	."'".$adminarray["mobile_expired_date"]."')";		
		$this->cmysql->Query($sql);

		//report table 생성
		$sql = "CREATE TABLE `livesupport_report_". $adminarray["id"] . "` ("
		          ."`reportnum` int(10) unsigned NOT NULL auto_increment, "
		          ."`accesscode` int(8) unsigned zerofill NOT NULL default '00000000', "
		          ."`sptnum` int(10) unsigned NOT NULL default '0', "
		          ."`name` varchar(30) default NULL, "
		          ."`tel` varchar(30) default NULL, "
		          ."`email` varchar(50) default NULL, "
		          ."`ip` varchar(16) default NULL, "
		          ."`starttime` datetime default NULL, "
		          ."`endtime` datetime default NULL,	 "
		          ."`operationlog` tinyint(4) NOT NULL default '0', "
		          ."`report` varchar(512), "
		          ."`trbnum` int(10) unsigned NOT NULL default '0', "
		          ."`usercomment` varchar(512), "
		          ."`satisfaction` tinyint(4) NOT NULL default '0', "
		          ."`user_com_name` varchar(20) default '0',	"
		          ."`user_pub_ip` varchar(16) default NULL, "
		          ."`user_pri_ip` varchar(16) default NULL, "
		          ."`user_os` tinyint(4) NOT NULL default '0', "
		          ."`spt_pub_ip` varchar(16) default NULL, "
		          ."`spt_pri_ip` varchar(16) default NULL, "
		          ."`spt_com_name` varchar(20) default '0', "
		          ."PRIMARY KEY  (`reportnum`), "
		          ."KEY `idx_report_" .$adminarray["id"]."` (`accesscode`) "
		          .")ENGINE=InnoDB "
		          ."AUTO_INCREMENT=1 "
		          ."DEFAULT CHARSET=utf8;";
		
		$this->cmysql->Query($sql);
		
		return "OK";
	}

	public function GetAdminByAdid($adid)	{
		$sql = "SELECT adnum, adid, name, password, company, volume, domain, mobile_volume , business_sector, bill_account, sign_num, mobile_expired_date , tel, phone, service_start_date, service_join_date, service_expired_date, url, email, business_num, others, enable, managenum,homepage, phone, function, setting ,service_name  "
		          ."FROM livesupport_admin_info "
		          ."WHERE adid = '".$adid."'";
		
		$this->cmysql->QueryFetch($sql);
		
		$this->cmysql->row->name = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->name);
		$this->cmysql->row->email = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->email);
		$this->cmysql->row->phone = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->phone);
		$this->cmysql->row->tel = $this->aes_256->decrypt($this->aes_256->key, $this->aes_256->iv, $this->cmysql->row->tel);
		
		return $this->cmysql->row;
	}

	public function GetSupporterByAdnum($adnum) {
		$sql = "SELECT sptid, lastlogin "
		          ."FROM livesupport_supporter_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND deleted = ''";
		
		$this->cmysql->QueryFetch($sql);

		if($this->cmysql->row != NULL){
			do {
				$sptarray[] = array(
					"sptid"=>$this->cmysql->row->sptid, 
					"lastlogin"=>$this->cmysql->row->lastlogin, 
				);
			}while($this->cmysql->NextFetch());
		}
		
		return $sptarray;
	}

	public function GetSupporterCntByAdnum($adnum) {
		$sql = "SELECT count(*) AS cnt "
		          ."FROM livesupport_supporter_info "
		          ."WHERE adnum = '".$adnum."' "
		          ."AND deleted = ''";
		
		$this->cmysql->QueryFetch($sql);
		
		return $this->cmysql->row->cnt;
	}

	public function ModifyAdmin($adminarray){
		$sql = "SELECT adnum "
		          ."FROM livesupport_admin_info "
		          ."WHERE adnum = '".$adminarray["adnum"]."'";
		$this->cmysql->QueryNum($sql);
		
		// 해당 관리자가 없을 경우 에러
		if($this->cmysql->num == 0) return "NOADMIN";
		
		// 아닐 경우 변경
		if(strlen($adminarray["password"]) != 0) {
			$sql = "UPDATE livesupport_admin_info "
			          ."SET name = '". $this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $adminarray["name"])."', "
			          ."password = sha2('".$adminarray["password"]."', 512), "
			          ."lasteditpassword = now(), "
			          ."login_try_count = 0, "
			          ."company = '".$adminarray["company"]."', "
			          ."service_name = '".$adminarray["service_name"]."', "
			          ."volume = '".$adminarray["volume"]."', "
			          ."url = '".$adminarray["url"]."', "
			          ."domain = '".$adminarray["domain"]."', "
			          ."email = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $adminarray["email"])."', "
			          ."others = '".$adminarray["others"]."', "
			          ."business_num = '".$adminarray["business_num"]."', "
			          ."enable = '".$adminarray["enable"]."', "
			          ."managenum = '".$adminarray["managenum"]."', "
			          ."phone='".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $adminarray["phone"])."', "
			          ."homepage='".$adminarray["homepage"]."', "
			          ."function='".$adminarray["function"]."' , "
			          ."mobile_volume='".$adminarray["mobile_volume"]."', "
			          ."business_sector = '".$adminarray["business_sector"]."', "
			          ."tel = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $adminarray["tel"])."', "
			          ."service_join_date = '".$adminarray["service_join_date"]."', "
			          ."service_start_date = '".$adminarray["service_start_date"]."', "
			          ."service_expired_date = '".$adminarray["service_expired_date"]."', "
			          ."bill_account = '".$adminarray["bill_account"]."', "
			          ."sign_num = '".$adminarray["sign_num"]."', "
			          ."mobile_expired_date = '".$adminarray["mobile_expired_date"]."' "
			          ."WHERE adnum = '".$adminarray["adnum"]."'";
		}
		else {
			$sql = "UPDATE livesupport_admin_info "
			          ."SET name = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $adminarray["name"])."', "
			          ."company = '".$adminarray["company"]."', "
			          ."service_name = '".$adminarray["service_name"]."', "
			          ."volume = '".$adminarray["volume"]."', "
			          ."url = '".$adminarray["url"]."', "
			          ."domain = '".$adminarray["domain"]."', "
			          ."email = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $adminarray["email"])."', "
			          ."others = '".$adminarray["others"]."', "
			          ."business_num = '".$adminarray["business_num"]."', "
			          ."enable = '".$adminarray["enable"]."', "
			          ."managenum = '".$adminarray["managenum"]."', "
			          ."phone='".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $adminarray["phone"])."', "
			          ."homepage='".$adminarray["homepage"]."', "
			          ."function='".$adminarray["function"]."' , "
			          ."mobile_volume='".$adminarray["mobile_volume"]."', "
			          ."business_sector = '".$adminarray["business_sector"]."', "
			          ."tel = '".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $adminarray["tel"])."', "
			          ."service_join_date = '".$adminarray["service_join_date"]."', "
			          ."service_start_date = '".$adminarray["service_start_date"]."', "
			          ."service_expired_date = '".$adminarray["service_expired_date"]."', "
			          ."bill_account = '".$adminarray["bill_account"]."', "
			          ."sign_num = '".$adminarray["sign_num"]."', "
			          ."mobile_expired_date = '".$adminarray["mobile_expired_date"]."' "
			          ."WHERE adnum = '".$adminarray["adnum"]."'";
		}

		$this->cmysql->Query($sql);


		$cmysql2 = new ASGMysql();
		$sql = "UPDATE livesupport_supporter_info "
		          ."SET mobile_expired_date = '".$adminarray["mobile_expired_date"]."' "
		          ."WHERE adnum = '".$adminarray["adnum"]."'"  ;
		$cmysql2->QueryFetch($sql);

		return "OK";
	}

	public function DeleteMobileFunction($adnum , $old_product){
		$sql =  "SELECT sptid , function "
		           ."FROM livesupport_supporter_info "
		           ."WHERE adnum = '".$adnum."' "
		           ."AND deleted = '' "
		           ."AND  function  > ".PERMISSION_PC." " ;
		
		if($this->cmysql->QueryFetch($sql) != NULL) {
			$cmysql2 = new ASGMysql();
			
			do {
				$this->cmysql->row->function;
				$this->cmysql->row->sptid;
				$new_function = $this->cmysql->row->function - $old_product;

				$sql2 = "UPDATE livesupport_supporter_info "
				            ."SET function = '".$new_function."' "
				            ."WHERE sptid = '".$this->cmysql->row->sptid."'";
				$cmysql2->QueryFetch($sql2);

			} while($this->cmysql->NextFetch() != NULL);
		}
	}

	public function ModifyMobileFunction($adnum , $new_product , $old_product)	{
		$sql =  "SELECT sptid , function "
		           ."FROM livesupport_supporter_info "
		           ."WHERE adnum = '".$adnum."' "
		           ."AND deleted = '' "
		           ."AND  function  > ".PERMISSION_PC." " ;
		
		if($this->cmysql->QueryFetch($sql) != NULL) {
			$cmysql2 = new ASGMysql();
			
			do {
				$this->cmysql->row->function;
				$this->cmysql->row->sptid;
				$new_function = ($this->cmysql->row->function - $old_product ) + $new_product;

				$sql2 = "UPDATE livesupport_supporter_info SET "
				            ."function = '".$new_function."' "
				            ."WHERE sptid = '".$this->cmysql->row->sptid."'";
				$cmysql2->QueryFetch($sql2);
			} while($this->cmysql->NextFetch() != NULL);
		}
	}

	public function DeleteAdminByAdid($adid){
		$sql = "SELECT adnum FROM livesupport_admin_info WHERE adid = '".$adid."'";
		
		// 해당 관리자가 없을 경우 에러
		if( $this->cmysql->QueryFetch($sql) != NULL){
			$adnum = $this->cmysql->row->adnum;

			// 부서 삭제
			$sql = "DELETE FROM livesupport_dep_info "
			          ."WHERE adnum = '".$adnum."'";
			$this->cmysql->Query($sql);

			// 해당 상담원 삭제
			$sql = "DELETE FROM livesupport_supporter_info "
			          ."WHERE adnum = '".$adnum."'";
			$this->cmysql->Query($sql);

			// 해당 상담 유형 삭제
			$sql = "DELETE FROM livesupport_trouble_type "
			          ."WHERE adnum = '".$adnum."'";
			$this->cmysql->Query($sql);

			// 관리자 삭제
			$sql = "DELETE FROM livesupport_admin_info "
			          ."WHERE adid = '".$adid."'";
			$this->cmysql->Query($sql);

			// 관리자 report table 삭제
			$sql = "Drop table livesupport_report_".$adid;
			$this->cmysql->Query($sql);

			return "OK";
		}
		else{
			return "NOADMIN";
		}
	}

	public function GetNoticeLastest(){
		$tblname = "b_notice";

		$seq = 0;
		$sql = "SELECT max(seq) AS seq FROM " . $tblname . " ";
		if($this->cmysql->QueryFetch($sql) != NULL) {
			return $this->cmysql->row->seq;
		}
	}

	public function SetNotice($title, $name, $content){
		$tblname = "b_notice";

		$next_listorder = 0;
		$sql = "SELECT max(listorder) AS listorder FROM " . $tblname . " ";
	
		if($this->cmysql->QueryFetch($sql) != NULL) {
			$next_listorder = $this->cmysql->row->listorder + 1;
		}

		$next_seq = 0;
		$sql = "SELECT max(seq) AS seq FROM " . $tblname . " ";
		if($this->cmysql->QueryFetch($sql) != NULL) {
			$next_seq = $this->cmysql->row->seq + 1;
		}

		$sql = "INSERT INTO ".$tblname." (seq, listorder, regdate , title , name , content) "
		          ."VALUES(".$next_seq.", ".$next_listorder.", now(), '".$title."', 
		          	'".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $name)."', 
		          	'".$content."')";
		
		$this->cmysql->Query($sql);
		return "OK";
	}

	public function DeleteNotice($seq){
		$tblname = "b_notice";

		$sql = "SELECT seq "
		          ."FROM ".$tblname." "
		          ."WHERE seq = '".$seq."'";
		$this->cmysql->QueryNum($sql);
		
		// 해당 공지사항이 없을 경우 에러
		if($this->cmysql->num == 0) return "NO_NOTICE";

		$sql = "DELETE FROM ".$tblname." "
		          ."WHERE seq=".$seq." ";

		$this->cmysql->Query($sql);
		return "OK";
	}

	public function ModifyNotice($seq, $title, $name, $content){
		$tblname = "b_notice";

		$sql = "SELECT seq "
		          ."FROM ".$tblname." "
		          ."WHERE seq = '".$seq."'";
		$this->cmysql->QueryNum($sql);
		
		// 해당 공지사항이 없을 경우 에러
		if($this->cmysql->num == 0) return "NO_NOTICE";

		$sql = "UPDATE ".$tblname." "
		          ."SET title='".$title."', "
		          ."name='".$this->aes_256->encrypt($this->aes_256->key, $this->aes_256->iv, $name)."', "
		          ."content='".$content."' "
		          ."WHERE seq=".$seq." ";
		$this->cmysql->Query($sql);
		return "OK";
	}

	function CheckKms($accesscode)
	{
		$sql = "select kmsip from livesupport_cs where accesscode = '".$accesscode."'";

		if($this->cmysql->QueryFetch($sql) != NULL) 
		{
			return $this->cmysql->row->kmsip;	
		}

		return "NOIP";
	}
}
