<?php
/**
 * Description
 * 	ASGConfig()				생성자. 설정 읽어오기
 * 	WriteConfig()				설정 내용 기록
 * 	
 * @ KSM | 2015.06.22 | CConfig.php modified	
 */
define (CONFIGURATION_FILE, dirname(__FILE__) . "neturo.conf");

class ASGConfig {
	# default language code
	VAR $DefaultLanguage;	# string

	# you need to redirect to HTTPS page?
	VAR $DoHTTPSRedirect;	# boolean

	# open popup notice window at main page?
	VAR $MainNoticePopup;	# boolean

	# we use recommender policy?
	VAR $AskRecommender;	# boolean

	# how many days you will extend validity of recommender
	VAR $AddToRecommender;	# integer

	# permit registration with same id if the old id was expired?
	VAR $OverwriteExpiredID;	# boolean

	# check Email string at user registration?
	VAR $DoEmailCheck;	# boolean

	# deny login of expired user?
	VAR $DenyExpiredUser;	# boolean

	# highest version of host
	VAR $HostVersion;	# string

	# highest version of viewer
	VAR $ViewerVersion;	# string

	# show expiration date to user?
	VAR $ShowExpireDate;	# boolean

	# support request email address for Korean user
	VAR $SupportEmailKR;	# string

	# support request email address for Japanese user
	VAR $SupportEmailJP;	# string

	# allowed networks for administrator login
	VAR $AdminAllowedNetwork;	# string

	VAR $CompanyName;	# string
	
	VAR $ProductName;	# string

	VAR $KoreanParticleCompanyRul;		# string
	
	VAR $KoreanParticleCompanyNun;		# string

	VAR $KoreanParticleCompanyGa;		# string
	
	VAR $KoreanParticleProductRul;		# string
	
	VAR $KoreanParticleProductNun;		# string

	VAR $KoreanParticleProductGa;		# string

	public function ASGConfig() {
		/* default values */

		$this->DefaultLanguage = "ko";
		$this->MainNoticePopup = FALSE;
		$this->DoHTTPSRedirect = TRUE;
		$this->AskRecommender = FALSE;
		$this->AddToRecommender = 0;
		$this->OverwriteExpiredID = FALSE;
		$this->DoEmailCheck = TRUE;
		$this->DenyExpiredUser = FALSE;
		$this->HostVersion = "1.0.2.3"; //customer
		$this->ViewerVersion = "1.0.4.2"; 
		$this->KoinoLoaderVersion = "2013,2,13,0";
		$this->ManualLauncherDir = "../_manualLauncher/ManualLauncher.exe";
		$this->ManualLauncher2Dir = "../_manualLauncher/ManualLauncher2.exe";
		$this->ShowExpireDate = FALSE;
		$this->SupportEmailKR = "anysupport@koino.net";
		$this->SupportEmailJP = "anysupport@koino.net";
		$this->AdminAllowedNetwork = "61.35.225.60";
		$this->CompanyName = "Evixar";
		$this->ProductName = "LiveSupport Corporate";
		$this->KoreanParticleCompanyRul = "를";
		$this->KoreanParticleCompanyNun = "는";
		$this->KoreanParticleCompanyGa = "가";
		$this->KoreanParticleProductRul = "를";
		$this->KoreanParticleProductNun = "는";
		$this->KoreanParticleProductGa = "가";
		$this->KmsAddress1 = "192.168.1.11";
		$this->KmsAddress2 = "192.168.1.11";
		$this->KmsAddress3 = "192.168.1.11";
		$this->DbAddress1 = "192.168.1.11";
		$this->DbAddress2 = "192.168.1.11";
		$this->DbAddress3 = "192.168.1.11";
		// 중복 로그인 (2012.08.27 두산 요청사항)
		$this->DuplicatePageMoveAdid = "koino;"
						 ."hntadmin;"
						 ."dsheavy;"
						 ."doosanad;"
						 ."doosanadmin;"
						 ."hntitadmin;"
						 ."yeramadmin;"
						 ."doosadmin;"
						 ."solaradmin;"
						 ."hiraadmin;"
						 ."canonadmin;"
						 ."cscadmin;"
						 ."kovasnet;";

		/* read configuration from file */
		if (!file_exists(CONFIGURATION_FILE)) {
			/* no configuration file */
			return;
		}

		$pairs = file(CONFIGURATION_FILE);

		for ($i = 0; $i < count($pairs); $i++) {
			$line = rtrim($pairs[$i]);
			$pair = explode("=", $line);

			switch ($pair[0]) {
			case ("DefaultLanguage"):
				$this->DefaultLanguage = $pair[1];
				switch ($this->DefaultLanguage) {
				case ("ko"):
				case ("ja"):
					break;
				default:
					$this->DefaultLanguage = "ko";
					break;
				}
				break;

			case ("MainNoticePopup"):
				if ($pair[1] == "yes") {
					$this->MainNoticePopup = TRUE;
				} else if ($pair[1] == "no") {
					$this->MainNoticePopup = FALSE;
				}
				break;

			case ("DoHTTPSRedirect"):
				if ($pair[1] == "yes") {
					$this->DoHTTPSRedirect = TRUE;
				} else if ($pair[1] == "no") {
					$this->DoHTTPSRedirect = FALSE;
				}
				break;

			case ("AskRecommender"):
				if ($pair[1] == "yes") {
					$this->AskRecommender = TRUE;
				} else if ($pair[1] == "no") {
					$this->AskRecommender = FALSE;
				}
				break;

			case ("AddToRecommender"):
				$this->AddToRecommender = intval($pair[1]);
				break;

			case ("OverwriteExpiredID"):
				if ($pair[1] == "yes") {
					$this->OverwriteExpiredID = TRUE;
				} else if ($pair[1] == "no") {
					$this->OverwriteExpiredID = FALSE;
				}
				break;

			case ("DoEmailCheck"):
				if ($pair[1] == "yes") {
					$this->DoEmailCheck = TRUE;
				} else if ($pair[1] == "no") {
					$this->DoEmailCheck = FALSE;
				}
				break;

			case ("DenyExpiredUser"):
				if ($pair[1] == "yes") {
					$this->DenyExpiredUser = TRUE;
				} else if ($pair[1] == "no") {
					$this->DenyExpiredUser = FALSE;
				}
				break;

			case ("HostVersion"):
				$this->HostVersion = $pair[1];
				break;

			case ("ViewerVersion"):
				$this->ViewerVersion = $pair[1];
				break;

			case ("ShowExpireDate"):
				if ($pair[1] == "yes") {
					$this->ShowExpireDate = TRUE;
				} else if ($pair[1] == "no") {
					$this->ShowExpireDate = FALSE;
				}
				break;

			case ("SupportEmailKR"):
				$this->SupportEmailKR = $pair[1];
				break;

			case ("SupportEmailJP"):
				$this->SupportEmailJP = $pair[1];
				break;

			case ("AdminAllowedNetwork"):
				$this->AdminAllowedNetwork = $pair[1];
				break;

			case ("CompanyName"):
				$this->CompanyName = $pair[1];
				break;

			case ("ProductName"):
				$this->ProductName = $pair[1];
				break;

			case ("KoreanParticleCompanyRul"):
				$this->KoreanParticleCompanyRul = $pair[1];
				break;

			case ("KoreanParticleCompanyNun"):
				$this->KoreanParticleCompanyNun = $pair[1];
				break;

			case ("KoreanParticleCompanyGa"):
				$this->KoreanParticleCompanyGa = $pair[1];
				break;

			case ("KoreanParticleProductRul"):
				$this->KoreanParticleProductRul = $pair[1];
				break;

			case ("KoreanParticleProductNun"):
				$this->KoreanParticleProductNun = $pair[1];
				break;

			case ("KoreanParticleProductGa"):
				$this->KoreanParticleProductGa = $pair[1];
				break;

			}				
		}				 
	}

	public function WriteConfig(){
		$fp = fopen(CONFIGURATION_FILE, "wt");
		if ($fp == NULL) {
			/* failed to open the file... shit_-_ */
			return;
		}

		if ($this->DefaultLanguage != "ko" &&
		$this->DefaultLanguage != "ja")
			$this->DefaultLanguage = "ko";
		if ($this->AdminAllowedNetwork == "")
			$this->AdminAllowedNetwork = "143.248.245";

		fwrite($fp, "DefaultLanguage=" . $this->DefaultLanguage . "\n");
		fwrite($fp, "MainNoticePopup=" . ($this->MainNoticePopup ? "yes" : "no") . "\n");
		fwrite($fp, "DoHTTPSRedirect=" . ($this->DoHTTPSRedirect ? "yes" : "no") . "\n");
		fwrite($fp, "AskRecommender=" . ($this->AskRecommender ? "yes" : "no") . "\n");
		fwrite($fp, "AddToRecommender=" . $this->AddToRecommender . "\n");
		fwrite($fp, "OverwriteExpiredID=" . ($this->OverwriteExpiredID ? "yes" : "no") . "\n");
		fwrite($fp, "DoEmailCheck=" . ($this->DoEmailCheck ? "yes" : "no") . "\n");
		fwrite($fp, "DenyExpiredUser=" . ($this->DenyExpiredUser ? "yes" : "no") . "\n");
		fwrite($fp, "ShowExpireDate=" . ($this->ShowExpireDate ? "yes" : "no") . "\n");
		fwrite($fp, "SupportEmailKR=" . $this->SupportEmailKR . "\n");
		fwrite($fp, "SupportEmailJP=" . $this->SupportEmailJP . "\n");
		fwrite($fp, "AdminAllowedNetwork=" . $this->AdminAllowedNetwork . "\n");
		fwrite($fp, "HostVersion=" . $this->HostVersion . "\n");
		fwrite($fp, "ViewerVersion=" . $this->ViewerVersion . "\n");
		fwrite($fp, "CompanyName=" . $this->CompanyName . "\n");
		fwrite($fp, "ProductName=" . $this->ProductName . "\n");
		fwrite($fp, "KoreanParticleCompanyRul=" . $this->KoreanParticleCompanyRul . "\n");
		fwrite($fp, "KoreanParticleCompanyNun=" . $this->KoreanParticleCompanyNun . "\n");
		fwrite($fp, "KoreanParticleCompanyGa=" . $this->KoreanParticleCompanyGa . "\n");
		fwrite($fp, "KoreanParticleProductRul=" . $this->KoreanParticleProductRul . "\n");
		fwrite($fp, "KoreanParticleProductNun=" . $this->KoreanParticleProductNun . "\n");
		fwrite($fp, "KoreanParticleProductGa=" . $this->KoreanParticleProductGa . "\n");

		fclose($fp);
	}
}
