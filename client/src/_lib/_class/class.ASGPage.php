<?php
/**
 * Description
 * 	page($page_per_num,$pagegrp_per_page)		필수 정보 받음
 * 	SetTotalNum($sql_body)				총 개수 구하기
 * 	SetPage($page)						페이지 지정
 * 	GetIsPrev()						이전 및 이전 화면에 대한 정보 받아오기
 *  	GetIsNext()						다음 및 다음 화면에 대한 정보 받아오기
 *  	GetPageList()						화면당 page numbering
 * 
 * @ KSM | 2015.06.22 | CPage.php modified
 */
class ASGPage {
	var $total_num;
	var $page_grp_first;
	var $page_first_data;
	var $total_page;
	var $page;
	var $page_per_num;		//페이지당 글갯수
	var $pagegrp_per_page;		//페이지그룹당 페이지수

	public function ASGPage($page_per_num,$pagegrp_per_page){
		$this->page_per_num = $page_per_num;
		$this->pagegrp_per_page = $pagegrp_per_page;
		$this->page_grp_first = 0;
	}

	public function SetTotalNum($sql_body){
		$result = mysql_query("SELECT count(*) from $sql_body"); 
		$res = mysql_fetch_row($result); //계속 가지고 있는 글의갯수 
		$this->total_num= $res[0];
	}

	public function SetPage($page){
		if($page == 0){	//페이지 지정 없으면 첫페이지
			$this->page = 1;
		}else{
			$this->page = $page;
		}

		$this->page_grp_first = floor(($this->page-1)/$this->pagegrp_per_page)*$this->pagegrp_per_page+1;	//페이지 그룹의 첫번째 데이타. 
		$this->page_first_data = ($this->page-1) * $this->page_per_num;
		$this->total_page = ceil($this->total_num/$this->page_per_num);
	}

	public function GetIsPrev(){// 2015.06.16 | KSM | get move prev btns
		$is_prev_arr = array();

		$older_page = $this->page_grp_first - 1;
		$prev_page = $this->page-1;

		$is_prev_arr['older_page'] = $older_page;
		$is_prev_arr['prev_page'] = $prev_page;

		if( $prev_page > 0 ){//set prev page
			$is_prev_arr['is_prev_page'] = TRUE;
		}else{
			$is_prev_arr['is_prev_page'] = FALSE;
		}

		if( $older_page > 0){//set older page
			$is_prev_arr['is_older_page'] = TRUE;
		}else if($older_page == 0 && $this->page > 1){	
			$is_prev_arr['older_page'] = 1;
			$is_prev_arr['is_older_page'] = TRUE;
		}else{
			$is_prev_arr['is_older_page'] = FALSE;
		}

		return $is_prev_arr;
	}

	public function GetIsNext(){// 2015.06.16 | KSM | get move next btns
		$is_next_arr = array();

		$left_data = $this->total_num - $this->page_per_num * ( $this->page - 1 );

		$newer_page = $this->page_grp_first + $this->pagegrp_per_page;
		$next_page = $this->page + 1;

		$is_next_arr['newer_page'] = $newer_page;
		$is_next_arr['next_page'] = $next_page;

		if($left_data > $this->page_per_num){
			$is_next_arr['is_next_page'] = TRUE;
		}else{
			$is_next_arr['is_next_page'] = FALSE;
		}
		
		if($newer_page < $this->total_page){
			$is_next_arr['is_newer_page'] = TRUE;
		}else if($this->total_page == $this->page){
			$is_next_arr['is_newer_page'] = FALSE;
		}else if($this->total_page == 0){
			$is_next_arr['is_newer_page'] = FALSE;
		}else{
			$is_next_arr['newer_page'] = $this->total_page;
			$is_next_arr['is_newer_page'] = TRUE;
		}

		return $is_next_arr;
	}

	public function GetPageList(){// 2015.06.16 | KSM | get number list
		$page_arr = array();

		for($i=0; $i<$this->pagegrp_per_page; $i++){
			$recent_page=$this->page_grp_first+$i;
				
			if($recent_page == $this->page){
				$page_num = $this->page;
				$page_arr[$page_num]['is_current'] = TRUE;//on
			}else if($recent_page <= $this->total_page){
				$page_num = $recent_page;
				$page_arr[$page_num]['is_current'] = FALSE;//off
			}

			if($i == ($this->pagegrp_per_page-1)){
				$page_arr[$page_num]['is_last'] = TRUE;//last
			}else{
				$page_arr[$page_num]['is_last'] = FALSE;//last
			}
		}
		return $page_arr;
	}
}