<?php
include_once("../_func/function.common.php");

include_once("../common/html_head.php");
include_once("../common/simple_header_admin.php");

$flag_remember_id_str = $_COOKIE['flag_remember_id'];//save id in cookie flag
if($flag_remember_id_str == 'true'){
	$remember_id_checked = "checked";
	$remember_id = $_COOKIE['remember_id'];
}

$alert_action = $_POST['alert_action'];
$alert_title = $_POST['alert_title'];
$alert_msg = $_POST['alert_msg'];
?>
<!-- 로그인 폼 -->
<form name="loginTryForm" method="post" action="/_func/function.login_by_admin.php">
	<fieldset>
		<legend>U+ Biz 원격지원 로그인 폼</legend>
		<div class="box_login_wrap">
			<div class="box_login_top"></div>
			<div class="box_login">
				<div class="box_input_wrap">
					<div class="b_input">
						<input type="text" name="user_id" id="user_id" placeholder="아이디" value="<? echo $remember_id; ?>" />
						<input type="password" name="user_pw" id="user_pw" placeholder="비밀번호" />
					</div>
					<!-- [D] ID,PW 입력이 잘못되었을 경우 하단에 [p class="alert"] 안내내용이 입력됨 -->
					<div class="b_btn">
						<a href="#" class="sign_in">
							<img src="<? echo IMG_DIR; ?>/sub/btn_login.gif" alt="로그인" />
						</a>
					</div>
				</div>

				<div class="login_check">
					<!-- [D] 체크박스를 체크하고 로그인하면 입력한ID는 저장되어 다음로그인시 자동입력됨 -->
					<div class="save_id">
						<input type="checkbox" id="save_id" <? echo $remember_id_checked; ?> />
						<label for="save_id">아이디저장</label>
					</div>
					<!-- [D] 
					-아이디찾기 클릭시: ID찾기 안내시스템팝업창 생성됨.
					-비밀번호찾기 클릭시: 클릭하면 [이메일인증시스템창]이 생성됨. / (이메일인증시스템창 에서)아이디,가입시 등록한 이메일 일치 확인 후 임시비밀번호를 가입시 등록한 이메일로 발송 -->
					<div class="find">
						<a href="#" class="bar open_pop_find_id">아이디 찾기</a>
						<a href="#" class="open_pop_find_pw">비밀번호 찾기</a>
					</div>
				</div>

				<!-- [D] 1~5회까지 카운트 되고, 잘못된 정보입력이 5회 이상이되면 접근이 차단되어 비밀번호 재설정해야함 -->
				<p class="alert" style="">
				<?
				if($alert_action == "open"){
				?>
					<strong><? echo $alert_title; ?></strong>
					<? echo $alert_msg; ?>
					
				<?
				}//end of : if($alert_case == "INVALIDPASSWORD")
				?>
				</p>
			</div>
			<div class="box_login_btm"></div>
		</div>
	</fieldset>
</form>
<!-- //로그인 폼 -->

<!-- 로그인화면 공지사항 -->
<?
include_once("../notice/rolling_list.php");
?>
<!-- //로그인화면 공지사항 -->

<!-- popup : find id -->
<?
include_once("./popup/find_admin_id.php");
?>
<!-- //popup : find id -->
<!-- popup : find pw -->
<?
include_once("./popup/find_admin_pw.php");
?>
<!-- //popup : find pw -->
<!-- popup : reset pw guide -->
<?
include_once("./popup/reset_pw_guide.php");
?>
<!-- //popup : reset pw guide -->
<!-- popup : reset pw send mail res -->
<?
include_once("./popup/reset_pw_error.php");
include_once("../common/popup/send_mail_fail.php");
include_once("../common/popup/send_mail_success.php");
?>
<!-- //popup : reset pw send mail res -->
<!-- popup : notice -->
<?
include_once("../notice/popup/list.php");
?>
<!-- //popup : notice -->
<?
include_once("../common/simple_footer.php");
include_once("../common/html_foot.php");