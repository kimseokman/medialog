<?php
include_once("../_func/function.common.php");

include_once("../super/common/html_head.php");
include_once("../super/common/simple_header.php");

$alert_action = $_POST['alert_action'];
$alert_title = $_POST['alert_title'];
$alert_msg = $_POST['alert_msg'];
?>
<!-- 로그인 폼 -->
<form name="superLoginTryForm" method="post" action="/_func/function.login_by_super.php">
	<fieldset>
		<legend>U+ Biz 원격지원 로그인 폼</legend>
		<div class="box_login_wrap">
			<div class="box_login_top"></div>
			<div class="box_login superadmin_box_login">
				<div class="box_input_wrap">
					<div class="b_input">
						<input type="text" name="super_id" id="user_id" placeholder="아이디" value="" />
						<input type="password" name="super_pw" id="user_pw" placeholder="비밀번호" />
					</div>
					<div class="b_btn">
						<a href="#" class="sign_in">
							<img src="<? echo IMG_DIR; ?>/sub/btn_login.gif" alt="로그인" />
						</a>
					</div>
				</div>
				
				<p class="alert" style="">
				<?
				if($alert_action == "open"){
				?>
					<strong><? echo $alert_title; ?></strong>
					<? echo $alert_msg; ?>
					
				<?
				}//end of : if($alert_case == "INVALIDPASSWORD")
				?>
				</p>
			</div>
			<div class="box_login_btm"></div>
		</div>
	</fieldset>
</form>
<!-- //로그인 폼 -->
<!-- popup : find id -->
<?
include_once("./popup/deny_old_super.php");
?>
<!-- //popup : find id -->
<?
include_once("../super/common/simple_footer.php");
include_once("../super/common/html_foot.php");
?>