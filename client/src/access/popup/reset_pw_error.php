<?php
$reset_pw_res_pop_action = $_POST['reset_pw_res_pop_action'];
$reset_pw_res_pop_msg = $_POST['reset_pw_res_pop_msg'];

if($reset_pw_res_pop_action == 'open'){
?>
<!-- reset password respond -->
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<!-- ID찾기 팝업 -->
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<a href="#" class="close close_pop_reset_pw_res" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p>
					<? echo $reset_pw_res_pop_msg; ?>
				</p>
				<div class="btn">
					<a href="#" class="confirm_pop_reset_pw_res"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //reset password respond -->
<?
}//end of : if($reset_res)
?>