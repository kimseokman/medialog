<!-- popup : id 찾기 -->
<?php
$find_id_pop_act = $_POST['find_id_popup_action'];//find id flag ('open', 'close')

if($find_id_pop_act == 'open'){
?>
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<!-- ID찾기 팝업 -->
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<a href="#" class="close close_pop_find_id" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p>분실하신 ID는 U+Biz 원격지원 고객센터<br /><strong>(<? echo CONTACT_US_TEL; ?>)</strong>으로 문의주시기 바랍니다.</p>
				<div class="btn">
					<a href="#" class="confirm_pop_find_id"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
	<!-- //ID찾기 팝업 -->
</div>
<?	
}//end of : if($find_id_pop_act == 'open')
?>
<!-- //popup : id 찾기 -->