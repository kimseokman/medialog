<!-- popup : pw 찾기 -->
<?php
$find_pw_pop_act = $_POST['find_pw_popup_action'];//find password flag ('open', 'close')

if($find_pw_pop_act == 'open'){
?>
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<!-- PW찾기 팝업 -->
	<!-- [D] 아이디와 가입시 등록한 이메일일치 확인후 임시비밀번호를 가입시 등록한 이메일로 발송함. / ID와 E-Mail 입력칸을 클릭하면 해당 문구는 사라짐. -->
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<a href="#" class="close close_pop_find_pw" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p>비밀번호를 분실하셨을 경우, <br />관리자에게 초기화 요청을 하시기 바랍니다.</p>

				<div class="btn">
					<a class="close_pop_find_pw" href="#"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<?
}//end of : if($flag_find_pw == TRUE)
?>
<!-- //popup : pw 찾기 -->