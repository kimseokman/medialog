<!-- popup : id 찾기 -->
<?php
$deny_old_super_action = $_POST['deny_old_super_action'];//find id flag ('open', 'close')

$show_interval = LAST_LOGIN_INTERVAL/30;

if($deny_old_super_action == 'open'){
?>
<div class="fixed_dim"></div>
<div id="popup" class="reset_pw">
	<div class="popup_title">
		<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
		<span class="popup_title_txt">비밀번호 변경안내</span>
		<a href="#" class="close close_pop_old_super" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
	</div>
	<div class="popup_content_wrap">
		<div class="popup_content">
			<div class="reset_pw_txt">
				<strong>
					고객님의 소중한 개인정보보호를 위해 비밀번호<br />
					변경이 필요합니다.
				</strong>
				<p>
					최근 <? echo $show_interval; ?>개월 이상 사이트를 이용하지 않아 로그인이 차단되었습니다.<br />
					프로그램 운영자에게 비밀번호 초기화를 요청하세요.
				</p>
			</div>

			<!-- [D] 버튼을 클릭하면 임시비밀번호찾기 시스템창이 생성됨. -->
			<div class="btn">
				<a href="#" class="confim_pop_old_super"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
			</div>
		</div>
	</div>
</div>
<?	
}//end of : if($find_id_pop_act == 'open')
?>
<!-- //popup : id 찾기 -->