<!-- popup : reset password guide -->
<?php
$reset_pw_guide_pop_act = $_POST['reset_pw_guide_popup_action'];//reset password flag ('open', 'close')

if($reset_pw_guide_pop_act == 'open'){
?>
<div class="fixed_dim"></div>
<!-- 비밀번호 재설정안내 레이어팝업 -->
<!-- [D] 비밀번호 오류 5회 이상 시, 레이어팝업 생성. 닫기 버튼을 클릭하면 팝업이 사라지고 로그인 화면이 나타남. -->
<div id="popup" class="reset_pw">
	<div class="popup_title">
		<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
		<span class="popup_title_txt">비밀번호 재설정 안내</span>
		<a href="#" class="close close_pop_reset_pw_guide" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
	</div>
	<div class="popup_content_wrap">
		<div class="popup_content">
			<div class="reset_pw_txt">
				<strong>잘못된 로그인 시도가 있어 임시로 접근을 차단하였습니다.<br />비밀번호를 분실하셨을 경우, 관리자에게 <span class="imp">초기화 요청</span>을 하시기 바랍니다.</strong>
				<p>
					연속 5회 이상 로그인 오류가 있어 비밀번호 재설정이 필요합니다.<br />
					개인 정보 유출로 인한 피해를 막기 위하여 로그인 5회 이상 실패 시, 임시로 로그인이 차단됩니다.<br />
					안전한 서비스 이용을 위하여 양해를 부탁드립니다.
				</p>
			</div>

			<!-- [D] 버튼을 클릭하면 임시비밀번호찾기 시스템창이 생성됨. -->
			<div class="btn">
				<a class="close_pop_reset_pw_guide" href="#"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
			</div>
		</div>
	</div>
</div>
<!-- //비밀번호 재설정안내 레이어팝업 -->
<?
}//end of : if($reset_pw_guide_pop_act)
?>
<!-- //popup : reset password guide -->