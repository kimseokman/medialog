<!-- popup : pw 찾기 -->
<?php
$find_pw_pop_act = $_POST['find_pw_popup_action'];//find password flag ('open', 'close')

if($find_pw_pop_act == 'open'){
?>
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<!-- PW찾기 팝업 -->
	<!-- [D] 아이디와 가입시 등록한 이메일일치 확인후 임시비밀번호를 가입시 등록한 이메일로 발송함. / ID와 E-Mail 입력칸을 클릭하면 해당 문구는 사라짐. -->
	<div id="popup" class="find_pw">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<a href="#" class="close close_pop_find_pw" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p>가입 시 등록한 아이디와 이메일 주소를<br />입력하시면 등록하신 이메일 주소로 임시비밀번호가 발송됩니다.</p>
				<table class="style_board_userinfo" summary="id, e-mail 입력으로 비밀번호 찾기" width="100%">
					<caption><span>비밀번호 찾기</span></caption>
					<colgroup>
						<col style="width:72px;" />
						<col style="width:*;" />
					</colgroup>
					<tbody>
						<tr>
							<th><label for="reset_user_id">아이디</label></th>
							<td><input type="text" id="reset_user_id" placeholder="아이디 입력" style="width:170px;" /></td>
						</tr>
						<tr class="last">
							<th><label for="reset_user_email">이메일</label></th>
							<td><input type="text" id="reset_user_email" placeholder="가입시 등록한 이메일 입력" style="width:170px;" /></td>
						</tr>
					</tbody>
				</table>

				<div class="btn">
					<a class="send_reset_mail" href="#"><img src="<? echo IMG_DIR; ?>/sub/btn_temppw.gif" alt="임시 비밀번호 받기" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<?
}//end of : if($flag_find_pw == TRUE)
?>
<!-- //popup : pw 찾기 -->