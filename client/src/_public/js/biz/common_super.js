/**
 * common_super.js
 * 2015.08.24 | KSM
 */

(function($){
	//popup : logout
	$('.open_pop_logout').on('click', {action_str: "open"}, logout_pop);
	$('.close_pop_logout').on('click', {action_str: "close"}, logout_pop);
	$('.pop_logout_no').on('click', {action_str: "close"}, logout_pop);
	$('.pop_logout_yes').on('click', logout);
})(jQuery);

//
//	popup : logout
//
function logout_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;

	var admin_id = $('.admin_id').val();

	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var logout_popup_action = $('<input type="hidden" value="'+pop_action+'" name="logout_popup_action" />');
	var admin_id_param = $('<input type="hidden" value="'+admin_id+'" name="adid" />');
	
	$form.append(logout_popup_action).append(admin_id_param);

	$form.submit();
}

//
//	logout
//
function logout(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var $form = $('<form></form>');
	$form.attr('action', '/_func/function.logout_by_super.php');
	$form.attr('method', 'post');
	$form.appendTo('body');
	
	$form.submit();
}