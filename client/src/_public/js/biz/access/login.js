/**
 * login.js
 * 2015.05.31 | KSM
 */
(function($){
	//sign in
	$('.sign_in').on('click', sign_in);
	$('#user_pw').on('keydown', user_pw_keyevent);

	//find id popup
	$('.open_pop_find_id').on('click', {action_str: "open"}, find_id_pop);
	$('.close_pop_find_id').on('click', {action_str: "close"}, find_id_pop);
	$('.confirm_pop_find_id').on('click', {action_str: "close"}, find_id_pop);
	//find password popup
	$('.open_pop_find_pw').on('click', {action_str: "open"}, find_pw_pop);
	$('.close_pop_find_pw').on('click', {action_str: "close"}, find_pw_pop);
	//reset password popup
	$('.close_pop_reset_pw_guide').on('click', {action_str: "close"}, reset_pw_guide_pop);
	$('.temp_pw_recive_btn').on('click', temp_pw_recive);
	//notice popup
	$('.open_pop_notice').on('click', {action_str: "open"}, notice_pop);
	$('.close_pop_notice').on('click', {action_str: "close"}, notice_pop);
	//reset password respond popup
	$('.close_pop_reset_pw_res').on('click', {action_str: "close"}, find_pw_pop);
	$('.confirm_pop_reset_pw_res').on('click', {action_str: "close"}, find_pw_pop);
	//send mail respond popup
	$('.close_pop_send_mail_success').on('click', {action_str: "close"}, send_mail_success_pop);
	$('.confirm_pop_send_mail_success').on('click', {action_str: "close"}, send_mail_success_pop);
	$('.close_pop_send_mail_fail').on('click', {action_str: "close"}, send_mail_fail_pop);
	$('.confirm_pop_send_mail_fail').on('click', {action_str: "close"}, send_mail_fail_pop);

	$('.send_reset_mail').on('click', reset_mail);
	$('#reset_user_email').on('keydown', reset_user_email_keyevent);

	//search notice ( ajax )
	$('.search_notice_btn').on('click', search_notice);
	$('.notice_search_val').on('keydown', notice_search_keyevent);

	$('.go_to_notice_list_btn').on('click', search_notice_trigger);

	notice_detail_direct_trigger();
})(jQuery);

//
//	popup notice : detail ( ajax )
//
function notice_detail_direct(e, num){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = 'open';
	
	var $form = $('<form></form>');
	$form.attr('action', '/access/login.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var notice_popup_action = $('<input type="hidden" value="'+pop_action+'" name="notice_popup_action" />');
	var direct_detail_action = $('<input type="hidden" value="ACTION" name="direct_detail_action" />');
	var seq = $('<input type="hidden" value="'+num+'" name="seq" />');
	
	$form.append(seq).append(direct_detail_action).append(notice_popup_action);
	
	$form.submit();
}

function notice_detail_direct_trigger(){
	var direct_detail_action = $('#direct_detail_action').val();

	if(direct_detail_action == "ACTION"){
		var seq_num = $('#direct_detail_seq').val();
		$.ajax({
			type: "POST",
			url:'/_ajax/notice/popup/ajax.detail.php',
			data: {seq: seq_num},
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			success: function(data){
				var res = data;

				$(".ajax_pop_notice_content").html(res);

				$(".pop_notice_footer .search_box").hide();
				$(".pop_notice_footer .btn_list").show();
			}
		});
	}
}

//
//	popup notice : detail ( ajax )
//
function ajax_pop_notice_detail(e, num){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var seq_num = num;

	$.ajax({
		type: "POST",
		url:'/_ajax/notice/popup/ajax.detail.php',
		data: {seq: seq_num},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_pop_notice_content").html(res);

			$(".pop_notice_footer .search_box").hide();
			$(".pop_notice_footer .btn_list").show();
		}
	});
}

//
//	popup notice : Key Event
//
function notice_search_keyevent(e){
	if(e.keyCode == 13){//case by 'ENTER'
		search_notice_trigger();
	}
}

function search_notice_trigger(){
	$('.search_notice_btn').trigger('click');
}

//
//	popup notice : search
//
function search_notice(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var page = 0;
	var sel_index = $('.u_biz_noti_input_pop .jq_sel ul li').find('a.selected').attr('index');
	var search_type = $('.u_biz_noti_input_pop .jq_sel_hide option').eq(sel_index).val();
	var search_val = $('.u_biz_noti_input_pop .notice_search_val').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/notice/popup/ajax.list.php',
		data: {page: page, search_type : search_type, search_val: search_val},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_pop_notice_content").html(res);

			$(".pop_notice_footer .search_box").show();
			$(".pop_notice_footer .btn_list").hide();
		}
	});
}

//
//	popup notice : pagination
//
function notice_pagination(e, page_num){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var page = page_num;
	var sel_index = $('.u_biz_noti_input_pop .jq_sel ul li').find('a.selected').attr('index');
	var search_type = $('.u_biz_noti_input_pop .jq_sel_hide option').eq(sel_index).val();
	var search_val = $('.u_biz_noti_input_pop .notice_search_val').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/notice/popup/ajax.list.php',
		data: {page: page, search_type : search_type, search_val: search_val},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_pop_notice_content").html(res);
		}
	});
}

//
//	send mail : reset password : Key Event
//
function reset_user_email_keyevent(e){
	if(e.keyCode == 13){//case by 'ENTER'
		reset_mail_trigger();
	}
}

function reset_mail_trigger(){
	$('.send_reset_mail').trigger('click');
}

//
//	send mail : reset password
//
function reset_mail(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var user_id = $('#reset_user_id').val();
	var user_email = $('#reset_user_email').val();
	var reset_type = "user";

	var $form = $('<form></form>');
	$form.attr('action', '/_func/function.send_reset_pw_mail.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var user_id = $('<input type="hidden" value="'+user_id+'" name="user_id" />');
	var user_email = $('<input type="hidden" value="'+user_email+'" name="user_email" />');
	var reset_user_type = $('<input type="hidden" value="'+reset_type+'" name="reset_user_type" />');
	
	$form.append(reset_user_type).append(user_id).append(user_email);
	
	$form.submit();
}

//
//	reset_pw_res_pop : notice (open, close)
//
function send_mail_success_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;
	
	var $form = $('<form></form>');
	$form.attr('action', '/access/login.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var reset_pw_res_pop_action = $('<input type="hidden" value="'+pop_action+'" name="reset_pw_res_pop_action" />');
	
	$form.append(reset_pw_res_pop_action);
	
	$form.submit();
}

//
//	reset_pw_res_pop : notice (open, close)
//
function send_mail_fail_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;
	
	var $form = $('<form></form>');
	$form.attr('action', '/access/login.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var reset_pw_res_pop_action = $('<input type="hidden" value="'+pop_action+'" name="reset_pw_res_pop_action" />');
	
	$form.append(reset_pw_res_pop_action);
	
	$form.submit();
}

//
//	reset_pw_res_pop : notice (open, close)
//
function reset_pw_res_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;
	
	var $form = $('<form></form>');
	$form.attr('action', '/access/login.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var reset_pw_res_pop_action = $('<input type="hidden" value="'+pop_action+'" name="reset_pw_res_pop_action" />');
	
	$form.append(reset_pw_res_pop_action);
	
	$form.submit();
}

//
//	popup : notice (open, close)
//
function notice_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;
	
	var $form = $('<form></form>');
	$form.attr('action', '/access/login.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var notice_popup_action = $('<input type="hidden" value="'+pop_action+'" name="notice_popup_action" />');
	
	$form.append(notice_popup_action);
	
	$form.submit();
}

//
//	revice temporary password
//
function temp_pw_recive(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var reset_pw_guide_pop_action = 'close';
	var find_pw_popup_action = 'open';
	
	var $form = $('<form></form>');
	$form.attr('action', '/access/login.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var reset_pw_guide_popup_action_param = $('<input type="hidden" value="'+reset_pw_guide_pop_action+'" name="reset_pw_guide_popup_action" />');
	var find_pw_popup_action_param = $('<input type="hidden" value="'+find_pw_popup_action+'" name="find_pw_popup_action" />');
	
	$form.append(reset_pw_guide_popup_action_param).append(find_pw_popup_action_param);
	
	$form.submit();
}

//
//	popup : reset password guide ( open, close )
//
function reset_pw_guide_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;
	
	var $form = $('<form></form>');
	$form.attr('action', '/access/login.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var reset_pw_guide_popup_action = $('<input type="hidden" value="'+pop_action+'" name="reset_pw_guide_popup_action" />');
	
	$form.append(reset_pw_guide_popup_action);
	
	$form.submit();
}

//
//	popup : find password ( open , close )
//	
function find_pw_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;
	
	var $form = $('<form></form>');
	$form.attr('action', '/access/login.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var find_pw_popup_action = $('<input type="hidden" value="'+pop_action+'" name="find_pw_popup_action" />');
	
	$form.append(find_pw_popup_action);
	
	$form.submit();
}

//
//	popup : find id ( open , close )
//
function find_id_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;

	var $form = $('<form></form>');
	$form.attr('action', '/access/login.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var find_id_popup_action = $('<input type="hidden" value="'+pop_action+'" name="find_id_popup_action" />');
	
	$form.append(find_id_popup_action);

	$form.submit();
}

//
//	login : Key Event
//
function user_pw_keyevent(e){
	if(e.keyCode == 13){//case by 'ENTER'
		sign_in_trigger();
	}
}

//
//	trigger : login
//
function sign_in_trigger(){
	$('.sign_in').trigger('click');
}

//
//	login
//
function sign_in(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var flag_remember_id = $('#save_id').is(":checked");

	$.cookie('flag_remember_id', flag_remember_id);//set checked

	if(flag_remember_id){
		$.cookie('remember_id', $("#user_id").val());
	}
	else{
		$.cookie('remember_id');
	}

	document.loginTryForm.submit();//submit
}
