/**
 * super_login.js
 * 2015.05.31 | KSM
 */
(function($){
	//sign in
	$('.sign_in').on('click', sign_in);
	$('#user_pw').on('keydown', user_pw_keyevent);

	$('.close_pop_old_super').on('click', {action_str: "close"}, pop_old_super);
	$('.confim_pop_old_super').on('click', {action_str: "close"}, pop_old_super);
})(jQuery);

function pop_old_super(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;
	
	var $form = $('<form></form>');
	$form.attr('action', '');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var deny_old_super_action = $('<input type="hidden" value="'+pop_action+'" name="deny_old_super_action" />');
	
	$form.append(deny_old_super_action);
	
	$form.submit();
}

//
//	login : Key Event
//
function user_pw_keyevent(e){
	if(e.keyCode == 13){//case by 'ENTER'
		sign_in_trigger();
	}
}

//
//	trigger : login
//
function sign_in_trigger(){
	$('.sign_in').trigger('click');
}

//
//	login
//
function sign_in(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	document.superLoginTryForm.submit();//submit
}
