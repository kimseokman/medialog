/**
 * app / record / info.js
 * 2015.08.25 | KSM
 */

(function($){
	//search
	$('.edit_record_btn').on('click', edit_record);

	$('.close_pop_record_edit_res').on('click', {action_str: "close"}, pop_record_edit);
	$('.confirm_pop_record_edit_res').on('click', {action_str: "close"}, pop_record_edit);
})(jQuery);

//
//	POPUP : Record Edit Respond
//
function pop_record_edit(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;
	var sptid = $('.sptid').val();
	var report_num = $('.report_num').val();
	
	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var record_edit_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="record_edit_res_popup_action" />');
	var sptid_param = $('<input type="hidden" value="'+sptid+'" name="sptid" />');
	var report_num_param = $('<input type="hidden" value="'+report_num+'" name="report_num" />');
	
	$form.append(record_edit_res_popup_action).append(sptid_param).append(report_num_param);
	
	$form.submit();
}

//
//	EDIT : Record
//
function edit_record(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	
	document.editRecordForm.submit();
}