/**
 * app / record / list.js
 * 2015.08.25 | KSM
 */

(function($){
	//search
	$('.search_record').on('click', search_record);

	//export
	$('.export_record').on('click', export_record_to_excel);
})(jQuery);

//
//	titile : set
//
function pageTitleSet(e, base_title, report_num){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var original_title = document.title;
	var new_title = base_title + " " + report_num;

	if(original_title == new_title){
		document.title = base_title;
	}
	
	document.title = new_title;
}

//
//	export : excel
//
function export_record_to_excel(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var sptid = $('.sptid').val();

	var start_YYYY_index = $('.search_record_box .start_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var startYear = $('.search_record_box .start_YYYY .jq_sel_hide option').eq(start_YYYY_index).val();
	var start_MM_index = $('.search_record_box .start_MM .jq_sel ul li').find('a.selected').attr('index');
	var startMonth = $('.search_record_box .start_MM .jq_sel_hide option').eq(start_MM_index).val();
	var start_DD_index = $('.search_record_box .start_DD .jq_sel ul li').find('a.selected').attr('index');
	var startDay = $('.search_record_box .start_DD .jq_sel_hide option').eq(start_DD_index).val();

	var start_time = startYear +"-"+ startMonth +"-"+ startDay;

	var end_YYYY_index = $('.search_record_box .end_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var endYear = $('.search_record_box .end_YYYY .jq_sel_hide option').eq(end_YYYY_index).val();
	var end_MM_index = $('.search_record_box .end_MM .jq_sel ul li').find('a.selected').attr('index');
	var endMonth = $('.search_record_box .end_MM .jq_sel_hide option').eq(end_MM_index).val();
	var end_DD_index = $('.search_record_box .end_DD .jq_sel ul li').find('a.selected').attr('index');
	var endDay = $('.search_record_box .end_DD .jq_sel_hide option').eq(end_DD_index).val();

	var end_time = endYear +"-"+ endMonth +"-"+ endDay;

	var $form = $('<form></form>');
	$form.attr('action', '/_export/app/record/export.list.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var sptid_param = $('<input type="hidden" value="'+sptid+'" name="sptid" />');
	var start_time_param = $('<input type="hidden" value="'+start_time+'" name="start_time" />');
	var end_time_param = $('<input type="hidden" value="'+end_time+'" name="end_time" />');
	
	$form.append(start_time_param).append(end_time_param).append(sptid_param);
	
	$form.submit();
}

//
//	trigger
//
function search_record_trigger(){
	$('.search_record').trigger('click');
}

//
//	search : search button
//	
function search_record(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var sptid = $('.sptid').val();

	var start_YYYY_index = $('.search_record_box .start_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var startYear = $('.search_record_box .start_YYYY .jq_sel_hide option').eq(start_YYYY_index).val();
	var start_MM_index = $('.search_record_box .start_MM .jq_sel ul li').find('a.selected').attr('index');
	var startMonth = $('.search_record_box .start_MM .jq_sel_hide option').eq(start_MM_index).val();
	var start_DD_index = $('.search_record_box .start_DD .jq_sel ul li').find('a.selected').attr('index');
	var startDay = $('.search_record_box .start_DD .jq_sel_hide option').eq(start_DD_index).val();

	var start_time = startYear +"-"+ startMonth +"-"+ startDay;

	var end_YYYY_index = $('.search_record_box .end_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var endYear = $('.search_record_box .end_YYYY .jq_sel_hide option').eq(end_YYYY_index).val();
	var end_MM_index = $('.search_record_box .end_MM .jq_sel ul li').find('a.selected').attr('index');
	var endMonth = $('.search_record_box .end_MM .jq_sel_hide option').eq(end_MM_index).val();
	var end_DD_index = $('.search_record_box .end_DD .jq_sel ul li').find('a.selected').attr('index');
	var endDay = $('.search_record_box .end_DD .jq_sel_hide option').eq(end_DD_index).val();

	var end_time = endYear +"-"+ endMonth +"-"+ endDay;

	$.ajax({
		type: "POST",
		url:'/_ajax/app/record/ajax.list.php',
		data: {start_time: start_time, end_time: end_time, sptid: sptid},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_record").html(res);
		}
	});
}