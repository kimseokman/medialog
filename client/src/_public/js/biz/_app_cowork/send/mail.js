/**
 * app / send / mail.js
 * 2015.09.07 | KSM
 */

(function($){
	$('.btn_email').on('click', send_mail);

	$('.close_pop_send_mail_res').on('click', {action_str: "close"}, pop_send_mail_res);
	$('.confirm_pop_send_mail_res').on('click', {action_str: "close"}, pop_send_mail_res);
})(jQuery);

function pop_send_mail_res(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;
	var sptid = $('.sptid').val();
	var access_code = $('.access_code').val();
	
	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var send_mail_pop_act = $('<input type="hidden" value="'+pop_action+'" name="send_mail_pop_act" />');
	var sptid_param = $('<input type="hidden" value="'+sptid+'" name="userid" />');
	var access_code_param = $('<input type="hidden" value="'+access_code+'" name="accessCode" />');
	
	$form.append(send_mail_pop_act).append(sptid_param).append(access_code_param);
	
	$form.submit();
}


function send_mail(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var sptid = $('.sptid').val();
	var access_code = $('.access_code').val();
	var receive_mail = $('.receive_mail').val();
	var access_url = $('.access_url').val();
	var public_access_url = $('.public_access_url').val();

	var $form = $('<form></form>');
	$form.attr('action', '/_func/function.send_request_remote_mail.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var sptid_param = $('<input type="hidden" value="'+sptid+'" name="sptid" />');
	var receive_mail_param = $('<input type="hidden" value="'+receive_mail+'" name="receive_mail" />');
	var access_url_param = $('<input type="hidden" value="'+access_url+'" name="access_url" />');
	var public_access_url_param = $('<input type="hidden" value="'+public_access_url+'" name="public_access_url" />');
	var access_code_param = $('<input type="hidden" value="'+access_code+'" name="access_code" />');

	$form.append(sptid_param).append(receive_mail_param).append(access_code_param).append(access_url_param).append(public_access_url_param);

	$form.submit();
}