/**
 * app / send / sms.js
 * 2015.10.14 | KSM
 */

(function($){
	$('#url_chek').on('click', {act_label: "url_chek"}, apply_url);
	$('#url_chek01').on('click', {act_label: "url_chek01"}, apply_url);

	$('.close_pop_send_sms_res').on('click', {action_str: "close"}, pop_send_sms_res);
	$('.confirm_pop_send_sms_res').on('click', {action_str: "close"}, pop_send_sms_res);

	$('.btn_sms').on('click', send_sms);
})(jQuery);

function apply_url(e){
	var label_class_name = e.data.act_label;

	var access_url = $('.' + label_class_name).text();

	$(".apply_url").html(access_url);
}

function pop_send_sms_res(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;
	var sptid = $('.sptid').val();
	var access_code = $('.access_code').val();
	
	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var send_sms_pop_act = $('<input type="hidden" value="'+pop_action+'" name="send_sms_pop_act" />');
	var sptid_param = $('<input type="hidden" value="'+sptid+'" name="userid" />');
	var access_code_param = $('<input type="hidden" value="'+access_code+'" name="accessCode" />');
	
	$form.append(send_sms_pop_act).append(sptid_param).append(access_code_param);
	
	$form.submit();
}

function send_sms(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var access_url = $(':radio[name="url_text"]:checked').val();
	var access_code = $('.access_code').val();
	var receive_num = $('.phone_num1').val();
	var send_num = $('.phone_num2').val();
	var userid = $('.userid').val();
	var usernum = $('.usernum').val();

	var $form = $('<form></form>');
	$form.attr('action', '/_func/function.send_sms.php');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var access_code_param = $('<input type="hidden" value="'+access_code+'" name="access_code" />');
	var access_url_param = $('<input type="hidden" value="'+access_url+'" name="access_url" />');
	var receive_num_param = $('<input type="hidden" value="'+receive_num+'" name="receive_num" />');
	var send_num_param = $('<input type="hidden" value="'+send_num+'" name="send_num" />');
	var userid_param = $('<input type="hidden" value="'+userid+'" name="userid" />');
	var usernum_param = $('<input type="hidden" value="'+usernum+'" name="usernum" />');
	
	$form.append(access_url_param).append(access_code_param).append(receive_num_param).append(send_num_param).append(userid_param).append(usernum_param);
	
	$form.submit();
}