/**
 * common_customer.js
 * 2015.08.21 | KSM
 */

(function($){
	$('.program_download').on('click', program_download);
})(jQuery);

//
//	프로그램 바로 시작
//
function program_download(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	document.location='/download/HostLauncher.exe';
}