/**
 * servcie.js
 * 2015.08.04 | KSM
 */
(function($){
	//notice popup
	$('.open_pop_notice').on('click', {action_str: "open"}, notice_pop);
	$('.close_pop_notice').on('click', {action_str: "close"}, notice_pop);

	//search notice ( ajax )
	$('.search_notice_btn').on('click', search_notice);
	$('.notice_search_val').on('keydown', notice_search_keyevent);

	$('.go_to_notice_list_btn').on('click', search_notice_trigger);

	notice_detail_direct_trigger();
})(jQuery);

//
//	popup notice : detail ( ajax )
//
function notice_detail_direct(e, num){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = 'open';
	
	var $form = $('<form></form>');
	$form.attr('action', '/agent/service.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var notice_popup_action = $('<input type="hidden" value="'+pop_action+'" name="notice_popup_action" />');
	var direct_detail_action = $('<input type="hidden" value="ACTION" name="direct_detail_action" />');
	var seq = $('<input type="hidden" value="'+num+'" name="seq" />');
	
	$form.append(seq).append(direct_detail_action).append(notice_popup_action);
	
	$form.submit();
}

function notice_detail_direct_trigger(){
	var direct_detail_action = $('#direct_detail_action').val();

	if(direct_detail_action == "ACTION"){
		var seq_num = $('#direct_detail_seq').val();
		$.ajax({
			type: "POST",
			url:'/_ajax/notice/popup/ajax.detail.php',
			data: {seq: seq_num},
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			success: function(data){
				var res = data;

				$(".ajax_pop_notice_content").html(res);

				$(".pop_notice_footer .search_box").hide();
				$(".pop_notice_footer .btn_list").show();
			}
		});
	}
}

//
//	popup notice : detail ( ajax )
//
function ajax_pop_notice_detail(e, num){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var seq_num = num;

	$.ajax({
		type: "POST",
		url:'/_ajax/notice/popup/ajax.detail.php',
		data: {seq: seq_num},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_pop_notice_content").html(res);

			$(".pop_notice_footer .search_box").hide();
			$(".pop_notice_footer .btn_list").show();
		}
	});
}

//
//	START : session
//
function open_pop_start_session(e, width, height){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	
	var w = width;
	var h = height;

	var url = '/agent/popup/start_session.php';
	var name = '';
	var attr = "width="+w+"px, height="+h+"px, top=300px, left=600px";

	window.open(url, name, attr);
}

//
//	popup : notice (open, close)
//
function notice_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;
	
	var $form = $('<form></form>');
	$form.attr('action', '/agent/service.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var notice_popup_action = $('<input type="hidden" value="'+pop_action+'" name="notice_popup_action" />');
	
	$form.append(notice_popup_action);
	
	$form.submit();
}

//
//	popup notice : Key Event
//
function notice_search_keyevent(e){
	if(e.keyCode == 13){//case by 'ENTER'
		search_notice_trigger();
	}
}

function search_notice_trigger(){
	$('.search_notice_btn').trigger('click');
}

//
//	popup notice : search
//
function search_notice(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var page = 0;
	var sel_index = $('.u_biz_noti_input_pop .jq_sel ul li').find('a.selected').attr('index');
	var search_type = $('.u_biz_noti_input_pop .jq_sel_hide option').eq(sel_index).val();
	var search_val = $('.u_biz_noti_input_pop .notice_search_val').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/notice/popup/ajax.list.php',
		data: {page: page, search_type : search_type, search_val: search_val},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_pop_notice_content").html(res);

			$(".pop_notice_footer .search_box").show();
			$(".pop_notice_footer .btn_list").hide();
		}
	});
}

//
//	popup notice : pagination
//
function notice_pagination(e, page_num){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var page = page_num;
	var sel_index = $('.u_biz_noti_input_pop .jq_sel ul li').find('a.selected').attr('index');
	var search_type = $('.u_biz_noti_input_pop .jq_sel_hide option').eq(sel_index).val();
	var search_val = $('.u_biz_noti_input_pop .notice_search_val').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/notice/popup/ajax.list.php',
		data: {page: page, search_type : search_type, search_val: search_val},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_pop_notice_content").html(res);
		}
	});
}