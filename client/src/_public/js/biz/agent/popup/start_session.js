/**
 * popup / start_session.js
 * 2015.08.20 | KSM
 */
(function($){
	$(window).load(function() {
 
		var strWidth;
		var strHeight;

		//innerWidth / innerHeight / outerWidth / outerHeight 지원 브라우저 
		if ( window.innerWidth && window.innerHeight && window.outerWidth && window.outerHeight ) {
			strWidth = $('#popup.program').outerWidth() + (window.outerWidth - window.innerWidth);
			strHeight = $('#popup.program').outerHeight() + (window.outerHeight - window.innerHeight);
		}
		else {
			var strDocumentWidth = $(document).outerWidth();
			var strDocumentHeight = $(document).outerHeight();

			window.resizeTo ( strDocumentWidth, strDocumentHeight );

			var strMenuWidth = strDocumentWidth - $(window).width();
			var strMenuHeight = strDocumentHeight - $(window).height();

			strWidth = $('#popup.program').outerWidth() + strMenuWidth;
			strHeight = $('#popup.program').outerHeight() + strMenuHeight;
		}

		//resize 
		window.resizeTo( strWidth, strHeight );

		program_download_trigger();
	});

	$('.direct_start').on('click', program_download);
})(jQuery);

function program_download_trigger(){
	$('.direct_start').trigger('click');
}

//
//	프로그램 바로 시작
//
function program_download(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	document.location='/download/SupporterLauncher.exe';
}