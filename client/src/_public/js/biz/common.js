/**
 * common.js
 * 2015.05.30 | KSM
 */

(function($){
	//popup : logout
	$('.open_pop_logout').on('click', {action_str: "open"}, logout_pop);
	$('.close_pop_logout').on('click', {action_str: "close"}, logout_pop);
	$('.pop_logout_no').on('click', {action_str: "close"}, logout_pop);
	$('.pop_logout_yes').on('click', logout);

	$('.close_pop_edit_pw_guide').on('click', {action_str: "close"}, edit_pw_guide_pop);
	$('.edit_pw_redirect').on('click', edit_pw_redirect);

	$('.program_download').on('click', program_download);
})(jQuery);

//
//	edit password redirect
//
function edit_pw_guide_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;

	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var edit_pw_guide_pop_action = $('<input type="hidden" value="'+pop_action+'" name="edit_pw_guide_pop_action" />');
	
	$form.append(edit_pw_guide_pop_action);

	$form.submit();
}

//
//	edit password redirect
//
function edit_pw_redirect(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = 'open';

	var $form = $('<form></form>');
	$form.attr('action', '/admin/setting/site/info.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var edit_pw_popup_action = $('<input type="hidden" value="'+pop_action+'" name="edit_pw_popup_action" />');
	
	$form.append(edit_pw_popup_action);

	$form.submit();
}

//
//	프로그램 바로 시작
//
function program_download(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	document.location='/download/AnyHelp_Setup.exe';
}

//
//	popup : logout
//
function logout_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;

	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var logout_popup_action = $('<input type="hidden" value="'+pop_action+'" name="logout_popup_action" />');
	
	$form.append(logout_popup_action);

	$form.submit();
}

//
//	logout
//
function logout(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var $form = $('<form></form>');
	$form.attr('action', '/_func/function.logout.php');
	$form.attr('method', 'post');
	$form.appendTo('body');
	
	$form.submit();
}