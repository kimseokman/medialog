/**
 * super / manage / customer / paid / add.js
 * 2015.08.31 | KSM
 */

(function($){
	$('.create_admin_btn').on('click', create_admin);

	$('#admin_pw').on('keyup', validate_pw);
	$('#admin_retype_pw').on('keyup', validate_retype_pw);

	$('#admin_pc_cnt').on('keydown', check_cnt);
	$('#admin_pc_m_n_v_cnt').on('keydown', check_cnt);

	$('#admin_pc_cnt').on('change', apply_volume);
	$('#admin_pc_m_n_v_cnt').on('change', apply_volume);

	//checkout popup
	$('.close_pop_admin_add_res').on('click', {action_str: "close"}, admin_add_res_pop);
	$('.confirm_pop_admin_add_res').on('click', {action_str: "close"}, admin_add_res_pop);

	$( "#admin_service_join_date" ).datepicker({
		showOn: "button",
		buttonImage: "/_public/images/super/calendar.gif",
		buttonImageOnly: true,
		buttonText: "Select date",
		dateFormat: 'yy-mm-dd',
		beforeShow: function() {
			setTimeout(function(){
				$('.ui-datepicker').css('z-index', 99999999999999);
			}, 0);
		}
	});

	$( "#admin_service_start_date" ).datepicker({
		showOn: "button",
		buttonImage: "/_public/images/super/calendar.gif",
		buttonImageOnly: true,
		buttonText: "Select date",
		dateFormat: 'yy-mm-dd',
		beforeShow: function() {
			setTimeout(function(){
				$('.ui-datepicker').css('z-index', 99999999999999);
			}, 0);
		}
	});

	$( "#admin_service_expired_date" ).datepicker({
		showOn: "button",
		buttonImage: "/_public/images/super/calendar.gif",
		buttonImageOnly: true,
		buttonText: "Select date",
		dateFormat: 'yy-mm-dd',
		beforeShow: function() {
			setTimeout(function(){
				$('.ui-datepicker').css('z-index', 99999999999999);
			}, 0);
		}
	});
})(jQuery);

//
//	validate retype password
//
function validate_retype_pw(e){
	var password = $('#admin_pw').val();
	var password2 = $('#admin_retype_pw').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/common/ajax.validate_retype_pw.php',
		data: {user_pw : password, user_retype_pw : password2},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;
			
			if(res.flag == true){
				$(".ajax_admin_add_password2_guide").html(res.msg);
			}
		}
	});
}

//
//	validate password
//
function validate_pw(e){
	var password = $('#admin_pw').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/common/ajax.validate_pw.php',
		data: {user_pw : password},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;
			
			if(res.flag == true){
				$(".ajax_admin_add_password_guide").html(res.msg);
			}
		}
	});
}

//
//	add admin res
//
function admin_add_res_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;

	var page = $('.remem_page').val();
	var search = $('.remem_search').val();
	var searchVal = $('.remem_searchVal').val();

	var remem_admin_id = $('.remem_admin_id').val();
	var remem_admin_enable = $('.remem_admin_enable').val();
	var remem_admin_company = $('.remem_admin_company').val();
	var remem_admin_service = $('.remem_admin_service').val();
	var remem_admin_business_num = $('.remem_admin_business_num').val();
	var remem_admin_name = $('.remem_admin_name').val();
	var remem_admin_product = $('.remem_admin_product').val();
	var remem_admin_pc_cnt = $('.remem_admin_pc_cnt').val();
	var remem_admin_pc_m_n_v_cnt = $('.remem_admin_pc_m_n_v_cnt').val();
	var remem_admin_all_cnt = $('.remem_admin_all_cnt').val();
	var remem_admin_url = $('.remem_admin_url').val();
	var remem_admin_manage_num = $('.remem_admin_manage_num').val();
	var remem_admin_mail = $('.remem_admin_mail').val();
	var remem_admin_memo = $('.remem_admin_memo').val();

	var remem_admin_business_sector = $('.remem_admin_business_sector').val();
	var remem_admin_phone = $('.remem_admin_phone').val();
	var remem_admin_tel = $('.remem_admin_tel').val();
	var remem_admin_service_join_date = $('.remem_admin_service_join_date').val();
	var remem_admin_service_start_date = $('.remem_admin_service_start_date').val();
	var remem_admin_service_expired_date = $('.remem_admin_service_expired_date').val();
	var remem_admin_bill_account = $('.remem_admin_bill_account').val();
	var remem_admin_sign_num = $('.remem_admin_sign_num').val();
	
	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var admin_add_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="admin_add_res_popup_action" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');

	var admin_id = $('<input type="hidden" value="'+remem_admin_id+'" name="admin_id" />');
	var admin_enable = $('<input type="hidden" value="'+remem_admin_enable+'" name="admin_enable" />');
	var admin_company = $('<input type="hidden" value="'+remem_admin_company+'" name="admin_company" />');
	var admin_service = $('<input type="hidden" value="'+remem_admin_service+'" name="admin_service" />');
	var admin_business_num = $('<input type="hidden" value="'+remem_admin_business_num+'" name="admin_business_num" />');
	var admin_name = $('<input type="hidden" value="'+remem_admin_name+'" name="admin_name" />');
	var admin_product = $('<input type="hidden" value="'+remem_admin_product+'" name="admin_product" />');
	var admin_pc_cnt = $('<input type="hidden" value="'+remem_admin_pc_cnt+'" name="admin_pc_cnt" />');
	var admin_pc_m_n_v_cnt = $('<input type="hidden" value="'+remem_admin_pc_m_n_v_cnt+'" name="admin_pc_m_n_v_cnt" />');
	var admin_all_cnt = $('<input type="hidden" value="'+remem_admin_all_cnt+'" name="admin_all_cnt" />');
	var admin_url = $('<input type="hidden" value="'+remem_admin_url+'" name="admin_url" />');
	var admin_manage_num = $('<input type="hidden" value="'+remem_admin_manage_num+'" name="admin_manage_num" />');
	var admin_mail = $('<input type="hidden" value="'+remem_admin_mail+'" name="admin_mail" />');
	var admin_memo = $('<input type="hidden" value="'+remem_admin_memo+'" name="admin_memo" />');

	var admin_business_sector = $('<input type="hidden" value="'+remem_admin_business_sector+'" name="admin_business_sector" />');
	var admin_phone = $('<input type="hidden" value="'+remem_admin_phone+'" name="admin_phone" />');
	var admin_tel = $('<input type="hidden" value="'+remem_admin_tel+'" name="admin_tel" />');
	var admin_service_join_date = $('<input type="hidden" value="'+remem_admin_service_join_date+'" name="admin_service_join_date" />');
	var admin_service_start_date = $('<input type="hidden" value="'+remem_admin_service_start_date+'" name="admin_service_start_date" />');
	var admin_service_expired_date = $('<input type="hidden" value="'+remem_admin_service_expired_date+'" name="admin_service_expired_date" />');
	var admin_bill_account = $('<input type="hidden" value="'+remem_admin_bill_account+'" name="admin_bill_account" />');
	var admin_sign_num = $('<input type="hidden" value="'+remem_admin_sign_num+'" name="admin_sign_num" />');

	$form.append(admin_add_res_popup_action).append(page_param).append(search_param).append(searchVal_param).
	append(admin_id).append(admin_enable).append(admin_company).append(admin_service).append(admin_business_num).append(admin_name).append(admin_product)
	.append(admin_pc_cnt).append(admin_pc_m_n_v_cnt).append(admin_all_cnt).append(admin_url).append(admin_manage_num).append(admin_mail).append(admin_memo)
	.append(admin_business_sector).append(admin_phone).append(admin_tel).append(admin_service_join_date)
	.append(admin_service_start_date).append(admin_service_expired_date).append(admin_bill_account).append(admin_sign_num);
	
	$form.submit();
}

//
//	check_cnt
//
function check_cnt(e){
	var key_error_msg = "숫자만 입력이 가능 합니다.";

	//문제 없이 입력되는 경우
	if( (e.keyCode > 47 && e.keyCode < 58) //숫자
		|| (e.keyCode > 95 && e.keyCode < 106) //키패드 숫자
		|| (e.keyCode > 36 && e.keyCode < 41) //방향키
		|| e.keyCode == 8 //backspace
		|| e.keyCode == 46 //delete
	  ){ 

		$('.admin_agent_cnt_msg').html('');
	}

	//그렇지 않은 경우. 빨간 글씨로 경고문구 띄움
	else{
		$('.admin_agent_cnt_msg').html(key_error_msg);
		e.preventDefault();
	}
}

//
//	apply_volume
//
function apply_volume(e){
	var volume_pc = Number($('#admin_pc_cnt').val());
	var volume_mobile = Number($('#admin_pc_m_n_v_cnt').val());
	
	$('#admin_all_cnt').val( volume_pc + volume_mobile );
}

//
//	create admin
//
function create_admin(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	document.adminAddForm.submit();
}

//
//	go to list
//
function admin_list(e, page, search, searchVal){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var $form = $('<form></form>');
	$form.attr('action', '/super/manage/customer/paid/list.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');
	
	$form.append(page_param).append(search_param).append(searchVal_param);
	
	$form.submit();
}