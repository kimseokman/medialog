/**
 * super / manage / customer / free / info.js
 * 2015.08.31 | KSM
 */

(function($){
	$('#admin_pw').on('keyup', validate_pw);
	$('#admin_retype_pw').on('keyup', validate_retype_pw);

	$('#admin_pc_cnt').on('keydown', check_cnt);
	$('#admin_pc_m_n_v_cnt').on('keydown', check_cnt);

	$('#admin_pc_cnt').on('change', apply_volume);
	$('#admin_pc_m_n_v_cnt').on('change', apply_volume);

	//popup : recent_login_by_agent
	$('.close_pop_recent_login_by_agent').on('click', {action_str: "close"}, recent_login_by_agent_pop);
	$('.confirm_pop_recent_login_by_agent').on('click', {action_str: "close"}, recent_login_by_agent_pop);
	
	$('.edit_admin_btn').on('click', edit_admin);

	//popup : delete admin
	$('.no_pop_admin_del').on('click', {action_str: "close"}, del_admin_pop);
	$('.close_pop_admin_del').on('click', {action_str: "close"}, del_admin_pop);

	$('.yes_pop_admin_del').on('click', del_admin);

	$('.close_pop_admin_add_res').on('click', {action_str: "close"}, admin_add_res_pop);
	$('.confirm_pop_admin_add_res').on('click', {action_str: "close"}, admin_add_res_pop);

	//popup : edit admin res
	$('.close_pop_admin_edit_res').on('click', {action_str: "close"}, edit_admin_res_pop);
	$('.confirm_pop_admin_edit_res').on('click', {action_str: "close"}, edit_admin_res_pop);

	$( "#admin_service_join_date" ).datepicker({
		showOn: "button",
		buttonImage: "/_public/images/super/calendar.gif",
		buttonImageOnly: true,
		buttonText: "Select date",
		dateFormat: 'yy-mm-dd',
		beforeShow: function() {
			setTimeout(function(){
				$('.ui-datepicker').css('z-index', 99999999999999);
			}, 0);
		}
	});

	$( "#admin_service_start_date" ).datepicker({
		showOn: "button",
		buttonImage: "/_public/images/super/calendar.gif",
		buttonImageOnly: true,
		buttonText: "Select date",
		dateFormat: 'yy-mm-dd',
		beforeShow: function() {
			setTimeout(function(){
				$('.ui-datepicker').css('z-index', 99999999999999);
			}, 0);
		}
	});

	$( "#admin_service_expired_date" ).datepicker({
		showOn: "button",
		buttonImage: "/_public/images/super/calendar.gif",
		buttonImageOnly: true,
		buttonText: "Select date",
		dateFormat: 'yy-mm-dd',
		beforeShow: function() {
			setTimeout(function(){
				$('.ui-datepicker').css('z-index', 99999999999999);
			}, 0);
		}
	});

	$(".pw_reset").on("click", unlock_pw);
})(jQuery);

//
//	add admin res
//
function admin_add_res_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;

	var page = $('.remem_page').val();
	var search = $('.remem_search').val();
	var searchVal = $('.remem_searchVal').val();

	var remem_admin_id = $('.admin_id').val();
	
	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var admin_add_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="admin_add_res_popup_action" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');

	var adid_param = $('<input type="hidden" value="'+remem_admin_id+'" name="adid" />');
	var admin_id = $('<input type="hidden" value="'+remem_admin_id+'" name="admin_id" />');

	$form.append(admin_add_res_popup_action).append(page_param).append(search_param).append(searchVal_param).
	append(admin_id).append(adid_param);
	
	$form.submit();
}

//
//	validate retype password
//
function validate_retype_pw(e){
	var password = $('#admin_pw').val();
	var password2 = $('#admin_retype_pw').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/common/ajax.validate_retype_pw.php',
		data: {user_pw : password, user_retype_pw : password2},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;
			
			if(res.flag == true){
				$(".ajax_admin_edit_password2_guide").html(res.msg);
			}
		}
	});
}

//
//	validate password
//
function validate_pw(e){
	var password = $('#admin_pw').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/common/ajax.validate_pw.php',
		data: {user_pw : password},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;
			
			if(res.flag == true){
				$(".ajax_admin_edit_password_guide").html(res.msg);
			}
		}
	});
}

//
//	popup : edit admin res
//
function edit_admin_res_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;

	var adid = $('.remem_adid').val();
	var page = $('.remem_page').val();
	var search = $('.remem_search').val();
	var searchVal = $('.remem_searchVal').val();

	var $form = $('<form></form>');
	$form.attr('action', '/super/manage/customer/free/info.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var admin_edit_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="admin_edit_res_popup_action" />');
	var adid_param = $('<input type="hidden" value="'+adid+'" name="adid" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');
	
	$form.append(admin_edit_res_popup_action).append(adid_param).append(page_param).append(search_param).append(searchVal_param);
	
	$form.submit();
}

//
//	DELETE : Admin
//
function del_admin(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var admin_act_type = $('.admin_act_type').val();

	var adid = $('.remem_adid').val();
	var page = $('.remem_page').val();
	var search = $('.remem_search').val();
	var searchVal = $('.remem_searchVal').val();

	var $form = $('<form></form>');
	$form.attr('action', '/_func/function.delete_admin.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var admin_act_type_param = $('<input type="hidden" value="'+admin_act_type+'" name="admin_act_type" />');
	var adid_param = $('<input type="hidden" value="'+adid+'" name="adid" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');
	
	$form.append(adid_param).append(page_param).append(search_param).append(searchVal_param).append(admin_act_type_param);
	
	$form.submit();
}

//
//	popup : del_admin_pop (close)
//
function del_admin_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;

	var adid = $('.remem_adid').val();
	var page = $('.remem_page').val();
	var search = $('.remem_search').val();
	var searchVal = $('.remem_searchVal').val();

	var $form = $('<form></form>');
	$form.attr('action', '/super/manage/customer/free/info.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var admin_del_popup_action = $('<input type="hidden" value="'+pop_action+'" name="admin_del_popup_action" />');
	var adid_param = $('<input type="hidden" value="'+adid+'" name="adid" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');
	
	$form.append(admin_del_popup_action).append(adid_param).append(page_param).append(search_param).append(searchVal_param);
	
	$form.submit();
}

//
//	popup: del_admin_pop (open)
//
function open_pop_admin_del(e, adid){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = 'open';

	var page = $('.page').val();
	var search = $('.search').val();
	var searchVal = $('.searchVal').val();

	var $form = $('<form></form>');
	$form.attr('action', '/super/manage/customer/free/info.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var admin_del_popup_action = $('<input type="hidden" value="'+pop_action+'" name="admin_del_popup_action" />');
	var adid_param = $('<input type="hidden" value="'+adid+'" name="adid" />');
	var remem_adid_param = $('<input type="hidden" value="'+adid+'" name="remem_adid" />');
	var remem_page_param = $('<input type="hidden" value="'+page+'" name="remem_page" />');
	var remem_search_param = $('<input type="hidden" value="'+search+'" name="remem_search" />');
	var remem_searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="remem_searchVal" />');
	
	$form.append(admin_del_popup_action).append(adid_param).append(remem_adid_param).append(remem_page_param).append(remem_search_param).append(remem_searchVal_param);
	
	$form.submit();
}

//
//	EDIT : Admin
//
function edit_admin(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	document.adminEditForm.submit();
}

//
//	POPUP : recent_login_by_agent_pop (close)
//
function recent_login_by_agent_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;

	var adid = $('.remem_adid').val();
	var page = $('.remem_page').val();
	var search = $('.remem_search').val();
	var searchVal = $('.remem_searchVal').val();

	var $form = $('<form></form>');
	$form.attr('action', '/super/manage/customer/free/info.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var recent_login_by_agent_popup_action = $('<input type="hidden" value="'+pop_action+'" name="recent_login_by_agent_popup_action" />');
	var adid_param = $('<input type="hidden" value="'+adid+'" name="adid" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');
	
	$form.append(recent_login_by_agent_popup_action).append(adid_param).append(page_param).append(search_param).append(searchVal_param);
	
	$form.submit();
}
	

//
//	POPUP : open_pop_recent_login_by_agent (open)
//
function open_pop_recent_login_by_agent(e, adid){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = 'open';

	var page = $('.page').val();
	var search = $('.search').val();
	var searchVal = $('.searchVal').val();

	var $form = $('<form></form>');
	$form.attr('action', '/super/manage/customer/free/info.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var recent_login_by_agent_popup_action = $('<input type="hidden" value="'+pop_action+'" name="recent_login_by_agent_popup_action" />');
	var adid_param = $('<input type="hidden" value="'+adid+'" name="adid" />');
	var remem_adid_param = $('<input type="hidden" value="'+adid+'" name="remem_adid" />');
	var remem_page_param = $('<input type="hidden" value="'+page+'" name="remem_page" />');
	var remem_search_param = $('<input type="hidden" value="'+search+'" name="remem_search" />');
	var remem_searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="remem_searchVal" />');
	
	$form.append(recent_login_by_agent_popup_action).append(adid_param).append(remem_adid_param).append(remem_page_param).append(remem_search_param).append(remem_searchVal_param);
	
	$form.submit();
}


//
//	check_cnt
//
function check_cnt(e){
	var key_error_msg = "숫자만 입력이 가능 합니다.";

	//문제 없이 입력되는 경우
	if( (e.keyCode > 47 && e.keyCode < 58) //숫자
		|| (e.keyCode > 95 && e.keyCode < 106) //키패드 숫자
		|| (e.keyCode > 36 && e.keyCode < 41) //방향키
		|| e.keyCode == 8 //backspace
		|| e.keyCode == 46 //delete
	  ){ 

		$('.admin_agent_cnt_msg').html('');
	}

	//그렇지 않은 경우. 빨간 글씨로 경고문구 띄움
	else{
		$('.admin_agent_cnt_msg').html(key_error_msg);
		e.preventDefault();
	}
}

//
//	apply_volume
//
function apply_volume(e){
	var volume_pc = Number($('#admin_pc_cnt').val());
	var volume_mobile = Number($('#admin_pc_m_n_v_cnt').val());
	
	$('#admin_all_cnt').val( volume_pc + volume_mobile );
}

//
//	go to list
//
function admin_list(e, page, search, searchVal){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var $form = $('<form></form>');
	$form.attr('action', '/super/manage/customer/free/list.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');
	
	$form.append(page_param).append(search_param).append(searchVal_param);
	
	$form.submit();
}

function unlock_pw(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var adid = $('.unlock_pw_adid').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/common/ajax.unlock_pw_admin.php',
		data: {id : adid},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			if(res.flag == true){
				$(".pw_status").html(res.msg);

				var pw_status = $(".pw_status").text();

				if(pw_status == "UNLOCK"){
					$(".pw_status").removeClass("imp");
					if(!$(".pw_status").hasClass("noti")){
						$(".pw_status").addClass("noti");
					}
				}
				else{
					$(".pw_status").removeClass("noti");
					if(!$(".pw_status").hasClass("imp")){
						$(".pw_status").addClass("imp");
					}
				}
			}
		}
	});
}