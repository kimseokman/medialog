/**
 * super / manage / customer / free / list.js
 * 2015.08.24 | KSM
 */

(function($){
	//checkout popup
	$('.close_pop_checkout_res').on('click', {action_str: "close"}, checkout_res_pop);
	$('.confirm_pop_checkout_res').on('click', {action_str: "close"}, checkout_res_pop);
	//search
	$('.admin_search_btn').on('click', search_admin);
	$('.admin_search_val').on('keydown', search_admin_keyevent);
	//add
	$('.add_admin_btn').on('click', add_admin);
	//add admin res popup
	$('.close_pop_admin_get_res').on('click', {action_str: "close"}, admin_get_res_pop);
	$('.confirm_pop_admin_get_res').on('click', {action_str: "close"}, admin_get_res_pop);
	//del admin res popup
	$('.close_pop_admin_del_res').on('click', {action_str: "close"}, admin_del_res_pop);
	$('.confirm_pop_admin_del_res').on('click', {action_str: "close"}, admin_del_res_pop);
})(jQuery);

//
//	POPUP : admin_del_res_pop
//
function admin_del_res_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;
	
	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var admin_del_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="admin_del_res_popup_action" />');
	
	$form.append(admin_del_res_popup_action);
	
	$form.submit();
}

//
//	POPUP : admin_get_res_pop
//
function admin_get_res_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;

	var page = $('.remem_page').val();
	var search = $('.remem_search').val();
	var searchVal = $('.remem_searchVal').val();
	
	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var admin_get_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="admin_get_res_popup_action" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');
	
	$form.append(admin_get_res_popup_action).append(page_param).append(search_param).append(searchVal_param);
	
	$form.submit();
}

//
//	INFO : admin
//
function info_admin(e, adid){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var page = $('.page').val();
	var search = $('.search').val();
	var searchVal = $('.searchVal').val();

	var $form = $('<form></form>');
	$form.attr('action', '/super/manage/customer/free/info.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var adid_param = $('<input type="hidden" value="'+adid+'" name="adid" />');

	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');

	$form.append(adid_param).append(page_param).append(search_param).append(searchVal_param);

	$form.submit();
}

//
//	ADD : admin
//
function add_admin(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var page = $('.page').val();
	var search = $('.search').val();
	var searchVal = $('.searchVal').val();

	var $form = $('<form></form>');
	$form.attr('action', '/super/manage/customer/free/add.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');

	$form.append(page_param).append(search_param).append(searchVal_param);

	$form.submit();
}

//
//	CHECKOUT POP ( close )
//
function checkout_res_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;

	var page = $('.remem_page').val();
	var search = $('.remem_search').val();
	var searchVal = $('.remem_searchVal').val();
	
	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var checkout_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="checkout_res_popup_action" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');
	
	$form.append(checkout_res_popup_action).append(page_param).append(search_param).append(searchVal_param);
	
	$form.submit();
}

//
//	CHECKOUT : ADMIN
//
function checkout_admin(e, adid, checkout_pw){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var page = $('.page').val();
	var search = $('.search').val();
	var searchVal = $('.searchVal').val();

	var $form = $('<form></form>');
	$form.attr('action', '/_func/function.checkout_admin.php');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var adid_param = $('<input type="hidden" value="'+adid+'" name="adid" />');
	var checkout_pw_param = $('<input type="hidden" value="'+checkout_pw+'" name="checkout_pw" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');
	
	$form.append(adid_param).append(checkout_pw_param).append(page_param).append(search_param).append(searchVal_param);
	
	$form.submit();
}

//
//	search keyevent trigger
//
function search_admin_trigger(){
	$('.admin_search_btn').trigger('click');
}

function search_admin_keyevent(e){
	if(e.keyCode == 13){//case by 'ENTER'
		search_admin_trigger();
	}
}

//
//	search : user
//
function search_admin(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var page = 1;
	var sel_index = $('.admin_search_box .jq_sel ul li').find('a.selected').attr('index');
	var search_type = $('.admin_search_box .jq_sel_hide option').eq(sel_index).val();
	var search_val = $('.admin_search_box .admin_search_val').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/super/free/admin/ajax.list.php',
		data: {page: page, search:search_type, searchVal:search_val},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_admin_list").html(res);
		}
	});
}

//
//	paging : user
//
function admin_pagination(e, page_num){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var page = page_num;

	$.ajax({
		type: "POST",
		url:'/_ajax/super/free/admin/ajax.list.php',
		data: {page: page},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_admin_list").html(res);
		}
	});
}