/**
 * setting / notice / info.js
 * 2015.08.20 | KSM
 */
(function($){
	$('.go_to_notice_list_btn').on('click', go_to_notice_list);

	$('.create_notice_btn').on('click', create_notice);
})(jQuery);

function create_notice(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	document.createNoticeForm.submit();
}

//
//	go list
//
function go_to_notice_list(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var remember_page = $('.remember_page').val();
	var remember_type = $('.remember_type').val();
	var remember_val = $('.remember_val').val();

	var $form = $('<form></form>');
	$form.attr('action', '/super/manage/service/notice/list.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var page_param = $('<input type="hidden" value="'+remember_page+'" name="page" />');
	var search_type_param = $('<input type="hidden" value="'+remember_type+'" name="search_type" />');
	var search_val_param = $('<input type="hidden" value="'+remember_val+'" name="search_val" />');

	$form.append(page_param).append(search_type_param).append(search_val_param);

	$form.submit();
}