/**
 * setting / notice / info.js
 * 2015.08.20 | KSM
 */
(function($){
	$('.go_to_notice_list_btn').on('click', go_to_notice_list);

	//popup : delete admin
	$('.no_pop_notice_del').on('click', {action_str: "close"}, del_notice_pop);
	$('.close_pop_notice_del').on('click', {action_str: "close"}, del_notice_pop);

	$('.yes_pop_notice_del').on('click', del_notice);

	//edit notice
	$('.edit_notice_btn').on('click', edit_notice);
})(jQuery);

//
//	edit : notice
//
function edit_notice(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	
	document.editNoticeForm.submit();
}

//
//	delete :  noitce
//
function del_notice(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var remember_seq = $('.remember_seq').val();

	var $form = $('<form></form>');
	$form.attr('action', '/_func/function.delete_notice.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var seq_param = $('<input type="hidden" value="'+remember_seq+'" name="seq" />');

	$form.append(seq_param);

	$form.submit();
}

//
//	popup : del notice
//
function del_notice_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;

	var remember_seq = $('.remember_seq').val();

	var remember_page = $('.remember_page').val();
	var remember_type = $('.remember_type').val();
	var remember_val = $('.remember_val').val();

	var $form = $('<form></form>');
	$form.attr('action', '');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var notice_del_popup_action = $('<input type="hidden" value="'+pop_action+'" name="notice_del_popup_action" />');
	var seq_param = $('<input type="hidden" value="'+remember_seq+'" name="seq" />');
	var page_param = $('<input type="hidden" value="'+remember_page+'" name="remember_page" />');
	var search_type_param = $('<input type="hidden" value="'+remember_type+'" name="remember_type" />');
	var search_val_param = $('<input type="hidden" value="'+remember_val+'" name="remember_val" />');

	$form.append(notice_del_popup_action).append(seq_param).append(page_param).append(search_type_param).append(search_val_param);

	$form.submit();
}

//
//	popup : del notice
//
function open_pop_notice_del(e, seq){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = 'open';

	var remember_page = $('.remember_page').val();
	var remember_type = $('.remember_type').val();
	var remember_val = $('.remember_val').val();

	var $form = $('<form></form>');
	$form.attr('action', '');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var notice_del_popup_action = $('<input type="hidden" value="'+pop_action+'" name="notice_del_popup_action" />');
	var seq_param = $('<input type="hidden" value="'+seq+'" name="seq" />');
	var remem_seq_param = $('<input type="hidden" value="'+seq+'" name="remember_seq" />');
	var page_param = $('<input type="hidden" value="'+remember_page+'" name="remember_page" />');
	var search_type_param = $('<input type="hidden" value="'+remember_type+'" name="remember_type" />');
	var search_val_param = $('<input type="hidden" value="'+remember_val+'" name="remember_val" />');

	$form.append(notice_del_popup_action).append(seq_param).append(remem_seq_param).append(page_param).append(search_type_param).append(search_val_param);

	$form.submit();
}

//
//	go list
//
function go_to_notice_list(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var remember_page = $('.remember_page').val();
	var remember_type = $('.remember_type').val();
	var remember_val = $('.remember_val').val();

	var $form = $('<form></form>');
	$form.attr('action', '/super/manage/service/notice/list.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var page_param = $('<input type="hidden" value="'+remember_page+'" name="page" />');
	var search_type_param = $('<input type="hidden" value="'+remember_type+'" name="search_type" />');
	var search_val_param = $('<input type="hidden" value="'+remember_val+'" name="search_val" />');

	$form.append(page_param).append(search_type_param).append(search_val_param);

	$form.submit();
}