/**
 * manage / support / history / list.js
 * 2015.08.06 | KSM
 */
(function($){
	//search
	$('.support_history_search').on('click', search_support_history);
	$('.support_history_search_keyword').on('keydown', search_support_history_keyevent);
	//export
	$('.export_support_history_to_excel').on('click', export_support_histroy_to_excel);

	//force search
	$('.week_a').on('click', {mode: 'week', term_day: 7}, force_search);
	$('.month_1_a').on('click', {mode: 'month', term_month: 1}, force_search);
	$('.month_3_a').on('click', {mode: 'month', term_month: 3}, force_search);
	$('.month_6_a').on('click', {mode: 'month', term_month: 6}, force_search);

	$('.close_pop_detail_report').on('click', {action_str: "close"}, pop_detail_report);
	$('.confirm_pop_detail_report').on('click', {action_str: "close"}, pop_detail_report);

	//popup
	$('.close_pop_search_res').on('click', {action_str: "close"}, search_res_popup);
	$('.confirm_pop_search_res').on('click', {action_str: "close"}, search_res_popup);
})(jQuery);

//
//	popup : detail report(close)
//
function pop_detail_report(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;

	var page = $('.remember_page').val();
	var sel_index = $('.support_history_search_keyword_box .jq_sel ul li').find('a.selected').attr('index');
	var search_type = $('.support_history_search_keyword_box .jq_sel_hide option').eq(sel_index).val();
	var search_val = $('.support_history_search_keyword').val();

	var start_YYYY_index = $('.support_history_search_term_box .start_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var startYear = $('.support_history_search_term_box .start_YYYY .jq_sel_hide option').eq(start_YYYY_index).val();
	var start_MM_index = $('.support_history_search_term_box .start_MM .jq_sel ul li').find('a.selected').attr('index');
	var startMonth = $('.support_history_search_term_box .start_MM .jq_sel_hide option').eq(start_MM_index).val();
	var start_DD_index = $('.support_history_search_term_box .start_DD .jq_sel ul li').find('a.selected').attr('index');
	var startDay = $('.support_history_search_term_box .start_DD .jq_sel_hide option').eq(start_DD_index).val();

	var start_time = startYear +"-"+ startMonth +"-"+ startDay;

	var end_YYYY_index = $('.support_history_search_term_box .end_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var endYear = $('.support_history_search_term_box .end_YYYY .jq_sel_hide option').eq(end_YYYY_index).val();
	var end_MM_index = $('.support_history_search_term_box .end_MM .jq_sel ul li').find('a.selected').attr('index');
	var endMonth = $('.support_history_search_term_box .end_MM .jq_sel_hide option').eq(end_MM_index).val();
	var end_DD_index = $('.support_history_search_term_box .end_DD .jq_sel ul li').find('a.selected').attr('index');
	var endDay = $('.support_history_search_term_box .end_DD .jq_sel_hide option').eq(end_DD_index).val();

	var end_time = endYear +"-"+ endMonth +"-"+ endDay;

	var $form = $('<form></form>');
	$form.attr('action', '');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var detail_report_popup_action = $('<input type="hidden" value="'+pop_action+'" name="detail_report_popup_action" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var input_search_type = $('<input type="hidden" value="'+search_type+'" name="search_type" />');
	var input_search_val = $('<input type="hidden" value="'+search_val+'" name="search_val" />');
	var input_start_time = $('<input type="hidden" value="'+start_time+'" name="start_time" />');
	var input_end_time = $('<input type="hidden" value="'+end_time+'" name="end_time" />');
	
	$form.append(detail_report_popup_action).append(page_param).append(input_search_type).append(input_search_val).append(input_start_time).append(input_end_time);
	
	$form.submit();
}

//
//	popup : detail report(open)
//
function open_pop_detail_report(e, report_num, page){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = 'open';

	var sel_index = $('.support_history_search_keyword_box .jq_sel ul li').find('a.selected').attr('index');
	var search_type = $('.support_history_search_keyword_box .jq_sel_hide option').eq(sel_index).val();
	var search_val = $('.support_history_search_keyword').val();

	var start_YYYY_index = $('.support_history_search_term_box .start_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var startYear = $('.support_history_search_term_box .start_YYYY .jq_sel_hide option').eq(start_YYYY_index).val();
	var start_MM_index = $('.support_history_search_term_box .start_MM .jq_sel ul li').find('a.selected').attr('index');
	var startMonth = $('.support_history_search_term_box .start_MM .jq_sel_hide option').eq(start_MM_index).val();
	var start_DD_index = $('.support_history_search_term_box .start_DD .jq_sel ul li').find('a.selected').attr('index');
	var startDay = $('.support_history_search_term_box .start_DD .jq_sel_hide option').eq(start_DD_index).val();

	var start_time = startYear +"-"+ startMonth +"-"+ startDay;

	var end_YYYY_index = $('.support_history_search_term_box .end_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var endYear = $('.support_history_search_term_box .end_YYYY .jq_sel_hide option').eq(end_YYYY_index).val();
	var end_MM_index = $('.support_history_search_term_box .end_MM .jq_sel ul li').find('a.selected').attr('index');
	var endMonth = $('.support_history_search_term_box .end_MM .jq_sel_hide option').eq(end_MM_index).val();
	var end_DD_index = $('.support_history_search_term_box .end_DD .jq_sel ul li').find('a.selected').attr('index');
	var endDay = $('.support_history_search_term_box .end_DD .jq_sel_hide option').eq(end_DD_index).val();

	var end_time = endYear +"-"+ endMonth +"-"+ endDay;

	var $form = $('<form></form>');
	$form.attr('action', '');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var detail_report_popup_action = $('<input type="hidden" value="'+pop_action+'" name="detail_report_popup_action" />');
	var detail_report_num_param = $('<input type="hidden" value="'+report_num+'" name="detail_report_num" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var input_search_type = $('<input type="hidden" value="'+search_type+'" name="search_type" />');
	var input_search_val = $('<input type="hidden" value="'+search_val+'" name="search_val" />');
	var input_start_time = $('<input type="hidden" value="'+start_time+'" name="start_time" />');
	var input_end_time = $('<input type="hidden" value="'+end_time+'" name="end_time" />');
	
	$form.append(detail_report_popup_action).append(detail_report_num_param).append(page_param).append(input_search_type).append(input_search_val).append(input_start_time).append(input_end_time);
	
	$form.submit();
}

//
//	search : force
// 
function force_search(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	$('.force_search').removeClass('on');
	$(this).addClass('on');

	var now = new Date();
	var end_YYYY = now.getFullYear();
	var end_month = now.getMonth()+1;
	var end_MM = (end_month<10 ? '0' : '') + end_month;
	var end_day = now.getDate();
	var end_DD = (end_day<10 ? '0' : '') + end_day;

	//apply end date select box : end YYYY
	var end_year_index = 0;
	$('.support_history_search_term_box .end_YYYY .jq_sel ul li').each(function(index){
		if($( this ).text() == end_YYYY){
			end_year_index = index;
		}
	});
	$('.support_history_search_term_box .end_YYYY .jq_sel div span').text(end_YYYY);
	$('.support_history_search_term_box .end_YYYY .jq_sel ul li a').removeClass('selected');
	$('.support_history_search_term_box .end_YYYY .jq_sel ul li a').eq(end_year_index).addClass('selected');
	var end_YYYY_index = $('.support_history_search_term_box .end_YYYY .jq_sel ul li').find('a.selected').attr('index');
	$('.support_history_search_term_box .end_YYYY .jq_sel_hide option').attr('selected', false);
	$('.support_history_search_term_box .end_YYYY .jq_sel_hide option').eq(end_YYYY_index).attr('selected', true);

	//apply end date select box : end MM
	var end_month_index = 0;
	$('.support_history_search_term_box .end_MM .jq_sel ul li').each(function(index){
		if($( this ).text() == end_MM){
			end_month_index = index;
		}
	});
	$('.support_history_search_term_box .end_MM .jq_sel div span').text(end_MM);
	$('.support_history_search_term_box .end_MM .jq_sel ul li a').removeClass('selected');
	$('.support_history_search_term_box .end_MM .jq_sel ul li a').eq(end_month_index).addClass('selected');
	var end_MM_index = $('.support_history_search_term_box .end_MM .jq_sel ul li').find('a.selected').attr('index');
	$('.support_history_search_term_box .end_MM .jq_sel_hide option').attr('selected', false);
	$('.support_history_search_term_box .end_MM .jq_sel_hide option').eq(end_MM_index).attr('selected', true);

	//apply end date select box : end DD
	var end_day_index = 0;
	$('.support_history_search_term_box .end_DD .jq_sel ul li').each(function(index){
		if($( this ).text() == end_DD){
			end_day_index = index;
		}
	});
	$('.support_history_search_term_box .end_DD .jq_sel div span').text(end_DD);
	$('.support_history_search_term_box .end_DD .jq_sel ul li a').removeClass('selected');
	$('.support_history_search_term_box .end_DD .jq_sel ul li a').eq(end_day_index).addClass('selected');
	var end_DD_index = $('.support_history_search_term_box .end_DD .jq_sel ul li').find('a.selected').attr('index');
	$('.support_history_search_term_box .end_DD .jq_sel_hide option').attr('selected', false);
	$('.support_history_search_term_box .end_DD .jq_sel_hide option').eq(end_DD_index).attr('selected', true);

	var start = new Date();
	var interval = 0;
	if(e.data.mode == 'week'){
		interval = e.data.term_day;

		start.setDate(start.getDate() - interval);
	}
	else if(e.data.mode == 'month'){
		interval = e.data.term_month;

		start.setFullYear(start.getFullYear(), start.getMonth() - interval);
	}

	var start_YYYY = start.getFullYear();
	var start_month = start.getMonth()+1;
	var start_MM = (start_month<10 ? '0' : '') + start_month;
	var start_day = start.getDate();
	var start_DD = (start_day<10 ? '0' : '') + start_day;

	//apply start date select box : start YYYY
	var start_year_index = 0;
	$('.support_history_search_term_box .start_YYYY .jq_sel ul li').each(function(index){
		if($( this ).text() == start_YYYY){
			start_year_index = index;
		}
	});
	$('.support_history_search_term_box .start_YYYY .jq_sel div span').text(start_YYYY);
	$('.support_history_search_term_box .start_YYYY .jq_sel ul li a').removeClass('selected');
	$('.support_history_search_term_box .start_YYYY .jq_sel ul li a').eq(start_year_index).addClass('selected');
	var start_YYYY_index = $('.support_history_search_term_box .start_YYYY .jq_sel ul li').find('a.selected').attr('index');
	$('.support_history_search_term_box .start_YYYY .jq_sel_hide option').attr('selected', false);
	$('.support_history_search_term_box .start_YYYY .jq_sel_hide option').eq(start_YYYY_index).attr('selected', true);

	//apply start date select box : start MM
	var start_month_index = 0;
	$('.support_history_search_term_box .start_MM .jq_sel ul li').each(function(index){
		if($( this ).text() == start_MM){
			start_month_index = index;
		}
	});
	$('.support_history_search_term_box .start_MM .jq_sel div span').text(start_MM);
	$('.support_history_search_term_box .start_MM .jq_sel ul li a').removeClass('selected');
	$('.support_history_search_term_box .start_MM .jq_sel ul li a').eq(start_month_index).addClass('selected');
	var start_MM_index = $('.support_history_search_term_box .start_MM .jq_sel ul li').find('a.selected').attr('index');
	$('.support_history_search_term_box .start_MM .jq_sel_hide option').attr('selected', false);
	$('.support_history_search_term_box .start_MM .jq_sel_hide option').eq(start_MM_index).attr('selected', true);

	//apply start date select box : start DD
	var start_day_index = 0;
	$('.support_history_search_term_box .start_DD .jq_sel ul li').each(function(index){
		if($( this ).text() == start_DD){
			start_day_index = index;
		}
	});
	$('.support_history_search_term_box .start_DD .jq_sel div span').text(start_DD);
	$('.support_history_search_term_box .start_DD .jq_sel ul li a').removeClass('selected');
	$('.support_history_search_term_box .start_DD .jq_sel ul li a').eq(start_day_index).addClass('selected');
	var start_DD_index = $('.support_history_search_term_box .start_DD .jq_sel ul li').find('a.selected').attr('index');
	$('.support_history_search_term_box .start_DD .jq_sel_hide option').attr('selected', false);
	$('.support_history_search_term_box .start_DD .jq_sel_hide option').eq(start_DD_index).attr('selected', true);

	//trigger
	
	search_support_history_trigger();
}

//
//	export
//
function export_support_histroy_to_excel(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var sel_index = $('.support_history_search_keyword_box .jq_sel ul li').find('a.selected').attr('index');
	var search_type = $('.support_history_search_keyword_box .jq_sel_hide option').eq(sel_index).val();
	var search_val = $('.support_history_search_keyword').val();

	var start_YYYY_index = $('.support_history_search_term_box .start_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var startYear = $('.support_history_search_term_box .start_YYYY .jq_sel_hide option').eq(start_YYYY_index).val();
	var start_MM_index = $('.support_history_search_term_box .start_MM .jq_sel ul li').find('a.selected').attr('index');
	var startMonth = $('.support_history_search_term_box .start_MM .jq_sel_hide option').eq(start_MM_index).val();
	var start_DD_index = $('.support_history_search_term_box .start_DD .jq_sel ul li').find('a.selected').attr('index');
	var startDay = $('.support_history_search_term_box .start_DD .jq_sel_hide option').eq(start_DD_index).val();

	var start_time = startYear +"-"+ startMonth +"-"+ startDay;

	var end_YYYY_index = $('.support_history_search_term_box .end_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var endYear = $('.support_history_search_term_box .end_YYYY .jq_sel_hide option').eq(end_YYYY_index).val();
	var end_MM_index = $('.support_history_search_term_box .end_MM .jq_sel ul li').find('a.selected').attr('index');
	var endMonth = $('.support_history_search_term_box .end_MM .jq_sel_hide option').eq(end_MM_index).val();
	var end_DD_index = $('.support_history_search_term_box .end_DD .jq_sel ul li').find('a.selected').attr('index');
	var endDay = $('.support_history_search_term_box .end_DD .jq_sel_hide option').eq(end_DD_index).val();

	var end_time = endYear +"-"+ endMonth +"-"+ endDay;

	var $form = $('<form></form>');
	$form.attr('action', '/_export/admin/support_log/export.list.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var input_search_type = $('<input type="hidden" value="'+search_type+'" name="search_type" />');
	var input_search_val = $('<input type="hidden" value="'+search_val+'" name="search_val" />');
	var input_start_time = $('<input type="hidden" value="'+start_time+'" name="start_time" />');
	var input_end_time = $('<input type="hidden" value="'+end_time+'" name="end_time" />');
	
	$form.append(input_search_type).append(input_search_val).append(input_start_time).append(input_end_time);
	
	$form.submit();
}

//
//	search keyevent trigger
//
function search_support_history_trigger(){
	$('.support_history_search').trigger('click');
}

function search_support_history_keyevent(e){
	if(e.keyCode == 13){//case by 'ENTER'
		search_support_history_trigger();
	}
}

//
//	pagination
//
function support_history_pagination(e, page_num){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var page = page_num;
	var sel_index = $('.support_history_search_keyword_box .jq_sel ul li').find('a.selected').attr('index');
	var search_type = $('.support_history_search_keyword_box .jq_sel_hide option').eq(sel_index).val();
	var search_val = $('.support_history_search_keyword').val();

	var start_YYYY_index = $('.support_history_search_term_box .start_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var startYear = $('.support_history_search_term_box .start_YYYY .jq_sel_hide option').eq(start_YYYY_index).val();
	var start_MM_index = $('.support_history_search_term_box .start_MM .jq_sel ul li').find('a.selected').attr('index');
	var startMonth = $('.support_history_search_term_box .start_MM .jq_sel_hide option').eq(start_MM_index).val();
	var start_DD_index = $('.support_history_search_term_box .start_DD .jq_sel ul li').find('a.selected').attr('index');
	var startDay = $('.support_history_search_term_box .start_DD .jq_sel_hide option').eq(start_DD_index).val();

	var start_time = startYear +"-"+ startMonth +"-"+ startDay;

	var end_YYYY_index = $('.support_history_search_term_box .end_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var endYear = $('.support_history_search_term_box .end_YYYY .jq_sel_hide option').eq(end_YYYY_index).val();
	var end_MM_index = $('.support_history_search_term_box .end_MM .jq_sel ul li').find('a.selected').attr('index');
	var endMonth = $('.support_history_search_term_box .end_MM .jq_sel_hide option').eq(end_MM_index).val();
	var end_DD_index = $('.support_history_search_term_box .end_DD .jq_sel ul li').find('a.selected').attr('index');
	var endDay = $('.support_history_search_term_box .end_DD .jq_sel_hide option').eq(end_DD_index).val();

	var end_time = endYear +"-"+ endMonth +"-"+ endDay;

	$.ajax({
		type: "POST",
		url:'/_ajax/admin/support_log/ajax.list.php',
		data: {page: page, search_type: search_type, search_val: search_val, start_time: start_time, end_time: end_time},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_support_history_content").html(res);
		}
	});
}

//
//	search
//
function search_support_history(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var page = 1;
	var sel_index = $('.support_history_search_keyword_box .jq_sel ul li').find('a.selected').attr('index');
	var search_type = $('.support_history_search_keyword_box .jq_sel_hide option').eq(sel_index).val();
	var search_val = $('.support_history_search_keyword').val();

	var start_YYYY_index = $('.support_history_search_term_box .start_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var startYear = $('.support_history_search_term_box .start_YYYY .jq_sel_hide option').eq(start_YYYY_index).val();
	var start_MM_index = $('.support_history_search_term_box .start_MM .jq_sel ul li').find('a.selected').attr('index');
	var startMonth = $('.support_history_search_term_box .start_MM .jq_sel_hide option').eq(start_MM_index).val();
	var start_DD_index = $('.support_history_search_term_box .start_DD .jq_sel ul li').find('a.selected').attr('index');
	var startDay = $('.support_history_search_term_box .start_DD .jq_sel_hide option').eq(start_DD_index).val();

	var start_time = startYear +"-"+ startMonth +"-"+ startDay;

	var end_YYYY_index = $('.support_history_search_term_box .end_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var endYear = $('.support_history_search_term_box .end_YYYY .jq_sel_hide option').eq(end_YYYY_index).val();
	var end_MM_index = $('.support_history_search_term_box .end_MM .jq_sel ul li').find('a.selected').attr('index');
	var endMonth = $('.support_history_search_term_box .end_MM .jq_sel_hide option').eq(end_MM_index).val();
	var end_DD_index = $('.support_history_search_term_box .end_DD .jq_sel ul li').find('a.selected').attr('index');
	var endDay = $('.support_history_search_term_box .end_DD .jq_sel_hide option').eq(end_DD_index).val();

	var end_time = endYear +"-"+ endMonth +"-"+ endDay;

	var startTime = new Date(startYear, startMonth, startDay);
	var endTime = new Date(endYear, endMonth, endDay);
	var currentTime = new Date();
	var oneYearsAgo = new Date().setFullYear(currentTime.getFullYear()-1);

	if(endTime < startTime){//search error
		var $form = $('<form></form>');
		$form.attr('action', '');
		$form.attr('method', 'post');
		$form.appendTo('body');

		var pop_act = "open";
		var pop_msg = "검색 기간을 다시 확인해주세요.";

		var pop_act_param = $('<input type="hidden" value="'+pop_act+'" name="search_res_popup_action" />');
		var pop_msg_param = $('<input type="hidden" value="'+pop_msg+'" name="search_res_popup_msg" />');
		
		$form.append(pop_act_param).append(pop_msg_param);
		
		$form.submit();
	}
	else if(startTime < oneYearsAgo){//1년 전 기간 검색하면
		var $form = $('<form></form>');
		$form.attr('action', '');
		$form.attr('method', 'post');
		$form.appendTo('body');

		var pop_act = "open";
		var pop_msg = "1년이하의 기록만 검색 가능합니다.";

		var pop_act_param = $('<input type="hidden" value="'+pop_act+'" name="search_res_popup_action" />');
		var pop_msg_param = $('<input type="hidden" value="'+pop_msg+'" name="search_res_popup_msg" />');
		
		$form.append(pop_act_param).append(pop_msg_param);
		
		$form.submit();
	}
	else{
		$.ajax({
			type: "POST",
			url:'/_ajax/admin/support_log/ajax.list.php',
			data: {page: page, search_type: search_type, search_val: search_val, start_time: start_time, end_time: end_time},
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			success: function(data){
				var res = data;

				$(".ajax_support_history_content").html(res);
			}
		});
	}
}

function search_res_popup(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;
	
	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var search_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="search_res_popup_action" />');
	
	$form.append(search_res_popup_action);
	
	$form.submit();
}
