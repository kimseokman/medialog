/**
 * manage / support / type / list.js
 * 2015.08.18 | KSM
 */
(function($){
	//edit
	$('.trouble_name_input').on('keydown', edit_type_name_key_event);
	//popup: edit res
	$('.close_pop_type_edit_res').on('click', {action_str : 'close'}, type_edit_res);
	$('.confirm_pop_type_edit_res').on('click', {action_str : 'close'}, type_edit_res);
	//popup: delete
	$('.close_pop_type_del').on('click', {action_str : 'close'}, pop_type_del);
	$('.no_pop_type_del').on('click', {action_str : 'close'}, pop_type_del);
	$('.yes_pop_type_del').on('click', type_del);
	//popup: del res
	$('.close_pop_type_del_res').on('click', {action_str : 'close'}, pop_type_del_res);
	$('.confirm_pop_type_del_res').on('click', {action_str : 'close'}, pop_type_del_res);

	$('.add_support_type_btn').on('click', type_add);
})(jQuery);

//
//	add : type
//
function type_add(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var $form = $('<form></form>');
	$form.attr('action', '/_func/function.create_support_type.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	$form.submit();
}

//
//	edit : order
//
function edit_order(e, trb_num, order, command){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var trbname = document.getElementsByName("trbname");

	var $form = $('<form></form>');
	$form.attr('action', '/_func/function.edit_support_type_order.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var trbnum_param = $('<input type="hidden" value="'+trb_num+'" name="trbnum" />');
	var trbcnt_param = $('<input type="hidden" value="'+trbname.length+'" name="trbcnt" />');
	var trborder_param = $('<input type="hidden" value="'+order+'" name="trborder" />');
	var trbcommand_param = $('<input type="hidden" value="'+command+'" name="trbcommand" />');

	$form.append(trbnum_param).append(trbcnt_param).append(trborder_param).append(trbcommand_param);

	$form.submit();
}

//
//	popup : delete support type res
//
function pop_type_del_res(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;

	var $form = $('<form></form>');
	$form.attr('action', '');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var type_del_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="type_del_res_popup_action" />');

	$form.append(type_del_res_popup_action);

	$form.submit();
}

//
//	delete support type
//
function type_del(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var trbnum = $('.del_trbnum').val();

	var $form = $('<form></form>');
	$form.attr('action', '/_func/function.delete_support_type.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var trbnum_param = $('<input type="hidden" value="'+trbnum+'" name="trbnum" />');

	$form.append(trbnum_param);

	$form.submit();
}

//
//	popup : delete type(close)
//
function pop_type_del(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;

	var $form = $('<form></form>');
	$form.attr('action', '');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var type_del_popup_action = $('<input type="hidden" value="'+pop_action+'" name="type_del_popup_action" />');

	$form.append(type_del_popup_action);

	$form.submit();
}

//
//	popup : delete type(open)
//
function open_pop_type_delete(e, trbnum){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = 'open';

	var $form = $('<form></form>');
	$form.attr('action', '');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var type_del_popup_action = $('<input type="hidden" value="'+pop_action+'" name="type_del_popup_action" />');
	var trbnum_param = $('<input type="hidden" value="'+trbnum+'" name="del_trbnum" />');

	$form.append(type_del_popup_action).append(trbnum_param);

	$form.submit();
}

//
//	popup : edit res
//
function type_edit_res(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;

	var edit_result = $('.type_edit_res').val();
	var fail_trb_num = $('.fail_trb_num').val();

	var $form = $('<form></form>');
	$form.attr('action', '');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var type_edit_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="type_edit_res_popup_action" />');
	$form.append(type_edit_res_popup_action);

	if(edit_result == "FAIL"){//case by fail
		var act_mode = "EDIT";
		var act_mode_param = $('<input type="hidden" value="'+act_mode+'" name="act_mode" />');
		var act_trb_num = $('<input type="hidden" value="'+fail_trb_num+'" name="act_trb_num" />');

		$form.append(act_mode_param).append(act_trb_num);
	}
	
	$form.submit();
}

//
//	edit: type_name
//
function edit_type_name_key_event(e){
	if(e.keyCode == 13){//case by 'ENTER'
		var trbname = $(this).val();//get trbname

		var class_arr = $(this).attr('class').split (" ");
		var trbnum = class_arr[1].split ("_")[1];//get trbnum

		edit_type_name(trbname, trbnum);
	}
}

function edit_type_name_confirm(e, trbnum){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var trbname = $(".trbnum_" + trbnum).val();

	edit_type_name(trbname, trbnum);
}

function edit_type_name(trbname, trbnum){
	var $form = $('<form></form>');
	$form.attr('action', '/_func/function.edit_support_type.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var trbnum_param = $('<input type="hidden" value="'+trbnum+'" name="trbnum" />');
	var trbname_param = $('<input type="hidden" value="'+trbname+'" name="trbname" />');
	
	$form.append(trbnum_param).append(trbname_param);
	
	$form.submit();
}

//
//	mode : edit
//
function type_edit_mode(e, trb_num, mode){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var act_mode ="EDIT";

	var $form = $('<form></form>');
	$form.attr('action', '');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var act_mode_param = $('<input type="hidden" value="'+act_mode+'" name="act_mode" />');
	var act_trb_num = $('<input type="hidden" value="'+trb_num+'" name="act_trb_num" />');
	
	$form.append(act_mode_param).append(act_trb_num);
	
	$form.submit();
}