/**
 * manage / agent / department / list.js
 * 2015.08.05 | KSM
 */
(function($){
	//popup : dept add
	$('.dept_add_btn').on('click', {action_str: "open"}, dept_add_pop);
	$('.close_pop_dept_add').on('click', {action_str: "close"}, dept_add_pop);
	$('.create_dept_btn').on('click', create_dept);
	$('#dept_name').on('keydown', dept_name_keyevent);
	$('#desc').on('keydown', desc_keyevent);

	//popup : dept add res
	$('.close_pop_dept_add_res').on('click', {action_str: "close", action_type: "close"}, dept_add_res_pop);
	$('.confirm_pop_dept_add_res').on('click', {action_str: "close", action_type: "confirm"}, dept_add_res_pop);

	//popup : dept del
	$('.close_pop_dept_del').on('click', {action_str: "close"}, dept_del_pop);
	$('.no_pop_dept_del').on('click', {action_str: "close"}, dept_del_pop);

	//popup : dept del res
	$('.close_pop_dept_del_res').on('click', {action_str: "close"}, dept_del_res_pop);
	$('.confirm_pop_dept_del_res').on('click', {action_str: "close"}, dept_del_res_pop);

	//popup : dept edit res
	$('.close_pop_dept_edit_res').on('click', {action_str: "close"}, dept_edit_res_pop);
	$('.confirm_pop_dept_edit_res').on('click', {action_str: "close"}, dept_edit_res_pop);
})(jQuery);

//
//	popup : edit department respond
//
function dept_edit_res_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;
	var page = $('.remember_page').val();
	var act_mode = $('.remember_act_mode').val();
	var act_dept_num = $('.remember_act_dept_num').val();

	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var dept_edit_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="dept_edit_res_popup_action" />');
	var act_mode_param = $('<input type="hidden" value="'+act_mode+'" name="act_mode" />');
	var act_dept_num_param = $('<input type="hidden" value="'+act_dept_num+'" name="act_dept_num" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	
	$form.append(dept_edit_res_popup_action).append(act_mode_param).append(act_dept_num_param).append(page_param);
	
	$form.submit();
}

//
//	edit : department
//
function dept_edit(e, dept_num, remember_page, act_mode, act_dept_num){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var dept_name = $('.input_dept_name_'+dept_num).val();
	var dept_memo = $('.input_dept_memo_'+dept_num).val();

	var $form = $('<form></form>');
	$form.attr('action', '/_func/function.edit_dept.php');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var dept_num_param = $('<input type="hidden" value="'+dept_num+'" name="dept_num" />');
	var dept_name_param = $('<input type="hidden" value="'+dept_name+'" name="dept_name" />');
	var dept_memo_param = $('<input type="hidden" value="'+dept_memo+'" name="dept_memo" />');
	var act_mode_param = $('<input type="hidden" value="'+act_mode+'" name="act_mode" />');
	var act_dept_num_param = $('<input type="hidden" value="'+act_dept_num+'" name="act_dept_num" />');
	var page_param = $('<input type="hidden" value="'+remember_page+'" name="page" />');
	
	$form.append(dept_num_param).append(dept_name_param).append(dept_memo_param).append(act_mode_param).append(act_dept_num_param).append(page_param);
	
	$form.submit();
}

//
//	change act mode : EDIT
//
function dept_edit_mode(e, dept_num, remember_page){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var mode_action = "EDIT";

	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var act_mode = $('<input type="hidden" value="'+mode_action+'" name="act_mode" />');
	var act_dept_num = $('<input type="hidden" value="'+dept_num+'" name="act_dept_num" />');
	
	var page_param = $('<input type="hidden" value="'+remember_page+'" name="page" />');
	
	$form.append(act_mode).append(act_dept_num).append(page_param);
	
	$form.submit();
}

//
//	popup : dept_del_pop (close)
//
function dept_del_res_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;
	var page = $('.remember_page').val();

	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var dept_del_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="dept_del_res_popup_action" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	
	$form.append(dept_del_res_popup_action).append(page_param);
	
	$form.submit();
}

//
//	delete : department
//
function delete_dept(e, dept_num, remember_page){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var $form = $('<form></form>');
	$form.attr('action', '/_func/function.delete_dept.php');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var dept_num_param = $('<input type="hidden" value="'+dept_num+'" name="dept_num" />');
	var page = $('<input type="hidden" value="'+remember_page+'" name="page" />');
	
	$form.append(dept_num_param).append(page);
	
	$form.submit();
}

//
//	popup : dept_del_pop (close)
//
function dept_del_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;
	var page = $('.remember_page').val();

	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var dept_del_popup_action = $('<input type="hidden" value="'+pop_action+'" name="dept_del_popup_action" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');

	$form.append(dept_del_popup_action).append(page_param);
	
	$form.submit();
}

//
//	delete : department
//
function open_pop_dept_del(e, dept_num, remember_page){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var depnum = dept_num;
	var pop_action = 'open';
	
	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var dept_del_popup_action = $('<input type="hidden" value="'+pop_action+'" name="dept_del_popup_action" />');
	var dept_del_num = $('<input type="hidden" value="'+depnum+'" name="dept_del_num" />');
	var page = $('<input type="hidden" value="'+remember_page+'" name="page" />');

	$form.append(dept_del_popup_action).append(dept_del_num).append(page);
	
	$form.submit();
}

//
//	paging : department
//
function dept_pagination(e, page_num){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var page = page_num;

	$.ajax({
		type: "POST",
		url:'/_ajax/admin/department/ajax.list.php',
		data: {page: page},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_dept_content").html(res);
		}
	});
}

//
//	popup : dept_add_res (open, close)
//
function dept_add_res_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;
	var pop_action_type = e.data.action_type;
	var dept_add_pop_act = '';
	var dept_add_res_popup_label = $('.dept_add_res_popup_label').val();
	
	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var dept_add_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="dept_add_res_popup_action" />');
	var dept_add_res_popup_msg = $('<input type="hidden" value="" name="dept_add_res_popup_msg" />');

	if(dept_add_res_popup_label == "OK"){//생성 성공
		dept_add_pop_act = 'close';
	}
	else{//생성 실패
		if(pop_action_type == "close"){// X 버튼 클릭 
			dept_add_pop_act = "close";
		}
		else{//확인 버튼 클릭
			dept_add_pop_act = "open";
		}
	}

	//trigger
	var dept_add_popup_action = $('<input type="hidden" value="'+dept_add_pop_act+'" name="dept_add_popup_action" />');
	
	$form.append(dept_add_res_popup_action).append(dept_add_res_popup_msg).append(dept_add_popup_action);
	
	$form.submit();
}

//
//	dept_name, dept_memo Keyevent
//
function dept_name_keyevent(e){
	if(e.keyCode == 13){//case by 'ENTER'
		create_dept_trigger();
	}
}

function desc_keyevent(e){
	if(e.keyCode == 13){//case by 'ENTER'
		create_dept_trigger();
	}
}

//
//	trigger : create dept
//
function create_dept_trigger(){
	$('.create_dept_btn').trigger('click');
}

//
//	create department
//
function create_dept(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	document.createDeptForm.submit();//submit
}

//
//	popup : dept_add (open, close)
//
function dept_add_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;
	
	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var dept_add_popup_action = $('<input type="hidden" value="'+pop_action+'" name="dept_add_popup_action" />');
	
	$form.append(dept_add_popup_action);
	
	$form.submit();
}