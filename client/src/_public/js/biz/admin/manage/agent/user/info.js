/**
 * manage / agent / user / info.js
 * 2015.08.06 | KSM
 */
(function($){
	$('.mobile_func.control').on('change', mobile_func_box);

	$('#user_pw').on('keyup', validate_pw);
	$('#user_pw2').on('keyup', validate_retype_pw);

	$('.open_pop_del_user').on('click', {action_str: "open"}, del_user_pop);
	$('.no_pop_user_del').on('click', {action_str: "close"}, del_user_pop);
	$('.close_pop_user_del').on('click', {action_str: "close"}, del_user_pop);
	$('.yes_pop_user_del').on('click', delete_user);

	$('.edit_user').on('click', edit_user);

	$('.close_pop_user_edit_res').on('click', {action_str: "close"}, edit_user_res_pop);
	$('.confirm_pop_user_edit_res').on('click', {action_str: "close"}, edit_user_res_pop);

	$(".pw_reset").on("click", unlock_pw);
})(jQuery);

//
//	popup : edit res
//
function edit_user_res_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var sptnum = $('.sptnum').val();
	var page = $('.page').val();
	var search = $('.search').val();
	var searchVal = $('.searchVal').val();

	var $form = $('<form></form>');
	$form.attr('action', '/admin/manage/agent/user/info.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var sptnum_param = $('<input type="hidden" value="'+sptnum+'" name="sptnum" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');
	
	$form.append(sptnum_param).append(page_param).append(search_param).append(searchVal_param);

	$form.submit();
}

//
//	edit : user
//
function edit_user(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	document.userEditForm.submit();
}

//
//	delete : user
//
function delete_user(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var sptnum = $('.sptnum').val();
	var page = $('.page').val();
	var search = $('.search').val();
	var searchVal = $('.searchVal').val();

	var $form = $('<form></form>');
	$form.attr('action', '/_func/function.delete_user.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var sptnum_param = $('<input type="hidden" value="'+sptnum+'" name="sptnum" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');
	
	$form.append(sptnum_param).append(page_param).append(search_param).append(searchVal_param);

	$form.submit();
}

//
//	popup : delete - user (open, close)
//
function del_user_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;
	var sptnum = $('.sptnum').val();
	var page = $('.page').val();
	var search = $('.search').val();
	var searchVal = $('.searchVal').val();

	var $form = $('<form></form>');
	$form.attr('action', '/admin/manage/agent/user/info.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var pop_act_param = $('<input type="hidden" value="'+pop_action+'" name="user_del_popup_action" />');
	var sptnum_param = $('<input type="hidden" value="'+sptnum+'" name="sptnum" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');
	
	$form.append(pop_act_param).append(sptnum_param).append(page_param).append(search_param).append(searchVal_param);
	
	$form.submit();
}

//
//	validate retype password
//
function validate_retype_pw(e){
	var password = $('#user_pw').val();
	var password2 = $('#user_pw2').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/common/ajax.validate_retype_pw.php',
		data: {user_pw : password, user_retype_pw : password2},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;
			
			if(res.flag == true){
				$(".ajax_user_edit_password2_guide").html(res.msg);
			}
		}
	});
}

//
//	validate password
//
function validate_pw(e){
	var password = $('#user_pw').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/common/ajax.validate_pw.php',
		data: {user_pw : password},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;
			
			if(res.flag == true){
				$(".ajax_user_edit_password_guide").html(res.msg);
			}
		}
	});
}

//
//	go to user list
//
function user_list(e, page, search, searchVal){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var $form = $('<form></form>');
	$form.attr('action', '/admin/manage/agent/user/list.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');
	
	$form.append(page_param).append(search_param).append(searchVal_param);
	
	$form.submit();
}

//
//	mobile function box : show & hide
//
function mobile_func_box(e){
	var is_checked = $('.mobile_func.control').is(":checked");

	if(is_checked){
		$('.mobile_setting_box').show();	
	}
	else{
		$('.mobile_setting_box').hide();
	}
}

function unlock_pw(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var sptid = $('.unlock_pw_sptid').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/common/ajax.unlock_pw_user.php',
		data: {id : sptid},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			if(res.flag == true){
				$(".pw_status").html(res.msg);

				var pw_status = $(".pw_status").text();

				if(pw_status == "UNLOCK"){
					$(".pw_status").removeClass("imp");
					if(!$(".pw_status").hasClass("noti")){
						$(".pw_status").addClass("noti");
					}
				}
				else{
					$(".pw_status").removeClass("noti");
					if(!$(".pw_status").hasClass("imp")){
						$(".pw_status").addClass("imp");
					}
				}
			}
		}
	});
}