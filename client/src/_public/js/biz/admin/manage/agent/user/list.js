/**
 * manage / agent / user / list.js
 * 2015.08.06 | KSM
 */
(function($){
	//user search
	$('.user_search_btn').on('click', search_user);
	$('.user_search_val').on('keydown', search_user_keyevent);
	//popup : user add
	$('.open_add_user_pop').on('click', {action_str: "open"}, add_user_pop);
	$('.close_add_user_pop').on('click', {action_str: "close"}, add_user_pop);
	//user add
	$('.add_user_btn').on('click', add_user);
	//popup : user add res
	$('.open_add_user_res_pop').on('click', {action_str: "open"}, add_user_res_pop);
	$('.close_pop_user_add_res').on('click', {action_str: "close"}, add_user_res_pop);
	$('.confirm_pop_user_add_res').on('click', {action_str: "close"}, add_user_res_pop);
	//user add password ajax
	$('#user_pw').on('keyup', validate_pw);
	$('#user_pw2').on('keyup', validate_retype_pw);
	//export
	$('.export_user_to_excel').on('click', download_user_to_excel);
	//popup : user del res
	$('.close_pop_user_del_res').on('click', {action_str: "close"}, del_user_res_pop);
	$('.confirm_pop_user_del_res').on('click', {action_str: "close"}, del_user_res_pop);
})(jQuery);

//
//	popup : delete user respond
//
function del_user_res_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;

	var $form = $('<form></form>');
	$form.attr('action', '/admin/manage/agent/user/list.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var pop_act_param = $('<input type="hidden" value="'+pop_action+'" name="user_del_res_popup_action" />');
	
	$form.append(pop_act_param);
	
	$form.submit();
}

//
//	export : excel
//
function download_user_to_excel(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var sel_index = $('.jq_sel ul li').find('a.selected').attr('index');
	var search = $('.jq_sel_hide option').eq(sel_index).val();
	var searchVal = $('.user_search_val').val();

	var $form = $('<form></form>');
	$form.attr('action', '/_export/admin/user/export.list.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');
	
	$form.append(search_param).append(searchVal_param);
	
	$form.submit();
}

//
//	validate retype password
//
function validate_retype_pw(e){
	var password = $('#user_pw').val();
	var password2 = $('#user_pw2').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/common/ajax.validate_retype_pw.php',
		data: {user_pw : password, user_retype_pw : password2},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;
			
			if(res.flag == true){
				$(".ajax_user_add_password2_guide").html(res.msg);
			}
		}
	});
}

//
//	validate password
//
function validate_pw(e){
	var password = $('#user_pw').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/common/ajax.validate_pw.php',
		data: {user_pw : password},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;
			
			if(res.flag == true){
				$(".ajax_user_add_password_guide").html(res.msg);
			}
		}
	});
}

//
//	popup : add user res
//
function add_user_res_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	
	var pop_action = e.data.action_str;

	var input_res = $('.input_res').val();
	
	var page = $('.page').val();
	var search = $('.search').val();
	var searchVal = $('.searchVal').val();

	var $form = $('<form></form>');
	$form.attr('action', '/admin/manage/agent/user/list.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var user_add_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="user_add_res_popup_action" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');

	$form.append(user_add_res_popup_action).append(page_param).append(search_param).append(searchVal_param);

	if(input_res == 'FAIL'){
		var retry_add_pop_action = 'open';

		var input_spt_id = $('.input_spt_id').val();
		var input_spt_pw = $('.input_spt_pw').val();
		var input_spt_pw2 = $('.input_spt_pw2').val();
		var input_spt_name = $('.input_spt_name').val();
		var input_spt_deptnum = $('.input_spt_deptnum').val();
		var input_spt_tel = $('.input_spt_tel').val();
		var input_spt_mail = $('.input_spt_mail').val();
		var input_spt_permit = $('.input_spt_permit').val();

		var user_add_popup_action = $('<input type="hidden" value="'+retry_add_pop_action+'" name="user_add_popup_action" />');
		var input_res_param = $('<input type="hidden" value="'+input_res+'" name="input_res" />');
		var input_spt_id_param = $('<input type="hidden" value="'+input_spt_id+'" name="input_spt_id" />');
		var input_spt_pw_param = $('<input type="hidden" value="'+input_spt_pw+'" name="input_spt_pw" />');
		var input_spt_pw2_param = $('<input type="hidden" value="'+input_spt_pw2+'" name="input_spt_pw2" />');
		var input_spt_name_param = $('<input type="hidden" value="'+input_spt_name+'" name="input_spt_name" />');
		var input_spt_deptnum_param = $('<input type="hidden" value="'+input_spt_deptnum+'" name="input_spt_deptnum" />');
		var input_spt_tel_param = $('<input type="hidden" value="'+input_spt_tel+'" name="input_spt_tel" />');
		var input_spt_mail_param = $('<input type="hidden" value="'+input_spt_mail+'" name="input_spt_mail" />');
		var input_spt_permit_param = $('<input type="hidden" value="'+input_spt_permit+'" name="input_spt_permit" />');

		$form.append(input_res_param).append(user_add_popup_action).append(input_spt_id_param).append(input_spt_pw_param).append(input_spt_pw2_param).append(input_spt_name_param).append(input_spt_deptnum_param).append(input_spt_tel_param).append(input_spt_mail_param).append(input_spt_permit_param);
	}
	
	$form.submit();
}

//
//	add_user
//
function add_user(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	document.createUserForm.submit();
}

//
//	popup : add user
//
function add_user_pop(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	
	var pop_action = e.data.action_str;

	var page = $('.page').val();
	var search = $('.search').val();
	var searchVal = $('.searchVal').val();

	var $form = $('<form></form>');
	$form.attr('action', '/admin/manage/agent/user/list.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var user_add_popup_action = $('<input type="hidden" value="'+pop_action+'" name="user_add_popup_action" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');
	
	$form.append(user_add_popup_action).append(page_param).append(search_param).append(searchVal_param);
	
	$form.submit();
}

//
//	go to user detail
//
function user_detail(e, spt_num, page, search, searchVal){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var sptnum = spt_num;

	var $form = $('<form></form>');
	$form.attr('action', '/admin/manage/agent/user/info.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var sptnum_param = $('<input type="hidden" value="'+sptnum+'" name="sptnum" />');
	var page_param = $('<input type="hidden" value="'+page+'" name="page" />');
	var search_param = $('<input type="hidden" value="'+search+'" name="search" />');
	var searchVal_param = $('<input type="hidden" value="'+searchVal+'" name="searchVal" />');
	
	$form.append(sptnum_param).append(page_param).append(search_param).append(searchVal_param);
	
	$form.submit();
}

//
//	search keyevent trigger
//
function search_user_trigger(){
	$('.user_search_btn').trigger('click');
}

function search_user_keyevent(e){
	if(e.keyCode == 13){//case by 'ENTER'
		search_user_trigger();
	}
}

//
//	search : user
//
function search_user(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var page = 1;
	var sel_index = $('.jq_sel ul li').find('a.selected').attr('index');
	var search_type = $('.jq_sel_hide option').eq(sel_index).val();
	var search_val = $('.user_search_val').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/admin/user/ajax.list.php',
		data: {page: page, search:search_type, searchVal:search_val},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_user_content").html(res);
		}
	});
}

//
//	paging : user
//
function user_pagination(e, page_num){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var page = page_num;

	$.ajax({
		type: "POST",
		url:'/_ajax/admin/user/ajax.list.php',
		data: {page: page},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_user_content").html(res);
		}
	});
}