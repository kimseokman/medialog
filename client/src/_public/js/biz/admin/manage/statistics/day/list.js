/**
 * manage / statistics / day / list.js
 * 2015.08.18 | KSM
 */
(function($){
	//search
	$('.search_day_statistics').on('click', search_day_statistics);

	//export
	$('.export_day_statistics').on('click', export_day_statistics_to_excel);

	//popup
	$('.close_pop_search_res').on('click', {action_str: "close"}, search_res_popup);
	$('.confirm_pop_search_res').on('click', {action_str: "close"}, search_res_popup);
})(jQuery);

//
//	export : excel
//
function export_day_statistics_to_excel(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var start_YYYY_index = $('.search_day_statistics_box .start_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var startYear = $('.search_day_statistics_box .start_YYYY .jq_sel_hide option').eq(start_YYYY_index).val();
	var start_MM_index = $('.search_day_statistics_box .start_MM .jq_sel ul li').find('a.selected').attr('index');
	var startMonth = $('.search_day_statistics_box .start_MM .jq_sel_hide option').eq(start_MM_index).val();

	var search_year = startYear;
	var search_month = startMonth;

	var $form = $('<form></form>');
	$form.attr('action', '/_export/admin/statistics/day/export.list.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var search_year_param = $('<input type="hidden" value="'+search_year+'" name="search_year" />');
	var search_month_param = $('<input type="hidden" value="'+search_month+'" name="search_month" />');
	
	$form.append(search_year_param).append(search_month_param);
	
	$form.submit();
}

//
//	trigger
//
function search_day_statistics_trigger(){
	$('.search_day_statistics').trigger('click');
}

//
//	search : search button
//	
function search_day_statistics(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var start_YYYY_index = $('.search_day_statistics_box .start_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var startYear = $('.search_day_statistics_box .start_YYYY .jq_sel_hide option').eq(start_YYYY_index).val();
	var start_MM_index = $('.search_day_statistics_box .start_MM .jq_sel ul li').find('a.selected').attr('index');
	var startMonth = $('.search_day_statistics_box .start_MM .jq_sel_hide option').eq(start_MM_index).val();

	var search_year = startYear;
	var search_month = startMonth;

	

	var currentTime = new Date();
	var startTime = new Date(startYear, search_month, currentTime.getDate());
	var oneYearsAgo = new Date().setFullYear(currentTime.getFullYear()-1);

	if(startTime < oneYearsAgo){//1년 전 기간 검색하면
		var $form = $('<form></form>');
		$form.attr('action', '');
		$form.attr('method', 'post');
		$form.appendTo('body');

		var pop_act = "open";
		var pop_msg = "1년이하의 기록만 검색 가능합니다.";

		var pop_act_param = $('<input type="hidden" value="'+pop_act+'" name="search_res_popup_action" />');
		var pop_msg_param = $('<input type="hidden" value="'+pop_msg+'" name="search_res_popup_msg" />');
		
		$form.append(pop_act_param).append(pop_msg_param);
		
		$form.submit();
	}
	else{

		$.ajax({
			type: "POST",
			url:'/_ajax/admin/statistics/day/ajax.list.php',
			data: {search_year: search_year, search_month: search_month},
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			success: function(data){
				var res = data;

				$(".ajax_day_statistics").html(res);
			}
		});

	}
}

function search_res_popup(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;
	
	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var search_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="search_res_popup_action" />');
	
	$form.append(search_res_popup_action);
	
	$form.submit();
}