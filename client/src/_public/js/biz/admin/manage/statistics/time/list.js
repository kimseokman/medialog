/**
 * manage / statistics / time / list.js
 * 2015.08.18 | KSM
 */
(function($){
	//search
	$('.search_time_statistics').on('click', search_time_statistics);

	//force search
	$('.week_a').on('click', {mode: 'week', term_day: 7}, force_search);
	$('.month_1_a').on('click', {mode: 'month', term_month: 1}, force_search);
	$('.month_3_a').on('click', {mode: 'month', term_month: 3}, force_search);
	$('.month_6_a').on('click', {mode: 'month', term_month: 6}, force_search);

	//export
	$('.export_time_statistics').on('click', export_time_statistics_to_excel);

	//popup
	$('.close_pop_search_res').on('click', {action_str: "close"}, search_res_popup);
	$('.confirm_pop_search_res').on('click', {action_str: "close"}, search_res_popup);
})(jQuery);

//
//	export : excel
//
function export_time_statistics_to_excel(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var start_YYYY_index = $('.search_time_statistics_box .start_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var startYear = $('.search_time_statistics_box .start_YYYY .jq_sel_hide option').eq(start_YYYY_index).val();
	var start_MM_index = $('.search_time_statistics_box .start_MM .jq_sel ul li').find('a.selected').attr('index');
	var startMonth = $('.search_time_statistics_box .start_MM .jq_sel_hide option').eq(start_MM_index).val();
	var start_DD_index = $('.search_time_statistics_box .start_DD .jq_sel ul li').find('a.selected').attr('index');
	var startDay = $('.search_time_statistics_box .start_DD .jq_sel_hide option').eq(start_DD_index).val();

	var start_time = startYear +"-"+ startMonth +"-"+ startDay;

	var end_YYYY_index = $('.search_time_statistics_box .end_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var endYear = $('.search_time_statistics_box .end_YYYY .jq_sel_hide option').eq(end_YYYY_index).val();
	var end_MM_index = $('.search_time_statistics_box .end_MM .jq_sel ul li').find('a.selected').attr('index');
	var endMonth = $('.search_time_statistics_box .end_MM .jq_sel_hide option').eq(end_MM_index).val();
	var end_DD_index = $('.search_time_statistics_box .end_DD .jq_sel ul li').find('a.selected').attr('index');
	var endDay = $('.search_time_statistics_box .end_DD .jq_sel_hide option').eq(end_DD_index).val();

	var end_time = endYear +"-"+ endMonth +"-"+ endDay;

	var $form = $('<form></form>');
	$form.attr('action', '/_export/admin/statistics/time/export.list.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var start_time_param = $('<input type="hidden" value="'+start_time+'" name="start_time" />');
	var end_time_param = $('<input type="hidden" value="'+end_time+'" name="end_time" />');
	
	$form.append(start_time_param).append(end_time_param);
	
	$form.submit();
}

//
//	search : force
// 
function force_search(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	$('.force_search').removeClass('on');
	$(this).addClass('on');

	var now = new Date();
	var end_YYYY = now.getFullYear();
	var end_month = now.getMonth()+1;
	var end_MM = (end_month<10 ? '0' : '') + end_month;
	var end_day = now.getDate();
	var end_DD = (end_day<10 ? '0' : '') + end_day;

	//apply end date select box : end YYYY
	var end_year_index = 0;
	$('.search_time_statistics_box .end_YYYY .jq_sel ul li').each(function(index){
		if($( this ).text() == end_YYYY){
			end_year_index = index;
		}
	});
	$('.search_time_statistics_box .end_YYYY .jq_sel div span').text(end_YYYY);
	$('.search_time_statistics_box .end_YYYY .jq_sel ul li a').removeClass('selected');
	$('.search_time_statistics_box .end_YYYY .jq_sel ul li a').eq(end_year_index).addClass('selected');
	var end_YYYY_index = $('.search_time_statistics_box .end_YYYY .jq_sel ul li').find('a.selected').attr('index');
	$('.search_time_statistics_box .end_YYYY .jq_sel_hide option').attr('selected', false);
	$('.search_time_statistics_box .end_YYYY .jq_sel_hide option').eq(end_YYYY_index).attr('selected', true);

	//apply end date select box : end MM
	var end_month_index = 0;
	$('.search_time_statistics_box .end_MM .jq_sel ul li').each(function(index){
		if($( this ).text() == end_MM){
			end_month_index = index;
		}
	});
	$('.search_time_statistics_box .end_MM .jq_sel div span').text(end_MM);
	$('.search_time_statistics_box .end_MM .jq_sel ul li a').removeClass('selected');
	$('.search_time_statistics_box .end_MM .jq_sel ul li a').eq(end_month_index).addClass('selected');
	var end_MM_index = $('.search_time_statistics_box .end_MM .jq_sel ul li').find('a.selected').attr('index');
	$('.search_time_statistics_box .end_MM .jq_sel_hide option').attr('selected', false);
	$('.search_time_statistics_box .end_MM .jq_sel_hide option').eq(end_MM_index).attr('selected', true);

	//apply end date select box : end DD
	var end_day_index = 0;
	$('.search_time_statistics_box .end_DD .jq_sel ul li').each(function(index){
		if($( this ).text() == end_DD){
			end_day_index = index;
		}
	});
	$('.search_time_statistics_box .end_DD .jq_sel div span').text(end_DD);
	$('.search_time_statistics_box .end_DD .jq_sel ul li a').removeClass('selected');
	$('.search_time_statistics_box .end_DD .jq_sel ul li a').eq(end_day_index).addClass('selected');
	var end_DD_index = $('.search_time_statistics_box .end_DD .jq_sel ul li').find('a.selected').attr('index');
	$('.search_time_statistics_box .end_DD .jq_sel_hide option').attr('selected', false);
	$('.search_time_statistics_box .end_DD .jq_sel_hide option').eq(end_DD_index).attr('selected', true);

	var start = new Date();
	var interval = 0;
	if(e.data.mode == 'week'){
		interval = e.data.term_day;

		start.setDate(start.getDate() - interval);
	}
	else if(e.data.mode == 'month'){
		interval = e.data.term_month;

		start.setFullYear(start.getFullYear(), start.getMonth() - interval);
	}

	var start_YYYY = start.getFullYear();
	var start_month = start.getMonth()+1;
	var start_MM = (start_month<10 ? '0' : '') + start_month;
	var start_day = start.getDate();
	var start_DD = (start_day<10 ? '0' : '') + start_day;

	//apply start date select box : start YYYY
	var start_year_index = 0;
	$('.search_time_statistics_box .start_YYYY .jq_sel ul li').each(function(index){
		if($( this ).text() == start_YYYY){
			start_year_index = index;
		}
	});
	$('.search_time_statistics_box .start_YYYY .jq_sel div span').text(start_YYYY);
	$('.search_time_statistics_box .start_YYYY .jq_sel ul li a').removeClass('selected');
	$('.search_time_statistics_box .start_YYYY .jq_sel ul li a').eq(start_year_index).addClass('selected');
	var start_YYYY_index = $('.search_time_statistics_box .start_YYYY .jq_sel ul li').find('a.selected').attr('index');
	$('.search_time_statistics_box .start_YYYY .jq_sel_hide option').attr('selected', false);
	$('.search_time_statistics_box .start_YYYY .jq_sel_hide option').eq(start_YYYY_index).attr('selected', true);

	//apply start date select box : start MM
	var start_month_index = 0;
	$('.search_time_statistics_box .start_MM .jq_sel ul li').each(function(index){
		if($( this ).text() == start_MM){
			start_month_index = index;
		}
	});
	$('.search_time_statistics_box .start_MM .jq_sel div span').text(start_MM);
	$('.search_time_statistics_box .start_MM .jq_sel ul li a').removeClass('selected');
	$('.search_time_statistics_box .start_MM .jq_sel ul li a').eq(start_month_index).addClass('selected');
	var start_MM_index = $('.search_time_statistics_box .start_MM .jq_sel ul li').find('a.selected').attr('index');
	$('.search_time_statistics_box .start_MM .jq_sel_hide option').attr('selected', false);
	$('.search_time_statistics_box .start_MM .jq_sel_hide option').eq(start_MM_index).attr('selected', true);

	//apply start date select box : start DD
	var start_day_index = 0;
	$('.search_time_statistics_box .start_DD .jq_sel ul li').each(function(index){
		if($( this ).text() == start_DD){
			start_day_index = index;
		}
	});
	$('.search_time_statistics_box .start_DD .jq_sel div span').text(start_DD);
	$('.search_time_statistics_box .start_DD .jq_sel ul li a').removeClass('selected');
	$('.search_time_statistics_box .start_DD .jq_sel ul li a').eq(start_day_index).addClass('selected');
	var start_DD_index = $('.search_time_statistics_box .start_DD .jq_sel ul li').find('a.selected').attr('index');
	$('.search_time_statistics_box .start_DD .jq_sel_hide option').attr('selected', false);
	$('.search_time_statistics_box .start_DD .jq_sel_hide option').eq(start_DD_index).attr('selected', true);

	//trigger
	
	search_time_statistics_trigger();
}

//
//	trigger
//
function search_time_statistics_trigger(){
	$('.search_time_statistics').trigger('click');
}

//
//	search : search button
//	
function search_time_statistics(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var start_YYYY_index = $('.search_time_statistics_box .start_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var startYear = $('.search_time_statistics_box .start_YYYY .jq_sel_hide option').eq(start_YYYY_index).val();
	var start_MM_index = $('.search_time_statistics_box .start_MM .jq_sel ul li').find('a.selected').attr('index');
	var startMonth = $('.search_time_statistics_box .start_MM .jq_sel_hide option').eq(start_MM_index).val();
	var start_DD_index = $('.search_time_statistics_box .start_DD .jq_sel ul li').find('a.selected').attr('index');
	var startDay = $('.search_time_statistics_box .start_DD .jq_sel_hide option').eq(start_DD_index).val();

	var start_time = startYear +"-"+ startMonth +"-"+ startDay;

	var end_YYYY_index = $('.search_time_statistics_box .end_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var endYear = $('.search_time_statistics_box .end_YYYY .jq_sel_hide option').eq(end_YYYY_index).val();
	var end_MM_index = $('.search_time_statistics_box .end_MM .jq_sel ul li').find('a.selected').attr('index');
	var endMonth = $('.search_time_statistics_box .end_MM .jq_sel_hide option').eq(end_MM_index).val();
	var end_DD_index = $('.search_time_statistics_box .end_DD .jq_sel ul li').find('a.selected').attr('index');
	var endDay = $('.search_time_statistics_box .end_DD .jq_sel_hide option').eq(end_DD_index).val();

	var end_time = endYear +"-"+ endMonth +"-"+ endDay;

	var startTime = new Date(startYear, startMonth, startDay);
	var endTime = new Date(endYear, endMonth, endDay);
	var currentTime = new Date();
	var oneYearsAgo = new Date().setFullYear(currentTime.getFullYear()-1);

	if(endTime < startTime){//search error
		var $form = $('<form></form>');
		$form.attr('action', '');
		$form.attr('method', 'post');
		$form.appendTo('body');

		var pop_act = "open";
		var pop_msg = "검색 기간을 다시 확인해주세요.";

		var pop_act_param = $('<input type="hidden" value="'+pop_act+'" name="search_res_popup_action" />');
		var pop_msg_param = $('<input type="hidden" value="'+pop_msg+'" name="search_res_popup_msg" />');
		
		$form.append(pop_act_param).append(pop_msg_param);
		
		$form.submit();
	}
	else if(startTime < oneYearsAgo){//1년 전 기간 검색하면
		var $form = $('<form></form>');
		$form.attr('action', '');
		$form.attr('method', 'post');
		$form.appendTo('body');

		var pop_act = "open";
		var pop_msg = "1년이하의 기록만 검색 가능합니다.";

		var pop_act_param = $('<input type="hidden" value="'+pop_act+'" name="search_res_popup_action" />');
		var pop_msg_param = $('<input type="hidden" value="'+pop_msg+'" name="search_res_popup_msg" />');
		
		$form.append(pop_act_param).append(pop_msg_param);
		
		$form.submit();
	}
	else{

		$.ajax({
			type: "POST",
			url:'/_ajax/admin/statistics/time/ajax.list.php',
			data: {start_time: start_time, end_time: end_time},
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			success: function(data){
				var res = data;

				$(".ajax_time_statistics").html(res);
			}
		});
	}
}

function search_res_popup(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;
	
	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var search_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="search_res_popup_action" />');
	
	$form.append(search_res_popup_action);
	
	$form.submit();
}