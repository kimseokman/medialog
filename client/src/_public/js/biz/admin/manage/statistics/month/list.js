/**
 * manage / statistics / month / list.js
 * 2015.08.18 | KSM
 */
(function($){
	//search
	$('.search_month_statistics').on('click', search_month_statistics);

	//export
	$('.export_month_statistics').on('click', export_month_statistics_to_excel);

	//popup
	$('.close_pop_search_res').on('click', {action_str: "close"}, search_res_popup);
	$('.confirm_pop_search_res').on('click', {action_str: "close"}, search_res_popup);
})(jQuery);

//
//	export : excel
//
function export_month_statistics_to_excel(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var start_YYYY_index = $('.search_month_statistics_box .start_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var startYear = $('.search_month_statistics_box .start_YYYY .jq_sel_hide option').eq(start_YYYY_index).val();

	var search_year = startYear;

	var $form = $('<form></form>');
	$form.attr('action', '/_export/admin/statistics/month/export.list.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var search_year_param = $('<input type="hidden" value="'+search_year+'" name="search_year" />');
	
	$form.append(search_year_param);
	
	$form.submit();
}

//
//	trigger
//
function search_month_statistics_trigger(){
	$('.search_month_statistics').trigger('click');
}

//
//	search : search button
//	
function search_month_statistics(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var start_YYYY_index = $('.search_month_statistics_box .start_YYYY .jq_sel ul li').find('a.selected').attr('index');
	var startYear = $('.search_month_statistics_box .start_YYYY .jq_sel_hide option').eq(start_YYYY_index).val();

	var search_year = startYear;

	$.ajax({
		type: "POST",
		url:'/_ajax/admin/statistics/month/ajax.list.php',
		data: {search_year: search_year},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_month_statistics").html(res);
		}
	});
}

function search_res_popup(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}
	var pop_action = e.data.action_str;
	
	var $form = $('<form></form>');
	$form.attr('action', '');//self
	$form.attr('method', 'post');
	$form.appendTo('body');

	var search_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="search_res_popup_action" />');
	
	$form.append(search_res_popup_action);
	
	$form.submit();
}