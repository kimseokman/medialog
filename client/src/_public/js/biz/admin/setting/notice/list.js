/**
 * setting / notice / list.js
 * 2015.08.20 | KSM
 */
(function($){
	//search notice ( ajax )
	$('.search_notice_btn').on('click', search_notice);
	$('.notice_search_val').on('keydown', notice_search_keyevent);
})(jQuery);

//
//	notice : detail
//
function notice_detail(e, num, page, type, val){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var seq_num = num;
	var remember_page = page;
	var remember_type = type;
	var remember_val = val;

	var $form = $('<form></form>');
	$form.attr('action', '/admin/setting/notice/info.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var seq_num_param = $('<input type="hidden" value="'+seq_num+'" name="seq" />');
	var remember_page_param = $('<input type="hidden" value="'+remember_page+'" name="remember_page" />');
	var remember_type_param = $('<input type="hidden" value="'+remember_type+'" name="remember_type" />');
	var remember_val_param = $('<input type="hidden" value="'+remember_val+'" name="remember_val" />');

	$form.append(seq_num_param).append(remember_page_param).append(remember_type_param).append(remember_val_param);

	$form.submit();
}

//
//	popup notice : Key Event
//
function notice_search_keyevent(e){
	if(e.keyCode == 13){//case by 'ENTER'
		search_notice_trigger();
	}
}

function search_notice_trigger(){
	$('.search_notice_btn').trigger('click');
}

//
//	popup notice : search
//
function search_notice(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var page = 0;
	var sel_index = $('.u_biz_noti_input .jq_sel ul li').find('a.selected').attr('index');
	var search_type = $('.u_biz_noti_input .jq_sel_hide option').eq(sel_index).val();
	var search_val = $('.u_biz_noti_input .notice_search_val').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/admin/setting/notice/ajax.list.php',
		data: {page: page, search_type : search_type, search_val: search_val},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_notice_content").html(res);
		}
	});
}

//
//	popup notice : pagination
//
function notice_pagination(e, page_num){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var page = page_num;
	var sel_index = $('.u_biz_noti_input .jq_sel ul li').find('a.selected').attr('index');
	var search_type = $('.u_biz_noti_input .jq_sel_hide option').eq(sel_index).val();
	var search_val = $('.u_biz_noti_input .notice_search_val').val();

	$.ajax({
		type: "POST",
		url:'/_ajax/admin/setting/notice/ajax.list.php',
		data: {page: page, search_type : search_type, search_val: search_val},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_notice_content").html(res);
		}
	});
}