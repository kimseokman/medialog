/**
 * setting / site / info_auth.js
 * 2015.11.03 | KSM
 */
(function($){
	//search
	$('.site_auth_btn').on('click', site_auth);
})(jQuery);

function site_auth(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	document.siteAuthForm.submit();
}
