/**
 * setting / site / info.js
 * 2015.08.19 | KSM
 */
(function($){
	//search
	$('.site_setting_save_btn').on('click', site_setting);

	$('.select_file').on('click', select_file);
	$('#select_file').on('change', show_file);

	//popup : setting site
	$('.close_pop_setting_site_res').on('click', {action_str : 'close'}, pop_setting_site_res);
	$('.confirm_pop_setting_site_res').on('click', {action_str : 'close'}, pop_setting_site_res);

	//popup : edit password
	$('.open_pop_edit_pw').on('click', {action_str : 'open'}, pop_edit_pw);
	$('.close_pop_edit_pw').on('click', {action_str : 'close'}, pop_edit_pw);
	$('.confirm_pop_edit_pw').on('click', edit_pw);
})(jQuery);

//
//	edit password
//
function edit_pw(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var now_pw = $('#now_pw').val();
	var new_pw = $('#new_pw').val();
	var retype_pw = $('#c_new_pw').val();

	var $form = $('<form></form>');
	$form.attr('action', '/_func/function.edit_password.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var now_pw_param = $('<input type="hidden" value="'+now_pw+'" name="now_pw" />');
	var new_pw_param = $('<input type="hidden" value="'+new_pw+'" name="new_pw" />');
	var retype_pw_param = $('<input type="hidden" value="'+retype_pw+'" name="retype_pw" />');

	$form.append(now_pw_param).append(new_pw_param).append(retype_pw_param);

	$form.submit();
}

//
//	popup : edit password
//
function pop_edit_pw(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;

	var $form = $('<form></form>');
	$form.attr('action', '');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var edit_pw_popup_action = $('<input type="hidden" value="'+pop_action+'" name="edit_pw_popup_action" />');

	$form.append(edit_pw_popup_action);

	$form.submit();
}

//
//	popup : setting site res
//
function pop_setting_site_res(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	var pop_action = e.data.action_str;
	var setting_site_edit_res_log = $('.setting_site_edit_res_log').val();
	var setting_site_edit_res_log_msg = $('.setting_site_edit_res_log_msg').val();

	var $form = $('<form></form>');
	$form.attr('action', '');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var setting_site_edit_res_popup_action = $('<input type="hidden" value="'+pop_action+'" name="setting_site_edit_res_popup_action" />');
	var setting_site_edit_res_log_param = $('<input type="hidden" value="'+setting_site_edit_res_log+'" name="setting_site_edit_res_log" />');
	var setting_site_edit_res_log_msg_param = $('<input type="hidden" value="'+setting_site_edit_res_log_msg+'" name="setting_site_edit_res_log_msg" />');

	$form.append(setting_site_edit_res_popup_action).append(setting_site_edit_res_log_param).append(setting_site_edit_res_log_msg_param);

	$form.submit();
}

//
//	show file
//
function show_file(e){
	var file_path = $('#select_file').val();

	$('#logo_file_path').val(file_path);
}

//
//	select file
//
function select_file(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	$('#select_file').trigger('click');
}

//
//	setting site
//
function site_setting(e){
	if( e.preventDefault ){ e.preventDefault(); } else {e.returnValue = false;}

	document.settingSiteForm.submit();
}