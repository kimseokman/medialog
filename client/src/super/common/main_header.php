<?php
$user_id = "GUEST";
if ($_SESSION['usertype'] == "OWNER"){
	$user_id = $_SESSION['owid'];
}

function print_nb_on($base, $current){//use in gnb & snb
	$class_name = "";
	
	if(strpos($current, $base)){
		$class_name = "on";
	}

	return $class_name;
}

function print_gnb_img_on($base, $current){
	$class_name = "";
	
	if(strpos($current, $base)){
		$class_name = "_on";
	}

	return $class_name;
}
?>
<!-- wrap -->
<div id="wrap">
<!-- header -->
<div id="header">
	<div class="top_head">
		<div class="login">
			<span class="user_id"><? echo $user_id;?>님 안녕하세요!</span>
			<span class="out open_pop_logout"><a href="#"><img src="<? echo IMG_DIR; ?>/sub/btn_logout.gif" alt="로그아웃" /></a></span>
		</div>
	</div>
	<div class="gnb_head">
		<h1><a href="#"><img src="<? echo IMG_DIR; ?>/super/logo_main.gif" alt="biz최고관리자페이지" /></a></h1>
		<div class="gnb">
			<ul>
				<li class="<? echo print_nb_on(SUPER_GNB_CUSTOMER, $current_url); ?>">
					<a href="<? echo SUPER_GNB_CUSTOMER_PAID_INDEX; ?>">
						<img src="<? echo IMG_DIR; ?>/super/gnb_customer_list<? echo print_gnb_img_on(SUPER_GNB_CUSTOMER, $current_url); ?>.gif" alt="고객 정보 관리" />
					</a>
				</li>
				<li class="<? echo print_nb_on(SUPER_GNB_SERVICE, $current_url); ?> last">
					<a href="<? echo SUPER_GNB_SERVICE_NOTICE_INDEX; ?>">
						<img src="<? echo IMG_DIR; ?>/super/gnb_service_list<? echo print_gnb_img_on(SUPER_GNB_SERVICE, $current_url); ?>.gif" alt="서비스 관리" />
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!-- //header -->
<!-- container -->
<div id="container">