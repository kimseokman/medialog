<?php
$admin_add_res_popup_action = $_POST['admin_add_res_popup_action'];
$admin_add_res_popup_msg = $_POST['admin_add_res_popup_msg'];

$remem_page = $_POST['page'];
$remem_search = $_POST['search'];
$remem_searchVal = $_POST['searchVal'];
$input_res = $_POST['$input_res'];

$admin_id = $_POST['admin_id'];
if($input_res != 'SUCCESS'){
	$admin_enable = $_POST['admin_enable'];
	$admin_company = $_POST['admin_company'];
	$admin_service = $_POST['admin_service'];
	$admin_business_num = $_POST['admin_business_num'];
	$admin_name = $_POST['admin_name'];
	$admin_produc_type = $_POST['admin_product'];
	$admin_volume_pc = $_POST['admin_pc_cnt'];
	$admin_volume_mobile = $_POST['admin_pc_m_n_v_cnt'];
	$admin_volume = $_POST['admin_all_cnt'];
	$admin_url = $_POST['admin_url'];
	$admin_manage_num = $_POST['admin_manage_num'];
	$admin_mail = $_POST['admin_mail'];
	$admin_memo = $_POST['admin_memo'];
	$admin_expired_date = $_POST['admin_expired_date'];

	$admin_business_sector = $_POST['admin_business_sector'];
	$admin_phone = $_POST['admin_phone'];
	$admin_tel = $_POST['admin_tel'];
	$admin_service_join_date = $_POST['admin_service_join_date'];
	$admin_service_start_date = $_POST['admin_service_start_date'];
	$admin_service_expired_date = $_POST['admin_service_expired_date'];
	$admin_bill_account = $_POST['admin_bill_account'];
	$admin_sign_num = $_POST['admin_sign_num'];
}


if($admin_add_res_popup_action == 'open'){
?>
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<span class="popup_title_txt"></span>
			<a href="#" class="close close_pop_admin_add_res" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p class="h_num70">
					<? echo $admin_add_res_popup_msg; ?>
				</p>
				<input type="hidden" class="remem_page" value="<? echo $remem_page; ?>" />
				<input type="hidden" class="remem_search" value="<? echo $remem_search; ?>" />
				<input type="hidden" class="remem_searchVal" value="<? echo $remem_searchVal; ?>" />

				<input type="hidden" class="remem_admin_id" value="<? echo $admin_id; ?>" />
				<input type="hidden" class="remem_admin_enable" value="<? echo $admin_enable; ?>" />
				<input type="hidden" class="remem_admin_company" value="<? echo $admin_company; ?>" />
				<input type="hidden" class="remem_admin_service" value="<? echo $admin_service; ?>" />
				<input type="hidden" class="remem_admin_business_num" value="<? echo $admin_business_num; ?>" />
				<input type="hidden" class="remem_admin_name" value="<? echo $admin_name; ?>" />
				<input type="hidden" class="remem_admin_product" value="<? echo $admin_produc_type; ?>" />
				<input type="hidden" class="remem_admin_pc_cnt" value="<? echo $admin_pc_cnt; ?>" />
				<input type="hidden" class="remem_admin_pc_m_n_v_cnt" value="<? echo $admin_pc_m_n_v_cnt; ?>" />
				<input type="hidden" class="remem_admin_all_cnt" value="<? echo $admin_all_cnt; ?>" />
				<input type="hidden" class="remem_admin_url" value="<? echo $admin_url; ?>" />
				<input type="hidden" class="remem_admin_manage_num" value="<? echo $admin_manage_num; ?>" />
				<input type="hidden" class="remem_admin_mail" value="<? echo $admin_mail; ?>" />
				<input type="hidden" class="remem_admin_memo" value="<? echo $admin_memo; ?>" />
				<input type="hidden" class="remem_admin_expired_date" value="<? echo $admin_expired_date; ?>" />

				<input type="hidden" class="remem_admin_business_sector" value="<? echo $admin_business_sector; ?>" />
				<input type="hidden" class="remem_admin_phone" value="<? echo $admin_phone; ?>" />
				<input type="hidden" class="remem_admin_tel" value="<? echo $admin_tel; ?>" />
				<input type="hidden" class="remem_admin_service_join_date" value="<? echo $admin_service_join_date; ?>" />
				<input type="hidden" class="remem_admin_service_start_date" value="<? echo $admin_service_start_date; ?>" />
				<input type="hidden" class="remem_admin_service_expired_date" value="<? echo $admin_service_expired_date; ?>" />
				<input type="hidden" class="remem_admin_bill_account" value="<? echo $admin_bill_account; ?>" />
				<input type="hidden" class="remem_admin_sign_num" value="<? echo $admin_sign_num; ?>" />

				<div class="btn">
					<a href="#" class="confirm_pop_admin_add_res"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<?
}//end of : if($admin_add_res_popup_action == 'open')
?>