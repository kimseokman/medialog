<?php
$admin_del_popup_action = $_POST['admin_del_popup_action'];
$remem_adid = $_POST['remem_adid'];
$remem_page = $_POST['remem_page'];
$remem_search = $_POST['remem_search'];
$remem_searchVal = $_POST['remem_searchVal'];

if($admin_del_popup_action == 'open'){
?>
<!-- 사용자 삭제 창 -->
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<span class="popup_title_txt"></span>
			<a href="#" class="close close_pop_admin_del" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p class="h_num70">선택하신 관리자를 삭제하시겠습니까?</p>

				<input type="hidden" class="remem_adid" value="<? echo $remem_adid; ?>" />

				<input type="hidden" class="remem_page" value="<? echo $remem_page; ?>" />
				<input type="hidden" class="remem_search" value="<? echo $remem_search; ?>" />
				<input type="hidden" class="remem_searchVal" value="<? echo $remem_searchVal; ?>" />
				
				<div class="btn">
					<a href="#" class="yes_pop_admin_del"><img src="<? echo IMG_DIR; ?>/popup/btn_yes.gif" alt="예" /></a>
					<a href="#" class="no_pop_admin_del"><img src="<? echo IMG_DIR; ?>/popup/btn_no.gif" alt="아니오" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //사용자 삭제 창 -->
<?
}//end of : if($admin_del_popup_action == 'open')
?>