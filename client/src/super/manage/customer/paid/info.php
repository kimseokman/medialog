<?php
include_once("../../../../_func/function.common.php");

include_once("../../../common/html_head.php");
include_once("../../../common/main_header.php");

$admin_act_type = ADMIN_ACT_TYPE_PAID;
$login_try_limit = LOGIN_TRY_COUNT_MAX;

$remember_page = $_POST['page'];
$remember_search = $_POST['search'];
$remember_searchVal = $_POST['searchVal'];

$adid = $_POST['adid'];

$id_length_max = ID_MAX_LENGTH;
$pw_length = PASSWORD_LENGTH;
$pw_length_min = PASSWORD_LENGTH_MIN;
$pw_length_max = PASSWORD_LENGTH_MAX;
$pw_safe_cnt = PASSWORD_SAFE_CNT;

$p_pc = PRODUCT_CODE_PC;
$p_mobile_paid = PRODUCT_CODE_MOBILE_PAID;
$p_mobile_free = PRODUCT_CODE_MOBILE_FREE;
$p_video_paid = PRODUCT_CODE_VIDEO_PAID;
$p_video_free = PRODUCT_CODE_VIDEO_FREE;
$p_mobile_video_paid = PRODUCT_CODE_MOBILE_VIDEO_PAID;
$p_mobile_video_free = PRODUCT_CODE_MOBILE_VIDEO_FREE;

$admin_info = $main_obj->GetAdminByAdid($adid);

$customer_access_url_header = CUSTOMER_ACCESS_URL_DEFAULT_HEADER;
$customer_access_url_footer = CUSTOMER_ACCESS_URL_DEFAULT_FOOTER;

$config_path = array(
	"GET_FAIL" => SUPER_GNB_CUSTOMER_PAID_INDEX
);

$config_msg = array(
	"NO_ADMIN" => "관리자 정보가 존재하지 않습니다.<br /> 다시 시도해주세요."
);

$pop_action_open = 'open';
$pop_action_close = 'close';

if($admin_info == NULL) {
?>
<form name="getAdminForm" method="post" action="<? echo $config_path['GET_FAIL']; ?>">
	<input type="hidden" name="admin_get_res_popup_action" value="<? echo $pop_action_open; ?>" />
	<input type="hidden" name="admin_get_res_popup_msg" value="<? echo $config_msg['NO_ADMIN']; ?>" />

	<input type="hidden" name="remem_page" value="<? echo $remem_page; ?>" />
	<input type="hidden" name="remem_search" value="<? echo $remem_search; ?>" />
	<input type="hidden" name="remem_searchVal" value="<? echo $remem_searchVal; ?>" />
</form>
<script>
	document.getAdminForm.submit();
</script>
<?
}

$volume = $admin_info->volume;
$mobile_volume = $admin_info->mobile_volume;
$pc_volume = $volume - $mobile_volume;
$admin_num = $admin_info->adnum;

$business_list = $utils_obj->GetBusinessList();

$agent_cnt = $main_obj->GetSupporterCntByAdnum($admin_num);

$try_cnt = $main_obj->GetLoginTryCount($adid);
$pw_status = "UNLOCK";
if($try_cnt >= $login_try_limit){
	$pw_status_class = "imp";
	$pw_status = "LOCK";
}
else{
	$pw_status_class = "noti";
	$pw_status = "UNLOCK";	
}

function print_selected($base, $current){
	$attr_val = "";
	if($base == $current)
		$attr_val = "selected";

	return $attr_val;	
}

function print_checked($base, $current){
	$class_name = "";
	if($base == $current)
		$class_name = "checked";

	return $class_name;	
}
?>
<!-- popup : logout -->
<?
include_once("../../../common/popup/logout.php");
?>
<!-- //popup : logout -->
<!-- 유료 사용자 추가 -->
<div class="conts_box">
	<?
	include_once("../side.php");
	?>
	<div class="conts_right">
		<h3><img src="<? echo IMG_DIR; ?>/super/h3_paid_customer_info.gif" alt="유료 고객 정보" /></h3>
	
		<div class="contents user_info">
			<div class="userlist_btn">
				<a href="#" onClick="open_pop_recent_login_by_agent(event, '<? echo $admin_info->adid; ?>')"><img src="<? echo IMG_DIR; ?>/super/btn_supporter_list.gif" alt="해당 관리자에게 속한 상담원 목록 보기" /></a>
				<a href="#" onClick="admin_list(event, <? echo $remember_page; ?>, '<? echo $remember_search; ?>', '<? echo $remember_searchVal; ?>')"><img src="<? echo IMG_DIR; ?>/sub/btn_list.gif" alt="목록보기" /></a>
			</div>

			<form name="adminEditForm" method="post" action="/_func/function.edit_admin.php">
				<input type="hidden" class="page" name="page" value="<? echo $remember_page; ?>" />
				<input type="hidden" class="search" name="search" value="<? echo $remember_search; ?>" />
				<input type="hidden" class="searchVal" name="searchVal" value="<? echo $remember_searchVal; ?>" />

				<input type="hidden" class="admin_act_type" name="admin_act_type" value="<? echo $admin_act_type; ?>" />
				
				<input type="hidden" class="admin_num" name="admin_num" value="<? echo $admin_num; ?>" />
				<input type="hidden" class="admin_id" name="admin_id" value="<? echo $adid; ?>" />
				
				<table class="style_board_userinfo" summary="" width="100%">
					<caption><span></span></caption>
					<colgroup>
						<col style="width:150px;" />
						<col style="width:*;" />
					</colgroup>
					<tbody>
						<tr>
							<th><label>ID</label></th>
							<td><? echo $adid; ?></td>
						</tr>
						<tr>
							<th><label for="admin_enable">활성화</label></th>
							<td>
								<div class="choice_box admin_enable_box" style="margin-bottom:0px;">
									<span class="comm_sel"><!-- [150811] -->
										<select name="admin_enable">
											<option value="Y" <? echo print_selected("Y", $admin_info->enable); ?>>Y</option>
											<option value="N" <? echo print_selected("N", $admin_info->enable); ?>>N</option>
										</select>
									</span>
								</div>
							</td>
						</tr>
						<tr>
							<th><label for="admin_pw">PW</label></th>
							<td>
								<input type="password" id="admin_pw" name="admin_pw" value="" style="width:160px;" />
								<span class="noti"><? echo $pw_length_min; ?>~<? echo $pw_length_max; ?>자 내의 영문 대/소문자, 숫자, 특수문자 조합</span>
								<p class="imp ajax_admin_edit_password_guide"></p>
							</td>
						</tr>
						<tr>
							<th><label for="admin_retype_pw">PW 확인</label></th>
							<td>
								<input type="password" id="admin_retype_pw" name="admin_retype_pw" value="" style="width:160px;" /><span class="noti">패스워드 변경을 원치 않으시면 패스워드를 빈칸으로 남겨두세요.</span>
								<p class="imp ajax_admin_edit_password2_guide"></p>
							</td>
						</tr>
						<tr>
							<th><label for="admin_pw_status">PW 상태</label></th>
							<td>
								<input type="hidden" class="unlock_pw_adid" value="<? echo $adid; ?>" />
								<a href="#" class="pw_reset"><img src="<? echo IMG_DIR; ?>/super/pw_re.gif" /></a>
								<span class="pw_status <? echo $pw_status_class; ?>"><? echo $pw_status; ?></span>
							</td>
						</tr>
						<tr>
							<th><label for="admin_company">회사명</label></th>
							<td><input type="text" id="admin_company" name="admin_company" value="<? echo $admin_info->company; ?>" style="width:160px;" /></td>
						</tr>
						<!--tr>
							<th><label for="admin_service">서비스명</label></th>
							<td><input type="text" id="admin_service" name="admin_service" value="<? echo $admin_info->service_name; ?>" style="width:160px;" /></td>
						</tr-->
						<tr>
							<th><label for="admin_business_num">사업자번호</label></th>
							<td><input type="text" id="admin_business_num" name="admin_business_num" value="<? echo $admin_info->business_num; ?>" style="width:300px;" /></td>
						</tr>
						<tr>
							<th><label for="admin_business_sector">업종</label><? echo $admin_info->business_sector; ?></th>
							<td>
								<div class="choice_box admin_business_sector_box" style="margin-bottom:0px;">
									<span class="comm_sel"><!-- [150811] -->
										<select name="admin_business_sector">
										<?
										$business_list_cnt = count($business_list);
										
										for($i = 0; $i < $business_list_cnt; $i++){
										?>
											<option value="<? echo $business_list[$i]["name"]; ?>" <? echo print_selected($business_list[$i]["name"], $admin_info->business_sector); ?>><? echo $business_list[$i]["name"]; ?></option>
										<?
										}//end of : for($i = 0; $i < $pc_func_cnt; $i++)
										?>
										</select>
									</span>
								</div>
							</td>
						</tr>
						<tr>
							<th><label for="admin_name">담당자명</label></th>
							<td><input type="text" id="admin_name" name="admin_name" value="<? echo $admin_info->name; ?>" style="width:160px;" /></td>
						</tr>
						<tr>
							<th><label for="admin_mail">이메일</label></th>
							<td><input type="text" id="admin_mail" name="admin_mail" value="<? echo $admin_info->email; ?>" style="width:300px;" /></td>
						</tr>
						<tr>
							<th><label for="admin_phone">연락처1</label></th>
							<td><input type="text" id="admin_phone" name="admin_phone" value="<? echo $admin_info->phone; ?>" style="width:300px;" /></td>
						</tr>
						<tr>
							<th><label for="admin_tel">연락처2</label></th>
							<td>
								<input type="text" id="admin_tel" name="admin_tel" value="<? echo $admin_info->tel; ?>" style="width:300px;" />
								<span class="imp">SMS 발송시 사용되는 번호</span>
							</td>
						</tr>
						<tr>
							<th><label for="admin_service_join_date">가입일자</label></th>
							<td><input type="text" id="admin_service_join_date" name="admin_service_join_date" value="<? echo $admin_info->service_join_date; ?>" style="width:160px;" /></td>
						</tr>
						<?
						if($admin_act_type == ADMIN_ACT_TYPE_PAID){
						?>
						<tr>
							<th><label for="admin_service_start_date">개통일자</label></th>
							<td><input type="text" id="admin_service_start_date" name="admin_service_start_date" value="<? echo $admin_info->service_start_date; ?>" style="width:160px;" /></td>
						</tr>
						<?
						}
						else if($admin_act_type == ADMIN_ACT_TYPE_FREE){
						?>
						<tr>
							<th><label for="admin_service_expired_date">만료일자</label></th>
							<td><input type="text" id="admin_service_expired_date" name="admin_service_expired_date" value="<? echo $admin_info->service_expired_date; ?>" style="width:160px;" /></td>
						</tr>
						<?
						}
						?>
						<tr>
							<th><label>상품 선택</label></th>
							<td>
								<input name="admin_old_product" type="hidden" id="admin_old_product" value="<? echo $admin_info->function; ?>">
								<p>
									<input type="radio" id="admin_product_pc" name="admin_product" value="<? echo $p_pc; ?>" <? echo print_checked($p_pc, $admin_info->function); ?> /> <label for="admin_product_pc">애니서포트 (PC)</label>
								</p>
								<p>
									<input type="radio" id="admin_product_pc_m" name="admin_product" value="<? echo $p_mobile_paid; ?>" <? echo print_checked($p_mobile_paid, $admin_info->function); ?> /> <label for="admin_product_pc_m">애니서포트 + Mobile</label> 
								</p>
								<p>	
									<input type="radio" id="admin_product_pc_v" name="admin_product" value="<? echo $p_video_paid; ?>" <? echo print_checked($p_video_paid, $admin_info->function); ?> /> <label for="admin_product_pc_v">애니서포트 + Video</label>
								</p>	
								<p>	
									<input type="radio" id="admin_product_pc_m_v" name="admin_product" value="<? echo $p_mobile_video_paid; ?>" <? echo print_checked($p_mobile_video_paid, $admin_info->function); ?> /> <label for="admin_product_pc_m_v">애니서포트 + Mobile + Video</label>
								</p>
							</td>
						</tr>
						<tr>
							<th><label>상담원 수</label></th>
							<td>
								<p>
									<label for="admin_pc_cnt">애니서포트 PC</label> : <input type="number" id="admin_pc_cnt" name="admin_pc_cnt" value="<? echo $pc_volume; ?>" min="0" max="500" />
								</p>
								<p>
									<label for="admin_pc_m_n_v_cnt">애니서포트 PC + Mobile / Video</label> : <input type="number" id="admin_pc_m_n_v_cnt" name="admin_pc_m_n_v_cnt" value="<? echo $mobile_volume; ?>" min="0" max="500" />
								</p>
								<p>
									<label for="admin_all_cnt">전체 : </label> <input type="number" id="admin_all_cnt" name="admin_all_cnt" value="<? echo $volume; ?>" min="0" max="500" readonly />
								</p>
								<p class="imp admin_agent_cnt_msg"></p>
							</td>
						</tr>
						<tr>
							<th><label>생성된 상담원 수</label></th>
							<td>
								<? echo $agent_cnt; ?>
							</td>
						</tr>
						<tr>
							<th><label for="admin_bill_account">청구계정번호</label></th>
							<td><input type="text" id="admin_bill_account" name="admin_bill_account" value="<? echo $admin_info->bill_account; ?>" style="width:160px;" /></td>
						</tr>
						<tr>
							<th><label for="admin_sign_num">가입번호</label></th>
							<td><input type="text" id="admin_sign_num" name="admin_sign_num" value="<? echo $admin_info->sign_num; ?>" style="width:160px;" /></td>
						</tr>
						<tr>
							<th><label for="admin_url">고객 접속 URL</label></th>
							<td>
								<span><? echo $customer_access_url_header; ?></span>
								<input type="text" id="admin_url" name="admin_domain" value="<? echo $admin_info->domain; ?>" style="width:100px;" />
								<span>.<? echo $customer_access_url_footer; ?></span>
							</td>
						</tr>
						<tr>
							<th><label for="admin_manage_num">관리 번호</label></th>
							<td><input type="text" id="admin_manage_num" name="admin_manage_num" value="<? echo $admin_info->managenum; ?>" style="width:160px;" /></td>
						</tr>
						<tr class="last">
							<th><label for="admin_memo">비고(메모)</label></th>
							<td><textarea type="text" id="admin_memo" name="admin_memo" maxlength="100" rows="4" cols="60"/><? echo $admin_info->others; ?></textarea></td>
						</tr>
					</tbody>
				</table>			
			</form>	

			<div class="userlist_sd_btn">
				<a href="#" class="edit_admin_btn"><img src="<? echo IMG_DIR; ?>/sub/btn_save.gif" alt="저장" /></a>
				<!-- [D] 사용자 삭제 / 버튼을 클릭하면 삭제 확인 팝업창이 뜨면서 선택에 따라 사용자 삭제/유지됨. -->
				<a href="#" onClick="open_pop_admin_del(event, '<? echo $adid; ?>')"><img src="<? echo IMG_DIR; ?>/sub/btn_delete.gif" alt="삭제" /></a>
			</div>	
		</div>
	</div>	
</div>		
<!-- //유료 사용자 추가 -->
<!-- popup : recent_login_by_agent -->
<?
include_once("./popup/recent_login_by_agent.php");
?>
<!-- //popup : recent_login_by_agent -->
<!-- popup : admin_del -->
<?
include_once("./popup/admin_del.php");
?>
<!-- //popup : admin_del -->
<!-- popup : admin_edit_res -->
<?
include_once("./popup/admin_edit_res.php");
?>
<!-- //popup : admin_edit_res -->
<!-- popup : admin_add_res -->
<?
include_once("./popup/admin_add_res.php");
?>
<!-- //popup : admin_add_res -->
<?
include_once("../../../common/main_footer.php");
include_once("../../../common/html_foot.php");