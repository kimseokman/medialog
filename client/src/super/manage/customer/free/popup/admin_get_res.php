<?php
$admin_get_res_popup_action = $_POST['admin_get_res_popup_action'];
$admin_get_res_popup_msg = $_POST['admin_get_res_popup_msg'];

$remem_page = $_POST['remem_page'];
$remem_search = $_POST['remem_search'];
$remem_searchVal = $_POST['remem_searchVal'];

if($admin_get_res_popup_action == 'open'){
?>
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<span class="popup_title_txt"></span>
			<a href="#" class="close close_pop_admin_get_res" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p class="h_num70">
					<? echo $admin_get_res_popup_msg; ?>
				</p>
				<input type="hidden" class="remem_page" value="<? echo $remem_page; ?>" />
				<input type="hidden" class="remem_search" value="<? echo $remem_search; ?>" />
				<input type="hidden" class="remem_searchVal" value="<? echo $remem_searchVal; ?>" />
				<div class="btn">
					<a href="#" class="confirm_pop_admin_get_res"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<?
}//end of : if($admin_get_res_popup_action == 'open')
?>