<?php
$recent_login_by_agent_popup_action = $_POST['recent_login_by_agent_popup_action'];
$remem_adid = $_POST['remem_adid'];

$agent_arr = $main_obj->GetSupporterByAdnum($admin_num);
$data_size = count($agent_arr);
$last_node = $data_size - 1;

$remem_page = $_POST['remem_page'];
$remem_search = $_POST['remem_search'];
$remem_searchVal = $_POST['remem_searchVal'];

function print_last_bot($base, $current){
	$class_name = "";

	if($base == $current){
		$class_name = "last_bot";
	}

	return $class_name;
}

if($recent_login_by_agent_popup_action == 'open'){
?>
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<div id="popup" class="recent_login">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<span class="popup_title_txt"></span>
			<a href="#" class="close close_pop_recent_login_by_agent" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content" style="height:395px;">
				<!-- 지원기록 관리 상단 고정 th -->
				<table class="style_board_default manager_tbl"style="width:100%;" summary="상담원 ID,최근 로그인 시간">
					<caption><span>상담원 목록</span></caption>
					<colgroup>
						<col style="width:60px;" />
						<col style="width:*;" />
					</colgroup>
					<thead>
						<tr>
							<th class="no_line">ID</th>
							<th class="no_line last" style="padding-right:10px;">최근 로그인</th>
						</tr>
					</thead>
				</table>
				<div class="recent_login_box">
					<table class="style_board_default first_tbl manager_tbl"style="width:100%;" summary="날짜, 고객명,시작시간,종료시간,고객os,해결여부표">
						<caption><span>상담원 목록</span></caption>
						<colgroup>
							<col style="width:60px;" />
							<col style="width:*;" />
						</colgroup>
						<tbody>
						<?
						if(($data_size == 1) && ($agent_arr[0]['sptid'] == '')){// not exist report
						?>
							<tr class="last_bot">
								<td colspan="2">상담원이 없습니다.</td>
							</tr>
						<?
						}
						else{
							for($i=0; $i<$data_size; $i++){
								$show_last_login = $agent_arr[$i]['lastlogin'];
								if($show_last_login == "0000-00-00 00:00:00"){
									$show_last_login = "미접속";
								}
						?>	
							<tr class="is_data <? echo print_last_bot($last_node, $i); ?>">
								<td><? echo $agent_arr[$i]['sptid']; ?></td>
								<td><? echo $show_last_login; ?></td>
							</tr>
						<?
							}
						}
						?>
						</tbody>
					</table>
				</div>
				
				<input type="hidden" class="remem_adid" value="<? echo $remem_adid; ?>" />

				<input type="hidden" class="remem_page" value="<? echo $remem_page; ?>" />
				<input type="hidden" class="remem_search" value="<? echo $remem_search; ?>" />
				<input type="hidden" class="remem_searchVal" value="<? echo $remem_searchVal; ?>" />

				<div class="btn">
					<a href="#" class="confirm_pop_recent_login_by_agent"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<?
}//end of : if($recent_login_by_agent_popup_action == 'open')
?>