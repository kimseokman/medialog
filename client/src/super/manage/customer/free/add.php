<?php
include_once("../../../../_func/function.common.php");

include_once("../../../common/html_head.php");
include_once("../../../common/main_header.php");

$admin_act_type = ADMIN_ACT_TYPE_FREE;

$remember_page = $_POST['page'];
$remember_search = $_POST['search'];
$remember_searchVal = $_POST['searchVal'];

$id_length_max = ID_MAX_LENGTH;
$pw_length = PASSWORD_LENGTH;
$pw_length_min = PASSWORD_LENGTH_MIN;
$pw_length_max = PASSWORD_LENGTH_MAX;
$pw_safe_cnt = PASSWORD_SAFE_CNT;

$p_pc = PRODUCT_CODE_PC;
$p_mobile_paid = PRODUCT_CODE_MOBILE_PAID;
$p_mobile_free = PRODUCT_CODE_MOBILE_FREE;
$p_video_paid = PRODUCT_CODE_VIDEO_PAID;
$p_video_free = PRODUCT_CODE_VIDEO_FREE;
$p_mobile_video_paid = PRODUCT_CODE_MOBILE_VIDEO_PAID;
$p_mobile_video_free = PRODUCT_CODE_MOBILE_VIDEO_FREE;

$customer_access_url_header = CUSTOMER_ACCESS_URL_DEFAULT_HEADER;
$customer_access_url_footer = CUSTOMER_ACCESS_URL_DEFAULT_FOOTER;

$remember_admin_id = $_POST['admin_id'];
$remember_admin_enable = $_POST['admin_enable'];
if(!$_POST['admin_enable']){
	$remember_admin_enable = ADMIN_ACTIVE_STATUS_DEFAULT;	
}
$remember_admin_company = $_POST['admin_company'];
$remember_admin_service = $_POST['admin_service'];
$remember_admin_business_num = $_POST['admin_business_num'];
$remember_admin_name = $_POST['admin_name'];
$remember_admin_product = $_POST['admin_product'];
if(!$_POST['admin_product']){
	$remember_admin_product = PRODUCT_CODE_PC;
}

$remember_admin_pc_cnt = $_POST['admin_pc_cnt'];
if(!$_POST['admin_pc_cnt']){
	$remember_admin_pc_cnt = PC_VOLUME_DEFAULT;
}
$remember_admin_pc_m_n_v_cnt = $_POST['admin_pc_m_n_v_cnt'];
if(!$_POST['admin_pc_m_n_v_cnt']){
	$remember_admin_pc_m_n_v_cnt = M_N_V_VOLUME_DEFAULT;
}
$remember_admin_all_cnt = $_POST['admin_all_cnt'];
if(!$_POST['admin_all_cnt']){
	$remember_admin_all_cnt = ALL_VOLUME_DEFAULT;
}
$remember_admin_domain = $_POST['admin_domain'];
$remember_admin_manage_num = $_POST['admin_manage_num'];
$remember_admin_mail = $_POST['admin_mail'];
$remember_admin_memo = $_POST['admin_memo'];
$remember_admin_business_sector = $_POST['admin_business_sector'];
$remember_admin_phone = $_POST['admin_phone'];
$remember_admin_tel = $_POST['admin_tel'];
$remember_admin_service_join_date = $_POST['admin_service_join_date'];
if(!$_POST['admin_service_join_date']){
	$remember_admin_service_join_date = CURRENT_DATE_DEFAULT;
}
$remember_admin_service_start_date = $_POST['admin_service_start_date'];
if(!$_POST['admin_service_start_date']){
	$remember_admin_service_start_date = CURRENT_DATE_DEFAULT;
}
$remember_admin_service_expired_date = $_POST['admin_service_expired_date'];
if(!$_POST['admin_service_expired_date']){
	$remember_admin_service_expired_date = date("Y-m-d", strtotime(CURRENT_DATE_DEFAULT . " + ". ( SERVICE_FREE_TERM - 1 ) ." day"));
}
$remember_admin_bill_account = $_POST['admin_bill_account'];
$remember_admin_sign_num = $_POST['admin_sign_num'];

$business_list = $utils_obj->GetBusinessList();

function print_selected($base, $current){
	$attr_val = "";
	if($base == $current)
		$attr_val = "selected";

	return $attr_val;	
}

function print_checked($base, $current){
	$class_name = "";
	if($base == $current)
		$class_name = "checked";

	return $class_name;	
}
?>
<!-- popup : logout -->
<?
include_once("../../../common/popup/logout.php");
?>
<!-- //popup : logout -->
<!-- 유료 사용자 추가 -->
<div class="conts_box">
	<?
	include_once("../side.php");
	?>
	<div class="conts_right">
		<h3><img src="<? echo IMG_DIR; ?>/super/h3_free_customer_create.gif" alt="무료 고객 생성" /></h3>
	
		<div class="contents user_info">
			<div class="userlist_btn">
				<a href="#" onClick="admin_list(event, <? echo $remember_page; ?>, '<? echo $remember_search; ?>', '<? echo $remember_searchVal; ?>')"><img src="<? echo IMG_DIR; ?>/sub/btn_list.gif" alt="목록보기" /></a>
			</div>

			<form name="adminAddForm" method="post" action="/_func/function.create_admin.php">
				<input type="hidden" name="page" value="<? echo $remember_page; ?>" />
				<input type="hidden" name="search" value="<? echo $remember_search; ?>" />
				<input type="hidden" name="searchVal" value="<? echo $remember_searchVal; ?>" />
				
				<input type="hidden" name="admin_act_type" value="<? echo $admin_act_type; ?>" />
				
				<table class="style_board_userinfo" summary="" width="100%">
					<caption><span></span></caption>
					<colgroup>
						<col style="width:150px;" />
						<col style="width:*;" />
					</colgroup>
					<tbody>
						<tr>
							<th><label for="admin_id">ID</label></th>
							<td><input type="text" id="admin_id" name="admin_id" value="<? echo $remember_admin_id; ?>" style="width:160px;" /><span class="noti">영문,숫자 구성으로 <? echo $id_length_max; ?>자 이내</span></td>
						</tr>
						<tr>
							<th><label for="admin_enable">활성화</label></th>
							<td>
								<div class="choice_box admin_enable_box" style="margin-bottom:0px;">
									<span class="comm_sel"><!-- [150811] -->
										<select name="admin_enable">
											<option value="Y" <? echo print_selected("Y", $remember_admin_enable); ?>>Y</option>
											<option value="N" <? echo print_selected("N", $remember_admin_enable); ?>>N</option>
										</select>
									</span>
								</div>
							</td>
						</tr>
						<tr>
							<th><label for="admin_pw">PW</label></th>
							<td>
								<input type="password" id="admin_pw" name="admin_pw" value="" style="width:160px;" /><span class="noti"><? echo $pw_length_min; ?>~<? echo $pw_length_max; ?>자 내의 영문 대/소문자, 숫자, 특수문자 조합</span>
								<p class="imp ajax_admin_add_password_guide"></p>
							</td>
						</tr>
						<tr>
							<th><label for="admin_retype_pw">PW 확인</label></th>
							<td>
								<input type="password" id="admin_retype_pw" name="admin_retype_pw" value="" style="width:160px;" />
								<p class="imp ajax_admin_add_password2_guide"></p>
							</td>
						</tr>
						<tr>
							<th><label for="admin_company">회사명</label></th>
							<td><input type="text" id="admin_company" name="admin_company" value="<? echo $remember_admin_company; ?>" style="width:160px;" /></td>
						</tr>
						<!--tr>
							<th><label for="admin_service">서비스명</label></th>
							<td><input type="text" id="admin_service" name="admin_service" value="<? echo $remember_admin_service; ?>" style="width:160px;" /></td>
						</tr-->
						<tr>
							<th><label for="admin_business_num">사업자번호</label></th>
							<td><input type="text" id="admin_business_num" name="admin_business_num" value="<? echo $remember_admin_business_num; ?>" style="width:300px;" /></td>
						</tr>
						<tr>
							<th><label for="admin_business_sector">업종</label></th>
							<td>
								<div class="choice_box admin_business_sector_box" style="margin-bottom:0px;">
									<span class="comm_sel"><!-- [150811] -->
										<select name="admin_business_sector">
										<?
										$business_list_cnt = count($business_list);
										
										for($i = 0; $i < $business_list_cnt; $i++){
										?>
											<option value="<? echo $business_list[$i]["name"]; ?>" <? echo print_selected($business_list[$i]["name"], $remember_admin_business_sector); ?>><? echo $business_list[$i]["name"]; ?></option>
										<?
										}//end of : for($i = 0; $i < $pc_func_cnt; $i++)
										?>
										</select>
									</span>
								</div>
							</td>
						</tr>
						<tr>
							<th><label for="admin_name">담당자명</label></th>
							<td><input type="text" id="admin_name" name="admin_name" value="<? echo $remember_admin_name; ?>" style="width:160px;" /></td>
						</tr>
						<tr>
							<th><label for="admin_mail">이메일</label></th>
							<td><input type="text" id="admin_mail" name="admin_mail" value="<? echo $remember_admin_mail; ?>" style="width:300px;" /></td>
						</tr>
						<tr>
							<th><label for="admin_phone">연락처1</label></th>
							<td><input type="text" id="admin_phone" name="admin_phone" value="<? echo $remember_admin_phone; ?>" style="width:300px;" /></td>
						</tr>
						<tr>
							<th><label for="admin_tel">연락처2</label></th>
							<td>
								<input type="text" id="admin_tel" name="admin_tel" value="<? echo $remember_admin_tel; ?>" style="width:300px;" />
								<span class="imp">SMS 발송시 사용되는 번호</span>
							</td>
						</tr>
						<tr>
							<th><label for="admin_service_join_date">가입일자</label></th>
							<td><input type="text" id="admin_service_join_date" name="admin_service_join_date" value="<? echo $remember_admin_service_join_date; ?>" style="width:160px;" /></td>
						</tr>
						<?
						if($admin_act_type == ADMIN_ACT_TYPE_PAID){
						?>
						<tr>
							<th><label for="admin_service_start_date">개통일자</label></th>
							<td><input type="text" id="admin_service_start_date" name="admin_service_start_date" value="<? echo $remember_admin_service_start_date; ?>" style="width:160px;" /></td>
						</tr>
						<?
						}
						else if($admin_act_type == ADMIN_ACT_TYPE_FREE){
						?>
						<tr>
							<th><label for="admin_service_expired_date">만료일자</label></th>
							<td><input type="text" id="admin_service_expired_date" name="admin_service_expired_date" value="<? echo $remember_admin_service_expired_date; ?>" style="width:160px;" /></td>
						</tr>
						<?
						}
						?>
						<tr>
							<th><label>상품 선택</label></th>
							<td>
								<p>
									<input type="radio" id="admin_product_pc" name="admin_product" value="<? echo $p_pc; ?>" <? echo print_checked($p_pc, $remember_admin_product); ?> /> <label for="admin_product_pc">애니서포트 (PC)</label>
								</p>
								<p>
									<input type="radio" id="admin_product_pc_m" name="admin_product" value="<? echo $p_mobile_paid; ?>" <? echo print_checked($p_mobile_paid, $remember_admin_product); ?> /> <label for="admin_product_pc_m">애니서포트 + Mobile</label> 
								</p>
								<p>	
									<input type="radio" id="admin_product_pc_v" name="admin_product" value="<? echo $p_video_paid; ?>" <? echo print_checked($p_video_paid, $remember_admin_product); ?> /> <label for="admin_product_pc_v">애니서포트 + Video</label>
								</p>	
								<p>	
									<input type="radio" id="admin_product_pc_m_v" name="admin_product" value="<? echo $p_mobile_video_paid; ?>" <? echo print_checked($p_mobile_video_paid, $remember_admin_product); ?> /> <label for="admin_product_pc_m_v">애니서포트 + Mobile + Video</label>
								</p>
							</td>
						</tr>
						<tr>
							<th><label>상담원 수</label></th>
							<td>
								<p>
									<label for="admin_pc_cnt">애니서포트 PC</label> : <input type="number" id="admin_pc_cnt" name="admin_pc_cnt" value="<? echo $remember_admin_pc_cnt; ?>" min="0" max="500" />
								</p>
								<p>
									<label for="admin_pc_m_n_v_cnt">애니서포트 PC + Mobile / Video</label> : <input type="number" id="admin_pc_m_n_v_cnt" name="admin_pc_m_n_v_cnt" value="<? echo $remember_admin_pc_m_n_v_cnt; ?>" min="0" max="500" />
								</p>
								<p>
									<label for="admin_all_cnt">전체 : </label> <input type="number" id="admin_all_cnt" name="admin_all_cnt" value="<? echo $remember_admin_all_cnt; ?>" min="0" max="500" readonly />
								</p>
								<p class="imp admin_agent_cnt_msg"></p>
							</td>
						</tr>
						<tr>
							<th><label for="admin_bill_account">청구계정번호</label></th>
							<td><input type="text" id="admin_bill_account" name="admin_bill_account" value="<? echo $remember_admin_bill_account; ?>" style="width:160px;" /></td>
						</tr>
						<tr>
							<th><label for="admin_sign_num">가입번호</label></th>
							<td><input type="text" id="admin_sign_num" name="admin_sign_num" value="<? echo $remember_admin_sign_num; ?>" style="width:160px;" /></td>
						</tr>
						<tr>
							<th><label for="admin_url">고객 접속 URL</label></th>
							<td>
								<span><? echo $customer_access_url_header; ?></span>
								<input type="text" id="admin_url" name="admin_domain" value="<? echo $remember_admin_domain; ?>" style="width:100px;" />
								<span>.<? echo $customer_access_url_footer; ?></span>
							</td>
						</tr>
						<tr>
							<th><label for="admin_manage_num">관리 번호</label></th>
							<td><input type="text" id="admin_manage_num" name="admin_manage_num" value="<? echo $remember_admin_manage_num; ?>" style="width:160px;" /></td>
						</tr>
						<tr class="last">
							<th><label for="admin_memo">비고(메모)</label></th>
							<td>
								<textarea type="text" id="admin_memo" name="admin_memo" maxlength="100" rows="4" cols="60"/><? echo $remember_admin_memo; ?></textarea>
							</td>
						</tr>
					</tbody>
				</table>			
			</form>	

			<div class="userlist_sd_btn">
				<a href="#" class="create_admin_btn"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm2.gif" alt="확인" /></a>
			</div>	
		</div>
	</div>	
</div>		
<!-- //유료 사용자 추가 -->
<!-- popup : admin_add_res -->
<?
include_once("./popup/admin_add_res.php");
?>
<!-- //popup : admin_add_res -->
<?
include_once("../../../common/main_footer.php");
include_once("../../../common/html_foot.php");