<!-- manage agent : side navigation bar -->
<div class="conts_left">
	<h2><img src="<? echo IMG_DIR; ?>/super/lnb_h2_customer_list.gif" alt="고객정보관리" /></h2>
	<ul class="lnb">
		<li class="<? echo print_nb_on(SUPER_GNB_CUSTOMER_PAID, $current_url); ?>">
			<a href="<? echo SUPER_GNB_CUSTOMER_PAID_INDEX; ?>">
				<span>유료고객관리</span>
			</a>
		</li>
		<li class="<? echo print_nb_on(SUPER_GNB_CUSTOMER_FREE, $current_url); ?>">
			<a href="<? echo SUPER_GNB_CUSTOMER_FREE_INDEX; ?>">
				<span>무료고객관리</span>
			</a>
		</li>
	</ul>
</div>
<!-- //manage agent : side navigation bar -->