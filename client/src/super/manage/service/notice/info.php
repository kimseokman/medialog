<?php
include_once("../../../../_func/function.common.php");

include_once("../../../common/html_head.php");
include_once("../../../common/main_header.php");

$seq = $_POST['seq'];
$remember_page = $_POST['remember_page'];
$remember_type = $_POST['remember_type'];
$remember_val = $_POST['remember_val'];
$data = $main_obj->GetNotice($seq);
?>
<!-- popup : logout -->
<?
include_once("../../../common/popup/logout.php");
?>
<!-- //popup : logout -->
<div class="conts_box">
	<?
	include_once("../side.php");
	?>
	<div class="conts_right">
		<!-- [D] 공지사항 전체리스트 화면 -->
		<h3><img src="<? echo IMG_DIR; ?>/sub/h3_site_noti.gif" alt="공지사항" /></h3>
		
		<div class="contents site_noti_view">
			<div class="userlist_btn">
				<a href="#" class="go_to_notice_list_btn">
					<img src="<? echo IMG_DIR; ?>/sub/btn_list.gif" alt="목록보기" />
				</a>
			</div>
			
			<!-- 공지사항 상세화면 -->
			<form name="editNoticeForm" method="post" action="/_func/function.edit_notice.php">
				<input type="hidden" class="remember_page" name="remember_page" value="<? echo $remember_page; ?>" />
				<input type="hidden" class="remember_type" name="remember_type" value="<? echo $remember_type; ?>" />
				<input type="hidden" class="remember_val" name="remember_val" value="<? echo $remember_val; ?>" />

				<input type="hidden" name="seq" value="<? echo $seq; ?>" />
				<table class="style_board_view" summary="글제목, 날짜가 있는 게시판" width="100%">
					<caption><span>글제목, 날짜가 있는 게시판</span></caption>
					<colgroup>
						<col style="width:675px;" />
						<col style="width:35px;" />
						<col style="width:68px;" />
					</colgroup>
					<thead>
						<tr>
							<th class="title"><input type="text" name="title" value="<? echo $data['title']; ?>" style="width:500px" /></th>
							<th class="date"><span>이름</span></th>
							<th><input type="text" name="name" value="<? echo $data['name']; ?>" style="width:80px" /></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="3">
								<textarea name="content" rows="5" cols="20" style="width:100%;height:423px;"><? echo htmlspecialchars($data['content']); ?></textarea>
							</td>
						</tr>
					</tbody>
				</table>
			</form>	
			<div class="btn_list">
				<a href="#" class="edit_notice_btn"><img src="<? echo IMG_DIR; ?>/sub/btn_save.gif" alt="저장" /></a>
				<a href="#" onClick="open_pop_notice_del(event, '<? echo $seq; ?>')"><img src="<? echo IMG_DIR; ?>/sub/btn_delete.gif" alt="삭제" /></a>
			</div>
		</div>
	</div>
</div>
<!-- popup : notice_del -->
<?
include_once("./popup/notice_del.php");
?>
<!-- //popup : notice_del -->	
<?
include_once("../../../common/main_footer.php");
include_once("../../../common/html_foot.php");