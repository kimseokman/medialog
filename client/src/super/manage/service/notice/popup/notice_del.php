<?php
$notice_del_popup_action = $_POST['notice_del_popup_action'];
$remember_seq = $_POST['remember_seq'];
$remember_page = $_POST['remember_page'];
$remember_type = $_POST['remember_type'];
$remember_val = $_POST['remember_val'];

if($notice_del_popup_action == 'open'){
?>
<!-- 사용자 삭제 창 -->
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<span class="popup_title_txt"></span>
			<a href="#" class="close close_pop_notice_del" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p class="h_num70">선택하신 관리자를 삭제하시겠습니까?</p>

				<input type="hidden" class="remember_seq" value="<? echo $remember_seq; ?>" />

				<input type="hidden" class="remember_page" value="<? echo $remember_page; ?>" />
				<input type="hidden" class="remember_type" value="<? echo $remember_type; ?>" />
				<input type="hidden" class="remember_val" value="<? echo $remember_val; ?>" />
				
				<div class="btn">
					<a href="#" class="yes_pop_notice_del"><img src="<? echo IMG_DIR; ?>/popup/btn_yes.gif" alt="예" /></a>
					<a href="#" class="no_pop_notice_del"><img src="<? echo IMG_DIR; ?>/popup/btn_no.gif" alt="아니오" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //사용자 삭제 창 -->
<?
}//end of : if($notice_del_popup_action == 'open')
?>