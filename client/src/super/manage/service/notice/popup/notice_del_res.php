<?php
$notice_del_res_popup_action = $_POST['notice_del_res_popup_action'];
$notice_del_res_popup_msg = $_POST['notice_del_res_popup_msg'];

if($notice_del_res_popup_action == 'open'){
?>
<!-- 사용자가 삭제된 결과 창 -->
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<span class="popup_title_txt"></span>
			<a href="#" class="close close_pop_notice_del_res" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p class="h_num70">
					<? echo $notice_del_res_popup_msg; ?>
				</p>

				<div class="btn">
					<a href="#" class="confirm_pop_notice_del_res"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //사용자가 삭제된 결과 창 -->
<?
}//end of : if($notice_del_res_popup_action == 'open')
?>