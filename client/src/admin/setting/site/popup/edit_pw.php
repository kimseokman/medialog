<?php
$edit_pw_popup_action = $_POST['edit_pw_popup_action'];

if($edit_pw_popup_action == 'open'){
	$edit_pw_popup_res = $_POST['edit_pw_popup_res'];
	$edit_pw_popup_msg = $_POST['edit_pw_popup_msg'];
	$edit_now_pw = $_POST['edit_now_pw'];
	$edit_new_pw = $_POST['edit_new_pw'];
	$edit_retype_pw = $_POST['edit_retype_pw'];
?>
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<!-- 새비밀번호설정 팝업 -->
	<div id="popup" class="new_pw">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<a href="#" class="close close_pop_edit_pw" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap" style="height:245px;">
			<div class="popup_content">
				<table class="style_board_userinfo" summary="비밀번호 변경" width="100%">
					<caption><span>비밀번호 변경</span></caption>
					<colgroup>
						<col style="width:118px;" />
						<col style="width:*;" />
					</colgroup>
					<tbody>
						<tr>
							<th><label for="now_pw">현재 비밀번호</label></th>
							<td><input type="password" id="now_pw" value="<? echo $edit_now_pw; ?>" size="20" /></td>
						</tr>
						<tr>
							<th><label for="new_pw">새 비밀번호</label></th>
							<td><input type="password" id="new_pw" value="<? echo $edit_new_pw; ?>" size="20" /></td>
						</tr>
						<tr class="last">
							<th><label for="c_new_pw">새 비밀번호 확인</label></th>
							<td><input type="password" id="c_new_pw" value="<? echo $edit_retype_pw; ?>" size="20" /></td>
						</tr>
					</tbody>
				</table>
				<!-- [D] 1. 입력 내용이 잘못되었을 때 출력되는 문구 -->
				
				<?
				if($edit_pw_popup_res == 'FAIL'){
				?>
				<p class="imp_ico"><? echo $edit_pw_popup_msg; ?></p>
				<?
				}//end of : if($edit_pw_popup_res == 'FAIL')
				?>
				
				<!-- [D] 2. 현재 비밀번호 정책에 맞지 않을 때 출력되는 문구 -->
				<!--p class="imp_ico">특수,영문자,숫자 중 최소 2개 이상 조합으로<br />설정해 주세요.</p-->
				<div class="btn">
					<a href="#" class="confirm_pop_edit_pw"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
	<!-- //새비밀번호설정 팝업 -->
</div>
<?
}//end of : if($edit_pw_popup_action == 'open')
?>