<?php
include_once("../../../_func/function.common.php");

include_once("../../../common/html_head.php");
include_once("../../../common/header.php");

if($_SESSION['site_auth'] == "PASS"){
	$utils_obj->RedirectURL(SETTING_SITE_INDEX);
}

$isMsg = FALSE;
if($_POST['msg'] != NULL){
	$isMsg = TRUE;
	$msg = $_POST['msg'];	
}

?>
<!-- popup : logout -->
<?
include_once("../../../common/popup/logout.php");
?>
<!-- //popup : logout -->
<div class="conts_box">
	<?
	include_once("../side.php");
	?>
	<div class="conts_right">
		<!-- [D] 사이트설정 화면 -->
		<h3><img src="<? echo IMG_DIR; ?>/sub/h3_site_set.gif" alt="사이트 설정" /></h3>
		<div class="contents site_set type_01">
			<form name="siteAuthForm" method="post" action="/_func/function.site_auth.php">
				<table class="style_board_userinfo" summary="" width="100%">
					<caption><span></span></caption>
					<colgroup>
						<col style="width:*;" />
					</colgroup>
					<tbody>
						<tr>
							<th>개인정보 보호를 위하여 비밀번호를 재확인 합니다.</th>
						</tr>
						<tr>
							<td>
								<div class="style_151102">
									<input type="hidden" name="adid" value="<? echo $admin->adid; ?>" />
									<input type="password" placeholder="비밀번호" class="type01" name="site_auth_password" style="width:239px;">
									<a href="#" class="site_auth_btn"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm3.gif" alt="확인"></a>
								</div>
							</td>
						</tr>
						<tr>
							<td class="type_01">
							<?
							if($isMsg){
							?>
								<p class="imp_ico"><? echo $msg; ?></p>
							<?
							}
							?>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
	</div>
</div>
<?
include_once("../../../common/footer.php");
include_once("../../../common/html_foot.php");