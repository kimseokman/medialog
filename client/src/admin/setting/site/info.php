<?php
include_once("../../../_func/function.common.php");

include_once("../../../common/html_head.php");
include_once("../../../common/header.php");

if($_SESSION['site_auth'] != "PASS"){
	$utils_obj->RedirectURL(SETTING_SITE_INDEX_AUTH);
}

$agent_cnt = $main_obj->GetSupporterNum();
$remain_cnt = $admin->volume - $agent_cnt;

$custom_url = "-";
if(!$admin->url){
	$custom_url = "-";
}
else{
	$custom_url = $admin->url;	
}

$logo_file = $admin->logo_url;
$logo_url = CUSTOMER_LOGO_DIR . "/" . $logo_file;
$logo_flag = file_exists($_SERVER['DOCUMENT_ROOT'] . $logo_url);

$setting_site_edit_res_log_action = $_POST['setting_site_edit_res_log_action'];
if(!$_POST['setting_site_edit_res_log_action']){
	$setting_site_edit_res_log_action = 'hide';
}
$setting_site_edit_res_log_msg = $_POST['setting_site_edit_res_log_msg'];
$setting_site_edit_res_log = $_POST['setting_site_edit_res_log'];
?>
<!-- popup : logout -->
<?
include_once("../../../common/popup/logout.php");
?>
<!-- //popup : logout -->
<div class="conts_box">
	<?
	include_once("../side.php");
	?>
	<div class="conts_right">
		<!-- [D] 사이트설정 화면 -->
		<h3><img src="<? echo IMG_DIR; ?>/sub/h3_site_set.gif" alt="사이트 설정" /></h3>
		<!-- 생성된 전체 부서 리스트 -->
		<div class="contents site_set">
			<!-- 사용자정보 폼 테이블 -->
			<!-- [D] 값들은 관리자가 임의로 수정 가능. 비밀번호설정은 LG U+ 비밀번호 설정 정책에 따름 -->
			<form name="settingSiteForm" method="POST" action="/_func/function.edit_setting_site.php" enctype="multipart/form-data">
				<table class="style_board_userinfo" summary="" width="100%">
					<caption><span></span></caption>
					<colgroup>
						<col style="width:150px;" />
						<col style="width:*;" />
					</colgroup>
					<tbody>
						<tr>
							<th><label for="admin_company">회사</label></th>
							<td>
								<input type="text" id="admin_company" name="company" placeholder="회사명을 입력하세요." value="<? echo $admin->company; ?>" style="width:250px;" />
								<?
								if($setting_site_edit_res_log == "EMPTY_COMPANY"){
								?>
								<span class="imp_ico"><? echo $setting_site_edit_res_log_msg; ?></span>
								<?
								}
								?>
							</td>
						</tr>
						<tr>
							<th>아이디</th>
							<td><? echo $admin->adid; ?></td>
						</tr>
						<tr>
							<th>비밀번호</th>
							<!-- [D] 버튼을 클릭하면 비밀번호변경 팝업창 생성. / 초기 비밀번호는 ID와 동일하게 지급 -->
							<td><a href="#" class="open_pop_edit_pw"><img src="<? echo IMG_DIR; ?>/sub/btn_changepw.gif" alt="비밀번호 변경" /></a></td>
						</tr>
						<tr>
							<th><label for="admin_name">담당자 이름</label></th>
							<td>
								<input type="text" id="admin_name" name="name" placeholder="담당자명을 입력하세요." value="<? echo $admin->name; ?>" style="width:250px;" />
								<?
								if($setting_site_edit_res_log == "EMPTY_NAME"){
								?>
								<span class="imp_ico"><? echo $setting_site_edit_res_log_msg; ?></span>
								<?
								}
								?>
							</td>
						</tr>
						<!-- [D] 이메일,연락처, 연락처2:
							텍스트 입력으로 변경가능. / 입력창을 클릭하면 안내텍스트 사라짐 -->
						<tr>
							<th><label for="site_email">이메일</label></th>
							<td>
								<input type="text" id="site_email" name="email" placeholder="이메일을 입력하세요. (ex. abc@lguplus.co.kr)" value="<? echo $admin->email; ?>" style="width:250px;" />
								<?
								if( ($setting_site_edit_res_log == "EMPTY_MAIL") || ($setting_site_edit_res_log == "LONG_MAIL") || ($setting_site_edit_res_log == "INVALIDE_MAIL") ){
								?>
								<span class="imp_ico"><? echo $setting_site_edit_res_log_msg; ?></span>
								<?
								}
								?>
							</td>
						</tr>
						<tr>
							<th><label for="site_contact">연락처</label></th>
							<td>
								<input type="text" id="site_contact" name="phone" placeholder="연락처를 입력해주세요." value="<? echo $admin->phone; ?>" style="width:250px;" />
								<?
								if($setting_site_edit_res_log == "EMPTY_PHONE"){
								?>
								<span class="imp_ico"><? echo $setting_site_edit_res_log_msg; ?></span>
								<?
								}
								?>
							</td>
						</tr>
						<tr>
							<th><label for="site_contact2">연락처2</label></th>
							<td>
								<input type="text" id="site_contact2" name="tel" placeholder="연락처2를 입력해주세요." value="<? echo $admin->tel; ?>" style="width:250px;" />
								<?
								if($setting_site_edit_res_log == "EMPTY_TEL"){
								?>
								<span class="imp_ico"><? echo $setting_site_edit_res_log_msg; ?></span>
								<?
								}
								?>
							</td>
						</tr>
						<!-- [D] 현재계정이 생성된 상담원 수(괄호안은 생성가능한 전체상담원 수) -->
						<tr>
							<th>상담원 수</th>
							<td><span><? echo $agent_cnt; ?> 명</span> ( 전체 : <span><? echo $admin->volume; ?> 명</span> | 생성 가능수 : <span><? echo $remain_cnt; ?> 명</span> )</td>
						</tr>
						<!-- [D] 전용 접속사이트가 있을때만 입력. / 없으면 '-'로 처리 -->
						<tr>
							<th>전용 접속 도메인</th>
							<td><? echo $custom_url; ?></td>
						</tr>
						<!-- [D] 전용 접속사이트에 노출되는 로고이미지 등록:
							파일로 첨부하여 등록할수 있고, 등록한 이미지는 전용 접속사이트에 노출됨 / 파일을 등록하면 이미지 사이즈는 자동으로 맞춰지 *사이즈:262X74* -->
						<tr class="last">
							<th>전용 접속 사이트<br />이미지 등록<br />( <span class="imp"><? echo LOGO_IMG_WIDTH; ?></span>px * <span class="imp"><? echo LOGO_IMG_HEIGHT; ?></span>px )</th>
							<td>
								<div class="fint_image">
									<input type="text" id="logo_file_path" value="" style="width:330px;" />
									<input type="file" id="select_file" name="logo_url" style="display:none;"/>
									<a href="#" class="select_file"><img src="<? echo IMG_DIR; ?>/sub/btn_find.gif" alt="찾아보기" /></a>
								</div>
								<div class="bg_submit_img">
								<?
								if($logo_flag && $admin->logo_url != ""){
								?>
								<img src="<? echo $logo_url; ?>" alt="전용 접속 사이트 이미지" style="width:262px; height:74px;" />
								<?	
								}
								else{
								?>
								<img src="<? echo BASE_LOGO_DIR . "/" . BASE_LOGO; ?>" alt="전용 접속 사이트 이미지" style="width:262px; height:74px;" />
								<?
								}
								?>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form>

			<!-- [D] 저장버튼 클릭후, 입력한 내용이 잘못되었을때 안내문구 출력 영역 / 저장이 완료되면 완료안내팝업 생성(시스템팝업) -->
			<!-- [D] (1) 이메일 주소 입력방식이 잘못되었을 때 안내문구 출력됨 (ex: xxxx@xxxx.xx.xx) -->
			<!--<p class="imp_ico">이메일 주소를 확인해주세요.</p>-->
			<!-- [D] (2) 입력되지 않은 항목이 있을때 안내문구 출력됨 -->

			<div class="userlist_sd_btn">
				<!-- [D] 적용내용 저장 -->
				<a href="#" class="site_setting_save_btn"><img src="<? echo IMG_DIR; ?>/sub/btn_save.gif" alt="저장" /></a>
			</div>
		</div>
	</div>
</div>
<!-- popup : edit_setting_site_res -->
<?
include_once("./popup/edit_setting_site_res.php");
?>
<!-- //popup : edit_setting_site_res -->
<!-- popup : edit_pw -->
<?
include_once("./popup/edit_pw.php");
?>
<!-- //popup : edit_pw -->
<?
include_once("../../../common/footer.php");
include_once("../../../common/html_foot.php");