<?php
include_once("../../../_func/function.common.php");

include_once("../../../common/html_head.php");
include_once("../../../common/header.php");

$notice_page = $_POST['page'];
if(!$_POST['page']){
	$notice_page = 1;//page : 0 -> total	
}

$notice_search_type = $_POST['search_type'];
if(!$_POST['search_type']){
	$notice_search_type = "";
}

$notice_search_val = $_POST['search_val'];
if(!$_POST['search_val']){
	$notice_search_val = "";
}

$notice_arr = $main_obj->GetNoticeList($notice_page, $notice_search_type, $notice_search_val);

$notice_pagination = $notice_arr['pagination'];
$notice_content = $notice_arr['content'];
$notice_page_num = $notice_arr['page_num'];

function print_last($row_index, $last_index){
	if($row_index == $last_index)
		return "last";
}

function print_imp($print_flag){
	if($print_flag)
		return "imp";
}

function print_selected($base, $current){
	$attr_val = "";
	if($base == $current)
		$attr_val = "selected";

	return $attr_val;	
}

?>
<!-- popup : logout -->
<?
include_once("../../../common/popup/logout.php");
?>
<!-- //popup : logout -->
<div class="conts_box">
	<?
	include_once("../side.php");
	?>
	<div class="conts_right">
		<!-- [D] 공지사항 전체리스트 화면 -->
		<h3><img src="<? echo IMG_DIR; ?>/sub/h3_site_noti.gif" alt="공지사항" /></h3>
		<div class="contents site_noti">
			<div class="ajax_notice_content">
				<!-- [D] 제목을 클릭하면 해당 공지사항의 상세화면으로 이동 -->
				<table class="style_board_default" summary="글번호, 분류, 제목, 작성자, 날짜가 있는 게시판" width="100%">
					<caption><span>글번호, 분류, 제목, 작성자, 날짜가 있는 게시판</span></caption>
					<colgroup>
						<col style="width:37px;" />
						<col style="width:*;" />
						<col style="width:56px;" />
						<col style="width:75px;" />
					</colgroup>
					<thead>
						<tr>
							<th>NO</th>
							<th>제목</th>
							<th>작성자</th>
							<th class="last">날짜</th>
						</tr>
					</thead>
					<tbody>
					<?
					$notice_content_size = sizeof($notice_content);
					if($notice_content_size == 0){
					?>
						<tr class="last">
							<td colspan="4">검색 결과 없음</td>
						</tr>
					<?
					}//end of : if($notice_content_size == 0)
					else{
						$notice_last_node_num = $notice_content_size - 1;
						for ($i = 0; $i < $notice_content_size; $i++){
					?>
						<tr class="<? echo print_last($i, $notice_last_node_num); ?>">
							<td><? echo $notice_content[$i]['num']; ?></td>
							<td class="title">
								<a href="#" onClick="notice_detail(event, <? echo $notice_content[$i]['num']; ?>, <? echo $notice_page; ?>, '<? echo $notice_search_type; ?>', '<? echo $notice_search_val; ?>')"><? echo $notice_content[$i]['title']; ?></a>
							</td>
							<td><? echo $notice_content[$i]['name']; ?></td>
							<td><? echo $notice_content[$i]['regdate']; ?></td>
						</tr>
					<?
						}//end of : for ($i = 0; $i < sizeof($notice_content); $i++)
					}//end of : if($notice_content_size != 0)
					?>
					</tbody>
				</table>

				<!-- [D] 공지 항목이 10개가 초과되면 페이징처리 -->
				<?
				$page_prev = $notice_pagination->GetIsPrev();
				$page_list = $notice_pagination->GetPageList();
				$page_next = $notice_pagination->GetIsNext();
				?>
				<div class="paging">
				<?
				if($page_prev['is_older_page']){
				?>
					<a href="#" class="btn_arrow ar_fst" onClick="notice_pagination(event, <? echo $page_prev['older_page']; ?>)">첫페이지</a>
				<?
				}
				else{
				?>
					<a href="#" class="btn_arrow ar_fst">첫페이지</a>
				<?
				}
				if($page_prev['is_prev_page']){
				?>
					<a href="#" class="btn_arrow ar_prev" onClick="notice_pagination(event, <? echo $page_prev['prev_page']; ?>)">이전페이지</a>
				<?
				}
				else{
				?>
					<a href="#" class="btn_arrow ar_prev">이전페이지</a>
				<?
				}

				while(list($k, $v) = each($page_list)){
				?>
					<a href="#" onClick="notice_pagination(event, <? echo $k; ?>)"><span class="<? echo print_imp($v['is_current']);?>"><? echo $k; ?></span></a>
				<?
				}//end of : while(list($k, $v) = each($page_list))
				
				if($page_next['is_next_page']){
				?>
					<a href="#" class="btn_arrow ar_next" onClick="notice_pagination(event, <? echo $page_next['next_page']; ?>)"><span class="hidden">다음페이지</span></a>
				<?
				}
				else{
				?>
					<a href="#" class="btn_arrow ar_next"><span class="hidden">다음페이지</span></a>
				<?
				}

				if($page_next['is_newer_page']){
				?>
					<a href="#" class="btn_arrow ar_last" onClick="notice_pagination(event, <? echo $page_next['newer_page']; ?>)"><span class="hidden">마지막페이지</span></a>
				<?
				}
				else{
				?>
					<a href="#" class="btn_arrow ar_last"><span class="hidden">마지막페이지</span></a>
				<?
				}
				?>
				</div>
			</div>
			<!-- [D] 서비스/이벤트 선택하고 검색어 입력후, 검색어 내용있을경우 해당내용 화면출력 / 검색어 내용없을경우 화면이동 없음. -->
			<div class="search_box">
				<div class="u_biz_noti">
					<div class="choice_box u_biz_noti_input">
						<span class="comm_sel w_105"><!-- [150811] -->
							<select>
								<option value="title" <? echo print_selected('title', $notice_search_type); ?> >제목</option>
								<option value="name" <? echo print_selected('name', $notice_search_type); ?> >작성자</option>
							</select>
						</span>
						<div class="fl"><!-- [150811] -->
							<input type="text" class="notice_search_val" value="<? echo $notice_search_val; ?>" style="width:180px;" /><a href="#" class="search_notice_btn"><img src="<? echo IMG_DIR; ?>/board/btn_search.gif" alt="검색" /></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?
include_once("../../../common/footer.php");
include_once("../../../common/html_foot.php");