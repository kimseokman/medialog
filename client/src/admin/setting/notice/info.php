<?php
include_once("../../../_func/function.common.php");

include_once("../../../common/html_head.php");
include_once("../../../common/header.php");

$seq = $_POST['seq'];
$remember_page = $_POST['remember_page'];
$remember_type = $_POST['remember_type'];
$remember_val = $_POST['remember_val'];
$data = $main_obj->GetNotice($seq);
?>
<!-- popup : logout -->
<?
include_once("../../../common/popup/logout.php");
?>
<!-- //popup : logout -->
<div class="conts_box">
	<?
	include_once("../side.php");
	?>
	<div class="conts_right">
		<!-- [D] 공지사항 전체리스트 화면 -->
		<h3><img src="<? echo IMG_DIR; ?>/sub/h3_site_noti.gif" alt="공지사항" /></h3>
		<div class="contents site_noti_view">
			<input type="hidden" class="remember_page" value="<? echo $remember_page; ?>" />
			<input type="hidden" class="remember_type" value="<? echo $remember_type; ?>" />
			<input type="hidden" class="remember_val" value="<? echo $remember_val; ?>" />
			<!-- 공지사항 상세화면 -->
			<table class="style_board_view" summary="글제목, 날짜가 있는 게시판" width="100%">
				<caption><span>글제목, 날짜가 있는 게시판</span></caption>
				<colgroup>
					<col style="width:675px;" />
					<col style="width:35px;" />
					<col style="width:68px;" />
				</colgroup>
				<thead>
					<tr>
						<th class="title"><? echo $data['title']; ?></th>
						<th class="date"><span>날짜</span></th>
						<th><? echo $data['regdate']; ?></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="3">
							<? echo preg_replace("/\r\n/", "<br />", $data['content']); ?>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="btn_list">
				<a href="#" class="go_to_notice_list_btn">
					<img src="<? echo IMG_DIR; ?>/sub/btn_notilist.gif" alt="목록" />
				</a>
			</div>
		</div>
	</div>
</div>	
<?
include_once("../../../common/footer.php");
include_once("../../../common/html_foot.php");