<?php
include_once("../../../../_func/function.common.php");

include_once("../../../../common/html_head.php");
include_once("../../../../common/header.php");

$start_time = $_POST['start_time'];
if(!$_POST['start_time']){
	$start_time = date("Y-m-d");
	$start_YYYY = date("Y");
	$start_MM = date("m");
	$start_DD = date("d");
}
else{
	$start_time_arr = explode("-", $start_time);
	$start_YYYY = $start_time_arr[0];
	$start_MM = $start_time_arr[1];
	$start_DD = $start_time_arr[2];
}

$end_time = $_POST['end_time'];
if(!$_POST['end_time']){
	$end_time = date("Y-m-d");
	$end_YYYY = date("Y");
	$end_MM = date("m");
	$end_DD = date("d");
}
else{
	$end_time_arr = explode("-", $end_time);
	$end_YYYY = $end_time_arr[0];
	$end_MM = $end_time_arr[1];
	$end_DD = $end_time_arr[2];
}

$year_arr = $utils_obj->GetYears();
$YYYY_cnt = count($year_arr);
$month_arr = $utils_obj->GetMonths();
$MM_cnt = count($month_arr);
$this_last_day = $utils_obj->GetLastDay(date('m'), date('Y'));
$day_arr = $utils_obj->GetDays($this_last_day);
$DD_cnt = count($day_arr);

function print_selected($base, $current){
	$str_value = "";

	if($base == $current){
		$str_value = "selected";
	}

	return $str_value;
}

$stats_arr = $main_obj->GetReportByTroubleType($start_time, $end_time);
$stats_size = count($stats_arr);
$last_node = $stats_size - 1;

$total = 0;
for($i=0; $i < $stats_size; $i++){

	$total += $stats_arr[$i]["res1"] + $stats_arr[$i]["res2"] + $stats_arr[$i]["res3"] + $stats_arr[$i]["res0"];
}

$totalPer = 0;
for($i=0; $i < $stats_size; $i++){

	$stats_sum = $stats_arr[$i]["res1"] + $stats_arr[$i]["res2"] + $stats_arr[$i]["res3"] + $stats_arr[$i]["res0"];
	if($total != 0){
		$per[$i] = $rate = $stats_sum/$total*100;
	}
	else{
		$per[$i] = 0;
	}
	$totalPer += $per[$i];
}

function print_last($base, $current){
	$class_name = "";

	if($base == $current){
		$class_name = "last";
	}

	return $class_name;
}
?>
<!-- popup : logout -->
<?
include_once("../../../../common/popup/logout.php");
?>
<!-- //popup : logout -->
<div class="conts_box">
	<?
	include_once("../side.php");
	?>
	<div class="conts_right">
		<!-- [D] 시간대별 통계확인 화면 -->
		<h3><img src="<? echo IMG_DIR; ?>/sub/h3_error_time.gif" alt="장애유형별 통계 보기" /></h3>
		<div class="contents supp_logg index_t">
			<!-- [D] 검색기간을 설정하여 조회 -->
			<div class="search_box2">
				<div class="search_box2_wrap">
					<strong class="search_box2_tit">검색기간</strong>
					<div class="search_box2_cont">
						<!-- [D] 버튼을 클릭하면 클릭한기간의 내용이 검색되어 하단에 출력됨. -->
						<div class="period">
							<a href="#" class="force_search week_a"><span class="week">1주일</span></a>
							<a href="#" class="force_search month_1_a"><span class="month_1">1개월</span></a>
							<a href="#" class="force_search month_3_a"><span class="month_3">3개월</span></a>
							<a href="#" class="force_search month_6_a"><span class="month_6">6개월</span></a>
						</div>
						<!-- [D] 디폴트는 당일날짜. 셀렉트박스의 날짜를 선택하여 기간설정 가능 -->
						<div class="date">
							<div class="choice_box search_trouble_statistics_box">
								<span class="comm_sel w_70 start_YYYY"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $YYYY_cnt; $i++){
									?>
										<option value="<? echo $year_arr[$i]; ?>" <? echo print_selected($start_YYYY, $year_arr[$i]);?> ><? echo $year_arr[$i]; ?></option>
									<?
									}//for($i=0; $i < $YYYY_cnt; $i++)
									?>
									</select>
								</span>
								<span class="comm_sel w_55 start_MM"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $MM_cnt; $i++){
									?>
										<option value="<? echo $month_arr[$i]; ?>" <? echo print_selected($start_MM, $month_arr[$i]); ?> ><? echo str_pad($month_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
									<?
									}//for($i=0; $i < $MM_cnt; $i++)
									?>
									</select>
								</span>
								<span class="comm_sel w_55 start_DD"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $DD_cnt; $i++){
									?>
										<option value="<? echo $day_arr[$i]; ?>" <? echo print_selected($start_DD, $day_arr[$i]); ?> ><? echo str_pad($day_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
									<?
									}//for($i=0; $i < $DD_cnt; $i++)pad
									?>
									</select>
								</span>
								<div class="cen_line">~ </div>
								<span class="comm_sel w_70 end_YYYY"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $YYYY_cnt; $i++){
									?>
										<option value="<? echo $year_arr[$i]; ?>" <? echo print_selected($end_YYYY, $year_arr[$i]);?> ><? echo $year_arr[$i]; ?></option>
									<?
									}//for($i=0; $i < $YYYY_cnt; $i++)
									?>
									</select>
								</span>
								<span class="comm_sel w_55 end_MM"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $MM_cnt; $i++){
									?>
										<option value="<? echo $month_arr[$i]; ?>" <? echo print_selected($end_MM, $month_arr[$i]); ?> ><? echo str_pad($month_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
									<?
									}//for($i=0; $i < $MM_cnt; $i++)
									?>
									</select>
								</span>
								<span class="comm_sel w_55 end_DD"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $DD_cnt; $i++){
									?>
										<option value="<? echo $day_arr[$i]; ?>" <? echo print_selected($end_DD, $day_arr[$i]); ?> ><? echo str_pad($day_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
									<?
									}//for($i=0; $i < $DD_cnt; $i++)pad
									?>
									</select>
								</span>
								<a href="#" class="search_trouble_statistics"><img src="<? echo IMG_DIR; ?>/board/btn_search.gif" alt="검색" /></a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="useradd_btn">
				<!-- [D] 엑셀다운로드버튼: 하단리스트에 대한 전체내용이 엑셀파일로 다운로드됨. -->
				<a href="#" class="export_trouble_statistics"><img src="<? echo IMG_DIR; ?>/sub/btn_exeldown.gif" alt="엑셀다운로드" /></a>
			</div>
			
			<div class="ajax_trouble_statistics">
				<!-- [D] 디폴트는 당일 시간대별 리스트 -->
				<table class="style_board_default" summary="번호, 부서명, 설명, 수정/삭제가 있는 게시판" width="100%">
					<caption><span>번호, 부서명, 설명, 수정/삭제가 있는 게시판</span></caption>
					<colgroup>
						<col style="width:18%;" />
						<col style="width:15%;" />
						<col style="width:15%;" />
						<col style="width:%;" />
						<col style="width:15%;" />
						<col style="width:15%;" />
					</colgroup>
					<thead>
						<tr>
							<th>장애유형</th>
							<th>해결</th>
							<th>보류</th>
							<th>직접지원 필요</th>
							<th>미체크</th>
							<th class="last">백분율</th>
						</tr>
					</thead>
					<tbody>
					<?
					for($i=0; $i < $stats_size; $i++){
					?>
						<tr class="<? echo print_last($last_node, $i); ?>">
							<td><? echo $stats_arr[$i]['trbname']; ?></td>
							<td><? echo $stats_arr[$i]['res1']; ?></td>
							<td><? echo $stats_arr[$i]['res2']; ?></td>
							<td><? echo $stats_arr[$i]['res3']; ?></td>
							<td><? echo $stats_arr[$i]['res0']; ?></td>
							<td><? echo sprintf("%.2f",$per[$i]); ?>%</td>
						</tr>
					<?
					}
					?>	
					</tbody>
				</table>
			</div>	
		</div>
	</div>
</div>
<!-- popup : info -->
<?
include_once("./popup/search_res.php");
?>
<!-- //popup : info -->
<?
include_once("../../../../common/footer.php");
include_once("../../../../common/html_foot.php");