<?php
include_once("../../../../_func/function.common.php");

include_once("../../../../common/html_head.php");
include_once("../../../../common/header.php");

$start_time = $_POST['start_time'];
if(!$_POST['start_time']){
	$start_time = date("Y-m-d");
	$start_YYYY = date("Y");
	$start_MM = date("m");
	$start_DD = date("d");
}
else{
	$start_time_arr = explode("-", $start_time);
	$start_YYYY = $start_time_arr[0];
	$start_MM = $start_time_arr[1];
	$start_DD = $start_time_arr[2];
}

$end_time = $_POST['end_time'];
if(!$_POST['end_time']){
	$end_time = date("Y-m-d");
	$end_YYYY = date("Y");
	$end_MM = date("m");
	$end_DD = date("d");
}
else{
	$end_time_arr = explode("-", $end_time);
	$end_YYYY = $end_time_arr[0];
	$end_MM = $end_time_arr[1];
	$end_DD = $end_time_arr[2];
}

$year_arr = $utils_obj->GetYears();
$YYYY_cnt = count($year_arr);
$month_arr = $utils_obj->GetMonths();
$MM_cnt = count($month_arr);
$this_last_day = $utils_obj->GetLastDay(date('m'), date('Y'));
$day_arr = $utils_obj->GetDays($this_last_day);
$DD_cnt = count($day_arr);

function print_selected($base, $current){
	$str_value = "";

	if($base == $current){
		$str_value = "selected";
	}

	return $str_value;
}

$result = $main_obj->GetReportByOS($start_time, $end_time);

$total = 0;

$os[0] = "";
$os[1] = OS_WIN_98;
$os[2] = OS_WIN_2000;
$os[3] = OS_WIN_XP;
$os[4] = OS_WIN_2003;
$os[5] = OS_WIN_VISTA;
$os[6] = OS_WIN_7;
$os[7] = OS_UNKNOWN;
$os[8] = OS_WIN_8;
$os[9] = OS_WIN_2008;
$os[10] = OS_WIN_2012;
$os[11] = OS_ANDROID;
$os[12] = OS_MAC;
$os[13] = OS_LINUX;
$os[14] = OS_WIN_8_1;
$os[15] = OS_WIN_10;
$os[16] = OS_WIN_SERVER;

$data_size = count($os);
$last_node = $data_size - 1;
for($i=0;$i<$data_size;$i++){
	$data[$i] = 0;
}

while($row = mysql_fetch_array($result)){


	$index = $row['os'];

	if($index == 100)
		$index  = 11;
	else if($index == 101)
		$index = 12;
	else if($index == 102)
		$index = 13;

	$data[$index] = $row['cnt'];

	$total += intVal($row['cnt']);

}
$data[7] += $data[0]; // 기존에 저장안되었던 os 정보는 기타 os에 저장한다.


$totalPer = 0;
for($i=1; $i < $data_size; $i++){
	if($total != 0){
		//$per[$i] = $rate=round($data[$i]/$total*1000)/10;
		$per[$i] = $rate = $data[$i]/$total*100;
	}else{
		$per[$i] = 0;
	}
	$totalPer += $per[$i];
}

function print_last($base, $current){
	$class_name = "";

	if($base == $current){
		$class_name = "last";
	}

	return $class_name;
}
?>
<!-- popup : logout -->
<?
include_once("../../../../common/popup/logout.php");
?>
<!-- //popup : logout -->
<div class="conts_box">
	<?
	include_once("../side.php");
	?>
	<div class="conts_right">
		<!-- [D] 시간대별 통계확인 화면 -->
		<h3><img src="<? echo IMG_DIR; ?>/sub/h3_manage_time.gif" alt="운영체제별 통계 보기" /></h3>
		<div class="contents supp_logg index_t">
			<!-- [D] 검색기간을 설정하여 조회 -->
			<div class="search_box2">
				<div class="search_box2_wrap">
					<strong class="search_box2_tit">검색기간</strong>
					<div class="search_box2_cont">
						<!-- [D] 버튼을 클릭하면 클릭한기간의 내용이 검색되어 하단에 출력됨. -->
						<div class="period">
							<a href="#" class="force_search week_a"><span class="week">1주일</span></a>
							<a href="#" class="force_search month_1_a"><span class="month_1">1개월</span></a>
							<a href="#" class="force_search month_3_a"><span class="month_3">3개월</span></a>
							<a href="#" class="force_search month_6_a"><span class="month_6">6개월</span></a>
						</div>
						<!-- [D] 디폴트는 당일날짜. 셀렉트박스의 날짜를 선택하여 기간설정 가능 -->
						<div class="date">
							<div class="choice_box search_os_statistics_box">
								<span class="comm_sel w_70 start_YYYY"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $YYYY_cnt; $i++){
									?>
										<option value="<? echo $year_arr[$i]; ?>" <? echo print_selected($start_YYYY, $year_arr[$i]);?> ><? echo $year_arr[$i]; ?></option>
									<?
									}//for($i=0; $i < $YYYY_cnt; $i++)
									?>
									</select>
								</span>
								<span class="comm_sel w_55 start_MM"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $MM_cnt; $i++){
									?>
										<option value="<? echo $month_arr[$i]; ?>" <? echo print_selected($start_MM, $month_arr[$i]); ?> ><? echo str_pad($month_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
									<?
									}//for($i=0; $i < $MM_cnt; $i++)
									?>
									</select>
								</span>
								<span class="comm_sel w_55 start_DD"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $DD_cnt; $i++){
									?>
										<option value="<? echo $day_arr[$i]; ?>" <? echo print_selected($start_DD, $day_arr[$i]); ?> ><? echo str_pad($day_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
									<?
									}//for($i=0; $i < $DD_cnt; $i++)pad
									?>
									</select>
								</span>
								<div class="cen_line">~ </div>
								<span class="comm_sel w_70 end_YYYY"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $YYYY_cnt; $i++){
									?>
										<option value="<? echo $year_arr[$i]; ?>" <? echo print_selected($end_YYYY, $year_arr[$i]);?> ><? echo $year_arr[$i]; ?></option>
									<?
									}//for($i=0; $i < $YYYY_cnt; $i++)
									?>
									</select>
								</span>
								<span class="comm_sel w_55 end_MM"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $MM_cnt; $i++){
									?>
										<option value="<? echo $month_arr[$i]; ?>" <? echo print_selected($end_MM, $month_arr[$i]); ?> ><? echo str_pad($month_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
									<?
									}//for($i=0; $i < $MM_cnt; $i++)
									?>
									</select>
								</span>
								<span class="comm_sel w_55 end_DD"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $DD_cnt; $i++){
									?>
										<option value="<? echo $day_arr[$i]; ?>" <? echo print_selected($end_DD, $day_arr[$i]); ?> ><? echo str_pad($day_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
									<?
									}//for($i=0; $i < $DD_cnt; $i++)pad
									?>
									</select>
								</span>
								<a href="#" class="search_os_statistics"><img src="<? echo IMG_DIR; ?>/board/btn_search.gif" alt="검색" /></a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="useradd_btn">
				<!-- [D] 엑셀다운로드버튼: 하단리스트에 대한 전체내용이 엑셀파일로 다운로드됨. -->
				<a href="#" class="export_os_statistics"><img src="<? echo IMG_DIR; ?>/sub/btn_exeldown.gif" alt="엑셀다운로드" /></a>
			</div>
			
			<div class="ajax_os_statistics">
				<!-- [D] 디폴트는 당일 시간대별 리스트 -->
				<table class="style_board_default" summary="번호, 부서명, 설명, 수정/삭제가 있는 게시판" width="100%">
					<caption><span>번호, 부서명, 설명, 수정/삭제가 있는 게시판</span></caption>
					<colgroup>
						<col style="width:33.3%;" />
						<col style="width:33.3%;" />
						<col style="width:33.3%;" />
					</colgroup>
					<thead>
						<tr>
							<th>운영체제</th>
							<th>접속횟수 (회)</th>
							<th class="last">백분율</th>
						</tr>
					</thead>
					<tbody>
					<?
					for($i=1;$i<$data_size;$i++){
					?>
						<tr class="<? echo print_last($last_node, $i); ?>">
							<td><? echo $os[$i]; ?></td>
							<td><? echo $data[$i]; ?></td>
							<td><? echo sprintf("%.2f",$per[$i]); ?>%</td>
						</tr>
					<?
					}//end of : for($i=1;$i<sizeof($data);$i++)
					?>
					</tbody>
				</table>
			</div>	
		</div>
	</div>
</div>
<!-- popup : info -->
<?
include_once("./popup/search_res.php");
?>
<!-- //popup : info -->
<?
include_once("../../../../common/footer.php");
include_once("../../../../common/html_foot.php");