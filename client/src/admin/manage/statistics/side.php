<!-- manage statistics : side navigation bar -->
<div class="conts_left">
	<h2><img src="<? echo IMG_DIR; ?>/sub/lnb_h2_index.gif" alt="통계관리" /></h2>
	<ul class="lnb">
		<li class="<? echo print_nb_on(MANAGE_STAT_TIME, $current_url); ?>">
			<a href="<? echo MANAGE_STAT_TIME_INDEX; ?>">
				<span>시간대별</span>
			</a>
		</li>
		<li class="<? echo print_nb_on(MANAGE_STAT_DAY, $current_url); ?>">
			<a href="<? echo MANAGE_STAT_DAY_INDEX; ?>">
				<span>일별</span>
			</a>
		</li>
		<li class="<? echo print_nb_on(MANAGE_STAT_MONTH, $current_url); ?>">
			<a href="<? echo MANAGE_STAT_MONTH_INDEX; ?>">
				<span>월별</span>
			</a>
		</li>
		<li class="<? echo print_nb_on(MANAGE_STAT_AGENT, $current_url); ?>">
			<a href="<? echo MANAGE_STAT_AGENT_INDEX; ?>">
				<span>상담원별</span>
			</a>
		</li>
		<li class="<? echo print_nb_on(MANAGE_STAT_SUPPORT_TIME, $current_url); ?>">
			<a href="<? echo MANAGE_STAT_SUPPORT_TIME_INDEX; ?>">
				<span>상담시간별</span>
			</a>
		</li>
		<li class="<? echo print_nb_on(MANAGE_STAT_OS, $current_url); ?>">
			<a href="<? echo MANAGE_STAT_OS_INDEX; ?>">
				<span>운영체제별</span>
			</a>
		</li>
		<li class="<? echo print_nb_on(MANAGE_STAT_TROUBLE, $current_url); ?>">
			<a href="<? echo MANAGE_STAT_TROUBLE_INDEX; ?>">
				<span>장애유형별</span>
			</a>
		</li>
	</ul>
	<!-- [D] 관리자페이지 로그인 후, 원격지원상담프로그램을 실행/다운로드 할수있는 버튼. *각 유형별 프로세스 내용은, [일반상담원로그인-프로그램실행/다운로드버튼]화면의 내용과 동일 -->
	<p class="lnb_quick">
		<a href="#" class="program_download"><img src="<? echo IMG_DIR; ?>/sub/btn_large_pdown.gif" alt="프로그램 다운로드" /></a>
		<a href="#"><!--img src="<? echo IMG_DIR; ?>/sub/btn_pstart.gif" alt="프로그램 실행" /--></a>
	</p>
</div>
<!-- //manage statistics : side navigation bar -->