<?php
include_once("../../../../_func/function.common.php");

include_once("../../../../common/html_head.php");
include_once("../../../../common/header.php");

$search_year = $_POST['search_year'];
if(!$_POST['search_year']){
	$search_year = date("Y");
	$start_YYYY = date("Y");
}

$search_month = $_POST['search_month'];
if(!$_POST['search_month']){
	$search_month = date("m");
	$start_MM = date("m");
}

$result = $main_obj->GetReportByDay($search_year, $search_month);

$total = 0;
if($search_year == ""){
	$tmp = mktime();
}
else{
	$tmp = mktime(0,0,1,$search_month,1,$search_year);
}

$days = date("t",$tmp);

for($i=0;$i<$days;$i++){
	$data[$i] = 0;
}

while($row = mysql_fetch_array($result)){
	$t = intVal(substr($row['starttime'],8,2));
	$z = --$t;
	$data[$z]++;
	$total++;
}

$totalPer = 0;
$data_size = count($data);
$last_node = $data_size - 1;
for($i=0; $i < $data_size; $i++){
	if($total != 0){
		$per[$i] = $rate = $data[$i]/$total*100;
	}else{
		$per[$i] = 0;
	}
	$totalPer += $per[$i];
}

$year_arr = $utils_obj->GetYears();
$YYYY_cnt = count($year_arr);
$month_arr = $utils_obj->GetMonths();
$MM_cnt = count($month_arr);

function print_selected($base, $current){
	$str_value = "";

	if($base == $current){
		$str_value = "selected";
	}

	return $str_value;
}

function print_last($base, $current){
	$class_name = "";

	if($base == $current){
		$class_name = "last";
	}

	return $class_name;
}
?>
<!-- popup : logout -->
<?
include_once("../../../../common/popup/logout.php");
?>
<!-- //popup : logout -->
<div class="conts_box">
	<?
	include_once("../side.php");
	?>
	<div class="conts_right">
		<!-- [D] 시간대별 통계확인 화면 -->
		<h3><img src="<? echo IMG_DIR; ?>/sub/h3_day_time.gif" alt="일별 통계 보기" /></h3>
		<div class="contents supp_logg index_t">
			<!-- [D] 검색기간을 설정하여 조회 -->
			<div class="search_box2">
				<div class="search_box2_wrap add_style">
					<strong class="search_box2_tit add_t_style">검색기간</strong>
					<div class="search_box2_cont">
						<div>
							<div class="choice_box search_day_statistics_box">
								<span class="comm_sel w_70 start_YYYY"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $YYYY_cnt; $i++){
									?>
										<option value="<? echo $year_arr[$i]; ?>" <? echo print_selected($start_YYYY, $year_arr[$i]);?> ><? echo $year_arr[$i]; ?></option>
									<?
									}//for($i=0; $i < $YYYY_cnt; $i++)
									?>
									</select>
								</span>
								<span class="comm_sel w_55 start_MM"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $MM_cnt; $i++){
									?>
										<option value="<? echo $month_arr[$i]; ?>" <? echo print_selected($start_MM, $month_arr[$i]); ?> ><? echo str_pad($month_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
									<?
									}//for($i=0; $i < $MM_cnt; $i++)
									?>
									</select>
								</span>
								<a href="#" class="search_day_statistics"><img src="<? echo IMG_DIR; ?>/board/btn_search.gif" alt="검색" /></a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="useradd_btn">
				<!-- [D] 엑셀다운로드버튼: 하단리스트에 대한 전체내용이 엑셀파일로 다운로드됨. -->
				<a href="#" class="export_day_statistics"><img src="<? echo IMG_DIR; ?>/sub/btn_exeldown.gif" alt="엑셀다운로드" /></a>
			</div>

			<!-- [D] 디폴트는 당일 시간대별 리스트 -->
			<div class="ajax_day_statistics">
				<table class="style_board_default" summary="번호, 부서명, 설명, 수정/삭제가 있는 게시판" width="100%">
					<caption><span>번호, 부서명, 설명, 수정/삭제가 있는 게시판</span></caption>
					<colgroup>
						<col style="width:15%;" />
						<col style="width:*;" />
						<col style="width:20%;" />
					</colgroup>
					<thead>
						<tr>
							<th>날짜(일)</th>
							<th>접속횟수(회)</th>
							<th class="last">백분율</th>
						</tr>
					</thead>
					<tbody>
					<?
					for($i=0; $i < $data_size; $i++){
					?>	
						<tr class="<? echo print_last($last_node, $i); ?>">
							<td><? echo $i+1; ?></td>
							<td><? echo $data[$i]; ?></td>
							<td><? echo sprintf("%.2f",$per[$i]); ?>%</td>
						</tr>
					<?
					}
					?>	
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- popup : info -->
<?
include_once("./popup/search_res.php");
?>
<!-- //popup : info -->
<?
include_once("../../../../common/footer.php");
include_once("../../../../common/html_foot.php");