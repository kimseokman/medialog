<?php
include_once("../../../../_func/function.common.php");

include_once("../../../../common/html_head.php");
include_once("../../../../common/header.php");

$login_try_limit = LOGIN_TRY_COUNT_MAX;
$pw_length = PASSWORD_LENGTH;
$pw_length_min = PASSWORD_LENGTH_MIN;
$pw_length_max = PASSWORD_LENGTH_MAX;
$pw_safe_cnt = PASSWORD_SAFE_CNT;

$sptnum = $_POST['sptnum'];
$remember_page = $_POST['page'];
$remember_search = $_POST['search'];
$remember_searchVal = $_POST['searchVal'];
$spt = $main_obj->GetSupporterBySptnum($sptnum);

if($spt == NULL){
	//alert
	//"상담원 정보를 얻을 수 없습니다. 다시 시도해주세요."
}

$dept = $main_obj->GetDepList();
$dept_size = count($dept);

for($i=0; $i < $dept_size; $i++){
	if($dept[$i]['depnum'] == $spt->depnum){
		$selected_dept_name = $dept[$i]['depname'];
	}
}

$pc_func = $utils_obj->GetFunctionList('PC_FUNCTION_LIST');
$pc_sett = $utils_obj->GetFunctionList('PC_SETTING_LIST');
$mob_func = $utils_obj->GetFunctionList('MOBILE_FUNCTION_LIST');

$try_cnt = $main_obj->GetLoginTryCount($spt->sptid);
$pw_status = "UNLOCK";
if($try_cnt >= $login_try_limit){
	$pw_status_class = "imp";
	$pw_status = "LOCK";
}
else{
	$pw_status_class = "noti";
	$pw_status = "UNLOCK";	
}

function print_last($current_node, $last_node){
	$class_name = "";
	if($current_node == $last_node)
		$class_name = "last";

	return $class_name;
}

function print_checked($base, $current){
	$class_name = "";
	if($base & $current)
		$class_name = "checked";

	return $class_name;	
}

function print_disabled($base, $current){
	$class_name = "";
	if(!$base & $current)
		$class_name = "disabled";

	return $class_name;	
}

function print_selected($base, $current){
	$attr_val = "";
	if($base == $current)
		$attr_val = "selected";

	return $attr_val;	
}

function print_show($flag){
	$class_name = "hide";
	if($flag){
		$class_name = "show";
	}
	else{
		$class_name = "hide";
	}
		

	return $class_name;	
}
?>
<!-- popup : logout -->
<?
include_once("../../../../common/popup/logout.php");
?>
<!-- //popup : logout -->
<div class="conts_box">
	<?
	include_once("../side.php");
	?>
	<div class="conts_right">
		<!-- [D] 사용자를 관리하기 위한 화면 -->
		<h3><img src="<? echo IMG_DIR; ?>/sub/h3_user_info.gif" alt="사용자정보" /></h3>

		<!-- 생성된 전체 부서 리스트 -->
		<div class="contents user_info">
			<!-- [D] 사용자관리 목록으로 이동 -->
			<div class="userlist_btn">
				<a href="#" onClick="user_list(event, <? echo $remember_page; ?>, '<? echo $remember_search; ?>', '<? echo $remember_searchVal; ?>')"><img src="<? echo IMG_DIR; ?>/sub/btn_list.gif" alt="목록보기" /></a>
			</div>

			<!-- 사용자정보 폼 테이블 -->
			<!-- [D] 값들은 관리자가 임의로 수정 가능. 비밀번호설정은 LG U+ 비밀번호 설정 정책에 따름 -->
			<form name="userEditForm" method="post" action="/_func/function.edit_user.php">
				<input type="hidden" class="sptnum" name="sptnum" value="<? echo $sptnum; ?>" />
				<input type="hidden" class="page" name="page" value="<? echo $remember_page; ?>" />
				<input type="hidden" class="search" name="search" value="<? echo $remember_search; ?>" />
				<input type="hidden" class="searchVal" name="searchVal" value="<? echo $remember_searchVal; ?>" />
				<table class="style_board_userinfo" summary="" width="100%">
					<caption><span></span></caption>
					<colgroup>
						<col style="width:150px;" />
						<col style="width:*;" />
					</colgroup>
					<tbody>
						<tr>
							<th><label for="user_name">이름</label></th>
							<td><input type="text" id="user_name" name="edit_user_name" value="<? echo $spt->name; ?>" style="width:160px;" /></td>
						</tr>
						<tr>
							<th>아이디</th>
							<td><? echo $spt->sptid; ?></td>
						</tr>
						<tr>
							<th><label for="user_pw">비밀번호</label></th>
							<td>
								<input type="password" id="user_pw" name="edit_user_pw" value="" style="width:160px;" /><span class="noti"><? echo $pw_length_min; ?>~<? echo $pw_length_max; ?>자 내의 영문 대/소문자, 숫자, 특수문자 조합</span>
								<p class="imp ajax_user_edit_password_guide"></p>
							</td>
						</tr>
						<tr>
							<th><label for="user_pw2">비밀번호 확인</label></th>
							<td>
								<input type="password" id="user_pw2" name="edit_user_pw2" value="" style="width:160px;" />
								<p class="imp ajax_user_edit_password2_guide"></p>
							</td>
						</tr>
						<tr>
							<th><label for="user_pw_status">비밀번호 상태</label></th>
							<td>
								<input type="hidden" class="unlock_pw_sptid" value="<? echo $spt->sptid; ?>" />
								<a href="#" class="pw_reset"><img src="<? echo IMG_DIR; ?>/super/pw_re.gif" /></a>
								<span class="pw_status <? echo $pw_status_class; ?>"><? echo $pw_status; ?></span>
							</td>
						</tr>
						<tr>
							<th><label for="user_dep">소속부서</label></th>
							<td>
								<div class="choice_box" style="margin-bottom:0px;">
									<span class="comm_sel w_172"><!-- [150811] -->
										<select name="edit_user_depnum">
										<?
										for($i=0; $i < $dept_size; $i++){
										?>
											<option value="<? echo $dept[$i]['depnum']; ?>" <? echo print_selected($dept[$i]['depnum'], $spt->depnum)?> ><? echo $dept[$i]['depname']; ?></option>
										<?
										}//end of : for($i=0; $i < $dept_size; $i++)
										?>
										</select>
									</span>
								</div>
							</td>
						</tr>
						<tr>
							<th><label for="user_num">전화번호</label></th>
							<td><input type="text" id="user_num" name="edit_user_tel" value="<? echo $spt->phone; ?>" style="width:160px;" /></td>
						</tr>
						<tr class="last">
							<th><label for="user_email">이메일</label></th>
							<td><input type="text" id="user_email" name="edit_user_mail" value="<? echo $spt->email; ?>" style="width:300px;" /></td>
						</tr>
					</tbody>
				</table>
			

				<h4><img src="<? echo IMG_DIR; ?>/sub/h3_user_info_func.gif" alt="기능옵션" /></h4>
				<!-- [D] 체크박스 체크/해제를 통하여 사용자가 사용할 수 있는 권한 부여 -->
				<table class="style_board_default" summary="옵션이름, 사용여부 옵션설명이 있는 게시판" width="100%">
					<caption><span>옵션이름, 사용여부 옵션설명이 있는 게시판</span></caption>
					<colgroup>
						<col style="width:228px;" />
						<col style="width:80px;" />
						<col style="width:*;" />
					</colgroup>
					<thead>
						<tr>
							<th>옵션이름</th>
							<th>사용여부</th>
							<th class="last">옵션설명</th>
						</tr>
					</thead>
					<tbody>
					<?
					$pc_func_cnt = count($pc_func);
					
					for($i = 0; $i < $pc_func_cnt; $i++){
						if( (($pc_func[$i]["tag"] == "func9") || ($pc_func[$i]["tag"] == "func14") || ($pc_func[$i]["tag"] == "func15") || ($pc_func[$i]["tag"] == "func19")) && HIDE_OPTION){//case hide
							//hide
							//상담원 화면 전송, 음성 채팅 숨김처리 요청 | 고지연 | 150824
							//안전모드 재부팅 후 자동 재접속 숨김처리 | 김석만 | 150930
						}
						else{//case show
					?>
						<tr>
							<td class="al_l func_name"><label for="<? echo $pc_func[$i]["tag"]; ?>"><? echo $pc_func[$i]["title"]; ?></label></td>
							<td>
								<input type="checkbox" id="<? echo $pc_func[$i]["tag"]; ?>" name="func[]" value="<? echo $pc_func[$i]["permission"]; ?>" <? echo print_checked($pc_func[$i]["permission"], $spt->function); ?> <? echo print_disabled($pc_func[$i]["permission"], $admin->function); ?> />
							</td>
							<td class="al_l"><? echo $pc_func[$i]["memo"]; ?></td>
						</tr>
					<?
						}
					}//end of : for($i = 0; $i < $pc_func_cnt; $i++)


					//set check & title & memo
					$mobile_func_title = "";
					$mobile_func_memo = "";
					$mobile_func_permission = PERMISSION_PC;//default
					$admin_permission = $admin->function;
					$mobile_func_print_disabled = "";
					$mobile_func_control_class = "mobile_func";

					if( ( $admin_permission & PERMISSION_MOBILE_PAID ) || ( $admin_permission & PERMISSION_MOBILE_FREE ) ){
						$mobile_func_title = "모바일 원격지원";
						$mobile_func_memo = "고객 모바일 단말을 원격지원 합니다.";
						if( $admin_permission & PERMISSION_MOBILE_PAID )		$mobile_func_permission = PERMISSION_MOBILE_PAID;
						else if( $admin_permission & PERMISSION_MOBILE_FREE )	$mobile_func_permission = PERMISSION_MOBILE_FREE;
						
						$mobile_func_print_disabled = "";
						$mobile_func_control_class = "mobile_func control";
					}			
					else if( ($admin_permission & PERMISSION_VIDEO_PAID ) || ( $admin_permission & PERMISSION_VIDEO_FREE ) ){
						$mobile_func_title = "영상통화 원격지원";
						$mobile_func_memo = "고객과 영상통화를 하면서 원격에서 현장지원을 합니다.";
						if( $admin_permission & PERMISSION_VIDEO_PAID )		$mobile_func_permission = PERMISSION_VIDEO_PAID;
						else if( $admin_permission & PERMISSION_VIDEO_FREE )	$mobile_func_permission = PERMISSION_VIDEO_FREE;

						$mobile_func_print_disabled = "";
						$mobile_func_control_class = "mobile_func";
					}			
					else if( ($admin_permission & PERMISSION_MOBILE_VIDEO_PAID) || ($admin_permission & PERMISSION_MOBILE_VIDEO_FREE) ){
						$mobile_func_title = "모바일 / 영상 원격지원";
						$mobile_func_memo = "고객 모바일 단말을 원격지원 과 영상통화 원격지원을 합니다.";
						if( $admin_permission & PERMISSION_MOBILE_VIDEO_PAID )		$mobile_func_permission = PERMISSION_MOBILE_VIDEO_PAID;
						else if( $admin_permission & PERMISSION_MOBILE_VIDEO_FREE )	$mobile_func_permission = PERMISSION_MOBILE_VIDEO_FREE;

						$mobile_func_print_disabled = "";
						$mobile_func_control_class = "mobile_func control";
					}
					else{
						$mobile_func_title = "모바일 원격지원";
						$mobile_func_memo = "고객 모바일 단말을 원격지원 합니다.";
						$mobile_func_permission = PERMISSION_PC;

						$mobile_func_print_disabled = "disabled";
						$mobile_func_control_class = "mobile_func";
					}					

					if($admin->mobile_volume <= 0 ){
						$mobile_func_print_disabled = "disabled";
					}

					//set checked for suppoter information
					$mobile_func_print_checked = "";
					$mobile_setting_enable = FALSE;
					if( ( $spt->function & PERMISSION_MOBILE_PAID ) || ( $spt->function & PERMISSION_MOBILE_FREE ) ){
						$mobile_func_print_checked = "checked";
						$mobile_setting_enable = TRUE;
					} 
					else if( ( $spt->function & PERMISSION_VIDEO_PAID ) || ( $spt->function & PERMISSION_VIDEO_FREE ) ){
						$mobile_func_print_checked = "checked";
						$mobile_setting_enable = FASLE;
					}
					else if( ( $spt->function & PERMISSION_MOBILE_VIDEO_PAID ) || ( $spt->function & PERMISSION_MOBILE_VIDEO_PAID ) ){
						$mobile_func_print_checked = "checked";
						$mobile_setting_enable = TRUE;
					}
					else{
						$mobile_func_print_checked = "";
						$mobile_setting_enable = FALSE;
					}
					?>
						<tr class="last">
							<td class="al_l func_name"><label for="mobile_func"><? echo $mobile_func_title; ?></label></td>
							<td>
								<input class="<? echo $mobile_func_control_class; ?>" type="checkbox" id="mobile_func" name="mobile_func" value="<? echo $mobile_func_permission; ?>" <? echo $mobile_func_print_checked; ?> <? echo $mobile_func_print_disabled; ?> />
								<input type="hidden" name="current_mobile_func" <? echo $mobile_func_print_checked; ?> value="<? echo $spt->function; ?>"/>
							</td>
							<td class="al_l"><? echo $mobile_func_memo; ?></td>
						</tr>
					</tbody>
				</table>

				<h4><img src="<? echo IMG_DIR; ?>/sub/h3_user_info_sett.gif" alt="설정옵션" /></h4>
				<table class="style_board_default" summary="옵션이름, 사용여부 옵션설명이 있는 게시판" width="100%">
					<caption><span>옵션이름, 사용여부 옵션설명이 있는 게시판</span></caption>
					<colgroup>
						<col style="width:228px;" />
						<col style="width:80px;" />
						<col style="width:*;" />
					</colgroup>
					<thead>
						<tr>
							<th>옵션이름</th>
							<th>사용여부</th>
							<th class="last">옵션설명</th>
						</tr>
					</thead>
					<tbody>
					<?
					$pc_sett_cnt = count($pc_sett);
					$pc_sett_last = $pc_sett_cnt-1;

					for($i = 0; $i < $pc_sett_cnt; $i++){
						if( (($pc_sett[$i]["tag"] == "sett1") || ($pc_sett[$i]["tag"] == "sett2") || ($pc_sett[$i]["tag"] == "sett3") || ($pc_sett[$i]["tag"] == "sett4") ||($pc_sett[$i]["tag"] == "sett5") ||($pc_sett[$i]["tag"] == "sett7") || ($pc_sett[$i]["tag"] == "sett8") || ($pc_sett[$i]["tag"] == "sett9") || ($pc_sett[$i]["tag"] == "sett11") || ($pc_sett[$i]["tag"] == "sett12") || ($pc_sett[$i]["tag"] == "sett13")) && HIDE_OPTION){//case hide
							//hide
							//일부기능 숨김처리 | 김석만 | 150930
						}
						else{//case show
					?>	
						<tr class="<? echo print_last($i, $pc_sett_last); ?>">
							<td class="al_l func_name"><label for="<? echo $pc_sett[$i]["tag"]; ?>"><? echo $pc_sett[$i]["title"]; ?></label></td>
							<td><input type="checkbox" id="<? echo $pc_sett[$i]["tag"]; ?>" name="sett[]" value="<? echo $pc_sett[$i]["permission"]; ?>" <? echo print_checked($pc_sett[$i]["permission"], $spt->setting); ?> /></td>
							<td class="al_l"><? echo $pc_sett[$i]["memo"]; ?></td>
						</tr>
					<?
						}	
					}//end of : for($i = 0; $i < $pc_sett_cnt; $i++)
					?>
					</tbody>
				</table>
				
				<div class="mobile_setting_box <? echo print_show($mobile_setting_enable); ?>">
					<h4><img src="<? echo IMG_DIR; ?>/sub/h3_user_info_mob.gif" alt="모바일 기능옵션" /></h4>
					<table class="style_board_default" summary="옵션이름, 사용여부 옵션설명이 있는 게시판" width="100%">
						<caption><span>옵션이름, 사용여부 옵션설명이 있는 게시판</span></caption>
						<colgroup>
							<col style="width:228px;" />
							<col style="width:80px;" />
							<col style="width:*;" />
						</colgroup>
						<thead>
							<tr>
								<th>옵션이름</th>
								<th>사용여부</th>
								<th class="last">옵션설명</th>
							</tr>
						</thead>
						<tbody>
						<?
						$mob_func_cnt = count($mob_func);
						$mob_func_last = $mob_func_cnt-1;

						for($i = 0; $i < $mob_func_cnt; $i++){
						?>	
							<tr class="<? echo print_last($i, $mob_func_last); ?>">
								<td class="al_l func_name"><label for="<? echo $mob_func[$i]["tag"]; ?>"><? echo $mob_func[$i]["title"]; ?></label></td>
								<td><input type="checkbox" id="<? echo $mob_func[$i]["tag"]; ?>" name="mob[]" value="<? echo $mob_func[$i]["permission"]; ?>" <? echo print_checked($mob_func[$i]["permission"], $spt->mobile_setting); ?> <? echo print_disabled($mob_func[$i]["permission"], $admin->mobile_setting); ?> /></td>
								<td class="al_l"><? echo $mob_func[$i]["memo"]; ?></td>
							</tr>
						<?	
						}//end of : for($i = 0; $i < $mob_func_cnt; $i++)
						?>	
						</tbody>
					</table>
				</div>
			</form>	
			<div class="userlist_sd_btn">
				<!-- [D] 적용내용 저장 -->
				<a href="#" class="edit_user"><img src="<? echo IMG_DIR; ?>/sub/btn_save.gif" alt="저장" /></a>
				<!-- [D] 사용자 삭제 / 버튼을 클릭하면 삭제 확인 팝업창이 뜨면서 선택에 따라 사용자 삭제/유지됨. -->
				<a href="#" class="open_pop_del_user"><img src="<? echo IMG_DIR; ?>/sub/btn_delete.gif" alt="삭제" /></a>
			</div>
		</div>
	</div>
</div>
<!-- popup : user_del -->
<?
include_once("./popup/user_del.php");
?>
<!-- //popup : user_del -->
<!-- popup : user_edit_res -->
<?
include_once("./popup/user_edit_res.php");
?>
<!-- //popup : user_edit_res -->
<?
include_once("../../../../common/footer.php");
include_once("../../../../common/html_foot.php");