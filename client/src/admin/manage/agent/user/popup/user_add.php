<?php
$user_add_popup_action = $_POST['user_add_popup_action'];
$input_res = $_POST['input_res'];

$id_length_max = ID_MAX_LENGTH;
$pw_length = PASSWORD_LENGTH;
$pw_length_min = PASSWORD_LENGTH_MIN;
$pw_length_max = PASSWORD_LENGTH_MAX;
$pw_safe_cnt = PASSWORD_SAFE_CNT;

function print_checked($base, $current){
	$attr_val = "";

	if($base == $current){
		$attr_val = "checked";
	}

	return $attr_val;
}

function print_selected($base, $current){
	$attr_val = "";

	if($base == $current){
		$attr_val = "selected";
	}

	return $attr_val;
}

if($user_add_popup_action == 'open'){

	$config_path = array(
		"CREATE_FAIL" => MANAGE_AGENT_USER_INDEX
	);

	$config_msg = array(
		"OVER_FLOW" => "상담원 수를 초과하였습니다.",
		"CREATE_DEPARTMENT"=> "부서를 생성해주세요."
	);

	if($spt_volume <= $pc_spt_cnt){
		

	?>
		<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
			<input type="hidden" name="user_add_res_popup_action" value="open" />
			<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['OVER_FLOW']; ?>" />
			<input type="hidden" name="page" value="<? echo $page; ?>" />
			<input type="hidden" name="search" value="<? echo $search; ?>" />
			<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
		</form>
		<script>
			document.createUserResForm.submit();
		</script>
	<?
	}

	$isDeptList = $main_obj->GetDepList();
	if(count($isDeptList) == 0){
	?>
	<form name="createUserResForm" method="post" action="<? echo $config_path['CREATE_FAIL']; ?>">
		<input type="hidden" name="user_add_res_popup_action" value="open" />
		<input type="hidden" name="user_add_res_popup_msg" value="<? echo $config_msg['CREATE_DEPARTMENT']; ?>" />
		<input type="hidden" name="page" value="<? echo $page; ?>" />
		<input type="hidden" name="search" value="<? echo $search; ?>" />
		<input type="hidden" name="searchVal" value="<? echo $searchVal; ?>" />
	</form>
	<script>
		document.createUserResForm.submit();
	</script>
	<?
	}


	$dept = $main_obj->GetDepList();
	$dept_size = count($dept);

	if($input_res == 'FAIL'){
		$input_spt_id = $_POST['input_spt_id'];
		$input_spt_pw = $_POST['input_spt_pw'];
		$input_spt_pw2 = $_POST['input_spt_pw2'];
		$input_spt_name = $_POST['input_spt_name'];
		$input_spt_deptnum = $_POST['input_spt_deptnum'];
		$input_spt_tel = $_POST['input_spt_tel'];
		$input_spt_mail = $_POST['input_spt_mail'];
		$input_spt_permit = $_POST['input_spt_permit'];
	}

	if(!$input_spt_permit){
		$input_spt_permit = 'p_pc';
	}
?>
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<!-- 사용자 생성추가 팝업 -->
	<div id="popup" class="add_user">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<span class="popup_title_txt"></span>
			<a href="#" class="close close_add_user_pop" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content" style="height:415px;">
				<!-- [D] 현재 생성이 가능한 사용자 수 안내텍스트(PC/모바일/비디오 각각분류) -->
				<div class="inform_usernum">
					<dl class="fst_usernum">
						<dt>생성 가능 수 :</dt>
						<dd><span class="imp"><? echo ($spt_volume - $spt_cnt); ?></span> 명 (총 <strong><? echo $spt_volume; ?></strong> 명)</dd>
					</dl>
					<dl class="snd_usernum">
						<dt>PC :</dt>
						<dd class="<? echo print_last2($is_pc); ?>"><span class="imp"><? echo ($pc_volume-$pc_spt_cnt); ?></span> 명 (총 <strong><? echo $pc_volume; ?></strong> 명)</dd>
					<?
					if( !$is_pc ){
					?>
						<dt><? echo $product_title; ?> :</dt>
						<dd class="last"><span class="imp"><? echo ($mobile_volume-$product_spt_cnt); ?></span> 명(총 <strong><? echo $mobile_volume; ?></strong> 명)</dd>
					<?
					}
					?>
					</dl>
				</div>

				<!-- 사용자정보 폼 테이블 -->
				<form name="createUserForm" method="post" action="/_func/function.create_user.php">
					<table class="style_board_userinfo" summary="" width="100%">
						<caption><span></span></caption>
						<colgroup>
							<col style="width:150px;" />
							<col style="width:*;" />
						</colgroup>
						<tbody>
							<tr>
								<th><label for="user_name">이름</label></th>
								<td><input type="text" id="user_name" name="user_name" value="<? echo $input_spt_name; ?>" size="25" /></td>
							</tr>
							<!-- [D] 아이디입력창 조건:영문,숫자 구성으로 12자 이내 -->
							<tr>
								<th><label for="user_id">아이디</label></th>
								<td>
									<input type="text" id="user_id" name="user_id" value="<? echo $input_spt_id; ?>" size="25" /><span class="noti">영문,숫자 구성으로 <? echo $id_length_max; ?>자 이내</span>
								</td>
							</tr>
							<!-- [D] 비밀번호입력창 조건:8~15자 내의 영문 대/소문자, 숫자, 특수문자 조합 -->
							<tr>
								<th><label for="user_pw">비밀번호</label></th>
								<td>
									<input type="password" id="user_pw" name="user_pw" value="<? echo $input_spt_pw; ?>" size="25" /><span class="noti"><? echo $pw_length_min; ?>~<? echo $pw_length_max; ?>자 내의 영문 대/소문자, 숫자, 특수문자 조합</span>
									<p class="imp ajax_user_add_password_guide"></p>
								</td>
							</tr>
							<tr>
								<th><label for="user_pw2">비밀번호 확인</label></th>
								<td>
									<input type="password" id="user_pw2" name="user_pw2" value="<? echo $input_spt_pw2; ?>" size="25" />
									<p class="imp ajax_user_add_password2_guide"></p>
								</td>
							</tr>
							<tr>
								<th><label for="user_dep">소속부서</label></th>
								<td>
									<div class="choice_box" style="margin-bottom:0px;">
										<span class="comm_sel w_168"><!-- [150811] -->
											<select name="dept_num">
											<?
											if($input_res == 'FAIL'){
											?>
												<?
												for($i=0; $i < $dept_size; $i++){
												?>
													<option value="<? echo $dept[$i]['depnum']; ?>" <? echo print_selected($input_spt_deptnum, $dept[$i]['depnum']); ?>  ><? echo $dept[$i]['depname']; ?></option>
												<?
												}//end of : for($i=0; $i < $dept_size; $i++)
												?>
											<?	
											}//end of : if($input_res == 'FAIL')
											else{
												for($i=0; $i < $dept_size; $i++){
											?>
												<option value="<? echo $dept[$i]['depnum']; ?>"><? echo $dept[$i]['depname']; ?></option>
											<?
												}//end of : for($i=0; $i < $dept_size; $i++)
											}
											?>
											</select>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<th><label for="user_num">전화번호</label></th>
								<td><input type="text" id="user_num" name="user_tel" value="<? echo $input_spt_tel; ?>" size="25" /></td>
							</tr>
							<tr>
								<th><label for="user_email">이메일</label></th>
								<td><input type="text" id="user_email" name="user_email" value="<? echo $input_spt_mail; ?>" size="40" /></td>
							</tr>
							<tr class="last">
								<th>권한</th>
								<td class="permit_radio">
									<input type="radio" id="p_pc" name="permit" value="p_pc" <? echo print_checked('p_pc', $input_spt_permit); ?> /><label for="p_pc">PC</label>
								<?
								if($admin_product == PRODUCT_MOBILE){
								?>
									<input type="radio" id="p_pcm" name="permit" value="p_pcm" <? echo print_checked('p_pcm', $input_spt_permit); ?> /><label for="p_pcm">PC+Mobile</label>
								<?
								}//end of : if($admin_product == PRODUCT_MOBILE)
								else if($admin_product == PRODUCT_VIDEO){
								?>
									<input type="radio" id="p_pcv" name="permit" value="p_pcv" <? echo print_checked('p_pcv', $input_spt_permit); ?> /><label for="p_pcv">PC+Video</label>
								<?
								}//end of : if($admin_product == PRODUCT_VIDEO)
								else if($admin_product == PRODUCT_MOBILE_VIDEO){
								?>
									<input type="radio" id="p_pcmv" name="permit" value="p_pcmv" <? echo print_checked('p_pcmv', $input_spt_permit); ?> /><label for="p_pcmv">PC+Mobile+Video</label>
								<?
								}//end of : if($admin_product == PRODUCT_MOBILE_VIDEO)
								?>
								</td>
							</tr>
						</tbody>
					</table>
				</form>

				<!-- [D] 버튼을 클릭했을때, 모든항목이 제대로 입력되면 저장완료팝업창과 함께 사용자가 추가저장되면서 해당창은 사라짐 / 항목이 1개라도 입력되지 않았거나 잘못입력되었을 시, 저장실패안내팝업창 생성 -->
				<div class="btn">
					<a href="#" class="add_user_btn"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
	<!-- //사용자 생성추가 팝업 -->
</div>
<?
}//end of : if($user_add_popup_action == 'open')