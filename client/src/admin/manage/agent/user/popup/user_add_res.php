<?php
$user_add_res_popup_action = $_POST['user_add_res_popup_action'];
$user_add_res_popup_msg = $_POST['user_add_res_popup_msg'];

$input_res = $_POST['input_res'];
$input_spt_id = $_POST['input_spt_id'];
$input_spt_pw = $_POST['input_spt_pw'];
$input_spt_pw2 = $_POST['input_spt_pw2'];
$input_spt_name = $_POST['input_spt_name'];
$input_spt_deptnum = $_POST['input_spt_deptnum'];
$input_spt_tel = $_POST['input_spt_tel'];
$input_spt_mail = $_POST['input_spt_mail'];
$input_spt_permit = $_POST['input_spt_permit'];

if($user_add_res_popup_action == 'open'){
?>
<!-- 사용자가 추가된 결과 창 -->
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<span class="popup_title_txt"></span>
			<a href="#" class="close close_pop_user_add_res" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p class="h_num70">
					<? echo $user_add_res_popup_msg; ?>
				</p>
				<?
				if($input_res == 'FAIL'){
				?>
				<input type="hidden" class="input_res" value="<? echo $input_res; ?>" />
				<input type="hidden" class="input_spt_id" value="<? echo $input_spt_id; ?>" />
				<input type="hidden" class="input_spt_pw" value="<? echo $input_spt_pw; ?>" />
				<input type="hidden" class="input_spt_pw2" value="<? echo $input_spt_pw2; ?>" />
				<input type="hidden" class="input_spt_name" value="<? echo $input_spt_name; ?>" />
				<input type="hidden" class="input_spt_deptnum" value="<? echo $input_spt_deptnum; ?>" />
				<input type="hidden" class="input_spt_tel" value="<? echo $input_spt_tel; ?>" />
				<input type="hidden" class="input_spt_mail" value="<? echo $input_spt_mail; ?>" />
				<input type="hidden" class="input_spt_permit" value="<? echo $input_spt_permit; ?>" />
				<?
				}//end of : if($input_res == 'FAIL')
				?>

				<div class="btn">
					<a href="#" class="confirm_pop_user_add_res"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //사용자가 추가된 결과 창 -->
<?
}//end of : if($user_add_res_popup_action == 'open')
?>