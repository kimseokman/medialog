<?php
$user_edit_res_popup_action = $_POST['user_edit_res_popup_action'];
$user_edit_res_popup_msg = $_POST['user_edit_res_popup_msg'];

if($user_edit_res_popup_action == 'open'){
	$sptnum = $_POST['sptnum'];
	$page = $_POST['page'];
	$search = $_POST['search'];
	$searchVal = $_POST['searchVal'];
?>
<!-- 사용자가 수정된 결과 창 -->
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<span class="popup_title_txt"></span>
			<a href="#" class="close close_pop_user_edit_res" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p class="h_num70">
					<? echo $user_edit_res_popup_msg; ?>
				</p>
				<input type="hidden" class="sptnum" value="<? echo $sptnum; ?>" />
				<input type="hidden" class="page" value="<? echo $page; ?>" />
				<input type="hidden" class="search" value="<? echo $search; ?>" />
				<input type="hidden" class="searchVal" value="<? echo $searchVal; ?>" />
				<div class="btn">
					<a href="#" class="confirm_pop_user_edit_res"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //사용자가 수정된 결과 창 -->
<?
}//end of : if($user_edit_res_popup_action == 'open')
?>