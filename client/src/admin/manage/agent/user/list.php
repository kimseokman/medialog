<?php
include_once("../../../../_func/function.common.php");

include_once("../../../../common/html_head.php");
include_once("../../../../common/header.php");

$page = $_POST['page'];
if(!$_POST['page']){
	$page = 1;
}

$search = $_POST['search'];
if(!$_POST['search']){
	$search = "";
}

$searchVal = $_POST['searchVal'];
if(!$_POST['searchVal']){
	$searchVal = "";
}

$spt_arr = $main_obj->GetSupporterListPage($page, $search, $searchVal);

$pagination = $spt_arr['pagination'];
$content = $spt_arr['content'];

$spt_size = count($content);

$user_arr = $main_obj->GetAdminInfo();

$spt_cnt = $main_obj->GetSupporterNum();				//생성 상담원 전체 수
$pc_spt_cnt = $main_obj->GetSupporterCnt('PC');						//only pc 상담원 수
$pc_n_mobile_spt_cnt = $main_obj->GetSupporterCnt('PC_N_MOBILE');				//mobile(pc + mobile) 상담원 수
$pc_n_vedio_spt_cnt = $main_obj->GetSupporterCnt('PC_N_VIDEO');				//video(pc + video) 상담원 수
$pc_n_mobile_n_vedio_spt_cnt = $main_obj->GetSupporterCnt('PC_N_MOBILE_N_VIDEO');	//all(pc + mobile&video) 상담원 수

$spt_volume = $user_arr->volume;
$mobile_volume = $user_arr->mobile_volume;
$pc_volume = $spt_volume - $mobile_volume;

$is_pc = TRUE;
if($admin_product == PRODUCT_MOBILE){
	$product_title = "PC + 모바일";
	$product_spt_cnt = $pc_n_mobile_spt_cnt;
	$is_pc = FALSE;
}
else if($admin_product == PRODUCT_VIDEO){
	$product_title = "PC + 비디오";
	$product_spt_cnt = $pc_n_vedio_spt_cnt;
	$is_pc = FALSE;
}
else if($admin_product == PRODUCT_MOBILE_VIDEO){
	$product_title = "PC + 모바일 + 비디오";
	$product_spt_cnt = $pc_n_mobile_n_vedio_spt_cnt;
	$is_pc = FALSE;
}

function print_last($current_node, $last_node){
	$class_name = "";

	if($current_node == $last_node){
		$class_name = "last";
	}

	return $class_name;
}

function print_last2($isPrint){
	$class_name = "";

	if($isPrint){
		$class_name = "last";
	}

	return $class_name;
}

function print_imp($print_flag){
	if($print_flag)
		return "imp";
}
?>
<!-- popup : logout -->
<?
include_once("../../../../common/popup/logout.php");
?>
<!-- //popup : logout -->
<div class="conts_box">
	<?
	include_once("../side.php");
	?>
	<div class="conts_right">
		<!-- [D] 사용자를 관리하기 위한 화면 -->
		<h3><img src="<? echo IMG_DIR; ?>/sub/h3_user_user.gif" alt="사용자관리" /></h3>
		<!-- 생성된 전체 부서 리스트 -->
		<div class="contents user_user">
			<!-- [D] 셀렉트항목 선택후 검색어입력 후 검색버튼 클릭하면 결과값 아래리스트에 출력 -->
			<div class="search_box">
				<strong>검색어</strong>
				<div class="choice_box" style="margin-bottom:0px;">
					<span class="comm_sel"><!-- [150811] -->
						<select>
							<option value="sptid">사용자 ID</option>
							<option value="name">사용자명</option>
							<option value="depname">소속부서</option>
						</select>
					</span>
					<div>
						<input class="user_search_val" type="text" style="width:180px;" placeholder="검색어" /><a href="#" class="user_search_btn"><img src="<? echo IMG_DIR; ?>/board/btn_search.gif" alt="검색" /></a>
					</div>
				</div>
			</div>

			<!-- [D] 사용자 라이선스 정보 안내 -->
			<div class="inform_usernum">
				<dl class="fst_usernum">
					<dt>생성 가능 수 :</dt>
					<dd><span class="imp"><? echo ($spt_volume - $spt_cnt); ?></span> 명 (총 <strong><? echo $spt_volume; ?></strong> 명)</dd>
				</dl>
				<dl class="snd_usernum">
					<dt>PC :</dt>
					<dd class="<? echo print_last2($is_pc); ?>"><span class="imp"><? echo ($pc_volume-$pc_spt_cnt); ?></span> 명 (총 <strong><? echo $pc_volume; ?></strong> 명)</dd>
				<?
				if( !$is_pc ){
				?>
					<dt><? echo $product_title; ?> :</dt>
					<dd class="last"><span class="imp"><? echo ($mobile_volume-$product_spt_cnt); ?></span> 명(총 <strong><? echo $mobile_volume; ?></strong> 명)</dd>
				<?
				}
				?>
				</dl>
			</div>

			<input type="hidden" class="page" name="page" value="<? echo $page; ?>" />
			<input type="hidden" class="search" name="search" value="<? echo $search; ?>" />
			<input type="hidden" class="searchVal" name="searchVal" value="<? echo $searchVal; ?>" />

			<div class="useradd_btn">
				<a href="#" class="open_add_user_pop"><img src="<? echo IMG_DIR; ?>/sub/btn_add.gif" alt="사용자 추가" /></a>
				<!-- [D] 엑셀다운로드버튼: 하단리스트에 대한 내용이 엑셀파일로 다운로드됨.(전체 내용이 엑셀다운로드됨) -->
				<a href="#" class="export_user_to_excel"><img src="<? echo IMG_DIR; ?>/sub/btn_exeldown.gif" alt="엑셀다운로드" /></a>
			</div>

			<div class="ajax_user_content">
				<!-- [D] 사용자ID를 클릭하면 사용자정보화면으로 이동 -->
				<table class="style_board_default" summary="번호, 부서명, 설명, 수정/삭제가 있는 게시판" width="100%">
					<caption><span>번호, 부서명, 설명, 수정/삭제가 있는 게시판</span></caption>
					<colgroup>
						<col style="width:70px;" />
						<col style="width:100px;" />
						<col style="width:100px;" />
						<col style="width:100px;" />
						<col style="width:80px;" />
						<col style="width:*;" />
						<col style="width:120px;" />
					</colgroup>
					<thead>
						<tr>
							<th>NO</th>
							<th>사용자ID</th>
							<th>사용자명</th>
							<th>소속 부서</th>
							<th>상태</th>
							<th>최근접속시간</th>
							<th class="last">지원권한</th>
						</tr>
					</thead>
					<tbody>
					<?
					if( ($spt_size == 1) && ($content[0]['sptnum']=='') ){
					?>
						<tr class="last">
							<td colspan="7">상담원 정보가 없습니다.</td>
						</tr>
					<?
					}//end of : if($spt_size == 0)
					else{
						$last_node_num = $spt_size - 1;
						for($i = 0; $i < $spt_size; $i++){
							//set dept name 
							$user_dept_num = $content[$i]['depnum'];
							$dept = $main_obj->GetDepByDepnum($user_dept_num);
							$dep_name = $dept->depname;

							//set status
							$user_status = $content[$i]['logon'];
							$user_status_call = $content[$i]['cnt'];
							if($user_status == 0){
								$status = "오프라인";
							}
							else if($user_status == 1) {
								if($user_status_call == 0) {
									$status = "온라인";
								} 
								else {
									$status = "지원중";
								}
							} 
							else if($user_status == 2) {
								$status = "지원중";
							}
							
							//set auth
							$user_auth = $content[$i]['function'];
							if(($user_auth & PERMISSION_MOBILE_VIDEO_PAID) || ($user_auth & PERMISSION_MOBILE_VIDEO_FREE)){
								$auth = PRODUCT_MOBILE_VIDEO;
							}
							else if(($user_auth & PERMISSION_MOBILE_PAID) || ($user_auth & PERMISSION_MOBILE_FREE)){
								$auth = PRODUCT_MOBILE;
							}
							else if(($user_auth & PERMISSION_VIDEO_PAID) || ($user_auth & PERMISSION_VIDEO_FREE)){
								$auth = PRODUCT_VIDEO;
							}
							else{
								$auth = PRODUCT_PC;
							}
					?>
						<tr class="<? echo print_last($i, $last_node_num); ?>">
							<td><? echo $content[$i]['num']; ?></td>
							<td><a href="#" onClick="user_detail(event, <? echo $content[$i]['sptnum']; ?>, <? echo $page; ?>, '<? echo $search; ?>', '<? echo $searchVal; ?>')"><? echo $content[$i]['sptid']; ?></a></td>
							<td><? echo $content[$i]['name']; ?></td>
							<td><? echo $dep_name; ?></td>
							<td><? echo $status; ?></td>
							<td><? echo $content[$i]['lastlogin']; ?></td>
							<td>
							<?
							if($auth == PRODUCT_MOBILE_VIDEO){
							?>
								<a href="#"><img src="<? echo IMG_DIR; ?>/sub/ico_pc.gif" alt="PC" /></a>
								<a href="#"><img src="<? echo IMG_DIR; ?>/sub/ico_mob.gif" alt="모바일" /></a>
								<a href="#"><img src="<? echo IMG_DIR; ?>/sub/ico_vid.gif" alt="비디오" /></a>
							<?
							}//end of : if($auth == PRODUCT_MOBILE_VIDEO)
							else if($auth == PRODUCT_MOBILE){
							?>
								<a href="#"><img src="<? echo IMG_DIR; ?>/sub/ico_pc.gif" alt="PC" /></a>
								<a href="#"><img src="<? echo IMG_DIR; ?>/sub/ico_mob.gif" alt="모바일" /></a>
							<?
							}//end of : if($auth == PRODUCT_MOBILE)
							else if($auth == PRODUCT_VIDEO){
							?>
								<a href="#"><img src="<? echo IMG_DIR; ?>/sub/ico_pc.gif" alt="PC" /></a>
								<a href="#"><img src="<? echo IMG_DIR; ?>/sub/ico_vid.gif" alt="비디오" /></a>
							<?
							}//end of : if($auth == PRODUCT_VIDEO)
							else if($auth == PRODUCT_PC){
							?>
								<a href="#"><img src="<? echo IMG_DIR; ?>/sub/ico_pc.gif" alt="PC" /></a>
							<?
							}//end of : if($auth == PRODUCT_PC)
							?>
							</td>
						</tr>
					<?
						}//end of : for($i = 0; $i < $dept_size; $i++)
					}//end of : if($spt_size != 0)
					?>
					</tbody>
				</table>
				<?
				$page_prev = $pagination->GetIsPrev();
				$page_list = $pagination->GetPageList();
				$page_next = $pagination->GetIsNext();
				?>
				<!-- [D] 사용자 항목이 15개를 초과하면 페이징 처리 -->
				<div class="paging">
				<?
				if($page_prev['is_older_page']){
				?>
					<a href="#" class="btn_arrow ar_fst" onClick="user_pagination(event, <? echo $page_prev['older_page']; ?>)">첫페이지</a>
				<?
				}
				else{
				?>
					<a href="#" class="btn_arrow ar_fst">첫페이지</a>
				<?
				}

				if($page_prev['is_prev_page']){
				?>
					<a href="#" class="btn_arrow ar_prev" onClick="user_pagination(event, <? echo $page_prev['prev_page']; ?>)">이전페이지</a>
				<?
				}
				else{
				?>
					<a href="#" class="btn_arrow ar_prev">이전페이지</a>
				<?
				}

				while(list($k, $v) = each($page_list)){
				?>
					<a href="#" onClick="user_pagination(event, <? echo $k; ?>)"><span class="<? echo print_imp($v['is_current']);?>"><? echo $k; ?></span></a>
				<?
				}//end of : while(list($k, $v) = each($page_list))
				
				if($page_next['is_next_page']){
				?>
					<a href="#" class="btn_arrow ar_next" onClick="user_pagination(event, <? echo $page_next['next_page']; ?>)"><span class="hidden">다음페이지</span></a>
				<?
				}
				else{
				?>
					<a href="#" class="btn_arrow ar_next"><span class="hidden">다음페이지</span></a>
				<?
				}

				if($page_next['is_newer_page']){
				?>
					<a href="#" class="btn_arrow ar_last" onClick="user_pagination(event, <? echo $page_next['newer_page']; ?>)"><span class="hidden">마지막페이지</span></a>
				<?
				}
				else{
				?>
					<a href="#" class="btn_arrow ar_last"><span class="hidden">마지막페이지</span></a>
				<?
				}
				?>
				</div>
			</div>	
		</div>
	</div>
</div>
<!-- popup : user_add -->
<?
include_once("./popup/user_add.php");
?>
<!-- //popup : user_add -->
<!-- popup : user_add_res -->
<?
include_once("./popup/user_add_res.php");
?>
<!-- //popup : user_add_res -->
<!-- popup : user_del_res -->
<?
include_once("./popup/user_del_res.php");
?>
<!-- //popup : user_del_res -->
<?
include_once("../../../../common/footer.php");
include_once("../../../../common/html_foot.php");