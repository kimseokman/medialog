<?php
$dept_del_popup_action = $_POST['dept_del_popup_action'];
$dept_del_num = $_POST['dept_del_num'];

if($dept_del_popup_action == 'open'){
?>
<!-- 부서 삭제 창 -->
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<span class="popup_title_txt"></span>
			<a href="#" class="close close_pop_dept_del" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p class="h_num70">해당 내용을 삭제하시겠습니까?</p>
				<input type="hidden" class="remember_page" value="<? echo $page; ?>" />
				<div class="btn">
					<a href="#" onClick="delete_dept(event, <? echo $dept_del_num; ?>, <? echo $page; ?>)"><img src="<? echo IMG_DIR; ?>/popup/btn_yes.gif" alt="예" /></a>
					<a href="#" class="no_pop_dept_del"><img src="<? echo IMG_DIR; ?>/popup/btn_no.gif" alt="아니오" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //부서 삭제 창 -->
<?
}//end of : if($dept_del_popup_action == 'open')
?>