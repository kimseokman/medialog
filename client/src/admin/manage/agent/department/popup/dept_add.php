<?php
$dept_add_popup_action = $_POST['dept_add_popup_action'];

if($dept_add_popup_action == 'open'){
?>
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<!-- 부서추가 팝업 -->
	<!-- [D] 모든 내용을 입력후 확인버튼을 클릭하면 해당 내용의 항목이 리스트에 추가 생성됨 -->
	<div id="popup" class="add_dept">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<a href="#" class="close close_pop_dept_add" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<form name="createDeptForm" method="post" action="/_func/function.create_dept.php">
				<div class="popup_content">
					<table class="style_board_userinfo" summary="부서명, 설명 입력하여 부서 추가" width="100%">
						<caption><span>부서 추가</span></caption>
						<colgroup>
							<col style="width:72px;" />
							<col style="width:*;" />
						</colgroup>
						<tbody>
							<tr>
								<th><label for="dept_name">부서명</label></th>
								<td><input type="text" name="dept_name" id="dept_name" style="width:160px;" /></td>
							</tr>
							<tr class="last">
								<th class="desc"><label for="desc">설명</label></th>
								<td><textarea id="desc" name="desc" rows="5" cols="20" style="width:150px;"></textarea></td>
							</tr>
						</tbody>
					</table>

					<div class="btn">
						<a href="#" class="create_dept_btn"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
					</div>
				</div>
			</form>
		</div>
	</div>
	<!-- //부서추가 팝업 -->
</div>
<?
}////end of : if($dept_add_popup_action == 'open')
?>