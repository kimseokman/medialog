<?php
$dept_del_res_popup_action = $_POST['dept_del_res_popup_action'];
$dept_del_res_popup_msg = $_POST['dept_del_res_popup_msg'];

if($dept_del_res_popup_action == 'open'){
?>
<!-- 부서가 삭제된 결과 창 -->
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<span class="popup_title_txt"></span>
			<a href="#" class="close close_pop_dept_del_res" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p class="h_num70">
					<? echo $dept_del_res_popup_msg; ?>
				</p>
				<input type="hidden" class="remember_page" value="<? echo $page; ?>" />	

				<div class="btn">
					<a href="#" class="confirm_pop_dept_del_res"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //부서가 삭제된 결과 창 -->
<?
}//end of : if($dept_del_res_popup_action == 'open')
?>