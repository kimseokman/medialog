<?php
include_once("../../../../_func/function.common.php");

include_once("../../../../common/html_head.php");
include_once("../../../../common/header.php");

$page = $_POST['page'];
if(!$_POST['page']){
	$page = 1;
}
$dept_arr = $main_obj->GetDepListPage($page);

$pagination = $dept_arr['pagination'];
$content = $dept_arr['content'];

$dept_size = count($content);
$dept_total_size = count($main_obj->GetDepList());

$able_create_cnt = DEPT_MAX_NUM - $dept_total_size;

function print_imp($print_flag){
	if($print_flag)
		return "imp";
}

$act_mode = $_POST['act_mode'];
$act_dept_num = $_POST['act_dept_num'];
if(!$_POST['act_mode']){
	$act_mode = "NORMAL";
	$act_dept_num = 0;
}

function print_disabled($mode, $base_num, $act_num){
	$attr_name = "disabled";

	if(($mode == "EDIT") && ($base_num == $act_num)){
		$attr_name = "";
	}

	return $attr_name;
}

function print_last($current_node, $last_node){
	$class_name = "";

	if($current_node == $last_node){
		$class_name = "last";
	}

	return $class_name;
}
?>
<!-- popup : edit pw guide -->
<?
include_once("../../../../common/popup/edit_pw_guide.php");
?>
<!-- //popup : edit pw guide -->
<!-- popup : logout -->
<?
include_once("../../../../common/popup/logout.php");
?>
<!-- //popup : logout -->
<div class="conts_box">
	<?
	include_once("../side.php");
	?>
	<div class="conts_right">
		<!-- [D] 로그인 후, 첫 진입화면 / 부서를 관리하기 위한 화면 -->
		<h3><img src="<? echo IMG_DIR; ?>/sub/h3_user_deprt.gif" alt="부서관리" /></h3>

		<!-- 생성된 전체 부서 리스트 -->
		<div class="contents user_dept">
			<div class="inform_usernum">
				<dl class="fst_usernum">
					<dt></dt>
					<dd>부서는 총 <span class="imp"><? echo $able_create_cnt; ?>개</span> 까지 생성이 가능합니다.</dd>
				</dl>
			</div>
			<!-- [D] 추가버튼: 부서를 추가팝업창이 뜨고, 모든내용입력 후 확인버튼 클릭하면 리스트에 추가생성됨 -->
			<div class="deptadd_btn">
			<?
			if($dept_total_size < DEPT_MAX_NUM){
			?>
				<a href="#" class="dept_add_btn">
					<img src="<? echo IMG_DIR; ?>/sub/btn_add.gif" alt="부서 추가" />
				</a>
			<?
			}//end of : if($dept_size => DEPT_MAX_NUM)
			?>
			</div>
			<div class="ajax_dept_content">
				<table class="style_board_default" summary="번호, 부서명, 설명, 수정/삭제가 있는 게시판" width="100%">
					<caption><span>번호, 부서명, 설명, 수정/삭제가 있는 게시판</span></caption>
					<colgroup>
						<col style="width:70px;" />
						<col style="width:190px;" />
						<col style="width:*;" />
						<col style="width:90px;" />
					</colgroup>
					<thead>
						<tr>
							<th>NO</th>
							<th>부서명</th>
							<th>설명</th>
							<th class="last">수정/삭제</th>
						</tr>
					</thead>
					<tbody>
					<?
					if( ($dept_size == 1) && ($content[0]['depnum'] == '') ){
					?>
					<tr class="last">
						<td colspan="4">부서 정보가 없습니다.</td>
					</tr>
					<?
					}//end of : if(count($department_arr) == 0)
					else{
						$last_node_num = $dept_size - 1;
						for($i = 0; $i < $dept_size; $i++){
					?>
					<tr class="<? echo print_last($i, $last_node_num); ?>">
						<td><? echo $content[$i]['num']; ?></td>
						<td><input type="text" class="input_dept_name_<? echo $content[$i]['depnum']; ?>" value="<? echo $content[$i]['depname']; ?>" placeholder="부서 이름 입력" size="25" style="width:160px;" <? echo print_disabled($act_mode, $act_dept_num, $content[$i]['depnum']); ?> /></td>
						<td><input type="text" class="input_dept_memo_<? echo $content[$i]['depnum']; ?>" value="<? echo $content[$i]['memo']; ?>" placeholder="부서 설명입니다." size="65" style="width:400px;" <? echo print_disabled($act_mode, $act_dept_num, $content[$i]['depnum']); ?> /></td>
						<td>
						<!-- [D] 항목 수정버튼: 클릭하면 부서명/부서설명 입력창이 활성화되어 내용수정 가능 -->
						<?
						if(print_disabled($act_mode, $act_dept_num, $content[$i][depnum]) == "disabled"){
						?>
							<a href="#" onClick="dept_edit_mode(event, <? echo $content[$i]['depnum']; ?>, <? echo $page; ?>)"><img src="<? echo IMG_DIR; ?>/sub/ico_edit.gif" alt="수정하기" /></a>
							<a href="#" onClick="open_pop_dept_del(event, <? echo $content[$i]['depnum']; ?>, <? echo $page; ?>)"><img src="<? echo IMG_DIR; ?>/sub/ico_del.gif" alt="삭제하기" /></a>
						<?
						}//end of : if(print_disabled($act_mode, $act_dept_num, $content[$i][depnum]) == "disabled")
						else{
						?>
							<a href="#" onClick="dept_edit(event, <? echo $content[$i]['depnum']; ?>, <? echo $page; ?>, '<? echo $act_mode; ?>', <? echo $act_dept_num; ?>)"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
						<?
						}//end of : if(print_disabled($act_mode, $act_dept_num, $content[$i][depnum]) == "")
						?>
						</td>
					</tr>
					<?
						}//end of : for($i = 0; $i < $dept_size; $i++)
					}//end of : if(count($department_arr) != 0)
					?>
					</tbody>
				</table>
				<?
				$page_prev = $pagination->GetIsPrev();
				$page_list = $pagination->GetPageList();
				$page_next = $pagination->GetIsNext();
				?>
				<!-- [D} 한 화면의 부서 항목이 10개를 초과하면 페이징 처리 -->
				<div class="paging">
				<?
				if($page_prev['is_older_page']){
				?>
					<a href="#" class="btn_arrow ar_fst" onClick="dept_pagination(event, <? echo $page_prev['older_page']; ?>)">첫페이지</a>
				<?
				}
				else{
				?>
					<a href="#" class="btn_arrow ar_fst">첫페이지</a>
				<?
				}

				if($page_prev['is_prev_page']){
				?>
					<a href="#" class="btn_arrow ar_prev" onClick="dept_pagination(event, <? echo $page_prev['prev_page']; ?>)">이전페이지</a>
				<?
				}
				else{
				?>
					<a href="#" class="btn_arrow ar_prev">이전페이지</a>
				<?
				}

				while(list($k, $v) = each($page_list)){
				?>
					<a href="#" onClick="dept_pagination(event, <? echo $k; ?>)"><span class="<? echo print_imp($v['is_current']);?>"><? echo $k; ?></span></a>
				<?
				}//end of : while(list($k, $v) = each($page_list))
				
				if($page_next['is_next_page']){
				?>
					<a href="#" class="btn_arrow ar_next" onClick="dept_pagination(event, <? echo $page_next['next_page']; ?>)"><span class="hidden">다음페이지</span></a>
				<?
				}
				else{
				?>
					<a href="#" class="btn_arrow ar_next"><span class="hidden">다음페이지</span></a>
				<?
				}

				if($page_next['is_newer_page']){
				?>
					<a href="#" class="btn_arrow ar_last" onClick="dept_pagination(event, <? echo $page_next['newer_page']; ?>)"><span class="hidden">마지막페이지</span></a>
				<?
				}
				else{
				?>
					<a href="#" class="btn_arrow ar_last"><span class="hidden">마지막페이지</span></a>
				<?
				}
				?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- popup : dept_add -->
<?
include_once("./popup/dept_add.php");
?>
<!-- //popup : dept_add -->
<!-- popup : dept_add_res -->
<?
include_once("./popup/dept_add_res.php");
?>
<!-- //popup : dept_add_res -->
<!-- popup : dept_del -->
<?
include_once("./popup/dept_del.php");
?>
<!-- //popup : dept_del -->
<!-- popup : dept_del_res -->
<?
include_once("./popup/dept_del_res.php");
?>
<!-- //popup : dept_del_res -->
<!-- popup : dept_edit_res -->
<?
include_once("./popup/dept_edit_res.php");
?>
<!-- //popup : dept_edit_res -->
<?
include_once("../../../../common/footer.php");
include_once("../../../../common/html_foot.php");