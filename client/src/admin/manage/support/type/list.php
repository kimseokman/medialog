<?php
include_once("../../../../_func/function.common.php");

include_once("../../../../common/html_head.php");
include_once("../../../../common/header.php");

$total_num = $main_obj->GetTroubleTypeCount();
$trouble_arr = $main_obj->GetTroubleTypeList();
$trouble_size = count($trouble_arr);

function print_last($base, $current){
	$class_name = "";

	if($base == $current){
		$class_name = "last";
	}

	return $class_name;
}

$act_mode = $_POST['act_mode'];
$act_trb_num = $_POST['act_trb_num'];
if(!$_POST['act_mode']){
	$act_mode = "NORMAL";
	$act_trb_num = 0;
}

$max_num = TROUBLE_TYPE_MAX;

function print_disabled($mode, $act_num, $current_num){
	$attr_val = "disabled";

	if($mode == "EDIT"){
		if($act_num == $current_num){
			$attr_val = "";			
		}
	}

	return $attr_val;
}
?>
<!-- popup : logout -->
<?
include_once("../../../../common/popup/logout.php");
?>
<!-- //popup : logout -->
<div class="conts_box">
	<?
	include_once("../side.php");
	?>
	<div class="conts_right">
		<!-- [D] 원격지원유형을 관리하는 화면 -->
		<h3><img src="<? echo IMG_DIR; ?>/sub/h3_supp_type.gif" alt="지원 유형 관리" /></h3>
		<div class="contents supp_type">
			<div class="addtype_btn"><!-- [D] 유형추가 버튼을 클릭하면, 원격지원유형을 추가할 수 있는 항목이 리스트 최하단에 생성됨. -->
			<?
			if($total_num < $max_num){
			?>
				<a href="#" class="add_support_type_btn"><img src="<? echo IMG_DIR; ?>/sub/btn_addtype.gif" alt="유형 추가" /></a>
			<?
			}//end of : if($total_num < TROUBLE_TYPE_MAX)
			?>
			</div>

			<!-- [D] 원격지원유형 전체리스트. -->
			<table class="style_board_default" summary="글번호, 원격지원유형이 있는 게시판" width="100%">
				<caption><span>글번호, 원격지원유형이 있는 게시판</span></caption>
				<colgroup>
					<col style="width:70px;" />
					<col style="width:*;" />
					<col style="width:75px;" />
				</colgroup>
				<thead>
					<tr>
						<th>NO</th>
						<th>원격 지원 유형</th>
						<th class="last"></th>
					</tr>
				</thead>
				<tbody>
				<?
				$last_node_num = $trouble_size -1;
				$first_node_num = 0;
				for($i=0; $i < $trouble_size; $i++){
					if($trouble_arr[$i]["deleted"] == 1) continue;

					if($trouble_arr[$i]["trborder"] == $last_node_num){
						$order = "LAST";
					}
					else if($trouble_arr[$i]["trborder"] == $first_node_num){
						$order = "FIRST";
					}
					else{
						$order = $trouble_arr[$i]["trborder"];
					}
				?>
				<tr class="<? echo print_last($last_node_num, $i); ?>">
					<td><? echo $trouble_arr[$i]["trborder"]+1; ?></td>
					<td>
						<input class="trouble_name_input trbnum_<? echo $trouble_arr[$i]["trbnum"]; ?>" type="text" style="width:535px;" name="trbname" value="<? echo $trouble_arr[$i]["trbname"]; ?>" <? echo print_disabled($act_mode, $act_trb_num, $trouble_arr[$i]["trbnum"]); ?> />
					<?
					if(print_disabled($act_mode, $act_trb_num, $trouble_arr[$i]["trbnum"]) == "disabled"){
					?>
						<!-- [D] 장애유형명칭 수정버튼: 클릭하면 지원유형이 입력된창이 활성화되어 내용을 수정할수있고, 엔터키로 저장됨. -->
						<a href="#" onClick="type_edit_mode(event, <? echo $trouble_arr[$i]["trbnum"]; ?>)"><img src="<? echo IMG_DIR; ?>/sub/ico_edit.gif" alt="수정하기" /></a>
						<!-- [D] 항목 삭제버튼: 해당항목이 삭제됨. 삭제전, 삭제여부확인 시스템팝업창 생성. *웹에서는 항목이 삭제되지만, DB에는 잔여되어야함.* -->
						<a href="#" onClick="open_pop_type_delete(event, <? echo $trouble_arr[$i]["trbnum"]; ?>)"><img src="<? echo IMG_DIR; ?>/sub/ico_del.gif" alt="삭제하기" /></a>
					<?
					}
					else{
					?>
						<a href="#" onClick="edit_type_name_confirm(event, <? echo $trouble_arr[$i]["trbnum"]; ?>)"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
					<?	
					}
					?>
						
					</td>
					<td>
					<?
					if($order != "FIRST"){
					?>
						<a href="#" onClick="edit_order(event, <? echo $trouble_arr[$i]["trbnum"]; ?>, '<? echo $order; ?>', 'up')"><img src="<? echo IMG_DIR; ?>/sub/btn_up.gif" alt="위로" /></a>
					<?
					}
					if($order != "LAST"){
					?>
						<a href="#" onClick="edit_order(event, <? echo $trouble_arr[$i]["trbnum"]; ?>, '<? echo $order; ?>', 'down')"><img src="<? echo IMG_DIR; ?>/sub/btn_down.gif" alt="아래로" /></a>
					<?
					}
					?>						
					</td>
				</tr>
				<?	
				}//end of : for($i=0; $i < $trouble_size; $i++)
				?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- popup : type_edit_res -->
<?
include_once("./popup/type_edit_res.php");
?>
<!-- //popup : type_edit_res -->
<!-- popup : type_order_edit_res -->
<?
include_once("./popup/type_order_edit_res.php");
?>
<!-- //popup : type_order_edit_res -->
<!-- popup : type_del -->
<?
include_once("./popup/type_del.php");
?>
<!-- //popup : type_del -->
<!-- popup : type_del_res -->
<?
include_once("./popup/type_del_res.php");
?>
<!-- //popup : type_del_res -->
<?
include_once("../../../../common/footer.php");
include_once("../../../../common/html_foot.php");