<?php
$type_del_res_popup_action = $_POST['type_del_res_popup_action'];
$type_del_res_popup_msg = $_POST['type_del_res_popup_msg'];

if($type_del_res_popup_action == 'open'){
?>
<!-- 지원 유형 삭제 결과 창 -->
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<span class="popup_title_txt"></span>
			<a href="#" class="close close_pop_type_del_res" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p class="h_num70">
					<? echo $type_del_res_popup_msg; ?>
				</p>
				
				<div class="btn">
					<a href="#" class="confirm_pop_type_del_res"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //지원 유형 삭제 결과 창 -->
<?
}// end of : if($type_del_res_popup_action == 'open')
?>