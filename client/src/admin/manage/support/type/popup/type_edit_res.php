<?php
$type_edit_res_popup_action = $_POST['type_edit_res_popup_action'];
$type_edit_res_popup_msg = $_POST['type_edit_res_popup_msg'];
$type_edit_res = $_POST['type_edit_res'];
$fail_trb_num = $_POST['fail_trb_num'];

if($type_edit_res_popup_action == 'open'){
?>
<!-- 지원 유형 수정 결과 창 -->
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<span class="popup_title_txt"></span>
			<a href="#" class="close close_pop_type_edit_res" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p class="h_num70">
					<? echo $type_edit_res_popup_msg; ?>
				</p>
				<input type="hidden" class="type_edit_res" value="<? echo $type_edit_res; ?>" />
				<input type="hidden" class="fail_trb_num" value="<? echo $fail_trb_num; ?>" />
				<div class="btn">
					<a href="#" class="confirm_pop_type_edit_res"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //지원 유형 수정 결과 창 -->
<?
}// end of : if($type_edit_res_popup_action == 'open')
?>