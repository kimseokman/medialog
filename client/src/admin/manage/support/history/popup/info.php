<?php
$detail_report_popup_action = $_POST['detail_report_popup_action'];
$detail_report_num = $_POST['detail_report_num'];
$remember_page = $_POST['page'];

function print_checked($base, $current){
	$attr_val =  "";

	if($base == $current){
		$attr_val =  "checked";
	}

	return $attr_val;
}

if($detail_report_popup_action == 'open'){
	$detail_report = $main_obj->GetReport($detail_report_num);

	if($detail_report->deleted == ""){
		$agent_id = $detail_report->sptid;
	}
	else{
		$agent_id = $detail_report->deleted;
	}

	$user_oper = $utils_obj->GetOperationLog($detail_report->operationlog);
	$trouble_arr = $main_obj->GetTroubleTypeList();

	$trouble_size = count($trouble_arr);
?>
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<!-- 지원기록보기 팝업 -->
	<!-- [D] 사용자ID를 클릭하면 지원기록 저장 내용이 팝업창으로 보여짐. / 내용은 사용자프로그램의 지원기록확인창의 내용과 동일하나, 수정은 불가능 -->
	<div id="popup" class="supp_log">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<span class="popup_title_txt"></span>
			<a href="#" class="close close_pop_detail_report" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<input type="hidden" class="remember_page" value="<? echo $remember_page; ?>" />
		<div class="popup_content_wrap">
			<div class="popup_content">
				<strong class="supp_log_tit">지원 기록 보기</strong>
				<!-- [D] 사용자ID, 사용자명 출력됨 -->
				<div class="supplog_view">
					<div class="fst_supplog_view">
						<dl>
							<dt>사용자ID :</dt>
							<dd><span class="imp"><? echo $agent_id; ?></span></dd>
						</dl>
						<dl>
							<dt>사용자명 :</dt>
							<dd><? echo $detail_report->sptname; ?></dd>
						</dl>
					</div>
					<ul class="snd_supplog_view">
						<li><strong>IP :</strong><span><? echo $detail_report->user_pub_ip." / ".$detail_report->user_pri_ip; ?></span></li>
						<li><strong>원격 지원 시작 :</strong><span><? echo $detail_report->starttime; ?></span></li>
						<li><strong>원격 지원 종료 :</strong><span><? echo $detail_report->endtime; ?></span></li>
					</ul>
				</div>

				<!-- 지원기록 폼 테이블 -->
				<form class="">
					<table class="style_board_userinfo" summary="" width="100%">
						<caption><span></span></caption>
						<colgroup>
							<col style="width:120px;" />
							<col style="width:*;" />
						</colgroup>
						<tbody>
							<tr>
								<th><label for="cus_name">고객명</label></th>
								<td><input type="text" id="cus_name" value="<? echo $detail_report->name; ?>" disabled style="width:160px;" /></td>
							</tr>
							<tr>
								<th><label for="cus_num">연락처</label></th>
								<td><input type="text" id="cus_num" value="<? echo $detail_report->tel; ?>" disabled style="width:160px;" /></td>
							</tr>
							<tr>
								<th><label for="cus_email">이메일</label></th>
								<td><input type="text" id="cus_email" value="<? echo $detail_report->email; ?>" disabled style="width:160px;" /></td>
							</tr>
							<tr>
								<th><label for="cus_type">유형</label></th>
								<td>
									<div class="choice_box" style="margin-bottom:0px;">
										<span class="comm_sel w_172"><!-- [150811] -->
											<select disabled>
											<?
											for($i=0; $i < $trouble_size; $i++){
											?>
												<option value="<? echo $trouble_arr[$i]['trbnum']; ?>" <? echo print_selected($trouble_arr[$i]['trbnum'], $detail_report->trbnum)?>><? echo $trouble_arr[$i]['trbname']; ?></option>
											<?
											}//end of : for($i=0; $i < $trouble_size; $i++)
											?>
											</select>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<th>내용</th>
								<td>
									<textarea id="desc" rows="5" cols="20" disabled style="width:335px;height:123px;"><? echo htmlspecialchars($detail_report->report); ?></textarea>
								</td>
							</tr>
							<tr class="last">
								<th>해결여부</th>
								<td class="result_radio">
									<input type="radio" id="r_comp" disabled name="result" <? echo print_checked(RESOLVE, $user_oper); ?> /><label for="r_comp">해결</label>
									<input type="radio" id="r_hold" disabled name="result" <? echo print_checked(HOLD, $user_oper); ?> /><label for="r_hold">보류</label>
									<input type="radio" id="r_help" disabled name="result" <? echo print_checked(NEED_DIRECT_SUPPORT, $user_oper); ?> /><label for="r_help">지원 필요</label>
									<input type="radio" id="r_unchecked" disabled name="result" <? echo print_checked(UNCHECKED, $user_oper); ?> /><label for="r_unchecked">미체크</label>
								</td>
							</tr>
						</tbody>
					</table>
				</form>

				<!-- [D] 확인버튼, X버튼 클릭하면 창 닫힘 -->
				<div class="btn">
					<a class="confirm_pop_detail_report" href="#"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
	<!-- //지원기록보기 팝업 -->
</div>
<?
}//end of : if($detail_report_popup_action == 'open')
?>