<?php
include_once("../../../../_func/function.common.php");

include_once("../../../../common/html_head.php");
include_once("../../../../common/header.php");

$page = $_POST['page'];
if(!$_POST['page']){
	$page = 1;
}

$search_type = $_POST['search_type'];
if(!$_POST['search_type']){
	$search_type = "";
}

$search_val = $_POST['search_val'];
if(!$_POST['search_val']){
	$search_val = "";	
}

$start_time = $_POST['start_time'];
if(!$_POST['start_time']){
	$start_time = date("Y-m-d");
	$start_YYYY = date("Y");
	$start_MM = date("m");
	$start_DD = date("d");
}
else{
	$start_time_arr = explode("-", $start_time);
	$start_YYYY = $start_time_arr[0];
	$start_MM = $start_time_arr[1];
	$start_DD = $start_time_arr[2];
}

$end_time = $_POST['end_time'];

if(!$_POST['end_time']){
	$end_time = date("Y-m-d");
	$end_YYYY = date("Y");
	$end_MM = date("m");
	$end_DD = date("d");
}
else{
	$end_time_arr = explode("-", $end_time);
	$end_YYYY = $end_time_arr[0];
	$end_MM = $end_time_arr[1];
	$end_DD = $end_time_arr[2];
}

$exist_report = $main_obj->CheckReportExsit($search_type, $search_val, $start_time, $end_time);
$report_arr = $main_obj->GetReportList($page, $search_type, $search_val, $start_time, $end_time);

$content = $report_arr["datainfo"];
$pagination = $report_arr["pageInfo"];
$content_size = count($content);

function print_last($base, $current){
	$class_name = "";

	if($base == $current){
		$class_name = "last";
	}

	return $class_name;
}

function print_imp($print_flag){
	if($print_flag)
		return "imp";
}

function print_selected($base, $current){
	$str_value = "";

	if($base == $current){
		$str_value = "selected";
	}

	return $str_value;
}

$year_arr = $utils_obj->GetYears();
$YYYY_cnt = count($year_arr);
$month_arr = $utils_obj->GetMonths();
$MM_cnt = count($month_arr);
$this_last_day = $utils_obj->GetLastDay(date('m'), date('Y'));
$day_arr = $utils_obj->GetDays($this_last_day);
$DD_cnt = count($day_arr);
?>
<!-- popup : logout -->
<?
include_once("../../../../common/popup/logout.php");
?>
<!-- //popup : logout -->
<div class="conts_box">
	<?
	include_once("../side.php");
	?>
	<div class="conts_right">
		<!-- [D] 원격지원된 기록을 확인하는 화면 -->
		<h3><img src="<? echo IMG_DIR; ?>/sub/h3_supp_log.gif" alt="사용자관리" /></h3>
		<div class="contents supp_logg">
			<!-- [D] 검색기간/검색어를 설정하여 조회 -->
			<div class="search_box2">
				<div class="search_box2_wrap">
					<strong class="search_box2_tit">검색기간</strong>
					<div class="search_box2_cont">
						<div class="period">
							<a href="#" class="force_search week_a"><span class="week">1주일</span></a>
							<a href="#" class="force_search month_1_a"><span class="month_1">1개월</span></a>
							<a href="#" class="force_search month_3_a"><span class="month_3">3개월</span></a>
							<a href="#" class="force_search month_6_a"><span class="month_6">6개월</span></a>
						</div>
						<!-- [D] 디폴트는 당일날짜. 셀렉트박스의 날짜를 선택하여 기간설정 가능 -->
						<div class="date">
							<div class="choice_box support_history_search_term_box">
								<span class="comm_sel w_70 start_YYYY"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $YYYY_cnt; $i++){
									?>
										<option value="<? echo $year_arr[$i]; ?>" <? echo print_selected($start_YYYY, $year_arr[$i]);?> ><? echo $year_arr[$i]; ?></option>
									<?
									}//for($i=0; $i < $YYYY_cnt; $i++)
									?>
									</select>
								</span>
								<span class="comm_sel w_55 start_MM"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $MM_cnt; $i++){
									?>
										<option value="<? echo $month_arr[$i]; ?>" <? echo print_selected($start_MM, $month_arr[$i]); ?> ><? echo str_pad($month_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
									<?
									}//for($i=0; $i < $MM_cnt; $i++)
									?>
									</select>
								</span>
								<span class="comm_sel w_55 start_DD"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $DD_cnt; $i++){
									?>
										<option value="<? echo $day_arr[$i]; ?>" <? echo print_selected($start_DD, $day_arr[$i]); ?> ><? echo str_pad($day_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
									<?
									}//for($i=0; $i < $DD_cnt; $i++)pad
									?>
									</select>
								</span>
								<div class="cen_line">~ </div>
								<span class="comm_sel w_70 end_YYYY"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $YYYY_cnt; $i++){
									?>
										<option value="<? echo $year_arr[$i]; ?>" <? echo print_selected($end_YYYY, $year_arr[$i]);?> ><? echo $year_arr[$i]; ?></option>
									<?
									}//for($i=0; $i < $YYYY_cnt; $i++)
									?>
									</select>
								</span>
								<span class="comm_sel w_55 end_MM"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $MM_cnt; $i++){
									?>
										<option value="<? echo $month_arr[$i]; ?>" <? echo print_selected($end_MM, $month_arr[$i]); ?> ><? echo str_pad($month_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
									<?
									}//for($i=0; $i < $MM_cnt; $i++)
									?>
									</select>
								</span>
								<span class="comm_sel w_55 end_DD"><!-- [150811] -->
									<select>
									<?
									for($i=0; $i < $DD_cnt; $i++){
									?>
										<option value="<? echo $day_arr[$i]; ?>" <? echo print_selected($end_DD, $day_arr[$i]); ?> ><? echo str_pad($day_arr[$i],"2","0",STR_PAD_LEFT); ?></option>
									<?
									}//for($i=0; $i < $DD_cnt; $i++)pad
									?>
									</select>
								</span>
							</div>
						</div>
					</div>
				</div>

				<div class="search_box2_wrap01">
					<strong class="search_box2_tit">검색어</strong>
					<div class="search_box2_cont">
						<!-- [D] 검색항목 선택하여 검색내용 입력후 검색버튼 클릭하면, 해당내용이 하단에 검색되어 출력됨. -->
						<div class="choice_box support_history_search_keyword_box">
							<span class="comm_sel"><!-- [150811] -->
								<select>
									<option value="sptid" <? echo print_selected('sptid', $search_type); ?>>사용자 ID</option>
									<option value="sptname" <? echo print_selected('sptname', $search_type); ?>>사용자명</option>
									<option value="name" <? echo print_selected('name', $search_type); ?>>고객명</option>
								</select>
							</span>
							<input class="support_history_search_keyword" type="text" size="30" placeholder="검색어" value="<? echo $search_val; ?>" />
							<a href="#" class="support_history_search"><img src="<? echo IMG_DIR; ?>/board/btn_search.gif" alt="검색" /></a>
						</div>
					</div>
				</div>
			</div>

			<div class="useradd_btn">
				<!-- [D] 엑셀다운로드버튼: 하단리스트에 대한 전체내용이 엑셀파일로 다운로드됨.(검색된 전체 내용이 엑셀다운로드됨) -->
				<a href="#" class="export_support_history_to_excel"><img src="<? echo IMG_DIR; ?>/sub/btn_exeldown.gif" alt="엑셀다운로드" /></a>
			</div>
			<div class="ajax_support_history_content">
				<!-- [D] 디폴트는 당일 지원 기록 . / 사용자ID를 클릭하면 저장된 지원기록시스템 팝업창이 생성됨 -->
				<table class="style_board_default" summary="번호, 부서명, 설명, 수정/삭제가 있는 게시판" width="100%">
					<caption><span>번호, 부서명, 설명, 수정/삭제가 있는 게시판</span></caption>
					<colgroup>
						<col style="width:70px;" />
						<col style="width:100px;" />
						<col style="width:100px;" />
						<col style="width:100px;" />
						<col style="width:80px;" />
						<col style="width:*;" />
						<col style="width:120px;" />
					</colgroup>
					<thead>
						<tr>
							<th>NO</th>
							<th>사용자ID</th>
							<th>사용자명</th>
							<th>고객명</th>
							<th>시작시각</th>
							<th>종료시각</th>
							<th>고객OS</th>
							<th class="last">해결여부</th>
						</tr>
					</thead>
					<tbody>
					<?
					if($exist_report == FALSE){// not exist report
					?>
						<tr class="last">
							<td colspan="8">원격 지원 기록이 없습니다.</td>
						</tr>
					<?
					}
					else{//exist report
						$page_num = $report_arr["pageNum"];
						//echo print_r($content);
						$last_node_num = $content_size - 1;
						for($i=0; $i < $content_size; $i++){
							$user_os = $utils_obj->GetOS($content[$i]["user_os"]);
							$user_oper = $utils_obj->GetOperationLog($content[$i]["operationlog"]);
					?>
						<tr class="<? echo print_last($last_node_num, $i)?>">
							<td><? echo $content[$i]["num"]; ?></td>
							<td><a href="#" onClick="open_pop_detail_report(event, <? echo $content[$i]["reportnum"]; ?>, <? echo $page; ?>)"><? echo $content[$i]['sptid']; ?></a></td>
							<td><? echo $content[$i]["sptname"]; ?></td>
							<td><? echo $content[$i]["name"]; ?></td>
							<td><? echo $content[$i]["starttime"]; ?></td>
							<td><? echo $content[$i]["endtime"]; ?></td>
							<td><? echo $user_os; ?></td>
							<td><? echo $user_oper; ?></td>
						</tr>
					<?
							$page_num--;
						}
					}
					?>	
					</tbody>
				</table>

				<?
				$page_prev = $pagination->GetIsPrev();
				$page_list = $pagination->GetPageList();
				$page_next = $pagination->GetIsNext();

				if($exist_report != FALSE){
				?>
				<!-- [D] 한 화면의 지원기록의 내역수가 15개를 초과하면 페이징 처리 -->
				<div class="paging">
					<?
					if($page_prev['is_older_page']){
					?>
					<a href="#" class="btn_arrow ar_fst" onClick="support_history_pagination(event, <? echo $page_prev['older_page']; ?>)">첫페이지</a>
					<?
					}
					else{
					?>
					<a href="#" class="btn_arrow ar_fst">첫페이지</a>
					<?
					}

					if($page_prev['is_prev_page']){
					?>
					<a href="#" class="btn_arrow ar_prev" onClick="support_history_pagination(event, <? echo $page_prev['prev_page']; ?>)">이전페이지</a>
					<?
					}
					else{
					?>
					<a href="#" class="btn_arrow ar_prev">이전페이지</a>
					<?
					}

					while(list($k, $v) = each($page_list)){
					?>
					<a href="#" onClick="support_history_pagination(event, <? echo $k; ?>)"><span class="<? echo print_imp($v['is_current']); ?>"><? echo $k; ?></span></a>
					<?
					}//end of : while(list($k, $v) = each($page_list))

					if($page_next['is_next_page']){
					?>
					<a href="#" class="btn_arrow ar_next" onClick="support_history_pagination(event, <? echo $page_next['next_page']; ?>)"><span class="hidden">다음페이지</span></a>
					<?
					}
					else{
					?>
					<a href="#" class="btn_arrow ar_next"><span class="hidden">다음페이지</span></a>
					<?
					}

					if($page_next['is_newer_page']){
					?>
					<a href="#" class="btn_arrow ar_last" onClick="support_history_pagination(event, <? echo $page_next['newer_page']; ?>)"><span class="hidden">마지막페이지</span></a>
					<?
					}
					else{
					?>
					<a href="#" class="btn_arrow ar_last"><span class="hidden">마지막페이지</span></a>
					<?
					}
					?>
				</div>
				<?
				}//end of : if($exist_report != FALSE)
				?>
			</div>	
		</div>
	</div>
</div>
<!-- popup : info -->
<?
include_once("./popup/info.php");
?>
<!-- //popup : info -->
<!-- popup : info -->
<?
include_once("./popup/search_res.php");
?>
<!-- //popup : info -->
<?
include_once("../../../../common/footer.php");
include_once("../../../../common/html_foot.php");