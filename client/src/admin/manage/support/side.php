<!-- manage support : side navigation bar -->
<div class="conts_left">
	<h2><img src="<? echo IMG_DIR; ?>/sub/lnb_h2_supp.gif" alt="지원기록관리" /></h2>
	<ul class="lnb">
		<li class="<? echo print_nb_on(MANAGE_SUPPORT_HISTORY, $current_url); ?>">
			<a href="<? echo MANAGE_SUPPORT_HISTORY_INDEX; ?>">
				<span>지원기록 보기</span>
			</a>
		</li>
		<li class="<? echo print_nb_on(MANAGE_SUPPORT_TYPE, $current_url); ?>">
			<a href="<? echo MANAGE_SUPPORT_TYPE_INDEX; ?>">
				<span>지원유형 관리</span>
			</a>
		</li>
	</ul>
	<!-- [D] 관리자페이지 로그인 후, 원격지원상담프로그램을 실행/다운로드 할수있는 버튼. *각 유형별 프로세스 내용은, [일반상담원로그인-프로그램실행/다운로드버튼]화면의 내용과 동일 -->
	<p class="lnb_quick">
		<a href="#" class="program_download"><img src="<? echo IMG_DIR; ?>/sub/btn_large_pdown.gif" alt="프로그램 다운로드" /></a>
		<a href="#"><!--img src="<? echo IMG_DIR; ?>/sub/btn_pstart.gif" alt="프로그램 실행" /--></a>
	</p>
</div>
<!-- //manage support : side navigation bar -->