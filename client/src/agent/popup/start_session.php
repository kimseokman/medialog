<?php
include_once("../../_func/function.common.php");

$isLogin = $main_obj->IsLogin();

if($isLogin == FALSE){
	exit(0);
}

$access = "";
$main_obj->GenerateTitleBar($access, $_SESSION["sptnum"]);

include_once("./header.php");
?>
<!-- [D] 프로그램 실행 시, 프로세스. [프로그램 바로 시작] 버튼을 클릭하면 로딩 없이 프로그램이  바로 시작됨 -->
<div id="popup" class="program">
	<div class="popup_title">
		<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
		<span class="popup_title_txt"></span>
		<a href="#" class="close" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
	</div>
	<div class="popup_content_wrap">
		<div class="popup_content">
			<div class="program_tit">
				<img src="<? echo IMG_DIR; ?>/popup/logo_program_tit.gif" alt="안전하고 정확한 U+Biz 원격지원" />
			</div>
			<!-- 프로그램실행 시 팝업1 -->
			<div class="program_cont">
				<strong>원격 지원 상담 프로그램 실행중</strong>
				<p class="program_cont_txt">실행이 30초 이상 지연되면, 아래 버튼을 클릭하여<br />프로그램을 실행하세요.</p>
				<a href="#" class="direct_start"><img src="<? echo IMG_DIR; ?>/popup/btn_pg_start.gif" alt="프로그램 바로 시작" /></a>
			</div>
		</div>
	</div>
</div>
<?
include_once("./footer.php");
?>