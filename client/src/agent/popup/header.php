<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=960" />
	<meta name="apple-mobile-web-app-title" content="UPLUS" />
	<title>
	<? 
	$is_login_page = $utils_obj->IsURL($current_url, 'access/login.php');
	if($is_login_page == TRUE){
		echo LOGIN_TITLE;
	}
	else{
		echo ETC_TITLE;
	}
	?>
	</title>
	<link rel="stylesheet" type="text/css" href="<? echo CSS_DIR; ?>/common.css" />
	<link rel="stylesheet" type="text/css" href="<? echo CSS_DIR; ?>/popup.css" />
	<link rel="stylesheet" type="text/css" href="<? echo CSS_DIR; ?>/board.css" />
	<link rel="stylesheet" type="text/css" href="<? echo CSS_DIR; ?>/select.css" />
	<link rel="stylesheet" type="text/css" href="<? echo CSS_DIR; ?>/biz_common.css" />

	<script type="text/javascript" src="<? echo INC_DIR; ?>/jquery/1.11.3/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="<? echo INC_DIR; ?>/jquery-cookie/1.4.1/jquery.cookie.js"></script>
	<script language="javascript">
		function setTitle(){
			<? echo "this.document.title = \"AnyHelp Supporter Launcher". $access."\";"; ?>
		}
	</script>
</head>
<body OnLoad="setTitle()">
	<!-- pop_wrap -->
	<div id="pop_wrap">
