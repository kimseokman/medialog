<?php
include_once("../_func/function.common.php");

include_once("../common/html_head.php");
include_once("../common/simple_header.php");
?>
<!-- popup : logout -->
<?
include_once("../common/popup/logout.php");
?>
<!-- //popup : logout -->
<!-- agent index -->
<div class="box_login_wrap">
	<div class="box_login_top"></div>
	<div class="box_login">
		<div class="box_loggedin_wrap">
			<p>
				<strong>환영합니다!</strong>
				<span>아래 버튼을 클릭하여 원격 지원 상담 서비스를 사용하세요.</span>
			</p>

			<div class="b_btn">
				<!-- [D] 
					프로그램 실행 버튼:로그인한 계정으로 원격지원상담 프로그램이 실행되고, 선택여부에 따라 바로가기 아이콘이 바탕화면에 설치되기도 함 
					프로그램 다운로드 버튼:사용자의 PC에 프로그램이 설치되고, 바탕화면에는 바로가기 아이콘이 설치됨.
				-->
				<a href="#" onClick="open_pop_start_session(event, 360, 275)"><img src="<? echo IMG_DIR; ?>/sub/btn_prog_start.gif" alt="프로그램 실행" /></a>
				<a href="#" class="program_download"><img src="<? echo IMG_DIR; ?>/sub/btn_prog_dwn.gif" alt="프로그램 다운로드" /></a>
			</div>
		</div>
	</div>
	<div class="box_login_btm"></div>
</div>
<!-- //agent index -->

<!-- 로그인화면 공지사항 -->
<?
include_once("../notice/rolling_list.php");
?>
<!-- //로그인화면 공지사항 -->

<!-- popup : notice -->
<?
include_once("../notice/popup/list.php");
?>
<!-- //popup : notice -->
<?
include_once("../common/simple_footer.php");
include_once("../common/html_foot.php");