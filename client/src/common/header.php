<?php
$user_id = "GUEST";
if ($_SESSION['usertype'] == "ADMIN"){
	$user_id = $_SESSION['adid'];
}

function print_nb_on($base, $current){//use in gnb & snb
	$class_name = "";
	
	if(strpos($current, $base)){
		$class_name = "on";
	}

	return $class_name;
}

function print_gnb_img_on($base, $current){
	$class_name = "";
	
	if(strpos($current, $base)){
		$class_name = "_on";
	}

	return $class_name;
}
?>
<!-- wrap -->
<div id="wrap">
<!-- header -->
<div id="header">
	<div class="top_head">
		<div class="login">
			<span class="user_id"><? echo $user_id;?>님 안녕하세요!</span>
			<span class="out open_pop_logout"><a href="#"><img src="<? echo IMG_DIR; ?>/sub/btn_logout.gif" alt="로그아웃" /></a></span>
		</div>
	</div>
	<div class="gnb_head">
		<h1><a href="#"><img src="<? echo IMG_DIR; ?>/sub/logo_main.gif" alt="biz관리자페이지" /></a></h1>
		<div class="gnb">
			<ul>
				<li class="<? echo print_nb_on(MANAGE_AGENT, $current_url); ?>">
					<a href="<? echo GNB_USER_MANAGE; ?>">
						<img src="<? echo IMG_DIR; ?>/sub/gnb_tex01<? echo print_gnb_img_on(MANAGE_AGENT, $current_url); ?>.gif" alt="사용자관리" />
					</a>
				</li>
				<li class="<? echo print_nb_on(MANAGE_SUPPORT, $current_url); ?>">
					<a href="<? echo GNB_HISTORY_MANAGE; ?>">
						<img src="<? echo IMG_DIR; ?>/sub/gnb_tex02<? echo print_gnb_img_on(MANAGE_SUPPORT, $current_url); ?>.gif" alt="지원기록관리" />
					</a>
				</li>
				<li class="<? echo print_nb_on(MANAGE_STAT, $current_url); ?>">
					<a href="<? echo GNB_STATISTICS_MANAGE; ?>">
						<img src="<? echo IMG_DIR; ?>/sub/gnb_tex03<? echo print_gnb_img_on(MANAGE_STAT, $current_url); ?>.gif" alt="통계관리" />
					</a>
				</li>
				<li class="<? echo print_nb_on(SETTING, $current_url); ?> last">
					<a href="<? echo ($_SESSION['site_auth'] == 'PASS') ? SETTING_SITE_INDEX : GNB_SITE_SETTING; ?>">
						<img src="<? echo IMG_DIR; ?>/sub/gnb_tex04<? echo print_gnb_img_on(SETTING, $current_url); ?>.gif" alt="사이트설정" />
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!-- //header -->
<!-- container -->
<div id="container">