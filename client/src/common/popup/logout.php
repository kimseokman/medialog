<?php
$logout_popup_action = $_POST['logout_popup_action'];

if($logout_popup_action == 'open'){
?>
<!-- reset password respond -->
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<!-- ID찾기 팝업 -->
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<a href="#" class="close close_pop_logout" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p>
					로그아웃 하시겠습니까?
				</p>
				<div class="btn">
					<a href="#" class="pop_logout_yes"><img src="<? echo IMG_DIR; ?>/popup/btn_yes.gif" alt="예" /></a>
					<a href="#" class="pop_logout_no"><img src="<? echo IMG_DIR; ?>/popup/btn_no.gif" alt="아니오" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //reset password respond -->
<?
}//end of : if($reset_res)
?>