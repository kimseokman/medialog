<?php
$send_mail_success_pop_action = $_POST['send_mail_success_pop_action'];

if($send_mail_success_pop_action == 'open'){
?>
<!-- reset password respond -->
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<!-- ID찾기 팝업 -->
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<a href="#" class="close close_pop_send_mail_success" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p>
					메일이 성공적으로 발송되었습니다.<br/>
					기타 궁금하신 사항은 U+Biz 원격지원 고객센터<br /><strong>(<? echo CONTACT_US_TEL; ?>)</strong>으로 문의주시기 바랍니다.
				</p>
				<div class="btn">
					<a href="#" class="confirm_pop_send_mail_success"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //reset password respond -->
<?
}//end of : if($reset_res)
?>