<?php
$send_mail_fail_pop_action = $_POST['send_mail_fail_pop_action'];

if($send_mail_fail_pop_action == 'open'){
?>
<!-- reset password respond -->
<div class="fixed_dim"></div>
<div id="pop_wrap">
	<!-- ID찾기 팝업 -->
	<div id="popup" class="find_id">
		<div class="popup_title">
			<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
			<a href="#" class="close close_pop_send_mail_fail" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
		</div>
		<div class="popup_content_wrap">
			<div class="popup_content">
				<p>
					메일 전송에 실패했습니다. 다시 시도해주시기 바랍니다.<br />
					문제가 지속 된다면 U+Biz 원격지원 고객센터<br /><strong>(<? echo CONTACT_US_TEL; ?>)</strong>으로 문의주시기 바랍니다.
				</p>
				<div class="btn">
					<a href="#" class="confirm_pop_send_mail_fail"><img src="<? echo IMG_DIR; ?>/sub/btn_confirm.gif" alt="확인" /></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //reset password respond -->
<?
}//end of : if($reset_res)
?>