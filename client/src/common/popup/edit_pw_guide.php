<!-- popup : edit password guide -->
<?php
$edit_pw_guide_pop_act = $_POST['edit_pw_guide_pop_action'];//reset password flag ('open', 'close')
$show_interval = EDIT_PASSWORD_INTERVAL/30;
if($edit_pw_guide_pop_act == 'open'){
?>
<div class="fixed_dim"></div>
<div id="popup" class="edit_pw_guide">
	<div class="popup_title">
		<img class="logo_popup" src="<? echo IMG_DIR; ?>/popup/logo_popup.gif" alt="Biz Support" />
		<span class="popup_title_txt">비밀번호 변경안내</span>
		<a href="#" class="close close_pop_edit_pw_guide" title="레이어팝업 닫기"><img src="<? echo IMG_DIR; ?>/popup/ico_close.gif" alt="레이어팝업 닫기" /></a>
	</div>
	<div class="popup_content_wrap">
		<div class="popup_content">
			<div class="reset_pw_txt">
				<strong>고객님의 소중한 개인정보보호를 위해 비밀번호 변경이 필요합니다.</strong><a href="#" class="edit_pw_redirect"><span class="imp edit_pw_redirect_imp">비밀번호 변경하기</span></a>
				<p>
					최근 인터넷상에서 개인정보 유출로 인한 피해가 확산됨에 따라 여러 사이트에서 유사한 ID 및 비밀번호를<br />
					사용할 경우 비밀번호는 노출의 위험이 높아지고 있습니다.<br />
					이에 저희 LG U+에서는 2008년 3월 20일부터 피해확산 방지 및 개인정보 보호 강화를 위하여 <span class="imp"><? echo $show_interval; ?>개월마다</span><br />
					<span class="imp">본인인증 후 비밀번호를 변경하도록 정책</span>을 마련하여 시행하고 있습니다.<br />
					조금 불편하시더라도 고객님의 소중한 개인정보보호를 위하여 현재의 비밀번호를 영문과 숫자, 특수문자<br />
					를 포함한 8자리 이상의 문자로 변경하여 주시기 바랍니다.
				</p>
			</div>
		</div>
	</div>
</div>
<!-- //비밀번호 재설정안내 레이어팝업 -->
<?
}//end of : if($edit_pw_guide_pop_act)
?>
<!-- //popup : edit password guide -->